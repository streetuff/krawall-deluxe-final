height = 1064
width = 320
number_of_charset = 16
charset_line:
first_videoram = $4400
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$4800
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5000
!byte >$5800
!byte >$5800
!byte >$5800
!byte >$5800
!byte >$5800
!byte >$5800
!byte >$5800
!byte >$5800
!byte >$6000
!byte >$6000
!byte >$6000
!byte >$6000
!byte >$6000
!byte >$6000
!byte >$6000
!byte >$6800
!byte >$6800
!byte >$6800
!byte >$6800
!byte >$6800
!byte >$7000
!byte >$7000
!byte >$7000
!byte >$7000
!byte >$7000
!byte >$7800
!byte >$7800
!byte >$7800
!byte >$7800
!byte >$7800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$8800
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b000
!byte >$b800
!byte >$b800
!byte >$b800
!byte >$b800
!byte >$b800
!byte >$b800
!byte >$b800
!byte >$b800
!byte >$b800
!byte >$c800
!byte >$c800
!byte >$c800
!byte >$c800
!byte >$c800
!byte >$d000
!byte >$d000
!byte >$d000
!byte >$d000
!byte >$d000
!byte >$d800
!byte >$d800
!byte >$d800
!byte >$d800
!byte >$d800
!byte >$d800
!byte >$d800
!byte >$e000
!byte >$e000
!byte >$e000
!byte >$e000
!byte >$e000
!byte >$e000
!byte >$e000
!byte >$e800
!byte >$e800
!byte >$e800
!byte >$e800
!byte >$e800
!byte >$e800
!byte >$e800
!byte >$f000
!byte >$f000
!byte >$f000
!byte >$f000
!byte >$f000
* = $4800

!bin "charset00"
* = $5000

!bin "charset01"
* = $5800

!bin "charset02"
* = $6000

!bin "charset03"
* = $6800

!bin "charset04"
* = $7000

!bin "charset05"
* = $7800

!bin "charset06"
* = $8800

!bin "charset07"
* = $b000

!bin "charset08"
* = $b800

!bin "charset09"
* = $c800

!bin "charset0a"
* = $d000

!bin "charset0b"
* = $d800

!bin "charset0c"
* = $e000

!bin "charset0d"
* = $e800

!bin "charset0e"
* = $f000

!bin "charset0f"
