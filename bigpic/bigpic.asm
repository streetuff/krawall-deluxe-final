	;free $b000 -$faff

	!ifdef RELEASE {
        !src "../framework/framework.inc"
        !src "config.asm"
	!to "bigpic.plain",plain
        * = PARTORG
PART_ONLY = 1
        } else {
        !to "bigpic.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
PART_ONLY = 0
        }

	!cpu 6510

	;wenn PART_ONLY auf 1 gesetzt wird, dann wird ohne SID und Test �bersetzt

!if PART_ONLY=1 {
	;hier sid adresse einsstellen !
	sid = $1000
}

THREE_TWO_PAGES = 1

COLOR_1 = $0c
COLOR_2 = $0b
COLOR_3 = $0f

FIRST_nmi_LINE = $10+312-256

modulo_tab = $0200
!ifdef CACHE_ENABLED {
map_addr_low = $0300
map_addr_high = $0400
} else {
map_addr_low = maptablow
map_addr_high = maptabhigh
}
map_addr_dest = $fb00
addr_dd00 = $fa00
addr_d018 = $f900

sprite_x_low = $16
sprite_x = $17
sprite_y = $18
secondline = $19
sprtmp = $1a
old_nmi_a = $1e
old_nmi_x = $1f
nmid018 = $20
nmid018_2 = $30

DISTANCE = nmid018_2-nmid018

nmidd00 = $40
nmidd00_2 = $50

nmid012 = $02e0
nmid012_2 = $02f0

zp_code = $60
xcoord = zp_code-2
ycoord = zp_code-4

new_xcoord = nmidd00_2-2
new_ycoord = nmidd00_2-4

char_y_offset = $0220
char_x_ptr_low = $0200
char_x_ptr_high = $0210

!ifdef CACHE_ENABLED {
line_cache = $9000
LINE_CACHE_SIZE = 27
}

	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}
	!macro align256 {
		* = (*+$ff) & $ff00
	}
	!if PART_ONLY!=1 {
	; das hier ist nur zur demo.
	; vorher ein bisschen schwarzer Bildshirm und danach auch, um
        ; den �bergang beim Start und beim Ende zu checken
        lda #$0b
        sta $d011
	lda #$00
	sta $02
        sta $d020
	!ifdef RELEASE {
        jsr FRAMEWORK_IRQ
	} else {
	tax
	jsr sid
	}
	lda #$7f
	sta $dc0d
	lda $dc0d
--	lda #$fa
-	cmp $d012
	bne -
	!ifdef RELEASE {
	jsr PLAY_MUSIC
	} else {
	jsr sid+3
	}
	inc $02
	bpl --
	;hier wird der eigentliche part gestartet
	jsr part
	;jetzt ist der zu ende
	;IRQ's und NMI's sind alle abgeschaltet
--	lda #$fa
-	cmp $d012
	bne -
	!ifdef RELEASE {
	jsr PLAY_MUSIC
	} else {
	;jsr sid+3
	}
	jmp --        

	* = $1000
sid:    rts
        nop
        nop
	inc $d020
;	lda $d012
;-	cmp #$e0
;	bcs do_jam
	ldx #$ff
-	dex
	bne -
        dec $d020
        rts
do_jam:	!byte $02
	                
;sid:	!bin "Wip3Out_2k6.sid",,126
        }

!macro asr {
	cmp #$80
        ror
}

!macro move .coord, .new_coord, .zp {
	lda .new_coord
	sec
	sbc .coord
	sta .zp
	lda .new_coord+1
	sbc .coord+1
        !for i, 6 {
	+asr
        ror .zp
        }
        sta .zp+1
        lda .coord
        clc
        adc .zp
        sta .coord
        lda .coord+1
        adc .zp+1
        sta .coord+1
}


	* = $2800
part:	jmp start
	;!macro calc_coord {
	;inc xcoord
	;bne +
	;inc xcoord+1
;+	inc ycoord
;	bne +
;	inc ycoord+1
;+       }

irq:	pha
	txa
	pha
        ;sty old_irq_y+1
	inc $d019
	lda secondline
        cmp $d012
	bcc irqsecondline
	beq irqsecondline
	sta $d012
	lda #$20
	sta irq_last_line_sid_caller
        
	ldx #$12
	stx $43f8
	stx $83f8
!ifdef THREE_TWO_PAGES	{
	stx $c3f8
}
	ldx #$16
	stx $43f9
	stx $83f9
!ifdef THREE_TWO_PAGES	{
	stx $c3f9
}        
	ldx #$1a
	stx $43fa
	stx $83fa
!ifdef THREE_TWO_PAGES	{
	stx $c3fa
}        
	ldx #$1e
	stx $43fb
	stx $83fb
!ifdef THREE_TWO_PAGES	{
	stx $c3fb
}        
	lda $d001
	clc
	adc #21*2
	sta $d001
	sta $d003
	sta $d005
	sta $d007
	pla
	tax
	pla
	rti

irqsecondline:
	lda $d012
	cmp #$cf
        bcs irq_last_line
        lda #$d0
        sta $d012
	ldx #$13
	stx $43fc
	stx $83fc
!ifdef THREE_TWO_PAGES	{
	stx $c3fc
}        
	ldx #$17
	stx $43fd
	stx $83fd
!ifdef THREE_TWO_PAGES	{
	stx $c3fd
}        
	ldx #$1b
	stx $43fe
	stx $83fe
!ifdef THREE_TWO_PAGES	{
	stx $c3fe
}        
	ldx #$1f
	stx $43ff
	stx $83ff
!ifdef THREE_TWO_PAGES	{
	stx $c3ff
}        
	lda $d009
	clc
	adc #21*2
	sta $d009
	sta $d00b
	sta $d00d
	sta $d00f
	lda $d012
	cmp #$b0
	bcc no_sid_here
	tya
	pha
	lda #$2c
	sta irq_last_line_sid_caller
	cli
	!ifdef RELEASE {
	jsr PLAY_MUSIC
	} else {
	jsr sid+3
	}
;        lda #$00
;        sta $d01c
;schriftfarbe:
;	lda #$01
;	sta $d02b
;	sta $d02c
;	sta $d02d
;	sta $d02e
	pla
	tay
no_sid_here:
        pla
	tax
	pla
	rti

irq_last_line:
	tya
	pha
	cli
irq_last_line_sid_caller:
	!ifdef RELEASE {
	jsr PLAY_MUSIC
	} else {
	jsr sid+3
	}
	jsr do_states
	lda #$20
	sta irq_last_line_sid_caller

	pla
	tay
	pla
	tax
	pla
        rti       

	!macro set_sprite {
	;in x x
	;in y y
	;in a x low

	lsr
	txa
	rol
	sta $d000
	sta $d008
	clc
	adc #24
	sta $d002
	sta $d00a
	clc
	adc #24
	sta $d004
	sta $d00c
	clc
	adc #24
	sta $d006
	sta $d00e
	sty $d001
	sty $d003
	sty $d005
	sty $d007
	tya
	clc
	adc #21
	sta $d009
	sta $d00b
	sta $d00d
	sta $d00f
	sta $d012
	clc
	adc #21
	sta secondline
	lda d010array,x
	sta $d010
	ldx #$10
	stx $43f8
	stx $83f8
!ifdef THREE_TWO_PAGES	{
	stx $c3f8
}        
	inx
	stx $43fc
	stx $83fc
!ifdef THREE_TWO_PAGES	{
	stx $c3fc
}        
	ldx #$14
	stx $43f9
	stx $83f9
!ifdef THREE_TWO_PAGES	{
	stx $c3f9
}        
	inx
	stx $43fd
	stx $83fd
!ifdef THREE_TWO_PAGES	{
	stx $c3fd
}        
	ldx #$18
	stx $43fa
	stx $83fa
!ifdef THREE_TWO_PAGES	{
	stx $c3fa
}        
	inx
	stx $43fe
	stx $83fe
!ifdef THREE_TWO_PAGES	{
	stx $c3fe
}        
	ldx #$1c
	stx $43fb
	stx $83fb
!ifdef THREE_TWO_PAGES	{
	stx $c3fb
}        
	inx
	stx $43ff
	stx $83ff
!ifdef THREE_TWO_PAGES	{
	stx $c3ff
}        
	}

start:
	!ifdef RELEASE {
	lda $ffff
        bpl start
        }
        ldx #$00
-       lda charset_copy,x
        sta charset,x
        lda charset_copy+$100,x
        sta charset+$100,x
        lda charset_copy+$200,x
        sta charset+$200,x
        lda charset_copy+$300,x
        sta charset+$300,x
        inx
        bne -
	lda #0
	sta $d017
	sta $d01d
;        ldx #$0e
;        lda #$80
;-	sta $d001,x
;	dex
;        dex
;        bpl -        
        jsr paintlines_nmis
	
	jsr init_code
        ;+calc_coord
!ifdef CACHE_ENABLED {
	jsr depack_first_lines
}        
	lda #COLOR_3
	sta $d021
	lda #COLOR_1
	sta $d022
	lda #COLOR_2
	sta $d023

-	lda $d011
	bpl -
        lda #$48
        sta $d012
	lda #FIRST_nmi_LINE
	clc
	sbc $d012
	sta $dd06
	lda $dd0d

	lda addr_dd00
	sta $dd00
	lda addr_d018
	sta $d018

	lda #$d8
	sta $d016
-	lda $d011
	bmi +
	lda $d012
	cmp #$f0
	bcc -
+
	jsr paintlines_nmis

	ldy sprite_y
	ldx sprite_x
	lda sprite_x_low
	+set_sprite

	;dec $d020
        ;dec $d020
	jsr paintlines
	;inc $d020
        ;inc $d020

	+move xcoord, new_xcoord, $02
	;sty x_cmp+1
	+move ycoord, new_ycoord, $04
        lda $02
        ora $03
        ora $04
        ora $05
        bne +
        lda #$01
        sta wait_for_scroll_state+1
+        
	jmp -

colorstab:
	!byte $00,$0b,$0c,$0f


;colortab_schriftfarbe:
;	!byte $01,$0f,$0c,$0b
;colortab_lightgrey:
;	!byte $0f,$0c,$0b,$00
;colortab_middlegrey:
;	!byte $0c,$0b,$0b,$00
;colortab_darkgrey:
;        !byte $09,$0b,$00,$00

colortab_size = 4
                

	;!ifdef COMMENTED_OUT {
joy:	lda $dc00
	tax
	and #$01
	bne ++
	lda ycoord
	ora ycoord+1
	beq ++
	lda ycoord
	bne +
	dec ycoord+1
+	dec ycoord
++	txa
	and #$02
	bne ++
	lda ycoord
	cmp #<((height)-24*8-1)
	lda ycoord+1
	sbc #>((height)-24*8-1)
	bcs ++
	inc ycoord
	bne ++
	inc ycoord+1
++
	txa
	and #$04
	bne ++
	lda xcoord
	ora xcoord+1
	beq ++
	lda xcoord
	bne +
	dec xcoord+1
+	dec xcoord
++	txa
	and #$08
	bne ++
	lda xcoord
	cmp #<((width*2)-39*8-1)
	lda xcoord+1
	sbc #>((width*2)-39*8-1)
	bcs ++
	inc xcoord
	bne ++
	inc xcoord+1
++	rts
	;}

	!macro get_y_char {
	lda ycoord+1
	sta $02
	lda ycoord
	lsr $02
	ror
	lsr $02
	ror
	lsr $02
	ror
	tax
	}

	!macro get_y_char_in_y {
	lda ycoord+1
	sta $02
	lda ycoord
	lsr $02
	ror
	lsr $02
	ror
	lsr $02
	ror
	tay
	}

			
paintlines_rts:
!ifdef CACHE_ENABLED {
        +get_y_char
        dex
        txa
        tay
        jsr depack_y_line
        +get_y_char
        txa
        clc
        adc #LINE_CACHE_SIZE-2
        tay
        jsr depack_y_line
}
-	lda $d011
        bmi -
        rts

        
paintlines:
	lda xcoord+1
	sta $02
	lda xcoord
	lsr $02
	ror
	lsr $02
	ror
	lsr $02
	ror
	tay
	+get_y_char
        
paintlines_old_x:
	cpx #$ff
        bne +
paintlines_old_y:
	cpy #$ff
        beq paintlines_rts               

+	stx paintlines_old_x+1
        sty paintlines_old_y+1
	sty paintline_old_y+1
	txa
	clc
	adc #25
	sta cpx_value+1
	;in x first line
	;in y x offset
	lda #$00
	sta $04
	sta screen_hi_add+1
paintline_old_y:
	lda #$00
	adc map_addr_low,x
	sta $02
	lda map_addr_high,x
	adc #$00
	sta $03

screen_hi_add:
	lda #$00
	ora map_addr_dest,x
	sta $05
	ldy #$00

	!for i, 39 {
	lda ($02),y
	sta ($04),y
	!if (i != 39) {
		iny
	} 
	}
	inx
cpx_value:
	cpx #$00
	beq do_rts
	lda $04
	adc #40
	sta $04
	bcc +
	clc
	inc screen_hi_add+1
+	jmp paintline_old_y
do_rts	rts

paintlines_nmis:
	lda paintlines_page+1
	eor #DISTANCE
	sta paintlines_page+1	
	;calculate nmi values
	lda ycoord
	and #$07
	tay
	lda yoffsettab,y ;base nmi value
	sta $03
paintlines_page:
	ldy #0
        +get_y_char
	clc
	adc #25
	sta paintlines_nmis_cpx+1
	lda addr_d018,x
	sta $02
	sta nmid018,y
	lda addr_dd00,x
	sta nmidd00,y
-	inx
paintlines_nmis_cpx:
	cpx #$00
	beq ++
	lda $03
	clc
	adc #$08
	sta $03
	lda addr_d018,x
	cmp $02
	beq -
	sta $02
	sta nmid018+1,y
	lda addr_dd00,x
	sta nmidd00+1,y
	lda $03
	sta nmid012,y
	iny
	jmp -

++	

	tya
	and #$0f
	beq make_pseudo_switch
	lda nmid012-1,y
	bmi there_is_a_switch
	; make pseudo switch
make_pseudo_switch:
	lda #$ff
	sta nmid012,y
	iny
there_is_a_switch:
	lda #$00
	sta nmid012,y
	lda xcoord
	and #$07
	tax
	lda d016tab,x
	sta d016val+1
	lda ycoord
	and #$07
	tax
	lda d011tab,x
	sta d011val+1
	rts

!ifdef CACHE_ENABLED {

depack_first_lines:
	lda #LINE_CACHE_SIZE-1
	sta $04
        lda ycoord
        sta $05
        lda ycoord+1
        lsr
        ror $05
        lsr
        ror $05
        lsr
        ror $05
        dec $05
-	ldy $05
	jsr depack_y_line
        inc $05
	dec $04
	bpl -
	rts
}

fade_down:
	!for i,3 {
	ldx #$00
	lda $d020+i
	and #$0f
	cmp #COLOR_3
	bne +
	ldx #COLOR_2
	bne ++
+	cmp #COLOR_2
	bne ++
	ldx #COLOR_1
++	stx $d020+i
	}
	rts
        
	!src "maptab.asm"

!ifdef CACHE_ENABLED {
	!src "map.asm"
} else {
	!src "map_unpacked.asm"
}
        
space_left = $4000 - *


end_of_demo:
	lda #$00
        sta $d021
        sta $d022
        sta $d023
        lda #$7f
        sta $dd0d
        lda $dd0d
        lda #$00
        sta $d01a
        inc $d019
        lda #$00
        sta $d020
        lda #$0b
        sta $d011
	lda #$00
        sta $d015
        sta $d01c
        sta $d01b
        rts
		

event_counter:
	!byte 0

d016tab:
	!byte $d7,$d6,$d5,$d4,$d3,$d2,$d1,$d0

d011tab:!byte $17,$16,$15,$14,$13,$12,$11,$10 

yoffsettab:
	!byte $36,$35,$34,$33,$32,$31,$30,$2f



init_map:
	ldx #0
!ifdef CACHE_ENABLED {
	lda #<line_cache
	sta <depack_dest
	lda #>line_cache
	sta <(depack_dest+1)
}        
-	lda charset_line,x
	tay
	and #$c0
	;cmp #$40
	;bne +
	;lda #$58
+	sta map_addr_dest,x
	tya
	rol
	rol
	rol
	eor #$03
	and #$03
	sta addr_dd00,x
	;cmp #$02
	lda #$00
	;bcc +
	;lda #$60
+	;sta orval+1
	tya
	and #$38
	lsr
	lsr
orval:	ora #$00
	sta addr_d018,x
	
!ifdef CACHE_ENABLED {
	lda <depack_dest
	sta map_addr_low,x
	lda <(depack_dest+1)
	sta map_addr_high,x
	lda <depack_dest
	clc
	adc #width/4
	sta <depack_dest
	bcc +
	inc <(depack_dest+1)
+	lda <depack_dest
	cmp #<(line_cache+width/4*LINE_CACHE_SIZE)
        lda <(depack_dest+1)
        sbc #>(line_cache+width/4*LINE_CACHE_SIZE)
        bcc +
        lda #<line_cache
        sta <depack_dest
        lda #>line_cache
        sta <(depack_dest+1)
+
}
	inx
	cpx #height/8
	bne -
	rts

!ifdef CACHE_ENABLED {
init_modulo_tab:
	ldx #$00
        ldy #$00
-       txa 
	sta modulo_tab,y
        sbx #$ff
        cpx #LINE_CACHE_SIZE
        bne +
        ldx #$00
+	iny
	bne -
        rts
}

init_depacker
	ldx #depack_line_len-1
-	lda depack_line_copy,x
	sta <depack_line,x
	dex
	bpl -
	rts

depack_line_copy:
	!pseudopc zp_code {
depack_line:
	;in a/x src (x low)
	;in depack_dest dest
	stx <depack_line_src_ptr
	sta <depack_line_src_ptr+1
	ldy #$00
	lda (depack_line_src_ptr),y
	sta <ascend_byte+1 ;current counter value
---
	ldx #$00
--	inc <(depack_line_src_ptr)
	bne +
	inc <(depack_line_src_ptr+1)
+	lax (depack_line_src_ptr,x)
	bmi no_ascending_byte_value
ascend_byte:
	lda #$00
	sta (depack_dest),y
	inc <ascend_byte+1
	iny
	dex
	bne ascend_byte
	cpy #width/4
	bcc --
	rts
no_ascending_byte_value:
	inc <(depack_line_src_ptr)
	bne +
	inc <(depack_line_src_ptr+1)
+	
depack_line_src_ptr = *+1
	lda $ffff
-
depack_dest = *+1	
	sta $ffff,y
	iny
	dex
	bmi -
	cpy #width/4
	bcc ---
	rts

charpos = *
char_x = *+2
char_y = *+3
ptr1 = *+4
ptr2 = *+6
ptr3 = *+8
back_up_pos = *+$a

depack_line_len = * - depack_line
	}

;init_spriteblocks:
;	ldx #$07
;-	txa
;	sta $43f8,x
;	sta $83f8,x
!ifdef THREE_TWO_PAGES	{
;        sta $c3f8,x
}
;	dex
;	bpl -
;	rts


        
	;!bin "mandelsprites"
	;!bin "mandeltext"

d010array:
	!bin "d010array"


credittab_low:
	!byte <credit_basic_fader
        !byte <credit_fish
        !byte <credit_anim
        !byte <credit_bouncer
        !byte <credit_kefrens
        !byte <credit_twister
        !byte <credit_monkey
        !byte <credit_vector
        !byte <credit_vbars
        !byte <credit_greetings
        !byte <credit_vertical
        !byte <credit_endpart
        !byte <credit_music
        !byte <credit_integration
        
credittab_high:
	!byte >credit_basic_fader
        !byte >credit_fish
        !byte >credit_anim
        !byte >credit_bouncer
        !byte >credit_kefrens
        !byte >credit_twister
        !byte >credit_monkey
        !byte >credit_vector
        !byte >credit_vbars
        !byte >credit_greetings
        !byte >credit_vertical
        !byte >credit_endpart
        !byte >credit_music
        !byte >credit_integration


credittab_len = * - credittab_high

;sinxtab:
;	!bin "sinxtab"
;sinxtab_len = * - sinxtab
;sinytab:
;	!bin "sinytab"
;sinytab_len = * - sinytab

	+align256

	; avoid page cross !
	+check_same_page_start
nmi_first:
	sta old_nmi_a
d016val:lda #$d0
	sta $d016
d011val:lda #$0b
	sta $d011
	lda #<nmi
	sta $fffa
        stx old_nmi_x
        sty old_nmi_y2+1
old_nmi_y2:
	ldy #$00
	ldx nmi_counter+1
	lda $d012
-	cmp $d012
	beq -
	ldy old_nmi_y2+1
	nop
	jmp nmi_without_ax

nmi:    sta old_nmi_a
        stx old_nmi_x
nmi_without_ax:
        ldx $dc04
	lda stable_tab_nmi,x
	sta bpl_addr2+1
bpl_addr2:
	bpl bpl_addr2

	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
	+check_same_page_end
        ;stable here
nmi_counter:
	ldx #DISTANCE
	lda nmid018,x
        sta $d018
	lda nmidd00,x
	sta $dd00
        ;sty old_nmi_y+1
	;+check_same_page_end
	inc nmi_counter+1
	lda nmid012,x
	bne make_next_nmi
	txa
	and #DISTANCE
	eor #DISTANCE
	sta nmi_counter+1
	lda #<nmi_first
	sta $fffa
	lda #FIRST_nmi_LINE
make_next_nmi:
;	ldx $dd06
;-	cpx $dd06
;	beq -
	ldx old_nmi_x
	clc
	sbc $d012
	sta $dd06
        lda #%01010001
        sta $dd0f
	lda old_nmi_a
;old_nmi_y:
;	ldy #$00
	jmp $dd0c


stable_tab_nmi:
	; table needed for stable interrupt

	+check_same_page_start
	!for i,$2a {
        !byte 17
        }
        !byte 17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0
        !for i,63-$2a-17 {
        !byte 0
	}
	+check_same_page_end

-	rts


	!ifdef COMMENTED_OUT {
depack_y_line:
	;in y line
        sty $fffc
        cpy #height/8
        bcs -
        lda map_addr_low,y
        sta <depack_dest
        lda map_addr_high,y
        sta <(depack_dest+1)
	ldx maptablow,y
	lda maptabhigh,y
	;jmp depack_line
	jsr depack_line
-	cpy #$51
	bcs -
        rts
	}
                
init_paint_char:
	ldy #$00
-	tya
	sta char_x_ptr_low,y
	sta char_x_ptr_low+3,y
	sta char_x_ptr_low+6,y
	sta char_x_ptr_low+9,y
	iny
	cpy #3
	bne -
	ldx #$00
	ldy #$44
-	tya
	sta char_x_ptr_high,x
	sta char_x_ptr_high+1,x
	sta char_x_ptr_high+2,x
	iny
	txa
	sbx #$fd
	cpx #3*4
	bne -

	clc
	ldy #$00
	tya
--	ldx #21
-	sta char_y_offset,y
	adc #$03
	iny
	dex
	bne -
	adc #$01
	cpy #21*4
	bne --
	rts

do_states:
run_state:
	ldx #$00
	lda state_tab_low,x
	sta jmp_ptr+1
	lda state_tab_high,x
	sta jmp_ptr+2
jmp_ptr:jmp 0
	
state_tab_low:
	!byte <init_state
	!byte <clear_state
	!byte <character_state
	!byte <fadein_state
	!byte <wait_for_scroll_state
	!byte <sleep_state
	!byte <fadeout_state
	!byte <end_state

state_tab_high:
	!byte >init_state
	!byte >clear_state
	!byte >character_state
	!byte >fadein_state
	!byte >wait_for_scroll_state
	!byte >sleep_state
	!byte >fadeout_state
	!byte >end_state

color_fadein_tab:
	!byte $06,$02,$0b,$0a,$0c,$0f,$07,$01

color_fadeout_tab:
	!byte $01,$07,$0f,$0c,$0a,$0b,$02,$06


sleep_state:
	lda direction_fade_out
        sta direction_fade_in
	dec sleep_state_counter+1
sleep_state_counter:
	lda #$00
        beq do_inc_runstate
        rts




direction_fade_in:
	!byte 0

direction_fade_out:
	!byte 0

!macro fade .fade_tab {
.fade_state:
	ldx #$00
	lda #$ff
	sta $d015
	txa
	lsr
	lsr
	tay
	lda .fade_tab,y
	!for i, 8 {
	sta $d027+(i-1)
	}
	inx
	stx .fade_state+1
	cpx #$20
	beq do_inc_runstate
	ldy direction_fade_in
	tya
	lsr
	bcc +
	dec sprite_y
+	lsr
	bcc +
	inc sprite_y
+	lsr
	bcc ++
	tay
	dec sprite_x_low
	lda sprite_x_low
	lsr
	bcc +
	dec sprite_x
+	tya
++	lsr
	bcc ++
	inc sprite_x_low
	lda sprite_x_low
	lsr
	bcs +
	inc sprite_x
+
++
	rts
}

fadein_state:
	+fade color_fadein_tab

wait_for_scroll_state:
	lda #$00
        beq +
do_inc_runstate:
	inc run_state+1        
+	rts        

fadeout_state:
	+fade color_fadeout_tab


end_state:
	lda #$00
        sta $d015
	inc end_state_index+1
end_state_index:
	ldx #$00
        cpx #credittab_len
        bne +
        ldx #$00
        stx end_state_index+1
+	lda credittab_low,x
	sta charpos
        lda credittab_high,x
        sta charpos+1
        lda #$00
        sta run_state+1                      
        rts

init_state:
	lda #50
        sta sleep_state_counter+1
	ldy #$00
	sty $d015
	sty fadein_state+1
	sty fadeout_state+1
        sty wait_for_scroll_state+1
	lda (charpos),y
	sta new_xcoord
	iny
	lda (charpos),y
	sta new_xcoord+1
	iny
	lda (charpos),y
	sta new_ycoord
	iny
	lda (charpos),y
	sta new_ycoord+1
	iny
	lda (charpos),y
	sta sprite_x
	lda #$00
	sta sprite_x_low
	iny
	lda (charpos),y
	sta sprite_y
	iny
	lda (charpos),y
	sta direction_fade_in
        iny
        lda (charpos),y
        sta direction_fade_out
	lax charpos
	sbx #-8
	stx charpos
	bcc +
	inc charpos+1
+
	lda #$44
	sta ptr1+1
	lda #$84
	sta ptr2+1
!ifdef THREE_TWO_PAGES	{
	lda #$c4
	sta ptr3+1
}        
	lda #$00
	sta char_x
	sta char_y
	sta back_up_pos+1
	sta ptr1
	sta ptr2
!ifdef THREE_TWO_PAGES	{
	sta ptr3
}        
	inc run_state+1
	rts

clear_state:
	ldy #$1f
	lda #$00
-	sta (ptr1),y
	sta (ptr2),y
!ifdef THREE_TWO_PAGES	{
	sta (ptr3),y
}        
	dey
	bpl -
	lda ptr1
	clc
	adc #$20
	sta ptr1
	sta ptr2
!ifdef THREE_TWO_PAGES	{
	sta ptr3
}        
	bcc +
	inc ptr1+1
	inc ptr2+1
!ifdef THREE_TWO_PAGES	{
	inc ptr3+1
}        
	lda ptr1+1
	cmp #$48
	beq next_state
+
	rts

character_state:
	ldy #$00
	lda (charpos),y
	bpl print_normal_char
	eor #$ff
	ldx charpos
	ldy charpos+1
	inx
	bne +
	iny
+
	stx back_up_pos
	sty back_up_pos+1
	tax
	lda name_tab_low,x
	sta charpos
	lda name_tab_high,x
	sta charpos+1
	bne character_state ; unconditional

print_normal_char:
	bne paint_char

found_backup:
	lda back_up_pos
	sta charpos
	lda back_up_pos+1
	sta charpos+1
	beq next_state
	ldx #$00
	stx back_up_pos+1
	rts

next_state:
	inc run_state+1
	rts

paint_char:
	inc charpos
	bne +
	inc charpos+1
+	cmp #CR
	bne +
	lda #$00
	sta char_x
	lda char_y
	clc
	adc #$10
	sta char_y
	rts
+	;in a character
	ldy #$00
	sty paint_y_offset+1
	sty paint_char_ptr+2
	asl
	rol paint_char_ptr+2
	asl
	rol paint_char_ptr+2
	asl
	rol paint_char_ptr+2
	asl
	rol paint_char_ptr+2
	clc
	adc #<charset
	sta paint_char_ptr+1
	lda paint_char_ptr+2
	adc #>charset
	sta paint_char_ptr+2
+
	ldx char_x
	lda char_x_ptr_low,x
	sta paint_ch_ptr1+1
	sta paint_ch_ptr2+1
!ifdef THREE_TWO_PAGES	{
	sta paint_ch_ptr3+1
}        
	lda char_x_ptr_high,x
	sta paint_ch_ptr1+2
	eor #$c0	;-$40 + $80
	sta paint_ch_ptr2+2
!ifdef THREE_TWO_PAGES	{
	ora #$40
	sta paint_ch_ptr3+2
}
	ldx char_y
-
paint_y_offset:	
	ldy #$00
paint_char_ptr:
	lda $ffff,y
	ldy char_y_offset,x
paint_ch_ptr1:
	sta $ffff,y
paint_ch_ptr2:
	sta $ffff,y
!ifdef THREE_TWO_PAGES	{
paint_ch_ptr3:
	sta $ffff,y
}
	inx
	lda #$0f
	sec
	isc paint_y_offset+1
	bcs paint_y_offset
	inc char_x
	rts

PEISELULLI = $ff
STREETUFF = $fe
BENSON = $fd
TITUS = $fc
_CODE = $fb
_GFX = $fa
_BY = $f9
_AND = $f8
_CHAR = $f7

CR = $7f

name_tab_low:
	!byte <peiselulli
	!byte <streetuff
	!byte <benson
	!byte <titus
	!byte <_code
	!byte <_gfx
	!byte <_by
	!byte <_and
        !byte <_char

name_tab_high:
	!byte >peiselulli
	!byte >streetuff
	!byte >benson
	!byte >titus
	!byte >_code
	!byte >_gfx
	!byte >_by
	!byte >_and
        !byte >_char

	!ct scr {

peiselulli:
	!text " peiselulli"
	!byte 0
streetuff:
	!text " streetuff"
	!byte 0
benson:
	!text " benson"
	!byte 0
titus:
	!text " titus"
	!byte 0
_code:
	!text "code"
	!byte 0
_gfx:
	!text "gfx"
	!byte 0
_by:
	!text " by "
	!byte 0

_and:
	!text " + "
	!byte 0

_char:
	!text "charset"
	!byte 0

UP_FADE = 1
DOWN_FADE = 2
LEFT_FADE = 4
RIGHT_FADE = 8


credit_basic_fader:
	!word 0,0
	!byte $70,$60, RIGHT_FADE, DOWN_FADE
	!byte _CODE, _AND, CR
        !byte _GFX, ':', CR
	!byte STREETUFF, 0


credit_fish:
	!word 390,352
	!byte $88,$60, DOWN_FADE, LEFT_FADE
	!byte _CODE, ':', CR
        !byte BENSON, CR
        !byte _GFX, ':', CR
	!byte TITUS, 0

credit_anim:
	!word 0,650
	!byte $68,$a0, UP_FADE, RIGHT_FADE
	!byte _CODE, ':', CR
        !byte BENSON, CR
        !byte _GFX, _AND, CR
        !text "animation:"
        !byte CR
	!byte TITUS, 0

credit_bouncer:
	!word 390,800
	!byte $10,$a0, RIGHT_FADE, UP_FADE
	!byte _CODE, ':', CR
        !byte PEISELULLI, CR
        !byte _GFX, ':', CR
        !byte TITUS, CR,0


credit_kefrens:
	!word 390,420
	!byte $30,$a0, LEFT_FADE, DOWN_FADE
	!byte _CODE, ':', CR
        !byte PEISELULLI, 0

credit_twister:
	!word 0,280
	!byte $78,$70, DOWN_FADE, RIGHT_FADE
	!byte _CODE, ':', CR
        !byte PEISELULLI, CR
        !text "rendering:"
	!byte CR
        !text " zero-x"
        !byte 0

credit_monkey:
	!word 0,800
	!byte $18,$a0, UP_FADE, LEFT_FADE
	!byte _CODE, ':', CR
        !byte STREETUFF, CR
        !byte _GFX, ':', CR
        !byte TITUS, CR,0

credit_vector:
	!word 390,0
	!byte $80,$50, LEFT_FADE, DOWN_FADE
	!byte _CODE, ':', CR
        !byte PEISELULLI, 0

credit_vbars:
	!word 0,500
	!byte $10,$a0, RIGHT_FADE, UP_FADE
	!byte _CODE, _AND, CR
        !byte _GFX, ':', CR
	!byte STREETUFF, 0

credit_greetings:
	!word 390,120
	!byte $10,$70, DOWN_FADE, LEFT_FADE
	!byte _CODE, _AND, CR
        !byte  _CHAR, ':' ,CR
        !byte STREETUFF, CR
        !byte _GFX, ':', CR
        !text " knoeki",0

credit_vertical:
	!word 390,650
	!byte $80,$70, DOWN_FADE, RIGHT_FADE
	!byte _CODE, ':', CR
	!byte STREETUFF, 0

credit_endpart:
	!word 0,932
	!byte $10,$70, DOWN_FADE, LEFT_FADE
	!byte _CODE, ':', CR
        !byte PEISELULLI, CR
	!byte _GFX, ':' , CR
	!byte STREETUFF, CR
        !byte TITUS,0

credit_music:
	!word 0,120
	!byte $10,$90, UP_FADE, RIGHT_FADE
        !text "music:"
	!byte CR
        !text " stainless"
        !byte CR
        !text " steel"
        !byte CR
        !text "coordinator:"
        !byte CR, STREETUFF
	!byte 0

credit_integration:
	!word 390,932
	!byte $70,$80, DOWN_FADE, LEFT_FADE
        !text "integration:"
	!byte CR
        !byte PEISELULLI, CR
        !byte STREETUFF, CR
        !text "loader:"
        !byte CR
        !text " krill"
	!byte 0

	}


        

	!set ADR = *
        * = $4000
charset_copy:
	!for i,64 {
        !bin "1x2-font.dat",8,$2+((i-1)*8)
        !bin "1x2-font.dat",8,$402+((i-1)*8)
        }

init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
        lda #$7f
        sta $dc0d
	sta $dd0d
        lda $dc0d
	lda $dd0d
        lda #%00000000
        sta $dc0f
	sta $dd0f
        lda #%10000000
        sta $dc0e
	sta $dd0e
        lda #$7f
        sta $dc0d
        sta $dd0d
	;nop
	;nop
	;nop
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dc04
        lda #>(63-1)
        sta $dc05
	; but not start it yet !

	+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	nop
	nop
	nop
        ldy #%10000001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
        sty $dc0e

	lda #$ff
	sta $dd06
        lda #$00
        sta $dd07
        lda #$7f
        sta $dd0d
        lda $dd0d
        lda #%00000000
        sta $dd0f
        lda #%10000000
        sta $dd0e
        lda #$82
        sta $dd0d
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dd04
        lda #>(63-1)
        sta $dd05
	!for i, 27 {
	nop
	}
        lda #%10010001
        sta $dd0e
        lda #%01010001
        sta $dd0f
	lda #$40
	sta $dd0c
	rts
	+check_same_page_end


init_code:
	!ifdef RELEASE {
	;jsr FRAMEWORK_IRQ
	} else {
	sei
	}
	lda #$31
	sta $01
	ldx #$00
	ldy #$00
-	lda $d000,x
	lda charset,y
	lda charset+1,y
	lda $d080,x
	lda charset+$100,y
	lda charset+$101,y
	lda $d100,x
	lda charset+$200,y
	lda charset+$201,y
	lda $d180,x
	lda charset+$300,y
	lda charset+$301,y
	inx
	iny
	iny
	bne -
	lda #$35
	sta $01

	ldx #$00
	ldy #$00
	+set_sprite

!ifdef CACHE_ENABLED {
        jsr init_modulo_tab
}        
	jsr init_map
	jsr init_depacker
	jsr init_paint_char
	ldx #$00
	stx xcoord
	stx xcoord+1
        stx ycoord
        stx ycoord+1
	stx new_xcoord
	stx new_xcoord+1
	stx new_ycoord
	stx new_ycoord+1
	stx $d020
	lda #$00+$08
-	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00,x
	inx
	bne -
	stx $14
	stx $15
	jsr init_stable_timer
	lda #<credit_basic_fader
	sta charpos
	lda #>credit_basic_fader
	sta charpos+1

	!ifdef RELEASE {
	sei
	}
doe:	lda $d012
	cmp #$39
        bne doe        
        lda #<irq
        sta $fffe
        lda #>irq
        sta $ffff
        lda #$81
        sta $d019
        sta $d01a
	lda #<nmi
	sta $fffa
	lda #>nmi
	sta $fffb
	lda #$7f
	sta $dc0d
	lda $dc0d
	;lda #$0b
	;sta $d011
	lda #$a0
	sta $d012
	cli
	;jsr init_spriteblocks
-	lda $d012
	cmp #$39
	bne -
        
        lda #$00
        sta $d01c
	ldx #$07
	lda #$01
-	sta $d027,x
	dex
	bpl -
	lda #$00
	sta sprite_x_low
	lda #$50
	sta sprite_x
	sta sprite_y
        lda #$80
        sta secondline
	;lda #$00
	;sta char_x
	;sta char_y
	;jsr paint_char
	rts

      
charset = $0400
        
	;* = (ADR + $ff) & $ff00

	!src "include.asm"


