#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main()
{
    FILE *file;
    unsigned char low_bytes[256];
    unsigned char high_bytes[256];

    int i,j;
    double f;
    for(i=0; i < 208; ++i)
    {
        f = ((double)i+104+52) / 104 * M_PI;
        f = sin(f) * 159.5;
        j = (int)(f)+160;
        low_bytes[i] = j & 0xff;
        high_bytes[i] = j >> 8;
        printf("%d ", j);
    }
    file = fopen("xlowtab", "wb");
    fwrite(low_bytes, 1, 208, file);
    fclose(file);
    file = fopen("xhightab", "wb");
    fwrite(high_bytes, 1, 208, file);
    fclose(file);

    printf("\n");
    for(i=0; i < 254; ++i)
    {
        f = ((double)i+254/2+254/4) / (254/2) * M_PI;
        f = sin(f) * 99.5;
        j = (int)(f)+100;
        low_bytes[i] = j & 0xff;
        printf("%d ", j);
    }
    file = fopen("ytab", "wb");
    fwrite(low_bytes, 1, 254, file);
    fclose(file);

}

