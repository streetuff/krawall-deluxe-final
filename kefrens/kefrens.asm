	;free space $e000-$f806

	!ifdef RELEASE {
        !src "config.asm"
        !src "../framework/framework.inc"
	!to "kefrens.plain",plain
        * = PARTORG
        } else {
        !to "kefrens.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
        }
	jmp start
        !cpu 6510

	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}

old_f7 = $f2
old_ff = $f1

	!macro asr {
	cmp #$80
	ror
	}

	!macro align16 {
		* = (*+$f) & $fff0
	}

	!macro align256 {
		* = (*+$ff) & $ff00
	}

	!macro align512 {
		* = (*+$1ff) & $fe00
	}

d016values = $0200
agspvalues = $0300
shiftedtab = $0500

CODE_INX = $e8
CODE_INY = $c8
CODE_DEX = $ca
CODE_DEY = $88

;blah2 = $fa

FIRST_LINE = $30
NUMBER_OF_LINES = 200
NUMBER_OF_KEF = 70

CODEBASE = $5000

	!ifdef RELEASE {
	} else {
	+align256
init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #%00000000
        sta $dc0f
        lda #%10000000
        sta $dc0e
        lda #$82
        sta $dc0d
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dc04
        lda #>(63-1)
        sta $dc05
	; but not start it yet !

	+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	nop
	nop
	nop
        ldy #%10000001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
        sty $dc0e
	+check_same_page_end
	rts
}

	+align256
sine1:
	!bin "kefsin2"
	!bin "kefsin2"

bar_color0_col0:
	!byte %01101110
	!byte %00011011
	!byte %00000110
	!byte %00000001


bar_color0_col1:
	!byte %01000000
	!byte %10010000
	!byte %11100100
	!byte %10111001

bar_mask_col0:
	!byte %00000000
	!byte %11000000
	!byte %11110000
	!byte %11111100

bar_mask_col1:
	!byte %00111111
	!byte %00001111
	!byte %00000011
	!byte %00000000

stable_tab_irq:

	; table needed for stable interrupt

	+check_same_page_start
	!for i,$28 {
        !byte 0
        }
        !byte 11,10,9,8,7,6,5,4,3,2,1,0
        !for i,63-$28-10 {
        !byte 0
	}
	+check_same_page_end


	!ifdef RELEASE {
        } else {
	* = $1000
sound:
	!bin "Wip3Out_2k6.sid",,126
        }
start:
	;ldx #$ff
	;txs
	!ifdef RELEASE {
	lda #$60
        sta PLAY_MUSIC
        ldy #newsid_len
	ldx #$00
src_ptr:lda newsid,x
dst_ptr:sta $0800,x
        inx
        bne src_ptr
        inc src_ptr+2
        inc dst_ptr+2
        dey
        bne src_ptr
        lda #$00
        jsr $0800
        } else {
        sei
        }
	lda $01
	pha
	lda #$30
	sta $01
	lda #$d0
	sta $03
	ldy #$00
	sty $02
	tya
	ldx #$10
-	sta ($02),y
	iny
	bne -
	inc $03
	dex
	bne -
	pla
	sta $01
        ;lda #$0b
        ;sta $d011
        jsr init_agsp_tabs
	jsr codegen

	lda #<enable_scroll
	sta $ed
	lda #>enable_scroll
	sta $ee

	;lda #<sine1
	;sta blah2
	;lda #>sine1
	;sta blah2+1

	lda #%00000011
	sta $dd02
	lda #$04
	sta $d022
	lda #$0a
	sta $d023
	;lda #$36
	;sta $01
        !ifdef RELEASE {
        } else {
	lda #$00
	jsr sound
        }
	ldx #$00
	;stx $3fff
-       lda #$08+7
        sta $d800,x
        sta $d900,x
        sta $da00,x
        sta $db00,x
        ;fix for black pixel (???)
        lda #$00
        sta $f800,x
        sta $f900,x
        sta $fa00,x
        sta $fb00,x        
        inx
        bne -
-	;lda $dc01
	;and #$10
	;bne -
	ldx #$00
-	txa
	sta $0401,x
	sta $0421,x
	sta $0441,x
	inx
	cpx #$20
	bne -
	ldx #$61
	lda #$30
-	sta $0400,x
	inx
	bne -
	lda #$00
	sta $30*8+7

-	lda #00
	sta $07,x
	!byte $9d, $87, $00
	txa
	sbx #$f8
	bne -

	lda #$30
	sta $01
	ldx #$00
-	lda $0400,x
	sta $d000,x
	inx
	bne -
	lda #$35
	sta $01
        lda #$7f
        sta $dc0d
        lda $dc0d
	;lda #$08
	;sta $d011
	lda #$00
	sta $dd00
	lda #$46
	sta $d018
	lda #$00
	sta $d0

	ldx #$00
-	txa
	lsr
	lsr
	sta shiftedtab,x
	inx
	bne -

	!ifdef RELEASE {
        lda $fff8
-	cmp $fff8
	bne -        
	} else {
	jsr init_stable_timer
	}
	lda #$00
-	cmp $d012
	bne -

	lda #$06
        sta $d020
	;lda #$00
        sta $d021
	lda #$08
	sta $d011
  	!ifdef RELEASE {
        lda #$4c
        sta PLAY_MUSIC                       
	}              
        lda #$2f
        sta $d012
        sei
	lda #$4c
	sta $08
        lda #<irq
        sta $09
        lda #>irq
        sta $0a
	lda #$08
	sta $fffe
	lda #$00
	sta $ffff
        lda #$81
        sta $d01a
        sta $d019
	lda $3fff
	sta old_3fff+1
 	cli
	!ifdef RELEASE {
	jsr LOAD_NEXT_PART
        ldx #$00
-       lda zwischenpart,x
        sta FRAMEWORK_ADDR-$100,x
        inx
        bne -
	}
l:	lda #$00
	beq l
	!ifdef RELEASE {
        lda $fff8
-	cmp $fff8
	bne -        
	lda #$00
	sta $d020
	sta $d021
	jmp FRAMEWORK_ADDR-$100
	} else {
	jmp l
	}

	!ifdef RELEASE {
zwischenpart:
	!pseudopc FRAMEWORK_ADDR-$100 {
        jsr FRAMEWORK_IRQ
        jsr DECRUNCH_NEXT_PART
        jmp START_NEXT_PART
        }
	}

fadefout_activate:
	;sei
	lda #<disable_scroll_ptr
	sta $ed
	lda #>disable_scroll_ptr
	sta $ee
	lda #$4c
	sta jump_after_scroll
	lda #<disable_scroll
	sta jump_after_scroll+1
	lda #>disable_scroll
	sta jump_after_scroll+2
	rts
	;cli
;l3:
	;jmp l3

init_agsp_tabs:

	; initialize the AGSP scroll tables

        ldx #$00
-       txa
        and #$07
        eor #$07
        ora #$d0
        sta d016values,x
        inx
        bne -

        ldy #00
-       tya
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
	inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        iny
        inx
        bne -
	rts



        !macro set_line_pos i {
.start:
	lda #((FIRST_LINE+i+2)& 07) +$18 ; 2 cycles
	sta $d011		       ; 4 cycles
!set call_pos = *+2-.start
	jsr CODEBASE + ((i/2)) *$100 + $30
!set line_pos_len = *-.start

        }

        !macro set_line_pos_inv i {
	lda #((FIRST_LINE+i+2)& 07) +$18 ; 2 cycles
	sta $d011		       ; 4 cycles
	!if i != 8 {
	ldx #10
-	dex
	bne -
	nop
	nop
	nop
	} else {
	ldx #8
-	dex
	bne -
	nop
	nop	; normaly wrong !
	ldx #$03
	lda #$10
	stx $dd00
	sta $d018
	}
	}

	+align256
	; avoid page cross !
irq:    sta irq_old_a+1
        stx irq_old_x+1
	bit $ea
        ldx $dc04
	lda stable_tab_irq,x
	sta bpl_addr2+1
bpl_addr2:
	bpl bpl_addr2

	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        ;stable here
	lda $f7
	sta old_f7
	bit $ea
	sty irq_old_y+1
d016value:
        lda #$d8
	sta $d016
xhigh_coord:
        bmi xhigh_coord+2
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        lda #$08
        sta $d011
	lda #$09
	sta $d011

	lda $ff
	sta old_ff
	lda #$00
	sta $f7
	sta $ff
	;!for i, 2 {
	;nop
	;}
	sta $3fff
	!for i, 8 {
        +set_line_pos_inv i
	}
x_reg:	ldx #$00
line_select_pos:
	!for i, NUMBER_OF_LINES-8 {
        +set_line_pos i
	}
        inc $d019
        ;lda #$00
        ;sta $d020
	lda #$17
	sta $d011
	;cli
	lda old_f7
	sta $f7
	lda old_ff
	sta $ff
	lda #$00
	sta $dd00
	lda #$46
	sta $d018
old_3fff:
	lda #$00
	sta $3fff

        !ifdef RELEASE {
        jsr PLAY_MUSIC
        } else {
	jsr sound+3
        }
;	ldx #00
;-	lda #00
;	sta $07,x
;	!byte $9d, $87, $00
;	txa
;	sbx #$f8
;	bne -
	lda #$00
	!for i, 30 {
        sta $07+((i-1)*8)
	}
	!for i, 8 {
        sta $107+((i-1)*8)
        }
	;dex
	;bpl -
	jsr set_plots
irq_old_y:
	ldy #$00
irq_old_a:
	lda #$00
irq_old_x:
	ldx #$00
rti_addr:
        rti


!macro codegenerate .zeropage, .index {
	!if .zeropage = 1 {
	lda #$a5
	} else {
	lda #$b5
	}
	sta ($d0),y

	iny
	lda $d2+.index
	sta ($d0),y
	iny
	;!if .zeropage = 0 {
	;lda $d3+.index
	;sta ($d0),y
	;iny
	;}

	lda #$29
	sta ($d0),y
	iny
	sty $d6
	txa
	and #$03
	tay
	lda bar_mask_col0,y
	ldy $d6
	sta ($d0),y
	iny

	lda #$09
	sta ($d0),y
	iny
	sty $d6
	txa
	and #$03
	tay
	lda bar_color0_col0,y
	ldy $d6
	sta ($d0),y
	iny

	!if .zeropage = 1 {
	lda #$85
	} else {
	lda #$95
	}
	sta ($d0),y
	iny
	lda $d2+.index
	sta ($d0),y
	iny
	;!if .zeropage = 0 {
	;lda $d3+.index
	;sta ($d0),y
	;iny
	;}

	lda $d2+.index
	clc
	adc #$08
	sta $d2+.index
	bcc +
	inc $d3+.index
+

	!if .zeropage = 1 {
	lda #$a5
	} else {
	lda #$b5
	}
	sta ($d0),y
	iny
	lda $d2+.index
	sta ($d0),y
	iny
	;!if .zeropage = 0 {
	;lda $d3+.index
	;sta ($d0),y
	;iny
	;}

	lda #$29
	sta ($d0),y
	iny
	sty $d6
	txa
	and #$03
	tay
	lda bar_mask_col1,y
	ldy $d6
	sta ($d0),y
	iny

	lda #$09
	sta ($d0),y
	iny
	sty $d6
	txa
	and #$03
	tay
	lda bar_color0_col1,y
	ldy $d6
	sta ($d0),y
	iny

	!if .zeropage = 1 {
	lda #$8d
	} else {
	lda #$95
	}
	sta ($d0),y
	iny
	lda $d2+.index
	sta ($d0),y
	iny
	!if .zeropage = 1 {
	lda $d3+.index
	sta ($d0),y
	iny
	} else {
	lda #$60
	sta ($d0),y
	}

	lda $d2+.index
	sec
	sbc #$08
	sta $d2+.index
	bcs +
	dec $d3+.index
+

}

!macro codegenerate_nop {
	lda #$a0
	sta ($d0),y
	iny
	lda #$08
	sta ($d0),y
	iny
	lda #$88
	sta ($d0),y
	iny
	lda #$d0
	sta ($d0),y
	iny
	lda #$fd
	sta ($d0),y
	iny
	lda #$ea
	sta ($d0),y
	iny
	sta ($d0),y
	iny
	lda #$60
	sta ($d0),y
}

codegen:
	lda #<CODEBASE
	sta $d0
	lda #>CODEBASE
	sta $d1
	ldx #$00
codecalcloop:
	lda #$00
	sta $d3
	sta $d5
	txa
	asl
	rol $d3
	and #$f8
	ora #$07
	sta $d2

	txa
	eor #$ff
	lsr
	sta $d7
	asl
	rol $d5
	and #$f8
	ora #$07
	sta $d4


	;cpx #160/2
	;bcc +
	;jmp abs_first
+	ldy #$00
	stx $d8
	+codegenerate 1, 0
	ldx $d7
	+codegenerate 0, 2

	ldy #$30
	+codegenerate_nop

	inc $d7
	lda $d7
	asl
	rol $d5
	and #$f8
	ora #$07
	sta $d4

	ldy #$40
	ldx $d8
	+codegenerate 1, 0
	ldx $d7
	+codegenerate 0, 2

	inc $d7
	lda $d7
	asl
	rol $d5
	and #$f8
	ora #$07
	sta $d4

	ldy #$80
	ldx $d8
	+codegenerate 1, 0
	ldx $d7
	+codegenerate 0, 2

	inc $d7
	lda $d7
	asl
	rol $d5
	and #$f8
	ora #$07
	sta $d4

	ldy #$c0
	ldx $d8
	+codegenerate 1, 0
	ldx $d7
	+codegenerate 0, 2
	ldx $d8



	;jmp continue
abs_first:
	;+codegenerate 0, 0
	;stx $d8
	;ldx $d7
	;+codegenerate 1, 2
	;ldx $d8
continue:

-	;lda #$ea
	;sta ($d0),y
	;iny
	;cpy #16+11
	;bne -
	;lda #$24
	;sta ($d0),y
	;iny
	;sta ($d0),y
	;iny
	inc $d1
	inx
	cpx #128
	beq +
	jmp codecalcloop
+	rts





set_plots:
	;inc $d020
blah2:	ldx #$00
blah1:
	ldy #$00
	!for i,NUMBER_OF_LINES-8 {
	lda sine1,y
	;clc
	;adc (blah2),y
	adc sine1,x
	sta line_select_pos + (line_pos_len*(i-1)) + call_pos
	;iny
	iny
	iny
	dex
	dex
	dex

	}

	lax blah1+1
	sbx #-2
	stx blah1+1

	lax blah2+1
	sbx #5
	stx blah2+1

xscroll:
	ldx #$00
	lda d016values,x
	sta d016value+1
	lda agspvalues,x
	sta xhigh_coord+1
	inc xscroll+1
jump_after_scroll:

	cpy #90
	bcs lowpos
	;dec $d020
	rts
lowpos:
	lda #$00
	sec
	sbc #$40
	sta lowpos+1
	nop
enable_scroll:
	!for i, NUMBER_OF_LINES-8 {
	bit line_select_pos + (line_pos_len*((NUMBER_OF_LINES-8-i))) + call_pos-1
	}
	bcs +
	lda x_reg+1
	sec
	sbc #$08
	sta x_reg+1
+

slowdown:
	lda #$00
	eor #$01
	sta slowdown+1
	beq +

	ldy #$00
	lda ($ed),y
	cmp #$2c
	bne activate_space
	lda #$8d
	sta ($ed),y
	lda $ed
	clc
	adc #$03
	sta $ed
	bcc +
	inc $ee
+	;dec $d020
codesequ_label:
	ldx #$00
-	lda codesequ_low,x
	sta + + 1
	lda codesequ_high,x
	sta + + 2
+	jmp 0

codesequ_low:
	!byte <nothing
	!byte <sleep

codesequ_high:
	!byte >nothing
	!byte >sleep

sleep:	lda #$a2
	inc sleep+1
	bne nothing
	jsr fadefout_activate
	dec codesequ_label+1 
nothing:
	rts

activate_space:
	inc codesequ_label+1
	lda #$60
	sta activate_space
	rts

disable_scroll:
	lda #$30
disable_scroll_ptr:
	!for i, NUMBER_OF_LINES-8 {
	bit line_select_pos + (line_pos_len*(i-1)) + call_pos-1
	}
	ldy #$00
	lda ($ed),y
	cmp #$2c
	bne ++
	lda #$8d
	sta ($ed),y
	lda $ed
	clc
	adc #$03
	sta $ed
	bcc +
	inc $ee
+	rts

++	lda #$01
	sta l+1
	rts

newsid:
	!bin "krawall_part2.sid",,2
newsid_len = >((* -  newsid + $ff))

        
