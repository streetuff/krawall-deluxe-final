        !cpu 6510

	!ifdef RELEASE {
        !src "../framework/framework.inc"
        !src "config.asm"
	!to "fishtro.plain",plain
         * = PARTORG
	lda #$35
	sta $01
	jmp start
        } else {
        !to "fishtro.prg",cbm
         *= $2000
	sei
	lda #$35
	sta $01

	jsr init_stable_timer

        lda #$7f
	sta $dc0d
	lda $dc0d
 

        }

 

	!set v_ptr = $02

        !macro spare .v {
        !set v_ptr = v_ptr+.v
        }
		
	!set m_ptr = vars

        !macro m_spare .v {
        !set m_ptr = m_ptr+.v
        }
		
		!macro waitblank {
			 lda #$e0
			 cmp $d012
			 bne *-3
		
		}

OWN_SPRITES = 0
		
HIGH_BIT_ENABLED = 1

NUM_OF_BUBBLES = 4

screen = $6000
color = $3c00
noise = $c000  ; temp speicher f�r das noise bitmap 
bitmap0 =$4000
bitmap1 =$3400
bitmap2 =$2c00
sprites =$6400


ROW_LEN   = 16                  ; Anzahl der Bytes pro Reihe 
ROW_LEN_BM = ROW_LEN *8         ; Anzahl der Bytes pro Reihe in der Bitmap
ROW_NUM = 12                    ; Anzahl der Reihen
ROW_MOD = 40- ROW_LEN           ; Anzahl der Modulo Bytse pro Reihe
ROW_MOD_BM = ROW_MOD *8         ; Anzahl der Modulo Bytse pro Reihe
BM_START= bitmap0 +6*320+18*8     ; Fernseher , oben links
CL_START= $d800 +6*40 +18       ; Fernseher , oben links, Coloram
SC_START= screen+6*40 +18       ; Fernseher , oben links, Screenram

BM_WAIT = 149			; Zeit zwischen zwei Bitmapwechseln in Frames
BM_CHANGES = 3			; n Logo wechsel vor dem n�chsten part

SPCOLOR_11 = 5
SPCOLOR_12 = 0

SPCOLOR_21 = $04
SPCOLOR_22 = $00

SPCOLOR_31 = $0a
SPCOLOR_32 = $02

SPCOLOR_41 = $0a
SPCOLOR_42 = $00


FISCH1_SPR=$94
FISCH2_SPR=$A5
FISCH3_SPR=$9E
MUSCHEL_SPR=$C6
BLASEN_SPR=$E6


IRQLINE0 =$f8
IRQLINE05 =$ce
IRQLINE1 =$b0
IRQLINE2 =$60

tmp1 = v_ptr
	+spare 1
virtual_x_high = v_ptr
	+spare 16+ NUM_OF_BUBBLES
virtual_x = v_ptr
	+spare 16+ NUM_OF_BUBBLES
virtual_y = v_ptr
	+spare 16+ NUM_OF_BUBBLES
virtual_block  = v_ptr
	+spare 16+ NUM_OF_BUBBLES
virtual_color  = v_ptr
	+spare 16+ NUM_OF_BUBBLES
	
screen_start = v_ptr
	+spare 2
color_start  = v_ptr
	+spare 2
bitmap_start  = v_ptr
	+spare 2
 


	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}
	!macro align256 {
		* = (*+$ff) & $ff00
	}
	!macro align64 {
		* = (*+$3f) & $ff40
	}

	!macro align8 {
		* = (*+$7) & $fff8
	}


!ifdef RELEASE {
     *= $2020
     }

start
	
	jsr init_vars

	; jsr fadein
        
	lda #$0f
	ldx #$07
-	sta $d027,x
	dex
	bpl -


        ldx #$0
-        
        lda color,x
        sta $d800,x
        lda color+$100,x
        sta $d900,x
        lda color+$200,x
        sta $da00,x
        lda color+$300,x
        sta $db00,x
        dex
        bne -
        
	
	
	lda #$02    ; Vic bei $4000
	sta $dd00
	lda #$80    ; BM bei $4000, Screen bei $6000
	sta $d018
	lda #$ff
	sta $d015
        lda #$0f
        sta $d01c
        lda #$00
        sta $d017
        sta $d01d


	lda #$f8
	sta $d012
	lda #$3b
	sta $d011
	lda #$d8
	sta $d016
        lda #$00
        sta $d021

	sei 

	lda #<irq0
	sta $fffe
	lda #>irq0
	sta $ffff
	lda #$81
	sta $d01a
	sta $d019

	
  	cli
 
        jsr fill_white
	;jsr getkey
	jsr generate_noise

        
        ; jsr getkey
        
	 lda #$5c
	 sta loopcount
-        jsr copy_noise        
	 dec loopcount
	 bne -
	 inc bm_noise        

	!ifdef RELEASE {
        jsr LOAD_NEXT_PART
        ldx #$00
-       lda zwischenpart,x
        sta $0200,x
        inx
        bne -
        }
waitflag:
	lda #$00
	beq waitflag
	!ifdef RELEASE {
        jmp $0200
        } else {
l:	
	;inc $d020
	jmp l
	}
	!ifdef RELEASE {

zwischenpart:
	!pseudopc $0200 {

        sei
	lda #$00
	sta $d015
	sta $d01c
        sta $d012
	lda #$0b
	sta $d011
        lda #<fadeirq
        sta $fffe
        lda #>fadeirq
        sta $ffff
        cli
        jsr DECRUNCH_NEXT_PART
-       lda $ffff
        cmp #$fc
        bcc -        
	lda #$00
	sta $d020
	sta $d021
	
	lda #$03
	sta $dd00
	lda #$0b
	sta $d011
	
        jmp START_NEXT_PART


fadeirq:
        sta fadeirq_old_a+1
        stx fadeirq_old_x+1
        sty fadeirq_old_y+1
        lda $01
        sta fadeirq_old_01+1
        lda #$35
        sta $01
        inc $d019
        jsr PLAY_MUSIC
fadeout_counter:
        ldy #$11 
        lda fadecolor,y
        sta $d020
fadeout_slowdown:
        lda #$00
        clc
        adc #$01
        sta fadeout_slowdown+1
        and #$01
        bne fadeirq_old_01
        dec fadeout_counter+1
        bpl fadeirq_old_01
        jsr FRAMEWORK_IRQ
         
fadeirq_old_01:
        lda #$00
        sta $01
fadeirq_old_y:
        ldy #$00
fadeirq_old_x:
        ldx #$00
fadeirq_old_a:
        lda #$00
        rti        
        
fadecolor 
	 !byte $00, $66, $bb, $99, $22, $88, $aa, $aa, $ff, $dd, $77, $11
	 !byte $11, $77, $dd, $ff, $aa, $aa, $aa

        }
        
	}

loopcount !byte 0	

irq0:
	sta irq0_old_a+1
	stx irq0_old_x+1
	sty irq0_old_y+1
!ifdef RELEASE {
}else{
	inc $d020
}
        lda $d019
        sta $d019




        jsr animate_fish
	
	jsr animate_bubbles

        jsr bm_copy

	!for i,4 {
        lda virtual_x+(i-1)
	sta $d008 + 2*(i-1)
        lda virtual_y+(i-1)
	sta $d009 + 2*(i-1)        
        lda virtual_color+(i-1)
	sta $d02b +   (i-1)    
        lda virtual_block+(i-1)
        sta screen+$3fc+(i-1)
            
	}

	!for i,4 {
        lda virtual_x+(i-1)+4
	sta $d000 + 2*(i-1)
        lda virtual_y+(i-1)+4
	sta $d001 + 2*(i-1)        
        lda virtual_color+(i-1)+4
	sta $d027 +   (i-1)    
        lda virtual_block+(i-1)+4
        sta screen+$3f8+(i-1)
            
	}
  !ifdef HIGH_BIT_ENABLED {

        lda #$00

	!for i,4 {
        ldx virtual_x_high+(i-1)+4
        beq +
        ora #2^(i-1)        
+                
        }

	!for i,4 {
        ldx virtual_x_high+(i-1)
        beq +
        ora #2^((i-1)+4)        
+                
        }
            
	sta $d010

  }
      	lda #SPCOLOR_11
	sta $d025
      	lda #SPCOLOR_12
	sta $d026

        lda #$f0
        sta $d01c

!ifdef RELEASE {
	jsr PLAY_MUSIC
}
	lda #<irq2
	sta $fffe
	lda #>irq2
	sta $ffff
        lda #IRQLINE2
        sta $d012


!ifdef RELEASE {
}else{
	dec $d020
}
irq0_old_a:
	lda #$00
irq0_old_x:
	ldx #$00
irq0_old_y:
	ldy #$00
	rti


irq05:
	sta irq05_old_a+1
	stx irq05_old_x+1
	sty irq05_old_y+1
!ifdef RELEASE {
}else{
	inc $d020
}
        lda $d019
        sta $d019

        

	!for i,4 {
        lda virtual_x+(i+15)
	sta $d008 + 2*(i-1)
        lda virtual_y+(i+15)
	sta $d009 + 2*(i-1)        
        lda virtual_color+(i+15)
	sta $d02b +   (i-1)    
        lda virtual_block+(i+15)
        sta screen+$3fc+(i-1)
            
	}
  !ifdef HIGH_BIT_ENABLED {
        lda $d010
        and #$0f
        
	!for i,4 {
        ldx virtual_x_high+16+(i-1)
        beq +
        ora #2^((i-1)+4)
+                
        }
            
	sta $d010
        
	
  }

      	lda #SPCOLOR_31
	sta $d025
      	lda #SPCOLOR_32
	sta $d026

        lda #$c0
        sta $d01c

	lda #<irq0
	sta $fffe
	lda #>irq0
	sta $ffff
        lda #IRQLINE0
        sta $d012
!ifdef RELEASE {
}else{
	dec $d020
}
irq05_old_a:
	lda #$00
irq05_old_x:
	ldx #$00
irq05_old_y:
	ldy #$00
	rti





irq1:
	sta irq1_old_a+1
	stx irq1_old_x+1
	sty irq1_old_y+1
!ifdef RELEASE {
}else{
	inc $d020
}
        lda $d019
        sta $d019

        

	!for i,4 {
        lda virtual_x+(i+7)
	sta $d008 + 2*(i-1)
        lda virtual_y+(i+7)
	sta $d009 + 2*(i-1)        
        lda virtual_color+(i+7)
	sta $d02b +   (i-1)    
        lda virtual_block+(i+7)
        sta screen+$3fc+(i-1)
            
	}
  !ifdef HIGH_BIT_ENABLED {
        lda $d010
        and #$0f
        
	!for i,4 {
        ldx virtual_x_high+8+(i-1)
        beq +
        ora #2^((i-1)+4)        
+                
        }
            
	sta $d010
        
	
  }

      	lda #SPCOLOR_41
	sta $d025
      	lda #SPCOLOR_42
	sta $d026

	lda #<irq05
	sta $fffe
	lda #>irq05
	sta $ffff
        lda #IRQLINE05
        sta $d012
!ifdef RELEASE {
}else{
	dec $d020
}
irq1_old_a:
	lda #$00
irq1_old_x:
	ldx #$00
irq1_old_y:
	ldy #$00
	rti





irq2:
	sta irq2_old_a+1
	stx irq2_old_x+1
	sty irq2_old_y+1
!ifdef RELEASE {
}else{
	inc $d020
}
        lda $d019
        sta $d019

        

	!for i,4 {
        lda virtual_x+(i+11)
	sta $d008 + 2*(i-1)
        lda virtual_y+(i+11)
	sta $d009 + 2*(i-1)        
        lda virtual_color+(i+11)
	sta $d02b +   (i-1)    
        lda virtual_block+(i+11)
        sta screen+$3fc+(i-1)
            
	}
  !ifdef HIGH_BIT_ENABLED {
        lda $d010
        and #$0f
        
	!for i,4 {
        ldx virtual_x_high+12+(i-1)
        beq +
        ora #2^((i-1)+4)        
+                
        }
            
	sta $d010
        
	
  }
      	lda #SPCOLOR_21
	sta $d025
      	lda #SPCOLOR_22
	sta $d026

	lda #<irq1
	sta $fffe
	lda #>irq1
	sta $ffff
        lda #IRQLINE1
        sta $d012
!ifdef RELEASE {
}else{
	dec $d020
}
irq2_old_a:
	lda #$00
irq2_old_x:
	ldx #$00
irq2_old_y:
	ldy #$00
	rti




;-------------------------------
;
; GETRAND
;
; Generate a somewhat random repeating sequence.  I use
; a typical linear congruential algorithm
;      I(n+1) = (I(n)*a + c) mod m
; with m=65536, a=5, and c=13841 ($3611).  c was chosen
; to be a prime number near (1/2 - 1/6 sqrt(3))*m.
;
; Note that in general the higher bits are "more random"
; than the lower bits, so for instance in this program
; since only small integers (0..15, 0..39, etc.) are desired,
; they should be taken from the high byte RANDOM+1, which
; is returned in A.
;
GETRAND:
         LDA RANDOM+1     
         STA TEMP1        
         LDA RANDOM       
         ASL              
         ROL TEMP1        
         ASL              
         ROL TEMP1        
; ASL
; ROL TEMP1
; ASL
; ROL TEMP1
         CLC              
         ADC RANDOM       
         PHA              
         LDA TEMP1        
         ADC RANDOM+1     
         STA RANDOM+1     
         PLA              
         ADC #$11         
         STA RANDOM       
         LDA RANDOM+1     
         ADC #$36         
         STA RANDOM+1     
         RTS              	
	
; -------------------------------------------------------------------------------------------


	
init_vars:

		ldx #NUM_OF_BUBBLES-1
-		jsr GETRAND
		sta restart_bubble+4,x
		jsr GETRAND
		sta virtual_x+4,x
		jsr GETRAND
		and #$03
		sta speed_bubble+4,x
		lda #$ff
		sta virtual_y+4,x
		
		dex
		bpl -


                ldx #16+NUM_OF_BUBBLES
-        
                lda vblock_init,x
                sta virtual_block,x
                lda vx_init,x
                sta virtual_x,x
                lda vx_hi_init,x
       	        sta virtual_x_high,x
                lda vy_init,x
                sta virtual_y,x
                lda vcol_init,x
                sta virtual_color,x
 
                dex
                bpl -


		ldy #$10

-		lda pause_init,y
		sta fish_pause,y
		dey
		bpl -


       	        lda #1       ; start imediatly with changing
		sta bm_count
		lda #$0
		sta bm_show
		lda #BM_CHANGES
		sta bm_changes


                
		rts


; -------------------------------------------------------------------------------------------
fadeincolor 
	 !byte $ee, $ee, $ee, $ee, $22, $88, $aa, $aa, $ff, $dd, $77, $11
	 !byte $11, $77, $dd, $ff, $ee, $ee, $ee



fadein
	lda #$0b
	sta $d011
	ldy #$11
	
-	lda fadeincolor,y
	sta $d020
	sta $d021
	
	ldx #2
	lda #$e0
--	cmp $d012
	bne --
	dex 
	bne --
	
	dey
	bpl -
    
	rts

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Wartet dadrauf, dass die Spacetaste gedrueckt und wieder losgelassen
; wird

getkey

wx1	lda $dc01
	and #$10
	bne wx1
wx2	lda $dc01
	and #$10
	beq wx2

	rts


; -------------------------------------------------------------------------------------------

bittab 		!byte 0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80
eorbittab 	!byte 0xfe,0xfd,0xfb,0xf3,0xef,0xdf,0xbf,0x3f

; 4 mal Fisch oben, 4 mal Blasen, 4 mal Fisch unten , 4 mal Fisch mitte, 4mal Muscheln

vblock_init !byte FISCH1_SPR+2,FISCH1_SPR,FISCH1_SPR+4,FISCH1_SPR+6
	    !byte BLASEN_SPR,BLASEN_SPR+1,BLASEN_SPR+2,BLASEN_SPR+3
	    !byte FISCH3_SPR,FISCH3_SPR     ,FISCH3_SPR,FISCH3_SPR 
	    !byte FISCH2_SPR,FISCH2_SPR     ,FISCH2_SPR,FISCH3_SPR
	    !byte MUSCHEL_SPR+16,MUSCHEL_SPR+16,MUSCHEL_SPR,MUSCHEL_SPR  
vx_init     !byte $01,$01+$18,$0b,$0b+$13  ,$20,$60,$80,$e0  ,$77,$77+18 ,$59,$20  ,$77,$87    ,$80,$82    ,$99,$20,$99,$20 
vx_hi_init  !byte $0 ,$0     ,$0 ,$0       ,$00,$00,$00,$00  ,$01,$01    ,$00,$01  ,$01,$01    ,$01,$01     ,$00,$01,$00,$01
vy_init     !byte $44,$44    ,$38,$38      ,$00,$00,$00,$00  ,$b8,$b6    ,$dc,$e4  ,$85,$78    ,$98,$68     ,$e3,$e4,$e3,$e4
vcol_init   !byte $0d,$0d    ,$07,$07      ,$01,$0f,$0c,$01  ,$06,$06    ,$07,$07  ,$0a,$0a    ,$0a,$06     ,$00,$00,$07,$07
pause_init  !byte $01,$2a    ,$40,$0a      ,$40,$40,$40,$40  ,$40,$40    ,$40,$40  ,$10,$a0    ,$60,$c0     ,$40,$40,$40,$40
anim_start  !byte $00,$00    ,$00,$00      ,$00,$00,$00,$00  ,$00,$00    ,$00,$00  ,$30,$90    ,$a0,$00     ,$00,$00,$00,$00
anim_play   !byte $00,$00    ,$00,$00      ,$00,$00,$00,$00  ,$00,$00    ,$00,$00  ,$00,$00    ,$00,$00     ,$00,$00,$00,$00
anim_dir    !byte $00,$00    ,$00,$00      ,$00,$00,$00,$00  ,$00,$00    ,$00,$00  ,$00,$00    ,$00,$00     ,$00,$00,$00,$00
 

animate_bubbles:


	ldy #4
-	
	lda restart_bubble,y
	beq +
	tya
	tax
	dec restart_bubble,x
	bne l5a

+	
	
	tya
	tax
	
	dec virtual_y,x
	bne l5a
	
	jsr GETRAND
	cmp #$40
	bcs +
	sta virtual_x,y
	lda bittab,y
	ora virtual_x_high,y	
	sta virtual_x_high,y
	bne ++
+	
	sta virtual_x,y
	
	lda eorbittab,y
	and virtual_x_high,y	
	sta virtual_x_high,y
	
	
++
	jsr GETRAND
	and #$1f
	sta restart_bubble,y
	jsr GETRAND
	and #$03
	;sta speed_bubble,y
	clc
        adc #BLASEN_SPR
        sta virtual_block,x
	

l5a	

	iny
	cpy#4+NUM_OF_BUBBLES
	bne -

	rts
; -------------------------------------------------------------------------------------------

animate_fish

        
        inc virtual_block
        lda virtual_block
        cmp #FISCH1_SPR+1+8
        bne +
        lda #FISCH1_SPR+1
        sta virtual_block
 
+        

        inc virtual_block+1
        lda virtual_block+1
        cmp #FISCH1_SPR+1+8
        bne +
        lda #FISCH1_SPR+1
        sta virtual_block+1
 
+       
        inc virtual_block+2
        lda virtual_block+2
        cmp #FISCH1_SPR+1+8
        bne +
        lda #FISCH1_SPR+1
        sta virtual_block+2
 
+        

        inc virtual_block+3
        lda virtual_block+3
        cmp #FISCH1_SPR+1+8
        bne +
        lda #FISCH1_SPR+1
        sta virtual_block+3
 
+       
	dec animslow2
	bne +
	lda #$3
	sta animslow2
        inc virtual_block+8
        inc virtual_block+9
        inc virtual_block+15
        lda virtual_block+8
        cmp #FISCH3_SPR+7
        bne +
        lda #FISCH3_SPR
        sta virtual_block+8
        sta virtual_block+9
        sta virtual_block+15

+       
	dec animslow3
	bne +
	lda #$3
	sta animslow3
        inc virtual_block+12
        inc virtual_block+13
        inc virtual_block+14
        lda virtual_block+12
        cmp #FISCH2_SPR+14
        bne +
        lda #FISCH2_SPR
        sta virtual_block+12
        sta virtual_block+13
        sta virtual_block+14

 

+
	ldx #$0c
-	lda anim_play,x
	beq +
	stx animslow3
	dec anim_play,x
	cmp #$02
	bne +
	lda anim_dir,x
	bne +++
	inc virtual_block,x
	lda virtual_block,x
	cmp #FISCH2_SPR+26
	bne ++
	inc anim_dir,x
	bne ++

+++	dec virtual_block,x	
	lda virtual_block,x
	cmp #FISCH2_SPR+13
	bne ++
	dec anim_dir,x
        lda #$00
	!byte $2c
++	lda #$0a
	sta anim_play,x
+
	inx
	cpx #$0f
	bne -

	; don't animate last fisch
	lda #$00
	sta anim_play,x
	

        dec fish_pause
        bne fisch2
        inc fish_pause

	ldy #$0
        
        
-       tya
        tax 
        inc virtual_x,x
        lda virtual_x_high,y
        bne +
        lda #$00
        !byte $2c
+        
        lda #$6e
        cmp virtual_x,y
        bne ++
        lda #$00
        sta virtual_x,y
        lda virtual_x_high,y
        eor #$01
        sta virtual_x_high,y
        bne ++
        cpy #$01
        bne ++
	

        jsr GETRAND
        and #$3f
        ora #$10
        asl
        asl
        sta fish_pause

++                
        
        iny
        cpy #$04
        bne -


fisch2 

        dec fish_pause+1
        bne ++
        inc fish_pause+1
        
        lda virtual_x+8
	sec
	sbc #$01
        sta virtual_x+8
        lda virtual_x_high+8
        bne +
        ldx #$73
        !byte $2c
+        
        ldx #$ff
        lda #$ff
        cmp virtual_x+8
        bne ++
        txa
        sta virtual_x+8
        lda virtual_x_high+8
        eor #$01
        sta virtual_x_high+8
        beq ++

        jsr GETRAND
        and #$3f
	clc
	adc #$30
        asl
        asl
        sta fish_pause+1

++                

        dec fish_pause+2
        bne ++
        inc fish_pause+2
        
        lda virtual_x+9
	sec
	sbc #$02
        sta virtual_x+9
        lda virtual_x_high+9
        bne +
        ldx #$73
        !byte $2c
+        
        ldx #$ff
        lda #$ff
        cmp virtual_x+9
        bne ++
        txa
        sta virtual_x+9
        lda virtual_x_high+9
        eor #$01
        sta virtual_x_high+9
        beq ++

        jsr GETRAND
        and #$3f
	clc
	adc #$30
        asl
        asl
        sta fish_pause+2

++                

 
        
fisch3



       

        ldy #$0c

        
-       tya
        tax 
        dec fish_pause,x
        bne ++
        inc fish_pause,x

	lda anim_play,y
	bne ++

        dec virtual_x,x
        lda virtual_x_high,y
        bne +
        ldx #$74
        !byte $2c
+        
        ldx #$ff
        lda virtual_x,y
	cmp anim_start,y
	bne +++
	lda #$5
	sta anim_play,y
+++	cmp #$ff
        bne ++

        txa
        sta virtual_x,y
        lda virtual_x_high,y
        eor #$01
        sta virtual_x_high,y
        beq ++
        jsr GETRAND
        and #$3f
        ora #$10
        asl
        sta fish_pause,y
++                

 
        
        iny
        cpy #$10
        bne -

fisch4
	dec muschelshow1
	bne +
	lda #$01
	sta muschel1
+	
	dec muschelshow2
	bne +
	lda #$01
	sta muschel2
+	
	dec animslow
	lda animslow
	beq +
	rts
+
	lda #$05
	sta animslow

	lda muschel1
	beq +
	cmp #$01
	beq geh_auf1
	dec virtual_block+$10
	dec virtual_block+$12
	lda virtual_block+$12
	cmp #MUSCHEL_SPR
	bne +
	lda #$60
	sta muschelshow1
	lda #$00
	sta muschel1
	beq +
	
geh_auf1
	inc virtual_block+$10
	inc virtual_block+$12
	lda virtual_block+$12
	cmp #MUSCHEL_SPR+14
	bne +
	inc muschel1
+

	lda muschel2
	beq +
	cmp #$01
	beq geh_auf2
	dec virtual_block+$11
	dec virtual_block+$13
	lda virtual_block+$13
	cmp #MUSCHEL_SPR
	bne +
	lda #$70
	sta muschelshow2
	lda #$00
	sta muschel2
	beq +
	
geh_auf2
	inc virtual_block+$11
	inc virtual_block+$13
	lda virtual_block+$13
	cmp #MUSCHEL_SPR+14
	bne +
	inc muschel2
+

        rts

muschel1 !byte 0
muschel2 !byte 0
animslow !byte 5
animslow2 !byte 5
animslow3 !byte 5
muschelshow1 !byte 0
muschelshow2 !byte 0

generate_code:
	

!ifdef RELEASE {
 } else {
	+align256

init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #%00000000
        sta $dc0f
        lda #%10000000
        sta $dc0e
        lda #$82
        sta $dc0d
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dc04
        lda #>(63-1)
        sta $dc05
	; but not start it yet !

	+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	nop
	nop
	nop
        ldy #%10000001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
        sty $dc0e
	+check_same_page_end
	rts

stable_tab_irq:

	; table needed for stable interrupt

	+check_same_page_start
	!for i,$2a {
        !byte -2
        }
        !byte 11,11,9,8,7,6,5,4,3,2,1,0
        !for i,63-$2a-12 {
        !byte -2
	}
	+check_same_page_end

	+align8
double_bit_tab:
	!byte $00,$01,$02, $03,$04,$05,$06,$07

}

	+align256
vars:

restart_bubble = m_ptr
	+m_spare 4+NUM_OF_BUBBLES
speed_bubble = m_ptr
	+m_spare 4+NUM_OF_BUBBLES
RANDOM = m_ptr
	+m_spare 2		
TEMP1 = m_ptr
	+m_spare 1

fish_pause = m_ptr
	+m_spare 16

!byte 0
	+align256
end:

;*= EXTENDED_CODE

; -------------------------------------------------------------------------------------------

rowc !byte 0
charc !byte 0
generate_noise:

        lda #<noise
        sta n1+1
        lda #>noise
        sta n1+2
        
        lda #ROW_NUM+2
        sta rowc
        
--        
        lda #ROW_LEN_BM
        sta charc
-
        jsr GETRAND        
n1      sta noise
        inc n1+1
        bne +
        inc n1+2
+
        dec charc
        bne -
        dec rowc
        bne --
        
                

        rts
        

; -------------------------------------------------------------------------------------------

copy_noise:

		+waitblank
			

        lda #<BM_START
        sta n2a+1
        lda #>BM_START
        sta n2a+2
		
		jsr GETRAND        
		sta noiseadd+1
		
        lda #<noise
		clc
noiseadd adc #0		
        sta n1a+1
        lda #>noise
        sta n1a+2
	
		
        
        ldy #ROW_NUM
        
--        
        ldx #ROW_LEN_BM

-
n1a        lda noise
n2a     sta BM_START
        inc n1a+1
        bne +
        inc n1a+2
+
        inc n2a+1
        bne +
        inc n2a+2
+
        dex 
        bne -
        clc
        lda n2a+1
        adc #<ROW_MOD_BM
        sta n2a+1
        lda n2a+2
        adc #>ROW_MOD_BM
        sta n2a+2
        
        dey 
        bne --
        

        rts


bm_count !byte 0
bm_show  !byte 0
bm_noise !byte 0
bm_changes !byte 0

-	rts

bm_copy:
	lda bm_noise
	beq -

	lda bm_changes
	bne +
	lda #$01
	sta waitflag+1

+ 

	lda bm_count
	beq +++
	dec bm_count
	bne -
	lda bm_show
	bne +
	jsr copy_bm1
	inc bm_show
	bne ++
+	jsr copy_bm2
	lda #$0
	sta bm_show

++
	lda #<BM_START
        sta bitmap_start
        lda #>BM_START
        sta bitmap_start+1

		
	lda #<CL_START
        sta color_start
        lda #>CL_START
        sta color_start+1

        lda #<SC_START
        sta screen_start
        lda #>SC_START
        sta screen_start+1

		
		
        ldy #ROW_NUM
	sty rowc

	dec bm_changes

+++
	jmp copy_do
	

		
copy_bm1:

			

 
  	
        lda #<bitmap1
        sta m1a+1
        lda #>bitmap1
        sta m1a+2

        lda #<color1
        sta g2a+1
        lda #>color1
        sta g2a+2

		
	lda #<screen1
        sta f2a+1
        lda #>screen1
        sta f2a+2

        rts


copy_bm2:

			

 	
        lda #<bitmap2
        sta m1a+1
        lda #>bitmap2
        sta m1a+2

        lda #<color2
        sta g2a+1
        lda #>color2
        sta g2a+2

		
	lda #<screen2
        sta f2a+1
        lda #>screen2
        sta f2a+2

	rts



copy_do:		
        

				
        
     
		;+waitblank
		;jsr clear_bm
		
		;+waitblank
		;jsr clear_row
  
		;+waitblank
  
		jsr fill_bm

		jsr fill_row

 

		

		
        dec rowc
        bne +
	lda #BM_WAIT
	sta bm_count
+
       


		rts

!if (0) {

coltab_clear
	!byte $00,$00, $07		
_coltab_clear
	!byte $01

clear_row:


		
		
		ldx #_coltab_clear-coltab_clear
		

--
		+waitblank
		
		ldy #ROW_LEN-1

        lda coltab_clear,x
		lda #$0
-		sta (color_start),y
		sta (screen_start),y

        dey
        bpl -
		
		dex
		lda #$ff
		bpl --
		
		rts

}

fill_row:


		
	ldy #0

-
g2a     lda color1,y
	sta (color_start),y
f2a	lda screen1,y
	sta (screen_start),y

        iny
	cpy #ROW_LEN
        bne -

        lda g2a+1
	clc
	adc #ROW_LEN
        sta g2a+1
        bcc +
        inc g2a+2
+
        lda f2a+1
	clc
	adc #ROW_LEN
	sta f2a+1
        bcc +
        inc f2a+2
+

	clc
	lda color_start
        adc #40
        sta color_start
	bcc +
	inc color_start+1
+		
        clc
	lda screen_start
        adc #40
        sta screen_start
	bcc +
	inc screen_start+1
+		
	rts

!if (0) {
clear_bm:
        ldy #0

	lda #$ff
-
	sta (bitmap_start),y
        iny
	cpy #ROW_LEN_BM
        bne -
		
		
	rts
	  
}		
fill_bm:
        ldy #0

-
m1a	lda bitmap1,y
	sta (bitmap_start),y
        iny
	cpy #ROW_LEN_BM
        bne -
	lda m1a+1
	clc
	adc #ROW_LEN_BM
	sta m1a+1
	bcc +
	inc m1a+2
+

	clc
        lda bitmap_start
        adc #<320
        sta bitmap_start
        lda bitmap_start+1
        adc #>320
        sta bitmap_start+1	
	rts
	  
	  
	  
	  
; -------------------------------------------------------------------------------------------
fill_white:        


        lda #<CL_START
        sta n3a+1
        lda #>CL_START
        sta n3a+2
        

        lda #<SC_START
        sta n3b+1
        lda #>SC_START
        sta n3b+2

        lda #ROW_NUM
        sta rowc
        
--        
        lda #ROW_LEN
        sta charc

-
        lda #$01
n3a     sta CL_START
        lda #$0b
n3b     sta SC_START
        inc n3a+1
        bne +
        inc n3a+2
+
        inc n3b+1
        bne +
        inc n3b+2
+
        dec charc
        bne -
        clc
        lda n3a+1
        adc #<ROW_MOD
        sta n3a+1
        lda n3a+2
        adc #>ROW_MOD
        sta n3a+2
        lda n3b+1
        adc #<ROW_MOD
        sta n3b+1
        lda n3b+2
        adc #>ROW_MOD
        sta n3b+2
        
        dec rowc
        bne --
        
                

        rts



        rts
        

; -------------------------------------------------------------------------------------------
		



* =sprites


       !macro sprite_line .x {
	      
        !byte ^.x, >.x, <.x

        }

        +sprite_line %......########..........
        +sprite_line %....##........##........
        +sprite_line %...#............#.......
        +sprite_line %..#..........#...#......
        +sprite_line %.#..........#.#...#.....
        +sprite_line %.#...........#.....#....
        +sprite_line %#..................#....
        +sprite_line %#...................#...
        +sprite_line %#...................#...
        +sprite_line %#...................#...
        +sprite_line %.#.................#....
        +sprite_line %.#.................#....
        +sprite_line %..#...............#.....
        +sprite_line %...#.............#......
        +sprite_line %....##.........##.......
        +sprite_line %......#########.........
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................

	
        !byte 0

        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %.......#####............
        +sprite_line %......#.....#...........
        +sprite_line %.....#....#..#..........
        +sprite_line %....#.........#.........
        +sprite_line %....#.........#.........
        +sprite_line %....#.........#.........
        +sprite_line %....#.........#.........
        +sprite_line %.....#.......#..........
        +sprite_line %......#.....#...........
        +sprite_line %.......#####............
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................


        !byte 0


        +sprite_line %...####.................
        +sprite_line %..#....#................
        +sprite_line %.#......#...............
        +sprite_line %#........#..............
        +sprite_line %#........#..............
        +sprite_line %#........#..............
        +sprite_line %#........#..............
        +sprite_line %.#......#...............
        +sprite_line %..#....#................
        +sprite_line %...####.................
        +sprite_line %..........######........
        +sprite_line %........##......##......
        +sprite_line %.......#.......#..#.....
        +sprite_line %......#.......#.#..#....
        +sprite_line %.....#........#.#...#...
        +sprite_line %.....#.........#....#...
        +sprite_line %.....#..............#...
        +sprite_line %......#............#....
        +sprite_line %.......#..........#.....
        +sprite_line %........##......##......
        +sprite_line %..........######........


        !byte 0



        +sprite_line %........########........
        +sprite_line %.....####......####.....
        +sprite_line %....##.......#....##....
        +sprite_line %...#........#.#.....#...
        +sprite_line %..#........#...#.....#..
        +sprite_line %.#.........#...#......#.
        +sprite_line %.#..........#.#.......#.
        +sprite_line %##...........#........##
        +sprite_line %#......................#
        +sprite_line %#......................#
        +sprite_line %#......................#
        +sprite_line %#......................#
        +sprite_line %##....................##
        +sprite_line %.#....................#.
        +sprite_line %.#....................#.
        +sprite_line %..#..................#..
        +sprite_line %...#................#...
        +sprite_line %....##............##....
        +sprite_line %.....####......####.....
        +sprite_line %........########........
        +sprite_line %........................



       !byte 0

!if (OWN_SPRITES) {


        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %....###.................
        +sprite_line %...#...#................
        +sprite_line %...#....#...............
        +sprite_line %..#......#..............
        +sprite_line %##........#####.........
        +sprite_line %...............#........
        +sprite_line %...........##...#.......
        +sprite_line %.....#.....##....##.....
        +sprite_line %....#..............##...
        +sprite_line %...#...........######...
        +sprite_line %..#..............##.....
        +sprite_line %##..............#.......
        +sprite_line %............####........
        +sprite_line %.......#####............
        +sprite_line %#######.................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................


       !byte 0



        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %......####..............
        +sprite_line %.....#....#.............
        +sprite_line %.....#.....#..........##
        +sprite_line %......#.....#........#..
        +sprite_line %.......#.....#.....##...
        +sprite_line %........#.....#####.....
        +sprite_line %.........#..............
        +sprite_line %.........#.............#
        +sprite_line %.........#............#.
        +sprite_line %.........#...........#..
        +sprite_line %........#.............##
        +sprite_line %.......#.....###........
        +sprite_line %......#.....#...##......
        +sprite_line %.....#.....#......######
        +sprite_line %.....#....#.............
        +sprite_line %......####..............
        +sprite_line %........................


       !byte 0



        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %................########
        +sprite_line %............####........
        +sprite_line %.........###............
        +sprite_line %.......##.....#####.####
        +sprite_line %#######.........#...#.#.
        +sprite_line %.......##.......#...#..#
        +sprite_line %.........###............
        +sprite_line %............####........
        +sprite_line %................########
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................
        +sprite_line %........................


       !byte 0

        +sprite_line %..............####......
        +sprite_line %.............#...#......
        +sprite_line %............#....#......
        +sprite_line %...........#.....#......
        +sprite_line %..........#......#......
        +sprite_line %##########.......#......
        +sprite_line %.................#......
        +sprite_line %................#.......
        +sprite_line %..###..##.......#.......
        +sprite_line %..##...##.......#.......
        +sprite_line %.###...##.......#.......
        +sprite_line %.................#......
        +sprite_line %.................#......
        +sprite_line %##########.......#......
        +sprite_line %..........#......#......
        +sprite_line %...........#.....#......
        +sprite_line %............#....#......
        +sprite_line %.............#...#......
        +sprite_line %..............####......
        +sprite_line %........................
        +sprite_line %........................

      !byte 0



        +sprite_line %.......................#
        +sprite_line %.......................#
        +sprite_line %.....................###
        +sprite_line %.....................#.#
        +sprite_line %.....................#.#
        +sprite_line %.....................#.#
        +sprite_line %...........#############
        +sprite_line %.........###............
        +sprite_line %.......##...............
        +sprite_line %.#....#.................
        +sprite_line %.##..#..................
        +sprite_line %.#.#.#..................
        +sprite_line %.#..##...............###
        +sprite_line %..#..#...........#####..
        +sprite_line %.#..##..................
        +sprite_line %.#.#.#..................
        +sprite_line %.##...#....#############
        +sprite_line %.#.....#....############
        +sprite_line %........##..............
        +sprite_line %..........###...........
        +sprite_line %............############

       !byte 0


        +sprite_line %##......................
        +sprite_line %........................
        +sprite_line %#####...................
        +sprite_line %######..................
        +sprite_line %######..................
        +sprite_line %######..................
        +sprite_line %#################.......
        +sprite_line %.................###....
        +sprite_line %....................##..
        +sprite_line %......................#.
        +sprite_line %.......................#
        +sprite_line %.......................#
        +sprite_line %#######................#
        +sprite_line %.......................#
        +sprite_line %.................##....#
        +sprite_line %................#..#...#
        +sprite_line %##########......#..#..#.
        +sprite_line %########.........##..#..
        +sprite_line %...................##...
        +sprite_line %................###.....
        +sprite_line %#################.......

       !byte 0

}else{


    !bin "ubderwater-sprites.spr",$1800



}


*=bitmap0
koala_pic
 
 !bin "monitor_underwater.kla",$1f40,2

 *=screen
 !bin "monitor_underwater.kla",$3e8,$1f40+2

*=color
 !bin "monitor_underwater.kla",$3e8,$1f40+$3e8+2
 
 
*=bitmap1
 	!for i,ROW_NUM {
        !bin "logo1-inprogress8.koa",ROW_LEN_BM,2+(i-1)*320+(6*320+18*8)
	}
screen1	
 	!for i,ROW_NUM {
        !bin "logo1-inprogress8.koa",ROW_LEN,2+(i-1)*40+$1f40+(6*40+18)
	}
color1	
 	!for i,ROW_NUM {
         !bin "logo1-inprogress8.koa",ROW_LEN,2+(i-1)*40+$1f40+$3e8+(6*40+18)
	}

*=bitmap2
 	!for i,ROW_NUM {
        !bin "logo2.koa",ROW_LEN_BM,2+(i-1)*320+(6*320+18*8)
	}
screen2	
 	!for i,ROW_NUM {
        !bin "logo2.koa",ROW_LEN,2+(i-1)*40+$1f40+(6*40+18)
	}
color2	
 	!for i,ROW_NUM {
         !bin "logo2.koa",ROW_LEN,2+(i-1)*40+$1f40+$3e8+(6*40+18)
	}
 


 
