!cpu 6510

	!ifdef RELEASE {
        !src "../framework/framework.inc"
        !src "config.asm"
	!to "vertical.plain",plain
        * = PARTORG		;das kommt von der config.inc
	lda #>MUSIC_START
        ldy #>$d000
        ldx #>MUSIC_LEN
        jsr MEMCPY

        jmp start		;wenn noetig
        } else {
	!to "vertical.prg", cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
        dec $01
	jmp start
        }

;*= $0801
;!byte $0c,$08,$0a,$00,$9e,$32,$30,$36,$34,$00,$00,$00,$00

col0 = 0
col1 = 9
col2 = 8
col3 = 12

			*=$2900
start:			!ifdef RELEASE {
                        } else {
			sei
                        }
			lda #$35
			sta $01
;	                 lda #$0b
;        	         sta $d011
			jsr speedgen
                        !ifdef RELEASE {
-                       lda $ffff
                        cmp #$fc
                        bcc -                        
                        }                        
			lda #24
			sta $d018
			lda #$d7
			sta $d016

			ldx #$00
ll1			
			lda #39
			sta $0400,x
			sta $0500,x
			sta $0600,x
			sta $0700,x
			lda #$08+col0
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
			bne ll1

			ldx #$00
			lda #%11111111
ll2			sta $2138,x
			inx
			cpx #8
			bne ll2
			
			ldx #$00
barloop		lda bar,x
			sta bar+64,x
			sta bar+128,x
			sta bar+192,x
			inx
			cpx #64
			bne barloop

;			jsr genscreen

			lda #col0
			sta $d020
			lda #col3
			sta $d021
			lda #col2
			sta $d022
			lda #col1
			sta $d023

			!ifdef RELEASE {
                        sei
                        }
			lda #$01
			sta $d01a
			lda #<raster
			sta $fffe
			lda #>raster
			sta $ffff
			lda #$ff
			sta $d012
			lda $d011
			and #$7f
			sta $d011
			lda #$7f
			sta $dc0d
			lda $dc0d
			
                        !ifdef RELEASE {
                        } else {
			lda #$00
			jsr $1000
			}
			cli


	!ifdef RELEASE {
        jsr LOAD_NEXT_PART
	lda #$01
	sta loadflag
        ldx #$00
-       lda zwischenpart,x
        sta $0200,x
        inx
        bne -
        }
waitflag:lda #$00		; hier wird gewartet, ob der part zu Ende ist ...
	beq waitflag
	!ifdef RELEASE {
        jmp $0200
        } else {
	jmp waitflag	;fuer Test als einzelner Part
	}

	!ifdef RELEASE {
zwischenpart:
	!pseudopc $0200 {
	lda #$0b	;Bildschirm aus
	sta $d011
	lda #$60
        sta PLAY_MUSIC
        lda #$00
        sta $d418
        jsr credit_subpart_start
	lda #>$d000
        ldy #>$0800
        ldx #>MUSIC_LEN
        jsr MEMCPY
	jsr RESET_DRIVE
        ldx #$18       
        lda #$00
-	sta $d400,x
	dex
        bpl -
        tax
        tay
        jsr $0800
        lda #$4c
        sta PLAY_MUSIC                       
        
        ldy #$a6
        jsr COPY_PACKED
        jsr DECRUNCH_NEXT_PART
        jmp START_NEXT_PART
        }
        }
			

raster
			sta raster_old_a+1
			stx raster_old_x+1
			sty raster_old_y+1
                        
main
			lda #$1b
			sta $d011

;			lda #$ff
;			cmp $d012
;			bne *-3
			

			lda inval
			cmp #40
			beq no_fadein
			jsr fade_in
no_fadein
			lda fadeflag
			cmp #1
			bne no_fadeout
			lda outval
			cmp #24
			bne fader_out
			
                 lda #$01
                 sta waitflag+1
                 !ifdef RELEASE {
	         jsr FRAMEWORK_IRQ
                 }
fader_out		jsr fade_out

no_fadeout
                 !ifdef RELEASE {
                 	SND_FADEOUT_TIME = $3e72-16*8 
                 	lda $fff9
                        ldx $fff8
                        cpx #<SND_FADEOUT_TIME
                        sbc #>SND_FADEOUT_TIME
                        bcc +
sound_fade_slowdown:	lda #$00
			clc
                        adc #$01
                        sta sound_fade_slowdown+1
                        and #$07
                        bne +                
                 	lda $10ac
                        beq +
                        dec $10ac
+			;jsr $0806
                 }
			jsr CODEGEN_START	; <- speedcode
			

;			dec $d020
        		!ifdef RELEASE {
        		jsr PLAY_MUSIC

			lda loadflag
			cmp #$01
                        bne end_not_reached
                        lda #$01
                        sta fadeflag
end_not_reached

        		} else {
			jsr $1003
                        }
;			inc $d020

			inc $d019
raster_old_a:	lda #$00
raster_old_x:	ldx #$00
raster_old_y:	ldy #$00
			rti
			
;			jmp main
;--------------------------------------------------
fadeflag	!byte $00
loadflag	!byte $00
;--------------------------------------------------
speedgen

			ldy #$00
speedloop0x	ldx #$00
speedloop1x	lda block2_start,x
			jsr writebyte
			inx
			cpx #block2_end-block2_start
			bne speedloop1x

			jsr modder1

			iny
			cpy #39
			bne speedloop0x



			ldx #$00
sloop0		lda movebar3,x
			jsr writebyte
			inx
			cpx #movebar3_end-movebar3
			bne sloop0


			ldy #$00
speedloop0		ldx #$00
speedloop1		lda block1_start,x
			jsr writebyte
			inx
			cpx #block1_end-block1_start
			bne speedloop1

			inc count1+1
			inc count2+1
			inc count3+1
			inc count4+1
			inc count5+1
			inc count6+1
			inc count7+1
			inc count8+1
			inc count9+1
			inc count10+1

			iny
			cpy #160-7
			bne speedloop0
			
			lda #$60
			jsr writebyte
			
			rts
			
writebyte		sta CODEGEN_START
			inc writebyte+1
			bne nohiwrite
			inc writebyte+2
nohiwrite
			rts
;--------------------------------------------------
modder1
			lda count11+1
			clc
			adc #4
			sta count11+1
			
			lda count12+1
			clc
			adc #4
			sta count12+1

			lda count13+1
			clc
			adc #4
			sta count13+1

			lda count14+1
			clc
			adc #4
			sta count14+1

			lda count11a+1
			clc
			adc #4
			sta count11a+1
			
			lda count12a+1
			clc
			adc #4
			sta count12a+1

			lda count13a+1
			clc
			adc #4
			sta count13a+1

			lda count14a+1
			clc
			adc #4
			sta count14a+1

			lda count15+1
			clc
			adc #8
			sta count15+1
			lda count15+2
			adc #0
			sta count15+2

			lda count16+1
			clc
			adc #8
			sta count16+1
			lda count16+2
			adc #0
			sta count16+2

			lda count17+1
			clc
			adc #8
			sta count17+1
			lda count17+2
			adc #0
			sta count17+2

			lda count18+1
			clc
			adc #8
			sta count18+1
			lda count18+2
			adc #0
			sta count18+2

			lda count19+1
			clc
			adc #8
			sta count19+1
			lda count19+2
			adc #0
			sta count19+2

			lda count20+1
			clc
			adc #8
			sta count20+1
			lda count20+2
			adc #0
			sta count20+2

			lda count21+1
			clc
			adc #8
			sta count21+1
			lda count21+2
			adc #0
			sta count21+2

			lda count22+1
			clc
			adc #8
			sta count22+1
			lda count22+2
			adc #0
			sta count22+2


			rts
;--------------------------------------------------
fade_in		ldx inval
			txa
			sta $0400+(0*40),x
			sta $0400+(1*40),x
			sta $0400+(2*40),x
			sta $0400+(3*40),x
			sta $0400+(4*40),x
			sta $0400+(5*40),x
			sta $0400+(6*40),x
			sta $0400+(7*40),x
			sta $0400+(8*40),x
			sta $0400+(9*40),x
			sta $0400+(10*40),x
			sta $0400+(11*40),x
			sta $0400+(12*40),x
			sta $0400+(13*40),x
			sta $0400+(14*40),x
			sta $0400+(15*40),x
			sta $0400+(16*40),x
			sta $0400+(17*40),x
			sta $0400+(18*40),x
			sta $0400+(19*40),x
			sta $0400+(20*40),x
			sta $0400+(21*40),x
			sta $0400+(22*40),x
			sta $0400+(23*40),x
			sta $0400+(24*40),x

			inc inval2
			lda inval2
			and #1
			bne no_inval
			inc inval
no_inval
			rts

inval		!byte 0
inval2		!byte 0
;--------------------------------------------------
fade_out		lda #39
			ldx #$00
fadr			sta $0400,x
			inx
			cpx #40
			bne fadr
			inc outval

			lda fadr+1
			clc
			adc #40
			sta fadr+1
			lda fadr+2
			adc #0
			sta fadr+2
			
			rts
			
outval		!byte $00
;--------------------------------------------------
movebar3
			inc $b0
			ldx $b0
			lda sine,x
			clc
			adc $b0
;			adc $b0
                        tay
			clc
movebar3_end			
;--------------------------------------------------
;--------------------------------------------------
block1_start
		tya
count1		adc table1
			tax
			lda bar,x

count2		sta $02
		tya
count3		adc table2
			tax
			lda bar,x
			beq *+4
count4		sta $02
		tya	
count5		adc table3
			tax
			lda bar,x
			beq *+4
count6		sta $02
		tya
count7		adc table4
			tax
			lda bar,x
			beq *+4
count8		sta $02
		tya
count9		adc table5
			tax
			lda bar,x
			beq *+4
count10		sta $02
block1_end
;--------------------------------------------------
;--------------------------------------------------

;--------------------------------------------------
;--------------------------------------------------
block2_start
count11		ldx $02
			lda tab4,x
count12		ldx $03
			ora tab3,x
count13		ldx $04
			ora tab2,x
count14		ldx $05
			ora tab1,x
count15		sta $2000
count16		sta $2002
count17		sta $2004
count18		sta $2006
count11a		ldx $03
			lda tab4,x
count12a		ldx $04
			ora tab3,x
count13a		ldx $05
			ora tab2,x
count14a		ldx $06
			ora tab1,x
count19		sta $2001
count20		sta $2003
count21		sta $2005
count22		sta $2007

block2_end
;--------------------------------------------------

;--------------------------------------------------
;--------------------------------------------------

tab1			!byte %00000011
			!byte %00000010
			!byte %00000001
			!byte %00000000

tab2			!byte %00001100
			!byte %00001000
			!byte %00000100
			!byte %00000000

tab3			!byte %00110000
			!byte %00100000
			!byte %00010000
			!byte %00000000

tab4			!byte %11000000
			!byte %10000000
			!byte %01000000
			!byte %00000000

INCLUDED = 1

!src "../credits_subpart/credits_subpart.asm"

	* = (* + $ff) & $ff00
MUSIC_START:
CODEGEN_START:
        !bin "krawall_part3.sid",,2
MUSIC_LEN = ((* + $ff) & $ff00) - MUSIC_START

;--------------------------------------------------
!ifdef RELEASE {
} else {
*=$1000
!bin "Music.sid",,126
}
;--------------------------------------------------
*=$2200
bar
!byte $00,$00,$01,$01,$02,$02,$03,$03,$03,$03,$03,$03,$02,$02,$01,$01
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

;--------------------------------------------------
*=$2300
table1
!bin "x2.bin",,2

*=$2400
table2
!bin "x1.8.bin",,2

*=$2500
table3
!bin "x1.6.bin",,2

*=$2600
table4
!bin "x1.4.bin",,2

*=$2700
table5
!bin "x1.2.bin",,2
;--------------------------------------------------
*=$2800
sine

!byte $00,$00,$00,$00,$00,$00,$01,$01,$02,$03,$03,$04,$05,$06,$07,$08
!byte $09,$0b,$0c,$0d,$0f,$10,$12,$13,$15,$17,$19,$1b,$1d,$1f,$21,$23
!byte $25,$27,$2a,$2c,$2e,$31,$33,$36,$39,$3b,$3e,$41,$43,$46,$49,$4c
!byte $4f,$52,$55,$58,$5b,$5e,$61,$64,$67,$6a,$6d,$70,$73,$77,$7a,$7d
!byte $80,$83,$86,$89,$8d,$90,$93,$96,$99,$9c,$9f,$a2,$a5,$a8,$ab,$ae
!byte $b1,$b4,$b7,$ba,$bc,$bf,$c2,$c5,$c7,$ca,$cc,$cf,$d1,$d4,$d6,$d8
!byte $da,$dd,$df,$e1,$e3,$e5,$e7,$e9,$ea,$ec,$ee,$ef,$f1,$f2,$f4,$f5
!byte $f6,$f7,$f8,$f9,$fa,$fb,$fc,$fd,$fd,$fe,$fe,$ff,$ff,$ff,$ff,$ff
!byte $ff,$ff,$ff,$ff,$ff,$ff,$fe,$fe,$fd,$fc,$fc,$fb,$fa,$f9,$f8,$f7
!byte $f6,$f4,$f3,$f2,$f0,$ef,$ed,$ec,$ea,$e8,$e6,$e4,$e2,$e0,$de,$dc
!byte $da,$d8,$d5,$d3,$d1,$ce,$cc,$c9,$c6,$c4,$c1,$be,$bc,$b9,$b6,$b3
!byte $b0,$ad,$aa,$a7,$a4,$a1,$9e,$9b,$98,$95,$92,$8f,$8c,$88,$85,$82
!byte $7f,$7c,$79,$76,$72,$6f,$6c,$69,$66,$63,$60,$5d,$5a,$57,$54,$51
!byte $4e,$4b,$48,$45,$43,$40,$3d,$3a,$38,$35,$33,$30,$2e,$2b,$29,$27
!byte $25,$22,$20,$1e,$1c,$1a,$18,$16,$15,$13,$11,$10,$0e,$0d,$0b,$0a
!byte $09,$08,$07,$06,$05,$04,$03,$02,$02,$01,$01,$00,$00,$00,$00,$00
;--------------------------------------------------

