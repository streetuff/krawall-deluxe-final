	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}

	ADDR_OF_CODE = $b000
        
copy_code_and_init:
	ldx #$00
-	lda code_start,x
	sta ADDR_OF_CODE,x
        inx
        bne -
-	lda code_start+$100,x
	sta ADDR_OF_CODE+$100,x
        inx        
        cpx #code_len-$100
        bne -
        
	sei
        !ifdef RELEASE {
        } else {
	jsr init_stable_timer
        }
	lda #$35
	sta $01
        lda #$0b
        sta $d011
        lda #$30
        sta $d012
        lda #$81
        sta $d019
        sta $d01a
        lda #<irq
        sta $fffe
        lda #>irq
        sta $ffff
        cli
        rts        


code_start:
	!pseudopc ADDR_OF_CODE {

stable_tab_irq:

	; table needed for stable interrupt

	+check_same_page_start
	!for i,$28-8 {
        !byte 0
        }
        !byte 11,10,9,8,7,6,5,4,3,2,1,0
        !for i,63-$28-12+8 {
        !byte 0
	}


irq:    sta irq_old_a+1
        stx irq_old_x+1
        lda $01
        pha
        lda #$35
        sta $01
        ldx $dc04
	lda stable_tab_irq,x
	sta bpl_addr2+1
bpl_addr2:
	bpl bpl_addr2

	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        ;stable here
        lda $d012
        clc
inc_value:
        adc #$01
        sta $d012
        sty irq_old_y+1
scroll_pix:
	lda #$00
        sta bpl_addr+1
bpl_addr:
	bpl *        
	!for i, 31 {
	cmp #$c9
        }
	bit $ea
        
        ldy #$00
	ldx #$40
c:
        lda #$06
-
c0:	bit $d020
        lda #$04
c1:        
        bit $d020
        lda #$0e
c2:
        bit $d020
        lda #$03
c3:
        bit $d020
        lda #$01
c4:
        bit $d020
        lda #$03
c5:
        bit $d020
        lda #$0e
c6:
        bit $d020
        lda #$04
c7:
        bit $d020
        lda #$02
c8:     bit $d020
	lda #$06
	dex
c9:
	!byte $0c
        !word $d020	;sty !!!
	nop
        bne -
        ldx scroll_pix+1
        inx
        cpx #63
        bne +
        ldx #$00
+       stx scroll_pix+1
                
	+check_same_page_end

	lda $d011
	bpl +
        lda #$fe
        sta inc_value+1
+	lda $d012
	cmp #$51+$20
        bne +
        lda #$02
        sta inc_value+1
+       

pointer:lda #$00
	;lsr
        ;lsr
        lsr
        lsr
        tax
	lda codetab,x
        bmi ++
        beq +
        tay
        lda #$8d
        sta c,y
        cpy #c8-c
        bne +
        lda #$8c
        sta c9
+       inc pointer+1
++      

	lda stable_tab_irq
        beq ++

pointer2:lda #$00
	lsr
        lsr
        tax
	lda codetab2,x
        bmi +
        tay
        lda #$0c
        sta c,y
        cpy #c8-c
        bne +++
        sta c9
+++        
        inc pointer2+1
	lda #$00
        !byte $0c
+       lda #$01
	sta stable_tab_irq+1
++
                           
	!ifdef RELEASE {
	jsr PLAY_MUSIC 
	} else {
	jsr $1003
	}
	inc $d019
        pla
	sta $01
        
irq_old_y:
	ldy #$00
irq_old_a:
	lda #$00
irq_old_x:
	ldx #$00
rti_addr:
        rti


codetab2:
	!byte c0-c
	!byte c1-c
	!byte c2-c
	!byte c3-c
	!byte c4-c
	!byte c5-c
	!byte c6-c
	!byte c7-c
	!byte c8-c
        !byte $ff
        
codetab:
	!byte 0
        !byte 0
        !byte 0
        !byte c8-c
	!byte c7-c
	!byte c6-c
	!byte c5-c
	!byte c4-c
	!byte c3-c
	!byte c2-c
	!byte c1-c
	!byte c0-c
	!byte $ff        
	}

code_len = *- code_start
        
