	;free space $e000-$faff
        !cpu 6510

	!ifdef RELEASE {
        !src "config.asm"
        !src "../framework/framework.inc"
	!to "monkey.plain",plain
	OFF = $f000-$0400
	* = PARTORG
        } else {
        !to "monkey.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
	jmp start
	OFF = 0
	}

TIME_DEACTIVE = $20a0	;23b0

;--------------------------------------------------
xplasma	=$40
yplasma	=$68

temp1	=$e1
temp2	=$e2
temp3	=$e3
temp4	=$e4
ytemp	=$e5

xcnt		=$e6
ycnt		=$e7

xcnt01	=$e8
xcnt02	=$e9
;xcnt1	=$ea
;xcnt2	=$eb

ycnt01	=$ec
ycnt02	=$ed
;ycnt1	=$ee
;ycnt2	=$ef

sinetab2       =sinetab1 + $100
sinetab3   =sinetab2 + $100
colortab       =sinetab3 + $100
colortab2      =colortab + $100
code_ende      =colortab2 + $100
;--------------------------------------------------
	!ifdef RELEASE {
	} else {
*= $0810
!bin "Cauldron_II_Remix.sid",,126+$10
}
;--------------------------------------------------

			!ifdef RELEASE {
			} else {
			*=$2000
			}
start_addr:
			jmp start
!bin "monkey4col.kla",8000,5

	!ifdef RELEASE {
        	* = (* + $ff) & $ff00
	} else {
music_firstbytes:
!bin "Cauldron_II_Remix.sid",$10,126
	}
start:
			!ifdef RELEASE {
			lda #$7b
			sta $d011
			jsr FRAMEWORK_IRQ
			} else {
			sei
			}
			!byte $a9
			!bin "monkey4col.kla",1,2
			sta start_addr
			!byte $a9
			!bin "monkey4col.kla",1,3
			sta start_addr+1
			!byte $a9
			!bin "monkey4col.kla",1,4
			sta start_addr+2
	!ifdef RELEASE {
	} else {
			ldx #$0f
-			lda music_firstbytes,x
			sta $0800,x
			dex
			bpl -
	}
			
			lda #$00
			sta $d020
			sta $d021
			sta temp2

			ldx #$00
loop1		
			lda #$00
			sta $0400+OFF,x
			sta $0500+OFF,x
			sta $0600+OFF,x
			sta $0700+OFF,x
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			sta colortab,x
			sta colortab2,x
			inx
			bne loop1
			!ifdef RELEASE {
			;syncronize with screen
			lda $fff8
-			cmp $fff8
			beq -
			}

			lda #$3b
			sta $d011
			
			lda #$d8
			sta $d016

			!ifdef RELEASE {
			lda #$00
			sta $dd00
			lda #$c0
			} else {
			lda #27
			}
			sta $d018

			ldx #$00
loop1x	 	lda sinetab1,x
			lsr
			sta sinetab2,x
			lsr
			sta sinetab3,x
			inx
			bne loop1x
			
			lda #$35
			sta $01

			sei
			lda #$01
			sta $d01a
			lda #<raster
			sta $fffe
			lda #>raster
			sta $ffff
			lda #$ff
			sta $d012
			lda $d011
			and #$7f
			sta $d011
			lda #$7f
			sta $dc0d
			lda $dc0d
			
		!ifdef RELEASE {
		} else {
			lda #$00
			jsr $0800
		}
			cli

		!ifdef RELEASE {
			jsr LOAD_NEXT_PART
-			lda $fff9
			ldx $fff8
			cpx #<TIME_DEACTIVE
			sbc #>TIME_DEACTIVE
                        bcc -                        
		} else {
			
			lda #239		; <---------- LOADER HERE!
			cmp $dc01
			bne *-3
		}	
			lda #$00
			sta temp2
		!ifdef RELEASE {
			inc check_temp2+1
		}
			tax

clear		sta colors1,x
			sta colors2,x
			inx
			cpx #16
			bne clear

			ldx #loader_code_len-1
-			lda loader_code,x
			sta $0200,x
			dex
			bpl -
			jmp $0200

loader_code:
			!pseudopc $0200 {
			!ifdef RELEASE {
                        ;ldy #>$b800
                        ;jsr COPY_PACKED
			jsr DECRUNCH_NEXT_PART
			} else {
			lda temp2
			cmp #$ff
			bne *-4
			}
			!ifdef RELEASE {
			;lda #$7b
			;sta $d011
			;jsr FRAMEWORK_IRQ
			jmp START_NEXT_PART
			} else {			
			dec $d020
			jmp *-3
			}
			}
loader_code_len = * - loader_code
;--------------------------------------------------
raster
			sta raster_old_a+1
			stx raster_old_x+1
			sty raster_old_y+1
			!ifdef RELEASE {
			lda $01
			sta raster_old_01+1
			lda #$35
			sta $01
			}
main
;			dec $d020
		!ifdef RELEASE {
			jsr PLAY_MUSIC 
		} else {
			jsr $0803
		}
			jsr drawplasma
;			inc $d020

			inc $d019
			!ifdef RELEASE {
raster_old_01
			lda #$00
			sta $01
			}
raster_old_a:	lda #$00
raster_old_x:	ldx #$00
raster_old_y:	ldy #$00
			rti



;--------------------------------------------------
drawplasma
		
		inc interlace
		lda interlace
		and #1
		beq nodraw2
		jsr draw2
		rts

nodraw2
		lda xcnt01
		sta xcnt1
		lda xcnt02
		sta xcnt2

;--------------------------------------------------
 			
		ldy #$00
                clc
loop4		
xcnt1 = *+1
		lda sinetab1

xcnt2 = *+1
		adc sinetab2
		sta xplasma,y
		
		lax xcnt1
		sbx #$100-$04 ; adc #$04  ;xadd1 
		stx xcnt1
		
		lax xcnt2
		sbx #$100-$f6; adc #$f6 ; xadd2 
		stx xcnt2
		
		iny
		cpy #40 ;xsize
		bne loop4 ; carry also cleared
		
;--------------------------------------------------

		lda ycnt01
		sta ycnt1
		lda ycnt02
		sta ycnt2

;--------------------------------------------------
	
		ldy #$00
		clc
loop5		
ycnt1 = *+1
		lda sinetab1
ycnt2 = *+1
		adc sinetab3
		sta yplasma,y

		;lda ycnt1
		;clc
		;adc #$01 ; yadd1 
		;sta ycnt1
                inc ycnt1
		
		lax ycnt2
		sbx #$100-$f3;  #$f3  ;yadd2 
		stx ycnt2
		
		iny
		cpy #25 ; ysize
		bne loop5 ; carry also cleared

;--------------------------------------------------

		;lda xcnt01
		;clc
		;adc #$ff ; xadd01 
		;sta xcnt01
                dec xcnt01

		lda xcnt02
		clc
		adc xadd02 
		sta xcnt02

		lax ycnt01
		;clc
		sbx #$100-$05 ; adc #$05 ; yadd01 
		stx ycnt01

		lda ycnt02
		clc
		adc yadd02
		sta ycnt02
		
;--------------------------------------------------
		
		ldx ycnt
		lda sinetab3,x
		lsr
		lsr
		sta yadd02
		
		ldx xcnt
		lda sinetab3,x
		lsr
		lsr
		sta xadd02
		
;--------------------------------------------------
		
		lax ycnt
		;clc
		sbx #$100-$fc ;adc #$fe ; yadd
		stx ycnt
		
		lax xcnt
		;clc
		sbx #$100-$03 ;adc #$03 ; xadd
		stx xcnt

;--------------------------------------------------
again
		ldx #$00
mloop2	
;--------------------------------------------------
!for i, 11 {
		lda yplasma+(i-1)
		adc xplasma,x
		
		tay
		lda colortab,y
		sta $0400+((i-1)*40)+OFF,x
		lda colortab2,y
		sta $d800+((i-1)*40),x
		}
		
;--------------------------------------------------

		inx
		cpx #40 ; xsize
		beq endz
		jmp mloop2
;--------------------------------------------------
endz

		jsr fader
		jsr fader
fader	
		lax temp2
		lsr
		lsr
		lsr
		lsr
		;and #$0f
		tay
		lda colors1,y
		sta colortab,x
		lda colors2,y
		sta colortab2,x
		inc temp2
		!ifdef RELEASE {
		bne +
check_temp2:	lda #$00
		bne ++
+               rts

++		lda #$7b
		sta $d011
		jmp copy_code_and_init

		} else {
		rts
		}
;--------------------------------------------------
draw2
		ldx #$00
mloop3

!for i, 14 {
		lda yplasma+(i+10)
		adc xplasma,x
		
		tay
		lda colortab,y
		sta $0400+((i+10)*40)+OFF,x
		lda colortab2,y
		sta $d800+((i+10)*40),x
		}
		inx
		cpx #40 ; xsize
		beq endz2
		jmp mloop3
endz2
		rts
;--------------------------------------------------
colors1
;		!byte $6e,$6e,$6e,$6e
;		!byte $6e,$6e,$6e,$6e
;		!byte $2a,$2a,$2a,$2a
;		!byte $2a,$2a,$2a,$2a

		!byte $00,$00,$06,$06
		!byte $6e,$6e,$6e,$6e
		!byte $6e,$6e,$6e,$6e
		!byte $06,$06,$00,$00


;		!byte $2a,$2a,$2a,$2a
;		!byte $2a,$2a,$2a,$2a
;		!byte $02,$02,$00,$00
;		!byte $00,$00,$02,$02

colors2
;		!byte $0f,$0f,$0f,$0f
;		!byte $0f,$0f,$0f,$0f
;		!byte $07,$07,$07,$07
;		!byte $07,$07,$07,$07

		!byte $06,$06,$0e,$0e
		!byte $0f,$0f,$0f,$0f
		!byte $0f,$0f,$0f,$0f
		!byte $0e,$0e,$06,$06


;		!byte $0f,$0f,$0f,$0f
;		!byte $0f,$0f,$0f,$0f
;		!byte $0a,$0a,$02,$00
;		!byte $00,$02,$0a,$0a

;xsize	!byte 40
;ysize	!byte 25


;xadd01	!byte $ff ; $fe
;yadd01	!byte $05 ; $05
xadd02	!byte $02 ; $01
yadd02	!byte $fc ; $fb

;xadd1	!byte $04 ; $04
;yadd1	!byte $01 ; $01
;xadd2	!byte $f6 ; $f6
;yadd2	!byte $f3 ; $f3

;xadd		!byte $03 ; $03
;yadd		!byte $fe ; $fe

interlace	!byte $00
;--------------------------------------------------
;sinetab1
;--------------------------------------------------
!ifdef RELEASE {
!src "vector_subpart.asm"

* = (*+$ff) & $ff00
} else {
*=$4500
}
sinetab1:
	
;--------------------------------------------------

!byte $00,$00,$00,$00,$00,$00,$01,$01,$02,$03,$03,$04,$05,$06,$07,$08
!byte $09,$0b,$0c,$0d,$0f,$10,$12,$13,$15,$17,$19,$1b,$1d,$1f,$21,$23
!byte $25,$27,$2a,$2c,$2e,$31,$33,$36,$39,$3b,$3e,$41,$43,$46,$49,$4c
!byte $4f,$52,$55,$58,$5b,$5e,$61,$64,$67,$6a,$6d,$70,$73,$77,$7a,$7d
!byte $80,$83,$86,$89,$8d,$90,$93,$96,$99,$9c,$9f,$a2,$a5,$a8,$ab,$ae
!byte $b1,$b4,$b7,$ba,$bc,$bf,$c2,$c5,$c7,$ca,$cc,$cf,$d1,$d4,$d6,$d8
!byte $da,$dd,$df,$e1,$e3,$e5,$e7,$e9,$ea,$ec,$ee,$ef,$f1,$f2,$f4,$f5
!byte $f6,$f7,$f8,$f9,$fa,$fb,$fc,$fd,$fd,$fe,$fe,$ff,$ff,$ff,$ff,$ff
!byte $ff,$ff,$ff,$ff,$ff,$ff,$fe,$fe,$fd,$fc,$fc,$fb,$fa,$f9,$f8,$f7
!byte $f6,$f4,$f3,$f2,$f0,$ef,$ed,$ec,$ea,$e8,$e6,$e4,$e2,$e0,$de,$dc
!byte $da,$d8,$d5,$d3,$d1,$ce,$cc,$c9,$c6,$c4,$c1,$be,$bc,$b9,$b6,$b3
!byte $b0,$ad,$aa,$a7,$a4,$a1,$9e,$9b,$98,$95,$92,$8f,$8c,$88,$85,$82
!byte $7f,$7c,$79,$76,$72,$6f,$6c,$69,$66,$63,$60,$5d,$5a,$57,$54,$51
!byte $4e,$4b,$48,$45,$43,$40,$3d,$3a,$38,$35,$33,$30,$2e,$2b,$29,$27
!byte $25,$22,$20,$1e,$1c,$1a,$18,$16,$15,$13,$11,$10,$0e,$0d,$0b,$0a
!byte $09,$08,$07,$06,$05,$04,$03,$02,$02,$01,$01,$00,$00,$00,$00,$00
;--------------------------------------------------

