#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main()
{
	FILE *f;
	double r, old_r;
	int i;
        unsigned char difftab[256];
        for(i=0; i <= 256; ++i)
        {
        	r = (double)i;
                r = (r-128.0);
                r = r*(fabs(r))/160.0;
                
                r = ((double)0x32+100.0-20.5) - r;
        	printf("%d %lf\n", i, r);
                if(i>0)
                {
                	difftab[i-1] = (int)(r+.5) - old_r;
                }
                old_r = (double) (int)(r+.5);
        }
        f = fopen("movedifftab.bin", "wb");
        fwrite(difftab, 1, sizeof(difftab), f);
        fclose(f);
        return 0; 
}        
	
