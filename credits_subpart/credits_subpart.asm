	TIME = 480


DST_CODE = $0300

y_coords = $40
y_coords_high = $48
y_coords_high_copy = $50
counter_low = $51
counter_high = $53


	!cpu 6510
        
	!ifdef INCLUDED {
	} else {
        !to "credits_subpart.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
        sei
        lda #$35
        sta $01
	jsr credit_subpart_start
-	jmp -        
        }
credit_subpart_start:
-	lda $d011
	bmi -
-       lda $d012
        bne -
	lda #$7b
        sta $d011
        lda #$15
        sta $d018
        lda #$03
        sta $dd00
        ldx #$00
        stx $d020
        ldy #codepages
src_ptr:lda part,x
dst_ptr:sta DST_CODE,x
	inx
        bne src_ptr
        inc src_ptr+2
        inc dst_ptr+2
        dey
        bne src_ptr
        lda #$00
        sta $d015
        lda #$00
        sta $d01b
        sta $d01c
        sta $d01d
        sta $d017
        ldx #$10
-       lda d000tab,x
        sta $d000,x
        dex
        bpl -
        ldx #$07
-       lda #$0f
 	sta $d027,x
        txa
        clc
        adc #<(cre_sprites/64)
        sta $07f8,x
        lda #130
        sta y_coords,x
        lda #$ff
        sta y_coords_high,x
	dex
        bpl -
        lda #<($10000-TIME)
        sta counter_low
        lda #>($10000-TIME)
        sta counter_high        
	sei
        lda #<cre_irq
        sta $fffe
        lda #>cre_irq
        sta $ffff
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #$81
        sta $d019
        sta $d01a
        lda #$00
        sta $d012
        cli
        rts

CRE_STARTPOS = $40

d000tab:
	!for i, 8 {
	!byte <(CRE_STARTPOS+(i-1)*30),128
        }
        !byte $80
        
part:
!pseudopc DST_CODE {

cre_sprites:
	!bin "../credits_subpart/sprites.raw"
        
cre_irq:pha
	txa
        pha
        tya
        pha
        lda $01
        pha
        lda #$35
        sta $01
        inc $d019
        lda $d011
        bpl ++
	lda #$00
        sta $d015
        sta $d012
        lda #$7b
        sta $d011
        jmp do_ret
++
        lda $d012
        bmi cre_irq1
        cmp #$10
        bcs cre_irq0
        ldx #$07
        lda #$00
-	ldy y_coords_high,x
	cpy #$01
        rol
        dex
        bpl -
        eor #$ff
        sta $d015
        ldx #$07
        ldy #$0e
-	lda y_coords_high,x
	beq +
        lda #$f0
        cmp y_coords,x
        bcc ++
+	lda y_coords,x
++	sta $d001,y
	lda y_coords_high,x
        sta y_coords_high_copy,y
        lda #$00
        sta $d027,x
        dey
        dey
        dex
        bpl -               
        
        jsr sprites_movement
        
	lda #28
        sta $d012
        bne do_ret ; unconditional

cre_irq0:
	lda #$0f
        !for i, 8 {
        sta $d027+(i-1)
        }
        lda #$f8
        sta $d012
        bne do_ret ; unconditional
        
cre_irq1:
        lda #$f3
        sta $d011
	ldx #$0e
        lda #$00
-	ldy $d001,x
	sec
	bmi +
	ldy y_coords_high_copy,x
	cpy #$01
+       rol
        dex
        dex
        bpl -
        sta $d015 

        lda #$23
        sta $d012
        !ifdef INCLUDED {
        !ifdef RELEASE {
        jsr PLAY_MUSIC
        }
        }       
        inc counter_low
        bne +
        inc counter_high
	bne +
        !ifdef INCLUDED {
        !ifdef RELEASE {
        lda #$00
        sta $d015
        jsr FRAMEWORK_IRQ
        }
        } else {
        inc $d020
        }
+
do_ret:        
      	pla
        sta $01
        pla
        tay
        pla
        tax
        pla
        rti        



sprites_movement:
	ldx #$00
difftabcounter:        
	ldy #$c0
        inc difftabcounter+1
loop:        
        lda difftab,y
        bmi make_diff_nega
        lda y_coords,x
        clc
        adc difftab,y
        sta y_coords,x
        bcs +
-	tya
	clc
        adc #$f0
        tay
	inx
        cpx #$08
        bne loop
        rts

make_diff_nega:
        lda y_coords,x
        clc
        adc difftab,y
        sta y_coords,x
        bcs -
+       lda y_coords_high,x
        eor #$ff
        sta y_coords_high,x
        jmp -


difftab:
	!bin "../credits_subpart/movedifftab.bin"



                
        
codepages = >(( (*+$ff) & $ff00) - DST_CODE)  
            
}
     
        
                         
