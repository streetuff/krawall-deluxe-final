;--------------------------------------------------
!cpu 6510

	!ifdef RELEASE {
        !src "../framework/framework.inc"
        !src "config.asm"
	!to "vbars.plain",plain
        * = PARTORG		;das kommt von der config.inc
        jmp start		;wenn noetig
        } else {
        !to "vbars.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
	jmp start
        }
;--------------------------------------------------
col0 = 0
col1 = 11	;9
col2 = 5
col3 = 13
;--------------------------------------------------
		!macro check_same_page_start {
		!set page_check_page_addr = * & $ff00
		}

		!macro check_same_page_end {
		!if (page_check_page_addr != ( * & $ff00)) {
			!error "not the same page"
		}
		}
		!macro align256 {
			* = (*+$ff) & $ff00
		}
;--------------------------------------------------
old_irq_a = $e2
old_irq_x = $e3
old_irq_y = $e4
;--------------------------------------------------

	!ifdef RELEASE {
        } else {
*=$0900
!bin "Music.sid",,126
	}
char=$2000

*=$2200
!bin "krawall.chr"

*=$2500
screen
!bin "krawall.scr"

*=$2900
sine
!byte $00,$00,$00,$00,$00,$00,$01,$01,$02,$03,$03,$04,$05,$06,$07,$08
!byte $09,$0b,$0c,$0d,$0f,$10,$12,$13,$15,$17,$19,$1b,$1d,$1f,$21,$23
!byte $25,$27,$2a,$2c,$2e,$31,$33,$36,$39,$3b,$3e,$41,$43,$46,$49,$4c
!byte $4f,$52,$55,$58,$5b,$5e,$61,$64,$67,$6a,$6d,$70,$73,$77,$7a,$7d
!byte $80,$83,$86,$89,$8d,$90,$93,$96,$99,$9c,$9f,$a2,$a5,$a8,$ab,$ae
!byte $b1,$b4,$b7,$ba,$bc,$bf,$c2,$c5,$c7,$ca,$cc,$cf,$d1,$d4,$d6,$d8
!byte $da,$dd,$df,$e1,$e3,$e5,$e7,$e9,$ea,$ec,$ee,$ef,$f1,$f2,$f4,$f5
!byte $f6,$f7,$f8,$f9,$fa,$fb,$fc,$fd,$fd,$fe,$fe,$ff,$ff,$ff,$ff,$ff
!byte $ff,$ff,$ff,$ff,$ff,$ff,$fe,$fe,$fd,$fc,$fc,$fb,$fa,$f9,$f8,$f7
!byte $f6,$f4,$f3,$f2,$f0,$ef,$ed,$ec,$ea,$e8,$e6,$e4,$e2,$e0,$de,$dc
!byte $da,$d8,$d5,$d3,$d1,$ce,$cc,$c9,$c6,$c4,$c1,$be,$bc,$b9,$b6,$b3
!byte $b0,$ad,$aa,$a7,$a4,$a1,$9e,$9b,$98,$95,$92,$8f,$8c,$88,$85,$82
!byte $7f,$7c,$79,$76,$72,$6f,$6c,$69,$66,$63,$60,$5d,$5a,$57,$54,$51
!byte $4e,$4b,$48,$45,$43,$40,$3d,$3a,$38,$35,$33,$30,$2e,$2b,$29,$27
!byte $25,$22,$20,$1e,$1c,$1a,$18,$16,$15,$13,$11,$10,$0e,$0d,$0b,$0a
!byte $09,$08,$07,$06,$05,$04,$03,$02,$02,$01,$01,$00,$00,$00,$00,$00

*=$2a00
bar
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$01,$01,$01,$01,$02,$02,$02,$02,$03,$03,$03,$03
!byte $03,$03,$03,$03,$02,$02,$02,$02,$01,$01,$01,$01,$00,$00,$00,$00
!byte $00
;--------------------------------------------------
tab1			!byte %00000011
			!byte %00000010
			!byte %00000001
			!byte %00000000

tab2			!byte %00001100
			!byte %00001000
			!byte %00000100
			!byte %00000000

tab3			!byte %00110000
			!byte %00100000
			!byte %00010000
			!byte %00000000

tab4			!byte %11000000
			!byte %10000000
			!byte %01000000
			!byte %00000000
;--------------------------------------------------
*=$2b00
random
!bin "random.bin"
;--------------------------------------------------
vtable =$2c00
;--------------------------------------------------
			*=$3200
start:			
			!ifdef RELEASE {
			jsr FRAMEWORK_IRQ
                        } else {
			sei
                        }
			lda #$35
			sta $01
;			lda #$0b
;			sta $d011
			lda #24
			sta $d018
			lda #$03
			sta $dd00
			lda #$d8
			sta $d016

			lda #col0
			sta $d020
			lda #col3
			sta $d021
			lda #col2
			sta $d022
			lda #col1
			sta $d023

			ldx #$00
			lda #$00	;#$40
ll1			sta $0400,x
			sta $0500,x
			sta $0600,x
			sta $0700,x
			lda #$08+col0
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x

			inx
			bne ll1
			
; Generate Perspective Table
			ldy #0
-			tya
			sta vtable,x
			tya
			adc #0 : step0 = *-1
			tay
			pla
			adc #$40 : step1 = *-1
			pha
			bcc +
			iny
+			lda step1
			adc #$0b	;0b
			sta step1
			bcc +
			inc step0
+			inx
			bne -

						
			
                        !ifdef RELEASE {
                        } else {
			lda #$00
			jsr $0900
                        }
;--------------------------------------------------
; stable irq
;--------------------------------------------------
			lda #$7f
			sta $dc0d
			lda $dc0d
			jsr init_stable_timer

			!ifdef RELEASE {
                        sei
                        }
			lda #$32
			sta $d012
			lda #$0b
			sta $d011
			lda #<irq
			sta $fffe
			lda #>irq
			sta $ffff
			lda #$81
			sta $d019
			sta $d01a
			cli

	!ifdef RELEASE {
        jsr LOAD_NEXT_PART
        ldx #$00
-       lda zwischenpart,x
        sta FRAMEWORK_ADDR - $100,x
        inx
        bne -
        }
waitflag:lda #$00		; hier wird gewartet, ob der part zu Ende ist ...
	beq waitflag
	!ifdef RELEASE {
        jmp FRAMEWORK_ADDR - $100
        } else {
	jmp waitflag	;fuer Test als einzelner Part
	}

	!ifdef RELEASE {
zwischenpart:
	!pseudopc FRAMEWORK_ADDR - $100  {
        jsr FRAMEWORK_IRQ
        lda #>leider_geil
        ldy #>$f000
        ldx #>leider_geil_len
        jsr MEMCPY
        jsr $f000
        ldy #$c0
        jsr COPY_PACKED        
	lda #$0b	;Bildschirm aus
	sta $d011
        jsr DECRUNCH_NEXT_PART
		lda #180+75
		cmp d8pos
		bne *-3
		lda #$7b
		sta $d011
        jsr FRAMEWORK_IRQ
        jmp START_NEXT_PART
        }
        }

			+align256

init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
			lda #$7f
			sta $dc0d
			lda $dc0d
			lda #%00000000
			sta $dc0f
			lda #%10000000
			sta $dc0e
			lda #$82
			sta $dc0d
	; 63 to zero because of 63 cycles per line
			lda #<(63-1)
			sta $dc04
			lda #>(63-1)
			sta $dc05
	; but not start it yet !

			+check_same_page_start
	; get out of bad line area
-			lda $d011
			bmi -
-			lda $d011
			bpl -
			lda $d012
-			cmp $d012
			beq -
			nop
			nop
			nop
			ldy #%10000001
again:
	;make a loop with 62 cycles
			ldx #10   ; 2
-			dex	  ; 2
			bne -     ; 3 (last one 2) -> 2+(10*5)-1
			lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-			cmp $d012 ; 4
			beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
			sty $dc0e
			+check_same_page_end
			rts


;;;;;;;;;;;;;;; IRQ ;;;;;;;;;;;;;;;;;;;;;;;;;;

			+check_same_page_start
irq:			sta old_irq_a
irq_no_a:
			stx old_irq_x
			ldx $dc04
			lda stable_tab_irq,x
			sta bpl_addr2+1
bpl_addr2:
			bpl bpl_addr2

			cmp #$c9
			cmp #$c9
			cmp #$c9
			cmp #$c9
			cmp #$c9
			cmp #$c9
			cmp #$c9
			cmp #$c9
			bit $ea
			+check_same_page_end
        ;stable here
			sty old_irq_y

			inc $d019

			jsr vertical
			lda scene
			cmp #6
			beq nomove
			jsr paintbar

			lda scene
			cmp #5
			bpl nomove
			jsr movebar3
nomove			
;			dec $d020
        		!ifdef RELEASE {
        		jsr PLAY_MUSIC
        		} else {
			
			jsr $0903
                        }
;			inc $d020

			jsr scenes

			lda old_irq_a
			ldx old_irq_x
			ldy old_irq_y
			rti

stable_tab_irq:

	; table needed for stable interrupt

			+check_same_page_start
		!for i,$2a {
			!byte 17
		}

		!byte 17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0
		!for i,63-$2a-17 {
			!byte 0
		}
			+check_same_page_end

;--------------------------------------------------
paintbar
!for i, 25 {

			ldx $02+((i-1)*4)
			lda tab4,x
			ldx $03+((i-1)*4)
			ora tab3,x
			ldx $04+((i-1)*4)
			ora tab2,x
			ldx $05+((i-1)*4)
			ora tab1,x
			sta $2000+((i-1)*8)
			sta $2001+((i-1)*8)
			sta $2002+((i-1)*8)
			sta $2003+((i-1)*8)
			sta $2004+((i-1)*8)
			sta $2005+((i-1)*8)
			sta $2006+((i-1)*8)
			sta $2007+((i-1)*8)
}		

!for i, 26 {

			ldx $02+((25-(i-1))*4)
			lda tab1,x
			ldx $03+((25-(i-1))*4)
			ora tab2,x
			ldx $04+((25-(i-1))*4)
			ora tab3,x
			ldx $05+((25-(i-1))*4)
			ora tab4,x
			sta $2000+(((i-1)+37)*8)
			sta $2001+(((i-1)+37)*8)
			sta $2002+(((i-1)+37)*8)
			sta $2003+(((i-1)+37)*8)
			sta $2004+(((i-1)+37)*8)
			sta $2005+(((i-1)+37)*8)
			sta $2006+(((i-1)+37)*8)
			sta $2007+(((i-1)+37)*8)
}		

			rts
;--------------------------------------------------
movebar3
			inc $70
			ldx $70
			lda sine,x
			clc
			adc $70
			sta $71

			clc
						
!for i, 100 {
			lda vtable+100-(i-1)
;			clc
			adc $71
			anc #31
			tax
			lda bar,x
			sta $02+(i-1)
;			sta $02+251-(i-1)
			}

			rts
;--------------------------------------------------
scenes		lda scene
			cmp #1
			beq scene1
			cmp #2
			beq scene2
			cmp #3
			beq scene3
			cmp #4
			beq scene4
			cmp #5
			beq scene5
			cmp #6
			beq scene6
			cmp #7
			beq scene7
			cmp #8
			beq scene8

			lda #1
			sta scene
			lda #0
			sta fval
			sta fval2
			rts
;--------------------------------------------------
scene1		jmp fadein_speedup
;--------------------------------------------------
scene2		jmp wait64
;--------------------------------------------------
scene3		jmp fadebar
;--------------------------------------------------
scene4		jmp wait512
;--------------------------------------------------
scene5		jmp fadeout
;--------------------------------------------------
scene6		jmp wait128
;--------------------------------------------------
scene7		jmp logofade
;--------------------------------------------------
scene8		jmp end
;--------------------------------------------------
wait64		inc w64
			lda w64
			cmp #64
			bne no_w64
			inc scene
			lda #$00
			sta w64
no_w64
			rts
w64	!byte $00
;--------------------------------------------------
wait128		inc w128
			lda w128
			cmp #162
			bne no_w128
			inc scene
			lda #$00
			sta w128
no_w128
			rts
w128	!byte $00
;--------------------------------------------------
wait512		inc w512
			bne no_hi512
			inc w512+1
no_hi512
			lda w512+1
			cmp #2
			bne no_w512
			inc scene
			lda #$00
			sta w512
			sta w512+1
no_w512
			rts
w512		!byte $00,$00
;--------------------------------------------------
fadein_speedup
			lda d011fix
			cmp #1
			bne nofix
			lda #$1b
			sta $d011
nofix
			jsr fadein
			jsr fadein
			jsr fadein
			jsr fadein
			lda #$01
			sta d011fix
			rts
;--------------------------------------------------
fadein		ldx fadeval
			lda random,x
			tax
			lda screen,x
			sta $0400,x
			lda screen+$100,x
			sta $0500,x
			lda screen+$200,x
			sta $0600,x
			lda screen+$300,x
			sta $0700,x
			inc fadeval
			bne fadeoff
			inc scene
fadeoff
			rts
;--------------------------------------------------
fadebar		inc slowdown
			lda slowdown
			and #7
			bne fadeoff
!for i,64 {
			lda bar+1+(i-1)
			sta bar+(i-1)
		}
			inc barval
			lda barval
			cmp #32
			bne baroff
			inc scene
baroff
			rts
;--------------------------------------------------
fadeval	!byte $00
barval	!byte $00
slowdown	!byte $00
scene	!byte $01
d011fix	!byte $00
;--------------------------------------------------
fadeout		ldx fval
			lda #$00
floop		sta $02,x
			inc fval
			lda fval
			cmp #101
			bne foutoff
			inc scene
foutoff
			rts
fval			!byte 0
;--------------------------------------------------
logofade		;inc $d020

			ldx #$00
			lda #$00
lfade		
lmod1		sta $0400,x
lmod2		sta $07c0,x
			inx
			cpx #$28
			bne lfade
			
			lda lmod1+1
			clc
			adc #40
			sta lmod1+1
			lda lmod1+2
			adc #0
			sta lmod1+2
			
			sec
			lda lmod2+1
			sbc #40
			sta lmod2+1
			lda lmod2+2
			sbc #0
			sta lmod2+2
			
			inc fval2
			lda fval2
			cmp #13
			bne no_end
			inc scene		
no_end
			rts
fval2		!byte $00
;--------------------------------------------------
end			lda #$01
			sta waitflag+1
			rts
;--------------------------------------------------
*=$2d00
vertical
			lda #$d0
!for i, 25 {
			sta $d016
			jsr waitx
			inc $d016
			jsr waity
			inc $d016
			jsr waity
			inc $d016
			jsr waity
			inc $d016
			jsr waity
			inc $d016
			jsr waity
			inc $d016
			jsr waity
			inc $d016
			jsr waity

}
			rts

waitx
			nop
			nop
			rts
waity
			ldy #$08
			dey
			bne *-1
			nop
			nop
			rts
;--------------------------------------------------

        * = $4500
leider_geil:
        !pseudopc 0xf000 {

			sei
			lda #$35
			sta $01			

			lda #$00
			sta $d012
			sta $d011
			lda $d011
			and #$7f
			sta $d011
			lda #$81
			sta $d01a
			sta $d019
			lda #<raster0
			sta $fffe
			lda #>raster0
			sta $ffff
			lda #$7f
			sta $dc0d
			lda $dc0d
     		cli

			lda #$00
			sta $d020
			sta $d021
		
			ldx #$00
lloop1		lda font,x
			sta $0200,x
			lda font+$100,x
			sta $0300,x
			lda #$40
			sta $0400,x
			sta $0500,x
			sta $0600,x
			sta $0700,x
			lda #$00
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
			bne lloop1

			ldx #$00
lloop2	
			lda text,x
			and #$1f
			clc
			adc #$40
			sta $0400+(10*40),x
			clc
			adc #27
			sta $0400+(11*40),x

			lda text+40,x
			and #$1f
			clc
			adc #$40
			sta $0400+(13*40),x
			clc
			adc #27
			sta $0400+(14*40),x

			inx
			cpx #40
			bne lloop2

			lda #%00010000
			sta $d018
			lda #$c8
			sta $d016

;			lda #$1b
;			sta $d011
			rts

;--------------------------------------------------
raster0
			sta raster_old0_a+1
			stx raster_old0_x+1
			sty raster_old0_y+1
                        lda $01
                        sta raster_old0_01+1
                        lda #$35
                        sta $01
                        lda #$1b
                        sta $d011

			inc $d019

			inc waitbit
			lda waitbit
			and #1
			bne no_wait
			jsr d8fade
no_wait
        		!ifdef RELEASE {
        		jsr PLAY_MUSIC
        		}
raster_old0_01: lda #$00
                sta $01
raster_old0_a:	lda #$00
raster_old0_x:	ldx #$00
raster_old0_y:	ldy #$00
			rti


;			lda d8pos
;			cmp #180
;			beq end
;			jmp main
		
;end
;			inc $d020
;			jmp *-3
;--------------------------------------------------
d8fade	
			ldx #80
d8loop		lda fadetemp,x
			sta fadetemp+1,x
			dex
			bpl d8loop

			ldx d8pos
			lda colortab,x
			sta fadetemp
			inc d8pos
		
			ldx #$00
setloop		lda fadetemp,x
			sta $d800+(10*40),x
			sta $d800+(11*40),x
			lda fadetemp+40,x
			sta $d800+(13*40),x
			sta $d800+(14*40),x
			inx
			cpx #40
			bne setloop
			rts
;--------------------------------------------------
d8pos	!byte $00
waitbit	!byte $00
;--------------------------------------------------
text
!text "@@@@krawallbrause@aus@tueten@trinken@@@@"
!text "@@@@@@@@@@@@@@leider@@geil@@@@@@@@@@@@@@"
;--------------------------------------------------
fadetemp
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
;--------------------------------------------------
colortab
!byte $0b,$09,$08,$05,$03,$0d,$07,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01

!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$01,$01

!byte $01,$01,$01,$07,$0d,$0f,$0a,$0c,$02,$0b
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
;--------------------------------------------------
font
!bin "1x2-font.bin"

        }
leider_geil_len = ((*+$ff) & $ff00) - leider_geil
 
