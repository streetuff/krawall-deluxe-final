


        !cpu 6510
     
	!ifdef RELEASE {
        !src "../framework/framework.inc"
        !src "config.asm"
	!to "mc_player.plain",plain

Music=0	; Musik im Framework
LOGO_DEBUG= 0 ; Kein Timing anzeiger ..


koala_pic_dest     = $d400
koala_screen_dest  = $ce80
koala_color_dest   = $da80


         * = PARTORG
	lda #$35
	sta $01
	jmp start


        } else {
        !to "mc_player.prg",cbm

        !src "../framework/framework.inc"


Music=0	; Keine Music im Speicher
;Music=1		; Music on

LOGO_DEBUG= 0 ; Timinganzeige an


!if Music = 1{	

*=$1000
		

!bin "no_motion_color.sid",,$7e
}


koala_pic = $e000
koala_screen    = $d000
koala_color 	  = $cc00


         *= $0800




	sei
	lda #$35
	sta $01
	jsr init_stable_timer

        lda #$7f
	sta $dc0d
	lda $dc0d

	jmp start

        }

 



;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Zp1	= $22		    ; zeropageadressen fuer Zeiger  
Zp2	= Zp1+1		    ; zeropageadressen fuer Zeiger  
Zp3	= $24		    ; zeropageadressen fuer Zeiger  
Zp4	= Zp3+1		    ; zeropageadressen fuer Zeiger  
Zp5  = $26 
Zp6  = Zp5+1
Zp8  = $28

MAX_Y_CHARS = 15		; Ausdehnung des Charfields	
MAX_X_CHARS = 17

SCREEN_OFF_X= (40/2)-(MAX_X_CHARS/2)	 ; Genau mittig
SCREEN_OFF_Y= 1

screen =$e800
charset= $e000 


SINTABS= $ec00
d016values = $ee00
agspvalues = $ef00

IRQLINE1=$31
IRQLINE2=$b3

DO_AGSP = 0
DO_MOVEIN = 1
off_screen =$f000

TIMESLICE =4		; 2 frames warten vor dem n�chsten Bild
REPEATS = 6



init_music=$1000
play_music=$1003
fade_music=$1006


	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}

	!macro asr {
	cmp #$80
	ror
	}

	!macro align16 {
		* = (*+$f) & $fff0
	}

	!macro align256 {
		* = (*+$ff) & $ff00
	}

	!macro align512 {
		* = (*+$1ff) & $fe00
	}


start
		
		lda #$0b   ; screen off for start	
		sta $d011
		lda #$18   ; Multicolor an	
		sta $d016
		sta ymover
		lda #$a8  ; Screen bei $e800, char bei $e000
		sta $d018
		;lda $dd00 ; Vic Basis auf $c000
		lda #$00
		;ora #$03
		;and #$fc
		sta $dd00
		lda #$00
		sta $d020
		lda #$35
		sta $01
		lda #$0e
	      ;  lda #$0f
		sta $d023
		lda #$02 
	;	lda #$0a 
		sta $d022
		lda #$00
		sta $d021  
		jsr init_stable_timer
		jsr init_agsp_tabs
		jsr copycram
		jsr fillscreen
		jsr copycharset1
		jsr copy_sintabs
		jmp mainprog
		

copycram
	ldx #$00
-	
	lda #$06+08
        ;lda #$01+08
	sta $d800,x
	sta $d900,x
	sta $da00,x
      ;  sta $db00,x
	lda #$00
	sta screen,x
	sta screen+$100,x
	sta screen+$200,x
	sta screen+$300,x
!if DO_MOVEIN {
	sta off_screen,x
	sta off_screen+$100,x
	sta off_screen+$200,x
	sta off_screen+$300,x
	sta off_screen+$400,x
}
	dex
	bne -
	sta charset+$07f8
	sta charset+$07f9
	sta charset+$07fa
	sta charset+$07fb
	sta charset+$07fc
	sta charset+$07fd
	sta charset+$07fe
	sta charset+$07ff
	rts

rowcount !byte MAX_X_CHARS

copy_sintabs

	ldx #$00
-
	lda SinTabs_org,x
	sta SINTABS,x
	lda SinTabs_org+$100,x
	sta SINTABS+$100,x
	lda SinTabs_org+$200,x
	sta SINTABS+$200,x
	dex
	bne -
	rts


fillscreen
	ldx #$00
cfs1	
	ldy #MAX_Y_CHARS
-	
	txa
!if DO_MOVEIN = 1 {
scadd   sta off_screen+SCREEN_OFF_X+($28*SCREEN_OFF_Y)
}else{
scadd   sta screen+SCREEN_OFF_X+($28*SCREEN_OFF_Y)
}
	inx
	lda scadd+1 
	clc
!if DO_MOVEIN = 1 {
	adc #$50      ; eine leerzeile lassen
}else{
	adc #$28
}
	sta scadd+1
	bcc +
	inc scadd+2
+	dey
	bne -
	lda scadd+1
	sec
!if DO_MOVEIN = 1 {
	sbc #<((MAX_Y_CHARS*$50)-1)
}else{
	sbc #<((MAX_Y_CHARS*$28)-1)
}
	sta scadd+1
	lda scadd+2
!if DO_MOVEIN = 1 {
	sbc #>((MAX_Y_CHARS*$50)-1)
}else{
	sbc #>((MAX_Y_CHARS*$28)-1)
}
	sta scadd+2
	dec rowcount
	bne cfs1	

	rts


	+align256

        
init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #%00000000
        sta $dc0f
        lda #%10000000
        sta $dc0e
        lda #$82
        sta $dc0d
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dc04
        lda #>(63-1)
        sta $dc05
	; but not start it yet !

	+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	nop
	nop
	nop
        ldy #%10000001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
        sty $dc0e
	+check_same_page_end
	rts

stable_tab_irq:

	; table needed for stable interrupt

	+check_same_page_start
	!for i,$28 {
        !byte 0
        }
        !byte 11,10,9,8,7,6,5,4,3,2,1,0
        !for i,63-$28-12 {
        !byte 0
	}
	+check_same_page_end


init_agsp_tabs:

	; initialize the AGSP scroll tables

        ldx #$00
-       txa
        and #$07
        eor #$07
        ora #$10
        sta d016values,x
        inx
        bne -

        ldy #00
-       tya
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
	inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        iny
        inx
        bne -
	rts



mainprog	

		
!if Music = 1  {
		ldx #0
		ldy #0
		lda #0
	        jsr init_music
}
	       
		lda #$00
		sta startup
		sta depackrun
		;Depacker erst starten, nachdem alles initialisiert ist ...

		
		
		lda #$01
		sta koalacopy   


		jsr initirq

		jmp main
		

initirq 

  	sei
	lda #$35
	sta $01

	lda #<irq
	sta $fffe
	lda #>irq
	sta $ffff
	lda #$01
	sta $d01a
	lda #IRQLINE1
	sta $d012
	inc $d019
	lda $dd0d
	lda #$7f
	sta $dc0d
	lda $dc0d
 
	lda #TIMESLICE
	sta speedp

	
	cli
	rts


!ifdef RELEASE {

copycolor

	ldx #$00
-	
	lda koala_color,x
	sta koala_color_dest,x
	lda koala_color+$80,x
	sta koala_color_dest+$80,x
	lda #$08
	sta koala_color_dest-$40,x    

	lda koala_screen,x
	sta koala_screen_dest,x
	lda koala_screen+$80,x
	sta koala_screen_dest+$80,x
	lda #$ff
	sta koala_screen_dest-$40,x    

	dex
	bne -

	rts


}else{

copycolor

	ldx #$00
-	
	lda koala_color+$278,x
	sta $da78,x
	lda koala_color+$300,x
	sta $db00,x
	dex
	bne -

	rts
			
}
!src "../common_src/streamdepack.src"

	  
streamno
	!byte $ff
repeater 
	!byte 0	
bytecount !byte 0,0
loadready !byte 0
loadwait !byte 0
loadnext !byte 0
depackrun !byte 0
streamready !byte 0
koalacopy !byte 0
      
main



	;jsr getkey

	 lda #REPEATS
	 sta repeater

	
	 jsr copycolor


	 lda #$00		
	 sta loadwait
	


	 lda #$00
	 sta loadready
	 lda #$01
	 sta streamready
	 sta loadnext 


; ---------------------------------------------------------------------


!ifdef RELEASE {
        jsr LOAD_NEXT_PART
        inc loadready           ; Jetzt darf der depacker starten .. 

        jsr LOAD_NEXT_PART	; gleich den n�chsten Stream laden


-	ldx loadwait		; Warten bis Stream 1 gespielt ist
	beq -

        jsr LOAD_NEXT_PART	; letzten Stream laden
	
-	ldx loadwait		; Warten bis Stream 2 gespielt ist
	beq -

        jsr LOAD_NEXT_PART	; n�chsten Part laden

-	ldx loadwait		; Warten bis Stream 3 gespielt ist
	cpx #$02    
	bne -

!if DO_MOVEIN =1 {

	lda #$ff
	sta movein

	; warten bis alles rausgemoved
-	lda movein
	bmi -
}

	lda #$0b
	sta $d011
        jsr FRAMEWORK_IRQ

        ldx #$00
-       lda zwischenpart,x
        sta $0200,x
        inx
        bne -
        jmp $0200


zwischenpart:
	!pseudopc $0200 {
;        jsr FRAMEWORK_IRQ
-	lda $d012
	cmp #$f8
	bne -
	lda #$00
	;sta $d020
	sta $d021

        lda #$03
	sta $dd00
	lda #$0b
	sta $d011
	lda #$00
	sta $d015
	sta $d01c
        jsr DECRUNCH_NEXT_PART
        jmp START_NEXT_PART
        }
        

; ---------------------------------------------------------------------



}else{
	lda #$00
        sta depackrun		; Jetzt darf der depacker starten .. 
	inc loadready

-	lda loadwait
	cmp #$02
	bne -

!if DO_MOVEIN =1 {

	lda #$ff
	sta movein

	; warten bis alles rausgemoved
-	lda movein
	bmi -
}

	inc $d020
	jmp *-3	    


}
; ---------------------------------------------------------------------

; This is picture depacker 		
main1
		         
			
	lda headerstart 
	sta Zp1
	lda headerstart+1 
	sta Zp2
	ldy #$04
	lda (Zp1),y
	sta no_o_pic_l		


	lda pacstart 
	sta Zp1
	lda pacstart+1 
	sta Zp2
	lda #>off16		 ; Higherbyte f�r die Sprungtabelle
	sta Zp6	
	lda #$1
	sta piccount
	lda #1
	sta speedp

	;jsr getkey 		   ; Tastatur abfrage nach jedem bild

	rts
   
	
picloop
	
!if DO_AGSP=1{
xscroll:
	ldx #$00
	lda d016values,x
	sta d016value+1
	lda agspvalues,x
	sta xhigh_coord+1
	inc xscroll+1

}
	lda #<(charset-1 ) 
	sta Zp3
	lda #>(charset-1 ) 
	sta Zp4

;	jsr getkey 		   ; Tastatur abfrage nach jedem bild

	jsr depack_pic             ; Zuerst die Farbtabelle depacken

	
;	jsr getkey 		   ; Tastatur abfrage nach jedem bild
	
	
	inc piccount

	ldx piccount
	
	cpx no_o_pic_l
;	cpx #130
	beq time2

	rts		; Naechstes Bild


			; Ende erreicht,
time2	
	dec repeater
	beq pre_streamchange
	jmp main1            ; Alles auf Anfang


pre_streamchange
	inc streamready
	
	rts	

streamchange
	inc streamno
	ldy streamno
        cpy #$3
        bne +

        jsr set_ik_color
+        
	lda streamtableoff,y
        bmi +
	tay
        
	lda streamtable,y
	sta headerstart	
	lda streamtable+1,y
	sta headerstart+1
	lda streamtable+2,y
	sta pacstart	
	lda streamtable+3,y
	sta pacstart+1
	lda streamtable+4,y
	sta jmpadd	
	lda streamtable+5,y
	sta jmpadd+1
	lda streamtable+6,y
	sta repeater
	lda streamtable+7,y
	sta loadwait
	jmp (jmpadd)
	
+	
	; We 're done , R�ckkehr mit Z-Flag nicht gesetzt
	inc depackrun	; depacker nicht mehr laufen lassen	
	lda #$02
	sta loadwait
	rts


loopcount !byte 0	

!if DO_MOVEIN = 1 {

movein    !byte  $70
ymover    !byte 0
} 
	

streamtableoff 
	!byte 0,8,16,24,32,$ff
streamtable
	
	!byte <stream,>stream,<stream+$800+8,>stream+$800+8,<copycharset1,>copycharset1,1,0
	!byte <stream2,>stream2,<stream2+$800+8,>stream2+$800+8,<copycharset2,>copycharset2,3,0
	!byte <stream3,>stream3,<stream3+$800+8,>stream3+$800+8,<copycharset3,>copycharset3,1,1
	!byte <stream4,>stream4,<stream4+$800+8,>stream4+$800+8,<copycharset4,>copycharset4,1,1
	!byte <stream5,>stream5,<stream5+$800+8,>stream5+$800+8,<copycharset5,>copycharset5,4,0
	
jmpadd 
	!byte 0,0	

copycharset1
	ldx #$00
-	
	lda stream+8,x
	sta charset,x
	lda stream+$100+8,x
	sta charset+$100,x
	lda stream+$200+8,x
	sta charset+$200,x
	lda stream+$300+8,x
	sta charset+$300,x
	lda stream+$400+8,x
	sta charset+$400,x
	lda stream+$500+8,x
	sta charset+$500,x
	lda stream+$600+8,x
	sta charset+$600,x
	lda stream+$6f8+8,x
	sta charset+$6f8,x
	dex
	bne -	
	jmp main1

copycharset2
	ldx #$00
-	
	lda stream2+8,x
	sta charset,x
	lda stream2+$100+8,x
	sta charset+$100,x
	lda stream2+$200+8,x
	sta charset+$200,x
	lda stream2+$300+8,x
	sta charset+$300,x
	lda stream2+$400+8,x
	sta charset+$400,x
	lda stream2+$500+8,x
	sta charset+$500,x
	lda stream2+$600+8,x
	sta charset+$600,x
	lda stream2+$6f8+8,x
	sta charset+$6f8,x
	dex
	bne -	
	jmp main1
	

copycharset3
	ldx #$00
-	
	lda stream3+8,x
	sta charset,x
	lda stream3+$100+8,x
	sta charset+$100,x
	lda stream3+$200+8,x
	sta charset+$200,x
	lda stream3+$300+8,x
	sta charset+$300,x
	lda stream3+$400+8,x
	sta charset+$400,x
	lda stream3+$500+8,x
	sta charset+$500,x
	lda stream3+$600+8,x
	sta charset+$600,x
	lda stream3+$6f8+8,x
	sta charset+$6f8,x
	dex
	bne -	
	jmp main1
	

copycharset4
	ldx #$00
-	
	lda stream4+8,x
	sta charset,x
	lda stream4+$100+8,x
	sta charset+$100,x
	lda stream4+$200+8,x
	sta charset+$200,x
	lda stream4+$300+8,x
	sta charset+$300,x
	lda stream4+$400+8,x
	sta charset+$400,x
	lda stream4+$500+8,x
	sta charset+$500,x
	lda stream4+$600+8,x
	sta charset+$600,x
	lda stream4+$6f8+8,x
	sta charset+$6f8,x
	dex
	bne -	
	jmp main1

copycharset5
	ldx #$00
-	
	lda stream5+8,x
	sta charset,x
	lda stream5+$100+8,x
	sta charset+$100,x
	lda stream5+$200+8,x
	sta charset+$200,x
	lda stream5+$300+8,x
	sta charset+$300,x
	lda stream5+$400+8,x
	sta charset+$400,x
	lda stream5+$500+8,x
	sta charset+$500,x
	lda stream5+$600+8,x
	sta charset+$600,x
	lda stream5+$6f8+8,x
	sta charset+$6f8,x
	dex
	bne -	
	jmp main1
 
 
set_ik_color

        lda #$0f
	sta $d023
	lda #$0a 
	sta $d022

	ldx #$00
-	
        lda #$01+08
	sta $d800,x
	sta $d900,x
	sta $d980,x
        dex
        bne -


        lda #$80
        sta movein
        rts


	+align256
	; avoid page cross !
irq:    pha 
	txa
	pha
  
        inc $d019
        ldx $dc04
	lda stable_tab_irq,x
	sta bpl_addr2+1
bpl_addr2:
	bpl bpl_addr2

	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9 
	cmp #$c9 
	bit $ea
        
        ;stable here
	lda #$00
	sta $d015
	tya
	pha
	bit $ea
!if DO_AGSP=1{
d016value:
        lda #$18
	sta $d016
xhigh_coord:
        bne xhigh_coord+2
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        lda #$78
        sta $d011
	lda #$79
	sta $d011

	!for i, 9+13 {
	nop
	}
x_reg:	ldx #$00
        lda #$00
        sta $d020


}else{


	lda #<irq2
	sta $fffe
	lda #>irq2
	sta $ffff
	lda #IRQLINE2
	sta $d012

	lda #$1b
	sta $d011
	lda #$a8  ; Screen bei $e800, char bei $e000
	sta $d018
	lda #$00 
	sta $d021

!ifdef RELEASE {

	lda koalacopy
	beq +
	lda #$7b  ; Don't show while copy
	sta $d011

+
}



!if DO_MOVEIN = 1 {

	lda ymover 
	sta $d016


	lda movein
	beq +

	jmp exit_irq
+

}else{
	lda #$18  ; Multicolormode an
	sta $d016
}
    
}


!ifdef RELEASE {


}
	
!if LOGO_DEBUG = 1  {
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	
	inc $d020
} 


	lda depackrun 
	bne exit_irq
	dec speedp
	bne exit_irq	

	inc depackrun
	cli			; IRQ wieder freigeben , da depack laenger als ein Frame

	lda loadready	     ; schon fertig  mit dem laden ?	
	beq str1
	lda streamready
	beq str0
	lda #$00
	sta streamready

	jsr streamchange     ; nachdem  letzten stream wird das Z-Flag gel�scht
	bne str1	   	
	

str0
	jsr picloop
str1
	lda #TIMESLICE
	sta speedp


	dec depackrun


exit_irq

        

!if LOGO_DEBUG = 1  {
	dec $d020
} 




        pla
	tay
	pla
	tax
	pla
rti_addr
	rti
	


irq2:   pha 
	txa
	pha
	tya
	pha

	inc $d019
	lda #$00 
!ifdef RELEASE {
	lda $d021 ;eliminate grey dot
} else {
	sta $d021
}
!if LOGO_DEBUG = 1  {
	inc $d020
}else{ 

        ldx #$07        
-        dex
        bne -
}

!ifdef RELEASE {
	lda #$30  ; Screen bei $cc00, Bitmap bei $c000
	sta $d018
}else{
	lda #$48  ; Screen bei $d000, Bitmap bei $e000
	sta $d018

}
	
	lda #$d8  ; Extended Colormode an
	sta $d016

	lda #$3b  ; Bitmap modus an
	sta $d011

!ifdef RELEASE {
	lda koalacopy
	beq +
	lda #$7b  ; Don't show while copy
	sta $d011

	ldx #$00
-
ko_cp1	lda koala_pic,x
	dec $01
ko_cp2	sta koala_pic_dest,x
	inc $01
	dex
	bne -
	inc ko_cp1+2
	inc ko_cp2+2
	lda ko_cp2+2
	cmp #$e0	; Bis maximal $e000 kopieren
	bne +
	
	lda #$00
	sta koalacopy
	
+
}


!if Music = 1  {
	jsr play_music
}else{
!ifdef RELEASE {
	jsr PLAY_MUSIC
}


}
!if DO_MOVEIN = 1 {

!ifdef RELEASE {

	lda koalacopy
	bne +
}
	ldy movein 
	beq +
	dec movein
	lda SinTab_x,y
	tay
	and #$07
	eor #$07
	ora #$10
	sta ymover
	tya
	lsr
	lsr
	lsr 
	tay 

	ldx #0
-
	!for i, (MAX_Y_CHARS-SCREEN_OFF_Y)-1 {
	
	 lda off_screen+((i-1+SCREEN_OFF_Y)*$50)+$28,y
	 sta screen+((i-1+SCREEN_OFF_Y)*$28),x
	}
	inx
	iny
	cpx #$28
	bne -
+
}

!if LOGO_DEBUG = 1  {
	dec $d020
} 


	lda #<irq
	sta $fffe
	lda #>irq
	sta $ffff
	lda #IRQLINE1
	sta $d012

        pla
	tay
	pla
	tax
	pla
	rti
 


;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Wartet dadrauf, dass die Spacetaste gedrueckt und wieder losgelassen
; wird

getkey

wx1	lda $dc01
	and #$10
	bne wx1
wx2	lda $dc01
	and #$10
	beq wx2

	rts
	

piccount !byte 0
speedp  !byte 0
shadow !byte 0

startup !byte 0


;charset = headerstart+8		; F�r Colorram

headerstart
!byte  <stream,>stream
no_o_pic_l !byte 0
no_o_pic_h !byte 0
pacstart 
!byte <stream+8+$800,>stream+8+$800





!ifdef RELEASE {

stream 
;!bin "astand toy0001.pac",,2
!bin "CL_A_0001.pac",,2

stream4 
;!bin "cstand_toy0001.pac",,2
!bin "A_0001.pac",,2


 ;Streams werden nachgeladen 
*= $5002 
stream2 = $5004

stream3 = $9004  

stream5 = $5004

}else{
!if Music = 1{
 *=$1a00
}else{
 *=$1100
}
; direkt nach der Musik, komplett im Speicher

stream 
;!bin "CF_010000.pac",,2
;!bin "seating-mode0000.pac",,2
;!bin "sitting toy0000.pac",,2
;!bin "lara0000.pac",,2
;!bin "stand toy0000.pac",,2
!bin "CL_A_0001.pac",,2


stream2
;!bin "homer_fm8617.pac",$0808,2
;!bin "lara0000.pac",,2
;!bin "stan-toon0000.pac",,2
;!bin "astand toy0001.pac",,2
!bin "CL_B_0007.pac",,2

stream3
;!bin "toons-characters0000.pac",,2
;!bin "Micky-mouse0000.pac",,2
;!bin "automotive-figure0000.pac",,2
;!bin "bstand toy0000.pac",,2
;!bin "C_0022.pac",,2
!bin "CL_C_0039.pac",,2

stream4 
;!bin "lara0000.pac",,2
;!bin "cstand_toy0001.pac",,2
!bin "A_0001.pac",,2

stream5
;!bin "lara0000.pac",,2
;!bin "dstand toy0000.pac",,2
!bin "B_0009.pac",,2


}


SinTabs_org

	!pseudopc SINTABS {

SinTab_x 

!byte $00,$00,$00,$00,$00,$00,$01,$01,$02,$02,$03,$03,$04,$05,$06,$07
!byte $08,$09,$0a,$0b,$0c,$0e,$0f,$10,$12,$13,$15,$17,$18,$1a,$1c,$1e
!byte $20,$21,$23,$25,$28,$2a,$2c,$2e,$30,$32,$35,$37,$39,$3c,$3e,$41
!byte $43,$46,$48,$4b,$4d,$50,$52,$55,$58,$5a,$5d,$60,$62,$65,$68,$6a
!byte $6d,$70,$72,$75,$78,$7a,$7d,$80,$82,$85,$87,$8a,$8d,$8f,$92,$94
!byte $97,$99,$9c,$9e,$a0,$a3,$a5,$a7,$aa,$ac,$ae,$b0,$b2,$b4,$b6,$b8
!byte $ba,$bc,$be,$bf,$c1,$c3,$c4,$c6,$c7,$c9,$ca,$cc,$cd,$ce,$cf,$d0
!byte $d1,$d2,$d3,$d4,$d5,$d6,$d6,$d7,$d8,$d8,$d8,$d9,$d9,$d9,$d9,$d9
!byte $d9,$d9,$d9,$d9,$d9,$d9,$d8,$d8,$d7,$d7,$d6,$d6,$d5,$d4,$d3,$d2
!byte $d1,$d0,$cf,$ce,$cd,$cb,$ca,$c9,$c7,$c6,$c4,$c2,$c1,$bf,$bd,$bb
!byte $b9,$b8,$b6,$b4,$b1,$af,$ad,$ab,$a9,$a7,$a4,$a2,$a0,$9d,$9b,$98
!byte $96,$93,$91,$8e,$8c,$89,$87,$84,$81,$7f,$7c,$79,$77,$74,$71,$6f
!byte $6c,$69,$67,$64,$61,$5f,$5c,$59,$57,$54,$52,$4f,$4c,$4a,$47,$45
!byte $42,$40,$3d,$3b,$39,$36,$34,$32,$2f,$2d,$2b,$29,$27,$25,$23,$21
!byte $1f,$1d,$1b,$1a,$18,$16,$15,$13,$12,$10,$0f,$0d,$0c,$0b,$0a,$09
!byte $08,$07,$06,$05,$04,$03,$03,$02,$01,$01,$01,$00,$00,$00,$00,$00




}


!ifdef RELEASE {
OFFSET=640
koala_pic
 !bin "krawall-wassonst-doubled.kla",$1f40-8*OFFSET,2+8*OFFSET

}else{
OFFSET=0

*=koala_pic
 !fill (8*OFFSET),0
 !bin "krawall-wassonst-doubled.kla",$1f40-8*OFFSET,2
}

!ifdef RELEASE {
 !fill  255,0
koala_screen
 !bin "krawall-wassonst-doubled.kla",$3e8-OFFSET,$1f40+2+OFFSET
}else{
*=koala_screen
 !fill OFFSET,0
 !bin "krawall-wassonst-doubled.kla",$3e8-OFFSET,$1f40+2
}


!ifdef RELEASE {
koala_color
 !bin "krawall-wassonst-doubled.kla",$3e8-OFFSET,$1f40+$3e8+2+OFFSET
}else{
*=koala_color
 !fill OFFSET,0
 !bin "krawall-wassonst-doubled.kla",$3e8-OFFSET,$1f40+$3e8+2
}





