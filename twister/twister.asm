	;free space $2000-$3bff

	!ifdef RELEASE {
        !src "config.asm"
        !src "../framework/framework.inc"
	!to "twister.plain",plain
        * = PARTORG
        } else {
        !to "twister.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
        }
        !cpu 6510

	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}

	!macro asr {
	cmp #$80
	ror
	}

	!macro align16 {
		* = (*+$f) & $fff0
	}

	!macro align256 {
		* = (*+$ff) & $ff00
	}

	!macro align512 {
		* = (*+$1ff) & $fe00
	}

dd00values = $0110
d016values = $0200
agspvalues = $0300


CODE_INX = $e8
CODE_INY = $c8
CODE_DEX = $ca
CODE_DEY = $88

	ptr1 = $02
	tmp = $04
	tmp2 = $05
	tmp3 = $06
	tmp4 = $07


FIRST_LINE = $30
NUMBER_OF_LINES = 200

blah2 = $e8

	!ifdef RELEASE {
	} else {
	jmp go
	* = $1000
sound:
	;!bin "TRSI_on_Mars.sid",,126
	!bin "Wip3Out_2k6.sid",,126

	* = $8000
	}
go:
	!ifdef RELEASE {
	} else {
	sei
	}
	lda #$77
	sta $d011
	lda #$30
	sta $01
	jsr init_data
	jsr gen_code1
	jsr gen_code2
	;clear sprites
	ldx #$00
	txa
-	sta $3dc0,x
	sta $3ec0,x
	inx
	bne -
	lda #$35
	sta $01

;	ldx #$00
;-
;	lda sine1,x
;	+asr
;	sta sine1,x
;	lda sine1+$100,x
;	+asr
;	sta sine1+$100,x
;	inx
;	bne -
	ldx #$ff
	txs
	lda #$00
	sta $ed
	sta blah2
	lda #>sine1
	sta $e9
	lda #$05
	sta $dc
	jmp start


	; sprite scroller stuff
	!macro init_sprites {
	ldx #$0e
	ldy #$07
-	lda #$fb
	sta $d001,x
	lda d000tab,y
	sta $d000,x
	lda #$01
	sta $d027,y
	tya
	clc
	adc #$f7
	sta $3ff8,y
	dey
	dex
	dex
	bpl -
	ldx #$00
	txa
-	sta $3dc0,x
	sta $3ec0,x
	lda #$$ff
	;sta $d015
	sta $d01d
	lda #$e0
	sta $d010

	!for i, 8 {
	ldx #>scrolltab_low
	ldy #>scrolltab_high
	lsr
	bcc +
	sty scroll_sprites_offset + (scrollsprite_looplen * (i-1)) + tableaddr_offset
	jmp ++
+	stx scroll_sprites_offset + (scrollsprite_looplen * (i-1)) + tableaddr_offset
++
        }
	}

	!macro scrollsprite .index {
.scrollsprite:
	ldy $d000+(.index *2)
.tableaddr:
	!set tableaddr_offset = *+2 - .scrollsprite
	lda scrolltab_low,y
	asl
	sta $d000+(.index *2)
	bcc +
	lda $d010
	eor #1<<.index
	sta $d010
        lda .tableaddr+2
        eor #$01
        sta .tableaddr+2
	!set scrollsprite_looplen = *-.scrollsprite
+	}

	!macro scroll_sprites {
scroll_sprites_offset:
	+scrollsprite 0
	+scrollsprite 1
	+scrollsprite 2
	+scrollsprite 3
	+scrollsprite 4
	+scrollsprite 5
	+scrollsprite 6
	+scrollsprite 7
	dec $dc
	bne .no_new_char
+	lda #08
	sta $dc
	ldx $ed
	lda character_tab_low,x
	sta $ee
	lda character_tab_high,x
	sta $ef
scrollptr:
	ldx scrolltext
	bne .do_scroll
	jsr deactivate
	ldx scrolltext
	bne +
.do_scroll
	inc scrollptr+1
	bne +
	inc scrollptr+2
+	lda font_addr_low,x
	sta $eb
	lda font_addr_high,x
	sta $ec
	!for i, 8 {
	!if (i=2) {
	iny
	} else {
	!if (i=3) {
	dey
	} else {
	ldy #(i-1)
	}
	}
	lda ($eb),y
	!if (i!=1) {
	ldy #(i-1)*3
	}
	sta ($ee),y
	}
	lda #number_of_chars
	isc $ed
	bcs .no_new_char
	lda #$00
	sta $ed
.no_new_char:
	}

character_tab_low:
	!byte <$3f80, <$3f81, <$3f82
	!byte <$3dc0, <$3dc1, <$3dc2
	!byte <$3e00, <$3e01, <$3e02
	!byte <$3e40, <$3e41, <$3e42
	!byte <$3e80, <$3e81, <$3e82
	!byte <$3ec0, <$3ec1, <$3ec2
	!byte <$3f00, <$3f01, <$3f02
	!byte <$3f40, <$3f41, <$3f42
number_of_chars = * - character_tab_low

character_tab_high:
	!byte >$3f80, >$3f81, >$3f82
	!byte >$3dc0, >$3dc1, >$3dc2
	!byte >$3e00, >$3e01, >$3e02
	!byte >$3e40, >$3e41, >$3e42
	!byte >$3e80, >$3e81, >$3e82
	!byte >$3ec0, >$3ec1, >$3ec2
	!byte >$3f00, >$3f01, >$3f02
	!byte >$3f40, >$3f41, >$3f42

	+align256
font:
	!bin "sm",,2
	!bin "bg",,2

font_addr_low:
	!for i, 128 {
	!byte <(font + (i-1) *8)
	}
font_addr_high:
	!for i, 128 {
	!byte >(font + (i-1) *8)
	}

	

FIRST_SPR = $1f

d000tab:
	!byte FIRST_SPR, FIRST_SPR+48, FIRST_SPR+48*2, FIRST_SPR+48*3
	!byte FIRST_SPR+48*4, <(FIRST_SPR+48*5), <(FIRST_SPR+48*6), <(FIRST_SPR+48*7)

	; sprite scroller stuff ends here

	;* = $4000
	;!bin "gfx",$4000,0


init_agsp_tabs:

	; initialize the AGSP scroll tables

        ldx #$00
-       txa
        and #$07
        eor #$07
        ora #$d0
        sta d016values,x
        inx
        bne -

        ldy #00
-       tya
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        inx
        sta agspvalues,x
        iny
        inx
        bne -
	rts

	!ifdef RELEASE {
	} else {
init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #%00000000
        sta $dc0f
        lda #%10000000
        sta $dc0e
        lda #$82
        sta $dc0d
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dc04
        lda #>(63-1)
        sta $dc05
	; but not start it yet !

	+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	nop
	nop
	nop
        ldy #%10000001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
        sty $dc0e
	+check_same_page_end
	rts
	}

stable_tab_irq:

	; table needed for stable interrupt

	+check_same_page_start
	!for i,$2a {
        !byte 0
        }
        !byte 9,8,7,6,5,4,3,2,1,0
        !for i,63-$2a-10 {
        !byte 0
	}
	+check_same_page_end



        !macro set_line_pos i {
.start:
	; after fade in here stands "lda #((FIRST_LINE+i)& 07) +$18"
	lda #((FIRST_LINE+i)& 07) +$78 ; 2 cycles
	sta $d011		       ; 4 cycles
	lda <$02+(NUMBER_OF_LINES+1-i) ; 2 cycles
	sta $d018		       ; 4 cycles
        asl			       ; 2 cycles
	sax $dd00                      ; 4 cycles (add maximal 3 cycless because of store,
				       ;			ends after bad line)
	;no nop here because function should be 16 bytes long
        cmp #$00		       ; 2 cycles
!set line_pos_len = *-.start           ; = 20 cycles (or 21 because of page crosssing
	;because of the store before badline 23 cycles + 40 cacless badline -> 64 cycles
        }

code_snippets_2:
	+set_line_pos 1
code_snippets_2_len = * - code_snippets_2

gen_code2:
	lda #<line_select_pos
	sta ptr1
	lda #>line_select_pos
	sta ptr1+1
	ldy #$00
	lda #NUMBER_OF_LINES
	sta tmp
--	ldx #0
-	lda code_snippets_2,x
	sta (ptr1),y
	iny
	bne +
	inc ptr1+1
+	inx
	cpx #code_snippets_2_len
	bne -
	dec code_snippets_2+6
	inc code_snippets_2+1
	bpl +
	lda #$78
	sta code_snippets_2+1
+	dec tmp
	bne --
+	lda #$4c
	sta (ptr1),y
	iny
	bne +
	inc ptr1+1
+	lda #<irq_cont
	sta (ptr1),y
	iny
	bne +
	inc ptr1+1
+	lda #>irq_cont
	sta (ptr1),y
	rts


start:
	!ifdef RELEASE {
        } else {
        sei
        }
	ldx #$00
	stx $d4
        lda #00
        sta $d3
	txa
-	sta $d800,x
	inx
	bne -
	lda #$04
	ldx #30
-	sta $d800,x
	inx
	cpx #50
	bne -

        jsr init_agsp_tabs

	lda #%00000011
	sta $dd02
	lda #$05
	sta $d022
	lda #$0d
	sta $d023
	!ifdef RELEASE {
	} else {
	lda #$36
	sta $01
	lda #$00
	jsr sound
	}
	ldx #$00
-	txa
	and #$7f
	tay
	lda dd00d018tab,y
	sta $02+1,x
	inx
	cpx #NUMBER_OF_LINES
	bne -
	lda #$00
        sta $d020
	;lda #$00
        sta $d021
        lda #$0f
-       sta $d800,x
        sta $d900,x
        sta $da00,x
        sta $db00,x
        inx
        bne -
-	;lda $dc01
	;and #$10
	;bne -
	lda #$35
	sta $01
        lda #$7f
        sta $dc0d
        lda $dc0d
	lda #$78
	sta $d011
	lda #$03
	sta $dd00
	lda #$00
	sta $d0

	!ifdef RELEASE {
	lda $fff8
-	cmp $fff8
	beq -        
	} else {
	jsr init_stable_timer
	lda #$00
-	cmp $d012
	bne -
	}

        sei
        lda #$2f
        sta $d012
        lda #<irq
        sta $fffe
        lda #>irq
        sta $ffff
        lda #$81
        sta $d01a
        sta $d019

 	cli
	;+init_sprites
	!ifdef RELEASE {
	jsr LOAD_NEXT_PART
        ldx #$00
-       lda zwischenpart,x
        sta FRAMEWORK_ADDR-$100,x
        inx
        bne -
	}	       
l:	lda #$00
	beq l
	!ifdef RELEASE {
	jmp FRAMEWORK_ADDR-$100
	} else {
	jmp l
	}

	!ifdef RELEASE {
zwischenpart:
	!pseudopc FRAMEWORK_ADDR-$100 {
        jsr FRAMEWORK_IRQ
	lda #$7b
	sta $d011
        jsr DECRUNCH_NEXT_PART
        jmp START_NEXT_PART
        }
	}


;startup_loop:
;	lda #25
;	beq activate_loop
;	dec startup_loop+1
;	jmp no_deactivate

activate_loop:
	lda #NUMBER_OF_LINES-1
	beq deactivate_loop
addr1:
	lda line_select_pos + (NUMBER_OF_LINES-2)*line_pos_len +1
	and #$1f
addr2:
	sta line_select_pos + (NUMBER_OF_LINES-2)*line_pos_len +1
	dec activate_loop+1
	lda addr1+1
	sec
	sbc #16
	sta addr1+1
	sta addr2+1
	bcs no_deactivate
	dec addr1+2
	dec addr2+2
	jmp no_deactivate
deactivate_loop:
	ldy #$ff
	bmi no_deactivate
	inc slowdown+1
slowdown:
	lda #$00
	lsr
	bcc no_deactivate
	sty $d5
	lda #$00
	tay
	asl $d5
	rol
	asl $d5
	rol
	asl $d5
	rol
	asl $d5
	rol
	sta $d6
	lda $d5
	clc
	adc #<(line_select_pos-line_pos_len+1 + (NUMBER_OF_LINES)*line_pos_len/2)
	sta $d7
	lda $d6
	adc #>(line_select_pos-line_pos_len+1 + (NUMBER_OF_LINES)*line_pos_len/2)
	sta $d8
	lda ($d7),y
	ora #$70
	sta ($d7),y
	lda #<(line_select_pos-line_pos_len+1 + (NUMBER_OF_LINES)*line_pos_len/2+line_pos_len)
	sec
	sbc $d5
	sta $d7
	lda #>(line_select_pos-line_pos_len+1 + (NUMBER_OF_LINES)*line_pos_len/2+line_pos_len)
	sbc $d6
	sta $d8
	lda ($d7),y
	ora #$70
	sta ($d7),y
	dec deactivate_loop+1
	bne no_deactivate
	lda #$01
	sta l+1

no_deactivate:
move_ptr:
        jmp initial_move
blah1:	ldy #$00
	clc
        ;inc $d020
	jsr code1 ; generated
	;dec $d020
	inc blah1+1

	lax blah2
	sbx #3
	stx blah2
	rts

deactivate:
	!ifdef COMMMENTED_OUT {
	lda #$00
	sta scrolltext+1
	lda #$20
	sta scrolltext
	lda #<scrolltext_fadeoff
	sta scrollptr+1
	lda #>scrolltext_fadeoff
	sta scrollptr+2
	}
	lda #NUMBER_OF_LINES/2
	sta deactivate_loop+1
	rts

switch_off_irq:
	sta switch_off_irq_old_a+1
	inc $d019
	;lda #$00
	;sta $d015
        lda #$2f
        sta $d012
        lda #<irq
        sta $fffe
        lda #>irq
        sta $ffff
	!ifdef COMMENTED_OUT {
spritesinpos:
	lda spritesin
	clc
spritesinpos2:
	adc spritesin
	lsr
	clc
	adc #$fb
	!for i,8 {
	sta $d001 + ((i-1)*2)
	}
	dec spritesinpos+1
	lda spritesinpos2+1
	sec
	sbc #$03
	sta spritesinpos2+1
}
switch_off_irq_old_a:
	lda #$00
rti_addr:
	rti


	+align512
scrolltab_low:
	!for i, 256 {
	!if i < 3 {
		!byte ($f6/2) | $80
	} else {
		!byte (i-3)/2
	}
	}

scrolltab_high:
	!for i, 256 {
	!if i < 3 {
		!byte $ff
	} else {
	!if i > 0xf8-23 {
		!byte (i-2)/2
	} else {
	!if i = 0xf8-23 {
		!byte ($80-25)/2
	}
	!if i = 0xf8-24 {
		!byte ($80-25)/2
	} else {
		!byte (i-3)/2
	}
	}
	}
	}

	+align256
sine1:
	!bin "sine1"
	!bin "sine1"
	+align256
;d018tab:
;	!bin "d018tab"
;	!bin "d018tab"
;dd00tab:
;	!bin "dd00tab"
;	!bin "dd00tab"
dd00d018tab:
	!bin "dd00d018_tab"
	!bin "dd00d018_tab"
	+align512
d016tab:
	!bin "d016table"
agsptab:
	!bin "agsptable"

	!ifdef COMMENTED_OUT {
spritesin:
	!bin "spritesin"
	}

codetablow:
        ;!byte <nothing
	!byte <new_red
	!byte <new_blue
	;!byte <new_white
	!byte <new_white2

codetabhigh:
	!byte >new_red
        ;!byte >nothing
	!byte >new_blue
	;!byte >new_white
	!byte >new_white2


new_blue:
	ldx #00
	lda #$06
-	sta $d800,x
	inx
	cpx #10
	bne -
        ;jmp blah1
new_white:
	ldx #10
	lda #$09
-	sta $d800,x
	inx
	cpx #30
	bne -
nothing:
        jmp blah1

new_white2:
	ldx #50
	lda #$0f
-	sta $d800,x
	inx
	cpx #70
	bne -
	jmp activate_new_movecode

new_red:
	ldx #70
	lda #$02
-	sta $d800,x
	inx
	cpx #80
	bne -
        jmp blah1




initial_move:
	ldx $d3
d016tabhigh:
	lda d016tab,x
	sta d016value+1
agsptabhigh:
	lda agsptab,x
	sta xhigh_coord+1
;-	lda $d011
;	bmi -
	;inc $d020
	;dec $d020
        
initial_timer:
        lda #110
        beq +
        dec initial_timer+1
        eor #$ff
        tax
        inx
        lda d016tab+$100,x
        sta d016value+1
        lda agsptab+$100,x
        sta xhigh_coord+1
        jmp blah1

+       inc $d3
	bne +
	lda d016tabhigh+2
	eor #$01
	sta d016tabhigh+2
	lda agsptabhigh+2
	eor #$01
	sta agsptabhigh+2
+       lda $d3
	cmp #$80
	bne +
	ldx $d4
	lda codetabhigh,x
	sta call+2
	lda codetablow,x
	sta call+1
	inc $d4
call	jmp 0
+       jmp blah1


        
activate_new_movecode:
        lda #<new_movecode
        sta move_ptr+1
        lda #>new_movecode
        sta move_ptr+2
        lda #$98
        sta $d3
        lda #$40
        sta $d4
	lda #$02
	sta $d2
        jmp blah1

new_movecode:
        ldx $d3
        lda bigsin_low,x
        sta $da
        lda bigsin_high,x
        sta $db
        ldx $d4
        lda littlesin,x
        bpl posi
        adc $da
        sta $da
        bcs +
        dec $db
        bcc +
posi:   adc $da
        sta $da
        bcc +
        inc $db
+       lda $db    
        lsr
        lax $da
        ror
        tay
        lda d016values,x
        sta d016value+1
        lda agspvalues,y
        sta xhigh_coord+1
        inc $d3
        lda $d3
        cmp #bigsin_length
        bcc +
        lda #$00
        sta $d3
	dec $d2
	bne +
	jsr deactivate
+       inc $d4
        lda $d4
        cmp #littlesin_len
        bcc +
        lda #$00
        sta $d4
+
        jmp blah1



	+align256

	+check_same_page_start
make_sprite_big_irq:
	sta make_sprite_big_irq_old_a+1
	stx make_sprite_big_irq_old_x+1
        inc $d019
        ldx $dc04
	lda stable_tab_irq,x
	sta bpl_addr3+1
bpl_addr3:
	bpl bpl_addr3
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
	+check_same_page_end
	inc $d019
	lda #$ff
	sta $d017
	lda $d012
	clc
	adc #$07
	sta $d012
	lda #<make_sprite_small_irq
	sta $fffe
	lda #>make_sprite_small_irq
	sta $ffff
make_sprite_big_irq_old_x:
	ldx #$00
make_sprite_big_irq_old_a:
	lda #$00
	rti

	+check_same_page_start
make_sprite_small_irq:
	sta make_sprite_small_irq_old_a+1
	stx make_sprite_small_irq_old_x+1
	inc $d019
        ldx $dc04
	lda stable_tab_irq,x
	sta bpl_addr4+1
bpl_addr4:
	bpl bpl_addr4
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
	+check_same_page_end
	lda #$00
	sta $d017
	lda #$77
	sta $d011
	lda #$00
	sta $d012
	lda #<switch_off_irq
	sta $fffe
	lda #>switch_off_irq
	sta $ffff
make_sprite_small_irq_old_x:
	ldx #$00
make_sprite_small_irq_old_a:
	lda #$00
	rti


	+align256

bigsin_low:
        !bin "bigsin_low"
bigsin_length = * - bigsin_low
bigsin_high:
        !bin "bigsin_high"
littlesin:
        !bin "littlesin"
littlesin_len = *-littlesin

	!ifdef COMMENTED_OUT {
scrolltext:
	!ct scr {
	!text "                   "
        !text "Wir gingen die paar hundert Meter bis zu seiner Villa "
        !text "und als wir ankamen war er schon so high wie'n Weltmeister. "
        !text "Er taumelte vor mir her in eine riesige Diele auf eine "
        !text "erlesene Sitzecke zu "
        !byte 0
        }

scrolltext_fadeoff:
	!ct scr {
	!text " ..."
	!for i, 80 {
	!byte $20
	}
	!byte 0
	}
	}

irq_cont:
	lda #$77 ; #$f7
	sta $d011
	lda #$03
	sta $dd00
	lda #$f0
	sta $d018
	!ifdef COMMENTED_OUT {
	lda #<make_sprite_big_irq
	sta $fffe
	lda #>make_sprite_big_irq
	sta $ffff
	;+scroll_sprites
	lda #$04
	sta $d012
	} else {
	lda #$00
	sta $d012
	lda #<switch_off_irq
	sta $fffe
	lda #>switch_off_irq
	sta $ffff
	}
	cli
	!ifdef RELEASE {
	jsr PLAY_MUSIC
	} else {
	jsr sound+3
	}
	jsr activate_loop
irq_old_y:
	ldy #$00
irq_old_a:
	lda #$00
irq_old_x:
	ldx #$00
        rti

	+align256
	; avoid page cross !
irq:    sta irq_old_a+1
        stx irq_old_x+1
        inc $d019
        ldx $dc04
	lda stable_tab_irq,x
	sta bpl_addr2+1
bpl_addr2:
	bpl bpl_addr2

	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        ;stable here
	;lda #$ff
	;sta $d015
	nop
	nop
	nop
	sty irq_old_y+1
	ldx #$03
	bit $ea
d016value:
        lda #$d0
	sta $d016
xhigh_coord:
        bmi xhigh_coord+2
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        lda #$78
        sta $d011
line_select_pos:
	;!for i, NUMBER_OF_LINES {
        ;+set_line_pos i
	;}
	;jmp irq_cont

line_select_pos_new:

code_snippets_1:
	lda sine1,y
	adc (blah2),y
	tax
	lda dd00d018tab,x
code_snippets_1_zp_addr:
	sta $02+(NUMBER_OF_LINES)
	iny
code_snippets_1_len = * - code_snippets_1

gen_code1:
	ldy #<code1
	sty ptr1
	lda #>code1
	sta ptr1+1
	lda #NUMBER_OF_LINES
	sta tmp
--	ldx #0
-	lda code_snippets_1,x
	sta (ptr1),y
	iny
	bne +
	inc ptr1+1
+	inx
	cpx #code_snippets_1_len
	bne -
	dec code_snippets_1_zp_addr+1
	dec tmp
	bne --
	cpy #$00
	beq +
	dey
+	lda #$60
	sta (ptr1),y
	rts

	;* = $c000
	;!bin "gfx",$4000,$8000
init_data:
	!src "data.asm"

code1 = $ad00

