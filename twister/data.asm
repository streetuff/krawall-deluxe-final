CALC_HIGHEST_ADDR = $dcf8
CALC_CHARS_WIDTH = 80
; copy the charset data
	ldx #<CALC_HIGHEST_ADDR
	ldy #CALC_CHARS_WIDTH
load_addr:
	lda data_end-CALC_CHARS_WIDTH-1,y
store_addr:
	sta CALC_HIGHEST_ADDR & $ff00,x
	txa
	sbx #$08
	bcs +
do_dec_store:
	dec store_addr+2
	lda store_addr+2
	cmp #$3f
	beq copy_end
	cmp #$bf
	bne +
	lda #$7f
	sta store_addr+2
+	cpx #CALC_CHARS_WIDTH-8
	bne no_store_addr_check
	lda store_addr+2
	and #$07
   	bne +
	; wrap around in charset
	ldx #$78 ; maximum reached char ((f9 * 8) & 0xff)
	bne do_dec_store ; unconditional
+	and #$03
	bne no_store_addr_check
	ldx #$f8
	bne do_dec_store ; unconditional
no_store_addr_check:
	tya
	bne +
	dec load_addr+2
+	dey
	jmp load_addr
copy_end:

;calculate screen content
	lda #<$4000
	sta ptr1
	lda #>$4000
	sta ptr1+1
	lda #2 ; number of $dd00 pages
	sta tmp2
	;lda #12
	;sta tmp3

---	;lda tmp3
	;cmp #$06
	;bcs +
	;!byte $2c
+	lda #12
	sta tmp

	ldx #20
--	txa
   sec
   sbc #10
   sta tmp4
   adc #20-1
   cmp #$82
   bne +
   lda #$8c
+  sta tmp3
   ldy #$00
-	txa
	sta (ptr1),y
	inx
	cpx #$80
	bne +
	txa
	sbx #(-CALC_CHARS_WIDTH/8)
+	   cpx tmp3
   bne +
   ldx tmp4
+  iny
	cpy #80 ; width
	bne -
   txa
   sbx #-20
   cpx #$8c
   bne +
   ldx #$96
+
	lda ptr1+1
	sec
	sbc #-$04
	sta ptr1+1
	dec tmp
	bne --
   lda #$c0
   sta ptr1+1
   ;lax tmp3
   ;sbx #$08
   ;stx tmp3
   dec tmp2
   bne ---
   rts
   !byte $00, $00, $01, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $80, $00, $00, $00, $00, $00, $bf
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $c0, $00, $00
   !byte $00, $00, $00, $7f, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $d0, $00, $00, $00, $00, $00, $7f
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $e0, $00, $00
   !byte $00, $00, $00, $2f, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $e0, $00, $00, $00, $00, $00, $1f
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $f4, $00, $00
   !byte $00, $00, $00, $0f, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $f4, $00, $00, $00, $00, $00, $0b
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $fc, $00, $00
   !byte $00, $00, $00, $03, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $fc, $00, $00, $00, $00, $00, $02
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $fd, $00, $00
   !byte $00, $00, $00, $02, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $fe, $00, $00, $00, $00, $00, $00
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $40, $00
   !byte $00, $00, $00, $00, $7f, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $40, $00, $00, $00, $00, $00
   !byte $7f, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $c0, $00
   !byte $00, $00, $00, $00, $3f, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $80, $00, $00, $00, $40, $00
   !byte $1f, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $c0, $00
   !byte $00, $01, $00, $00, $0f, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $d0, $00, $00, $04, $40, $00
   !byte $0b, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $e0, $00
   !byte $00, $01, $00, $00, $03, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $f0, $00, $00, $05, $40, $00
   !byte $02, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $f4, $00
   !byte $00, $05, $00, $00, $01, $ff, $ff, $ff
   !byte $ff, $bf, $ff, $bf, $ff, $bf, $ff, $ff
   !byte $ff, $ff, $f4, $00, $00, $05, $44, $00
   !byte $00, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $fc, $00
   !byte $00, $15, $10, $00, $00, $7f, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $fb, $ff, $ff
   !byte $ff, $ff, $fc, $00, $00, $15, $54, $00
   !byte $00, $7f, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $fd, $00
   !byte $00, $15, $10, $00, $00, $2f, $ff, $ff
   !byte $bf, $bf, $bb, $bf, $bf, $bf, $bb, $bf
   !byte $bf, $ff, $fd, $00, $00, $59, $44, $00
   !byte $00, $1f, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $fe, $00
   !byte $00, $25, $10, $00, $00, $0b, $ff, $ff
   !byte $fb, $fb, $fb, $fb, $fb, $fb, $fb, $fb
   !byte $ff, $ff, $fe, $00, $00, $99, $54, $40
   !byte $00, $07, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $40
   !byte $00, $55, $11, $00, $00, $03, $ff, $ff
   !byte $ff, $bb, $bb, $bb, $bb, $bb, $bb, $bb
   !byte $bf, $ff, $ff, $40, $00, $99, $54, $44
   !byte $00, $02, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $80
   !byte $01, $a5, $51, $10, $00, $00, $ff, $ff
   !byte $fb, $bb, $bb, $bb, $bb, $bb, $bb, $bb
   !byte $fb, $ff, $ff, $80, $01, $e9, $54, $44
   !byte $00, $00, $bf, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $c0
   !byte $02, $a5, $11, $10, $00, $00, $3f, $ff
   !byte $fb, $bb, $bb, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $ff, $ff, $c0, $02, $99, $55, $44
   !byte $40, $00, $2f, $ff, $ff, $ef, $ef, $ef
   !byte $ef, $ef, $ef, $ef, $ef, $ff, $ff, $d0
   !byte $02, $a5, $51, $11, $00, $00, $1f, $ff
   !byte $fb, $bb, $bb, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $ff, $ff, $e0, $06, $e9, $55, $54
   !byte $40, $00, $0b, $ff, $ff, $fe, $fe, $fe
   !byte $ee, $fe, $ee, $fe, $fe, $ff, $ff, $e0
   !byte $07, $a5, $51, $11, $00, $00, $03, $ff
   !byte $ff, $bb, $bb, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $bf, $ff, $e0, $0b, $a9, $55, $55
   !byte $44, $00, $02, $ff, $ff, $ee, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ef, $ff, $ff, $e0
   !byte $0b, $a5, $55, $51, $10, $00, $01, $ff
   !byte $ff, $bb, $bb, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $bf, $ff, $f0, $0e, $e9, $95, $55
   !byte $54, $40, $00, $ff, $ff, $fe, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ff, $ff, $f0
   !byte $1f, $a6, $55, $11, $11, $00, $00, $3f
   !byte $ff, $bb, $ab, $ab, $ab, $ab, $ab, $ab
   !byte $ab, $bb, $ff, $f4, $1e, $e9, $55, $55
   !byte $55, $44, $00, $2f, $ff, $ee, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ef, $ff, $f8
   !byte $2f, $a5, $55, $55, $51, $10, $00, $2f
   !byte $ff, $bb, $ba, $ba, $ba, $ba, $ba, $ba
   !byte $ba, $bb, $ff, $f8, $2e, $e9, $95, $55
   !byte $55, $54, $00, $0f, $ff, $fe, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ef, $ff, $fc
   !byte $3f, $a6, $55, $15, $55, $11, $00, $07
   !byte $ff, $bb, $ab, $aa, $ab, $aa, $aa, $aa
   !byte $ab, $ab, $ff, $fc, $2f, $e9, $95, $55
   !byte $55, $54, $40, $07, $ff, $ee, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ef, $ff, $fc
   !byte $3f, $a6, $55, $55, $55, $51, $10, $02
   !byte $ff, $fb, $ba, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $bb, $ff, $fc, $7f, $e9, $99, $95
   !byte $95, $95, $54, $00, $ff, $fe, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ee, $ff, $fd
   !byte $3f, $a6, $55, $55, $55, $55, $10, $00
   !byte $7f, $bb, $aa, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $ab, $bf, $fd, $7f, $ea, $99, $59
   !byte $59, $55, $54, $00, $3f, $ee, $ee, $ae
   !byte $ae, $ae, $ae, $ae, $ae, $ae, $ff, $fd
   !byte $7f, $a6, $65, $55, $55, $55, $51, $00
   !byte $2f, $fb, $aa, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $ab, $bf, $fe, $7f, $e9, $99, $99
   !byte $99, $99, $95, $40, $1f, $fe, $ea, $ea
   !byte $ea, $ea, $ea, $ea, $ea, $ee, $ff, $fe
   !byte $7f, $aa, $56, $55, $55, $55, $55, $10
   !byte $07, $bb, $aa, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $bf, $fe, $bf, $ea, $99, $99
   !byte $99, $99, $99, $44, $07, $ee, $ee, $aa
   !byte $ae, $aa, $aa, $aa, $aa, $ae, $ff, $fe
   !byte $7f, $aa, $65, $65, $65, $65, $55, $51
   !byte $02, $fa, $aa, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $bf, $fe, $ff, $ea, $99, $99
   !byte $99, $99, $99, $94, $41, $fe, $ea, $aa
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $ff, $fe
   !byte $bf, $aa, $66, $56, $56, $66, $55, $55
   !byte $10, $7a, $aa, $aa, $6a, $6a, $6a, $6a
   !byte $6a, $6a, $ab, $ff, $ff, $ea, $99, $99
   !byte $99, $99, $99, $95, $44, $6e, $aa, $aa
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $af, $fe
   !byte $bf, $ba, $66, $66, $66, $66, $66, $65
   !byte $51, $2a, $aa, $a6, $a6, $a6, $a6, $a6
   !byte $a6, $aa, $bb, $ff, $ff, $ea, $a9, $a9
   !byte $99, $a9, $99, $99, $95, $5e, $aa, $aa
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $ef, $fe
   !byte $bf, $aa, $66, $66, $66, $66, $66, $66
   !byte $55, $16, $aa, $66, $66, $66, $66, $66
   !byte $66, $66, $ab, $ff, $ff, $ee, $99, $9a
   !byte $9a, $9a, $9a, $99, $99, $56, $aa, $9a
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $af, $fe
   !byte $ff, $ba, $66, $66, $66, $66, $66, $66
   !byte $65, $56, $66, $66, $66, $66, $66, $66
   !byte $a6, $66, $ab, $ff, $ff, $ea, $a9, $a9
   !byte $aa, $aa, $aa, $a9, $a9, $99, $99, $a9
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $af, $fe
   !byte $bf, $ba, $66, $66, $66, $66, $66, $66
   !byte $66, $56, $56, $66, $66, $66, $66, $66
   !byte $66, $66, $6b, $ff, $ff, $ee, $aa, $9a
   !byte $9a, $aa, $9a, $9a, $9a, $99, $99, $99
   !byte $99, $9a, $9a, $9a, $9a, $9a, $af, $ff
   !byte $bf, $fa, $a6, $66, $66, $66, $66, $66
   !byte $66, $66, $55, $66, $66, $66, $66, $66
   !byte $66, $66, $ab, $ff, $ff, $fe, $aa, $aa
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $a5, $99
   !byte $99, $a9, $a9, $a9, $a9, $aa, $ae, $ff
   !byte $bf, $ba, $a6, $66, $66, $66, $66, $66
   !byte $66, $6a, $a5, $15, $56, $66, $66, $66
   !byte $66, $66, $6b, $bf, $bf, $ee, $aa, $aa
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $a9, $55
   !byte $59, $99, $99, $99, $99, $9a, $ae, $ff
   !byte $bf, $fa, $a6, $a6, $aa, $a6, $a6, $a6
   !byte $a6, $aa, $a9, $11, $55, $66, $66, $66
   !byte $66, $66, $ab, $bf, $bf, $fe, $ea, $aa
   !byte $aa, $aa, $aa, $aa, $aa, $aa, $ed, $45
   !byte $59, $99, $99, $99, $99, $99, $aa, $fe
   !byte $7f, $fa, $aa, $6a, $6a, $aa, $aa, $aa
   !byte $6a, $aa, $ab, $01, $15, $56, $56, $56
   !byte $56, $56, $6b, $bf, $bf, $fe, $aa, $aa
   !byte $aa, $aa, $aa, $aa, $aa, $ae, $ef, $84
   !byte $55, $59, $99, $99, $99, $99, $aa, $fe
   !byte $7f, $fa, $aa, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $bb, $d0, $11, $55, $65, $65
   !byte $65, $66, $6a, $be, $7f, $fe, $ea, $ea
   !byte $aa, $ea, $aa, $aa, $aa, $ee, $ef, $e0
   !byte $45, $59, $99, $99, $99, $99, $aa, $fe
   !byte $3f, $fb, $aa, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $bb, $f0, $01, $15, $55, $55
   !byte $55, $56, $6b, $be, $7f, $fe, $ee, $aa
   !byte $ae, $ae, $aa, $aa, $ae, $ae, $ef, $fc
   !byte $04, $55, $59, $99, $99, $99, $ae, $fe
   !byte $7f, $ff, $ba, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $bb, $fc, $00, $11, $55, $55
   !byte $55, $66, $6b, $be, $7f, $ff, $ee, $ea
   !byte $ee, $ea, $ee, $ee, $ee, $ee, $ef, $fd
   !byte $00, $55, $55, $95, $99, $99, $aa, $fd
   !byte $3f, $ff, $aa, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $ab, $bf, $00, $01, $15, $55
   !byte $55, $56, $6b, $bd, $3f, $ff, $ee, $ee
   !byte $ae, $ae, $ae, $ae, $ae, $ae, $ef, $ff
   !byte $80, $05, $55, $55, $55, $59, $ae, $fd
   !byte $3f, $ff, $ba, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $bb, $ff, $c0, $01, $11, $55
   !byte $55, $55, $6b, $fd, $2f, $ff, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ee, $ef, $ff
   !byte $e0, $00, $55, $55, $55, $99, $aa, $fc
   !byte $2f, $ff, $bb, $aa, $aa, $aa, $aa, $aa
   !byte $aa, $aa, $ab, $bf, $f0, $00, $11, $15
   !byte $15, $55, $6b, $b8, $1f, $ff, $ee, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ee, $ef, $ff
   !byte $f8, $00, $45, $55, $55, $59, $aa, $fc
   !byte $2f, $ff, $fb, $ba, $ba, $ba, $ba, $ba
   !byte $ba, $ba, $bb, $ff, $f8, $00, $11, $11
   !byte $55, $55, $6a, $b8, $1f, $ff, $fe, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ee, $ef, $ff
   !byte $fd, $00, $04, $55, $55, $55, $aa, $f8
   !byte $1f, $ff, $fb, $ab, $ab, $ab, $ab, $ab
   !byte $ab, $ab, $ab, $bf, $fe, $00, $01, $11
   !byte $15, $15, $6a, $b4, $0f, $ff, $fe, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ee, $ee, $ff
   !byte $ff, $80, $00, $45, $55, $55, $9a, $f4
   !byte $0b, $ff, $fb, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $bb, $bb, $bf, $ff, $80, $00, $11
   !byte $11, $55, $6a, $b0, $0b, $ff, $fe, $fe
   !byte $ee, $ee, $ee, $ee, $ee, $ee, $ee, $ff
   !byte $ff, $d0, $00, $44, $55, $55, $aa, $e0
   !byte $07, $ff, $ff, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $bb, $bb, $bf, $ff, $e0, $00, $01
   !byte $11, $15, $6a, $a0, $07, $ff, $ff, $ee
   !byte $ee, $ee, $ee, $ee, $ee, $ee, $ef, $ff
   !byte $ff, $f0, $00, $04, $45, $55, $9a, $d0
   !byte $03, $ff, $ff, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $bb, $bb, $bb, $ff, $f8, $00, $00
   !byte $11, $11, $66, $90, $07, $ff, $ff, $fe
   !byte $fe, $fe, $fe, $fe, $fe, $fe, $ff, $ff
   !byte $ff, $fc, $00, $00, $55, $55, $aa, $d0
   !byte $03, $ff, $ff, $bb, $bb, $bb, $bb, $bb
   !byte $bb, $bb, $bb, $bb, $ff, $fe, $00, $00
   !byte $01, $15, $56, $80, $02, $ff, $ff, $ff
   !byte $ef, $ef, $ef, $ef, $ef, $ef, $ef, $ff
   !byte $ff, $ff, $00, $00, $04, $55, $5a, $80
   !byte $02, $ff, $ff, $fb, $bb, $bb, $bb, $bb
   !byte $bb, $bb, $bb, $bb, $ff, $ff, $80, $00
   !byte $01, $11, $66, $40, $01, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $c0, $00, $04, $55, $9a, $80
   !byte $00, $ff, $ff, $fb, $bb, $bb, $bb, $bb
   !byte $bb, $bb, $bb, $bb, $ff, $ff, $d0, $00
   !byte $01, $11, $56, $00, $00, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $e0, $00, $04, $45, $5a, $00
   !byte $00, $7f, $ff, $fb, $fb, $bb, $fb, $bb
   !byte $fb, $bb, $fb, $fb, $ff, $ff, $f0, $00
   !byte $00, $11, $65, $00, $00, $7f, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $fc, $00, $00, $45, $99, $00
   !byte $00, $3f, $ff, $ff, $bb, $bf, $bb, $bb
   !byte $bb, $bb, $bb, $bb, $bf, $ff, $fc, $00
   !byte $00, $01, $54, $00, $00, $2f, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $fd, $00, $00, $05, $58, $00
   !byte $00, $2f, $ff, $ff, $fb, $fb, $fb, $fb
   !byte $fb, $fb, $fb, $fb, $ff, $ff, $ff, $00
   !byte $00, $11, $54, $00, $00, $1f, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $40, $00, $05, $54, $00
   !byte $00, $1f, $ff, $ff, $ff, $bf, $bf, $bf
   !byte $bf, $bf, $bf, $bf, $ff, $ff, $ff, $80
   !byte $00, $01, $10, $00, $00, $0f, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $c0, $00, $04, $50, $00
   !byte $00, $07, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $e0
   !byte $00, $01, $10, $00, $00, $07, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $f0, $00, $00, $40, $00
   !byte $00, $03, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $f4
   !byte $00, $00, $00, $00, $00, $02, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $fc, $00, $00, $40, $00
   !byte $00, $02, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $fd
   !byte $00, $00, $00, $00, $00, $01, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $fe, $00, $00, $00, $00
   !byte $00, $00, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $00, $00, $00, $00, $00, $00, $bf, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $80, $00, $00, $00
   !byte $00, $00, $7f, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $80, $00, $00, $00, $00, $00, $7f, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $c0, $00, $00, $00
   !byte $00, $00, $2f, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $e0, $00, $00, $00, $00, $00, $1f, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $e0, $00, $00, $00
   !byte $00, $00, $1f, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $f4, $00, $00, $00, $00, $00, $0b, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $fc, $00, $00, $00
   !byte $00, $00, $07, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $fc, $00, $00, $00, $00, $00, $07, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $fd, $00, $00, $00
   !byte $00, $00, $03, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $fe, $00, $00, $00, $00, $00, $02, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
   !byte $ff, $ff, $ff, $ff, $ff, $00, $00, $00
data_end:
