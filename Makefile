DEMONAME = krawall
DEMOYEAR = 2012
DIRS = fishtro mc_player mc_anim_0 mc_anim_1 mc_anim_2  bouncer kefrens twister \
        monkey vector vbars greetings vertical bigpic

SHELL = /bin/bash

all:    bin/$(DEMONAME).d64

files = framework/framework.prg $(foreach dir,$(DIRS), $(dir)/$(dir)+header.prg)

framework/framework.prg:
	@make -C framework && cp framework/framework.prg bin/framework;
	
subdirs:
	@hexdigits=( {0..9} {A..Z} ); \
	((number = 0)); \
	for i in $(DIRS) ; do \
		rm -f bin/$${hexdigits[$$number]}$$i; \
		$(MAKE) -C $$i release; \
		if [ $$? != 0 ]; then exit -1; fi; \
		cp $$i/$$i+header.prg bin/$${hexdigits[$$number]}$$i; \
                ((number = number + 1)); \
	done

$(files):	subdirs

bin/$(DEMONAME).d64:	$(files)
	@hexdigits=( {0..9} {A..Z} ); \
	uppername=`echo $(DEMONAME) | tr \"[:lower:]\" \"[:upper:]\"`;\
	string="cc1541 -n \"$$uppername\" -i \"$(DEMOYEAR)\" -f $$uppername -w bin/framework";\
	((number = 0)); \
	for i in $(DIRS) ; do \
        	string=$$string" -f "$${hexdigits[$$number]}" -w "bin/$${hexdigits[$$number]}$$i; \
                ((number = number + 1)); \
        done; \
	`echo $$string bin/$(DEMONAME).d64`

clean:
	@for i in framework $(DIRS) ; do \
        	make -C $$i clean; \
        done
	@rm -f bin/*
	@echo "dummy" >bin/dummy


go:	bin/$(DEMONAME).d64
	x64 bin/$(DEMONAME).d64
