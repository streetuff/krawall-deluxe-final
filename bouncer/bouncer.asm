	;free $9800-$bfff

        !cpu 6510

	!ifdef RELEASE {
        !src "../framework/framework.inc"
        !src "config.asm"
	!to "bouncer.plain",plain
        * = PARTORG
        } else {
        !to "bouncer.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
        dec $01
	jmp start
        }

	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}

	!macro asr {
	cmp #$80
	ror
	}

	!macro align16 {
		* = (*+$f) & $fff0
	}

	!macro align256 {
		* = (*+$ff) & $ff00
	}

	!macro align512 {
		* = (*+$1ff) & $fe00
	}
CODE_ZOOM_CLEAR = 20

CODE_INX = $e8
CODE_INY = $c8
CODE_DEX = $ca
CODE_DEY = $88
CODE_LDA_ABY = $B9
CODE_STA_ZPX = $95
CODE_RTS = $60
CODE_LDA_IMM = $a9
CODE_LDY_IMM = $a0
CODE_STA_ZPX = $95
CODE_STY_ZPX = $94
CODE_TYA = $98
CODE_TAY = $a8
CODE_CLC = $18
CODE_SEC = $18
CODE_ADC_ABS = $6d
CODE_INX = $e8
CODE_BCC = $90
CODE_BIT_ABS = $2c

UNUSED_LINE = $01 ; first byte of "dd00d018_tab"

FIRST_LINE = $30
NUMBER_OF_LINES = 200
ROLL_HEIGHT = 60

GENERATED_CODE = $2000 ; calc_chars
GENERATED_CODE_2 = GENERATED_CODE + $200
GENERATED_CODE_3 = GENERATED_CODE + $400
GENERATED_CODE_4 = GENERATED_CODE + $600
GENERATED_CODE_5 = GENERATED_CODE + $800
GENERATED_CODE_6 = GENERATED_CODE + $a00
GENERATED_CODE_7 = GENERATED_CODE + $c00
GENERATED_CODE_8 = GENERATED_CODE + $e00

GENERATED_ZOOM_CODE = GENERATED_CODE + $1000
GENERATED_PICTURES = GENERATED_ZOOM_CODE + $300

;blah2 = $e8

	!ifdef RELEASE {
        } else {
	* = $b000
sid:
        !bin "TRSI_on_Mars.sid",,126
	* = $8000
        }
start:                
	;ldx #$ff
	;txs
	!ifdef RELEASE {
        } else {
        sei
        }
        lda #$0b
        sta $d011
        lda #$00
        sta $d020
        lda #$30
	sta $01
	jsr calc_chars
        jsr generate_code
	jsr generate_zoom_code
	jsr generate_pictures
	jsr paint_final_picture
	lda #$35
        sta $01
	ldx #$00
	lda #$08
-	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00,x
	inx
	bne -
	lda #$05	;5
	sta $d022
	lda #$03	;d
	sta $d023
        !ifdef RELEASE {
        } else {
	lda #$00
        jsr sid
        }
	ldx #$00
	lda #UNUSED_LINE
-	sta $02+1,x
	inx
	cpx #NUMBER_OF_LINES
	bne -
	lda #$00
        sta $d020
	lda #$0d	;1
        sta $d021
-	;lda $dc01
	;and #$10
	;bne -
	lda #$35
	sta $01
        lda #$7f
        sta $dc0d
        lda $dc0d
	lda #$08
	sta $d011
	;lda #$03
	;sta $dd00
	lda #$00
	sta $d0

	jsr init_stable_timer
	lda #$00
-	cmp $d012
	bne -
        
        lda #$2f
        sta $d012
	!ifdef RELEASE {
        sei
        }
        lda #<irq
        sta $fffe
        lda #>irq
        sta $ffff
        lda #$81
        sta $d01a
        sta $d019
  	cli
	!ifdef RELEASE {
        jsr LOAD_NEXT_PART
	}
        ldx #$00
-       lda zwischenpart,x
        sta $0200,x
        inx
        bne -
waitflag:lda #$00
	beq waitflag
	jsr switch_final_picture
	!ifdef RELEASE {
        jmp $0200
        } else {
l:	jmp l
	} 

zwischenpart:
	!pseudopc $0200 {
	!ifdef RELEASE {
        jsr DECRUNCH_NEXT_PART
-	lda $ffff
	cmp #>zwischenirq
	beq -
        jmp START_NEXT_PART
	}
zwischenirq:
	sta zwischenirq_old_a+1
	stx zwischenirq_old_x+1
	sty zwischenirq_old_y+1
	lda $01
	sta zwischenirq_old_01+1
	lda #$35
	sta $01
	inc $d019
	lda #$01
zwischenirq_left:
	ldx #39
	beq +
	sta SCREENLINE,x
	dec zwischenirq_left+1
+	
zwischenirq_right:
	ldx #$00
	cpx #40
	bcs +
	sta SCREENLINE,x
	inc zwischenirq_right+1
	bne ++
+
	!ifdef RELEASE {
        jsr FRAMEWORK_IRQ
	}
	lda #$7b
	sta $d011
++
	!ifdef RELEASE {
        jsr PLAY_MUSIC
        } else {
	jsr sid+3
        }
zwischenirq_old_01:
	lda #$00
	sta $01
zwischenirq_old_y:
	ldy #$00
zwischenirq_old_x:
	ldx #$00
zwischenirq_old_a:
	lda #$00
	rti
        }
        
	;* = $4000
	;!bin "gfx",$4000,0

	+align256
zoomsizes:
	!bin "zoomsizes"

init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #%00000000
        sta $dc0f
        lda #%10000000
        sta $dc0e
        lda #$82
        sta $dc0d
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dc04
        lda #>(63-1)
        sta $dc05
	; but not start it yet !
 
	+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	nop
	nop
	nop
        ldy #%10000001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
        sty $dc0e
	+check_same_page_end
	rts

stable_tab_irq:

	; table needed for stable interrupt

	+check_same_page_start
	!for i,$28 {
        !byte 0
        }
        !byte 11,10,9,8,7,6,5,4,3,2,1,0
        !for i,63-$28-12 {
        !byte 0
	}
	+check_same_page_end



        !macro set_line_pos i {
.start:
	!if i != NUMBER_OF_LINES {
	lda #((FIRST_LINE+i)& 07) +$18 ; 2 cycles
	} else {
	lda #$7f
	}
	sta $d011		       ; 4 cycles
	lda <$02+i                     ; 3 cycles
	sta $d018		       ; 4 cycles
        asl			       ; 2 cycles
        ora #$fc		       ; 2 cycles
	sax $dd00                      ; 4 cycles (add maximal 3 cycles because of store,
				       ;			ends after bad line)
!set line_pos_len = *-.start           ; = 20 cycles (or 21 because of page crosssing
	;because of the store before badline 23 cycles + 40 cacless badline -> 64 cycles
        }

	+align256
	; avoid page cross !
irq:    sta irq_old_a+1
        stx irq_old_x+1
        inc $d019
        ldx $dc04
	lda stable_tab_irq,x
	sta bpl_addr2+1
bpl_addr2:
	bpl bpl_addr2

	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        ;stable here
	;lda $dd00
	;ora #$03
        nop
        nop
        lda #$03
	tax
	tya
	pha
d016value:
        lda #$d0
	sta $d016
xhigh_coord:
        bmi xhigh_coord+2
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	cmp #$c9
	bit $ea
        
        lda #$78
        lda $d011
line_select_pos:
	!for i, NUMBER_OF_LINES {
        +set_line_pos i
	}
	lda #$77
	lda $d011
	;lda #$03
	;sta $dd00
	;lda #$f0
	;sta $d018
	cli
	lda $f3
	sta old_f3+1
	lda $f4
	sta old_f4+1
	lda $f5
	sta old_f5+1
	lda $f6
	sta old_f6+1
	lda $f7
	sta old_f7+1
	lda $f8
	sta old_f8+1
	lda $f9
	sta old_f9+1
	lda $fa
	sta old_fa+1
	lda $fb
	sta old_fb+1
	lda $fc
	sta old_fc+1
	lda $fd
	sta old_fd+1
	lda $fe
	sta old_fe+1
	lda $ff
	sta old_ff+1
	;inc $d020
move_ptr:
	jsr bounce_1
	lda #$2f
	sta $00
	lda #$35
	sta $01
old_f3:
	lda #0
	sta $f3
old_f4:
	lda #0
	sta $f4
old_f5:
	lda #0
	sta $f5
old_f6:
	lda #0
	sta $f6
old_f7:
	lda #0
	sta $f7
old_f8:
	lda #0
	sta $f8
old_f9:
	lda #0
	sta $f9
old_fa:
	lda #0
	sta $fa
old_fb:
	lda #0
	sta $fb
old_fc:
	lda #0
	sta $fc
old_fd:
	lda #0
	sta $fd
old_fe:
	lda #0
	sta $fe
old_ff:
	lda #0
	sta $ff

	!ifdef RELEASE {
        jsr PLAY_MUSIC
        } else {
	jsr sid+3
        }
	;dec $d020
	pla
	tay
irq_old_a:
	lda #$00
irq_old_x:
	ldx #$00
rti_addr:
        rti

direction:
	!byte 4


bounce_y:
	!byte 204
bounce_y_low:
	!byte 0
bounce_dy:
	!byte 0
bounce_dy_low:
	!byte 10

bounce_y_2:
	!byte 200-64
bounce_y_low_2:
	!byte 0
bounce_dy_2:
	!byte -3
bounce_dy_low_2:
	!byte 0

bounce_counter:
	!byte 1

bounce_roll:
	!byte 0

bounce_roll_2:
	!byte 0

dual_bounce:
	lda bounce_y
	sec
	sbc #$08
	tax
	ldy #$10
	jsr clear

	lda bounce_y
	clc
	adc #ROLL_HEIGHT-8
	tax
	ldy #$10
	jsr clear

	lda bounce_y_2
	sec
	sbc #$08
	tax
	ldy #$10
	jsr clear

	lda bounce_y_2
	clc
	adc #ROLL_HEIGHT-8
	tax
	ldy #$10
	jsr clear

	ldx bounce_y
	ldy bounce_roll
	jsr GENERATED_CODE
	ldx bounce_y_2
	ldy bounce_roll_2
	jsr GENERATED_CODE
	lax bounce_y_2
	dex
	ldy #UNUSED_LINE
	sty $02-1,x
	clc
	adc #2+ROLL_HEIGHT
	tax
	sty $00,x

	dec bounce_roll
	inc bounce_roll_2

	;roll #1

        lda bounce_y_low
        clc
        adc bounce_dy_low
        sta bounce_y_low
        lda bounce_y
        adc bounce_dy
        sta bounce_y
        lda bounce_dy_low
        clc
        adc #10
        sta bounce_dy_low
        bcc +
        inc bounce_dy
+
	lda bounce_y
        cmp #200-ROLL_HEIGHT
        bcc +
        lda bounce_dy
        cmp #$03
        bcc +
        lda #$-4
        sta bounce_dy
        lda #$b8
        sta bounce_dy_low
+

	;roll #2

        lda bounce_y_low_2
        clc
        adc bounce_dy_low_2
        sta bounce_y_low_2
        lda bounce_y_2
        adc bounce_dy_2
        sta bounce_y_2
        lda bounce_dy_low_2
        clc
        adc #20
        sta bounce_dy_low_2
        bcc +
        inc bounce_dy_2
+
	lda bounce_y_2
        cmp #200-ROLL_HEIGHT
        bcc +
        lda bounce_dy_2
        cmp #$03
        bcc +
        lda #$-5
        sta bounce_dy_2
        lda #$88
        sta bounce_dy_low_2
+
	lda bounce_y
        cmp bounce_y_2
        bne +
        inc bounce_counter
        lda bounce_counter
	and #$03
        bne +
	lda #<bounce_1
	sta move_ptr+1
	lda #>bounce_1
	sta move_ptr+2
	lda #>GENERATED_CODE
	sta bounce_roll_up_ptr+2
        lda bounce_roll_2
        sta bounce_roll
        lda #$2
        sta bounce_counter
+	rts

dual_rollup:
	ldx #$00
	ldy #$00
dual_rollup_up_ptr_1:
        jsr GENERATED_CODE_8
	ldx #200-64
	ldy #$00
dual_rollup_up_ptr_2:
        jsr GENERATED_CODE_8

	lda dual_rollup_up_ptr_1+2
        sec
        sbc #>(GENERATED_CODE_2 - GENERATED_CODE)
        sta dual_rollup_up_ptr_1+2
	sta dual_rollup_up_ptr_2+2
        cmp #(>GENERATED_CODE)
        bcs +
        lda #<dual_bounce
        sta move_ptr+1
	!if (>dual_bounce) != (>dual_rollup) {
	lda #>dual_bounce
        sta move_ptr+2
	}
	ldx #$00
	stx bounce_y
	stx bounce_y_low
	stx bounce_roll
	lda #$00
	sta bounce_dy
	lda #$00
	sta bounce_dy_low
	rts


dual_zoom:
	ldy bounce_counter
	lda dual_zoom_tab,y
	tay
	lda zoomsizes,y
	sta $0100
	ldx #$00
	jsr GENERATED_ZOOM_CODE
	ldy bounce_counter
	lda dual_zoom_tab,y
	tay
	tya
	eor #$ff
	clc
	adc #200
	tax
	jsr GENERATED_ZOOM_CODE
	inc bounce_counter
	lda bounce_counter
	cmp #dual_zoom_tab_len
	bne +
	lda #<dual_rollup
	sta move_ptr+1
	!if (>dual_rollup) != (>dual_zoom) {
	lda #>dual_rollup
        sta move_ptr+2
	}
+	rts

dual_zoom_tab:
	!byte $08,$0c,$10,$14,$18,$1c,$20,$24,$28,$2c,$30,$34,$38,$3a,$3c,$3e,$40,$44,$48,$44,$40
        !byte $3e,$3c,$3e,$40,$42,$44,$42,$40,$3e,$3c,$3e,$40,$42,$44,$43,$42,$41,$40,$3f,$40,$3f
dual_zoom_tab_len = * - dual_zoom_tab

clear_riss:
	ldx #$00
	ldy #32
	jsr clear
	ldx #200-32
	ldy #32
	jsr clear
	lda dd00d018tab+63
	sta $03
	lda dd00d018tab
	sta $02+199
	lda #<dual_zoom
	sta move_ptr+1
	!if (>dual_zoom) != (>clear_riss) {
	lda #>dual_zoom
        sta move_ptr+2
	}
	lda #2
	sta bounce_counter
	rts


riss:
	ldx #0
riss_display_loop:
	lda GENERATED_PICTURES,x
	sta $02,x
	inx
	cpx #200
	bne riss_display_loop

	inc riss_display_loop+2
	lda riss_display_loop+2
generated_picture_numbers:
	cmp #$00
	bne +
	lda #<clear_riss
        sta move_ptr+1
	!if (>clear_riss) != (>riss) {
	lda #>clear_riss
        sta move_ptr+2
	}
+	rts

bounce_strech:
	ldx #$00
bounce_strech_high:
	lda strech_size_tab,x
        tay
        cpy #$ff
        bne +
        lda #<riss
        sta move_ptr+1
	!if (>riss) != (>bounce_strech) {
        lda #>riss
        sta move_ptr+2
	}
        rts

+	tya
	eor #$ff
	clc
	adc #$01
	lsr
	sec
	sbc #(256-200)/2-1
	tax

	lda zoomsizes,y
	sta $0100
	jsr GENERATED_ZOOM_CODE
        inc bounce_strech+1
        bne +
        inc bounce_strech_high+2
+	rts

end:	lda #$01
	sta waitflag+1
	rts
        
bounce_strech2:
	ldx #$00
	lda strech_size_tab_2,x
        tay
        cpy #$ff
        bne +
        lda #<end
        sta move_ptr+1
	!if (>end) != (>bounce_strech2) {
        lda #>end
        sta move_ptr+2
	}
        rts

+	tya
	eor #$ff
	clc
	adc #$01
	lsr
	sec
	sbc #(256-200)/2-1
	tax

	lda zoomsizes,y
	sta $0100
	jsr GENERATED_ZOOM_CODE
        inc bounce_strech2+1
+	rts
        

bounce_roll_up:
	inc bounce_counter
        lda bounce_counter
        and #$03
        bne +
	ldx bounce_y
        ldy bounce_roll
bounce_roll_up_ptr:
        jsr GENERATED_CODE
	lda bounce_roll_up_ptr+2
        clc
        adc #>(GENERATED_CODE_2 - GENERATED_CODE)
        sta bounce_roll_up_ptr+2
        cmp #(>GENERATED_CODE_8)+1
        bcc +
        lda bounce_strech+1
        beq first_run
        lda #<bounce_strech2
        sta move_ptr+1
	!if (>bounce_strech2) != (>bounce_roll_up) {
	lda #>bounce_strech2
        sta move_ptr+2
        }
	rts
        
	;first run      
first_run:
        lda #<bounce_strech
        sta move_ptr+1
	!if (>bounce_strech) != (>bounce_roll_up) {
	lda #>bounce_strech
        sta move_ptr+2
	}
+	rts

bounce_1_slowdown:
	ldx bounce_y
        ldy bounce_roll
        jsr GENERATED_CODE
        ldy #4
        lda bounce_y
        sec
        sbc #$04
        tax
        jsr clear
        lda bounce_roll
        ;cmp #2
        beq +
        inc bounce_roll
+	lda bounce_y
	cmp #100-(ROLL_HEIGHT/2)-1
        beq +
	inc bounce_y
        rts
+       dec bounce_counter
	bne +
	lda #<bounce_roll_up
        sta move_ptr+1
	!if (>bounce_roll_up) != (>bounce_1_slowdown) {
	lda #>bounce_roll_up
        sta move_ptr+2
	}
        lda #$00
        sta bounce_counter
+	rts        

bounce_1:
	ldx bounce_y
        ldy bounce_roll
        jsr GENERATED_CODE
        ldy #4
        lda bounce_y
        sec
        sbc #$04
        tax
        jsr clear
        ldy #4
        lda bounce_y
        clc
        adc #ROLL_HEIGHT
        tax
        jsr clear
        lda bounce_y_low
        clc
        adc bounce_dy_low
        sta bounce_y_low
        lda bounce_y
        adc bounce_dy
        sta bounce_y
        lda bounce_dy_low
        clc
        adc #10
        sta bounce_dy_low
        bcc +
        inc bounce_dy
+
	lda bounce_y
        cmp #200-ROLL_HEIGHT
        bcc +
        lda bounce_dy
        cmp #$03
        bcc +
        lda #$-4
        sta bounce_dy
        lda #$b8
        sta bounce_dy_low
+       inc bounce_roll
	lda bounce_y
        cmp #20
	bne +
        dec bounce_counter
        bne +
	lda #<bounce_1_slowdown
        sta move_ptr+1
	!if (>bounce_1_slowdown) != (>bounce_1) {
	lda #>bounce_1_slowdown
        sta move_ptr+2
	}
bounce_counter_new:
        lda #100
        sta bounce_counter
        lda #117-64
        sta bounce_counter_new+1
+       rts

strech_size_tab_2:
	!for i,63 {
        !byte 64-i
        }
        !byte $ff
        
strech_size_tab:
	!for i, 20 {
        !byte 64
        }
	!for i,8 {
	!byte 64,66,68,69,70,69,68,66
        }
	!for i,4 {        
        !byte 64,68,71,74,76,78,79,80,79,78,76,74,71,68
        }
        !byte 64,74,83,92,100,107,113,119,124,128,131,133,134
        !byte 133,131,128,124,119,113,107,100,92,83,74

	!byte 64,78,91,103,114,124,133,141,148,152,157,161,164,167,169,171
        !byte 169,167,164,161,157,152,148,141,133,124,114,103,91,78
        
        !byte 64,78,91,103,114,124,133,141,148,152,157,161,164,167,169,171
       	!for i,50 {
        !byte 171
        }
        !byte 169,167,164,161,157,152,148,141,133,124,114,103,91,78
        
        !byte 64,84,103,121,138,154,169,183,196,218,229,230,230
       	!for i,20 {
        !byte 236
        }
        !byte 230,229,218,196,183,169,154,138,121,103,84
        !byte 64,84,103,121,138,154,169,183,196,218,222,230
        !for i,8 {
        !byte 240,235,230,226,222,226,230,235
        }
        !byte 240
        
        !byte $ff
                        
move:
	
	!ifdef COMMENTED_OUT {
	lda #$00
	ldx #$00
	and #$3c
	lsr
	lsr
	cmp #$08
	bcc +
	lda zoomsizes+64
	sta $0100
	inc move+1
	jmp GENERATED_ZOOM_CODE
+	asl
	clc
	adc #>GENERATED_CODE
	sta _jmp+2
	inc move+1
_jmp:	jmp GENERATED_CODE
	
	ldy #$c0
;	ldx #$00
;-	lda dd00d018tab,y
;	sta $00,x
;	iny
;	inx
;	bne -
	lda zoomsizes,y
	sta $0100
        
        ;eor #$ff
        ;tax
	tya
	eor #$ff
	clc
	adc #$01
	lsr
	sec
	sbc #(256-200)/2
	tax
	;jsr GENERATED_CODE
	jsr GENERATED_ZOOM_CODE
-	lda move+1
	clc
	adc direction
	sta move+1
	beq change_direction
	cmp #$f0
	bne +
change_direction:
	lda direction
	eor #$ff
	clc
	adc #$01
	sta direction
	jmp -
+       rts

	txa
        sec
        sbc #$10
        tax
        ldy #$10
        jsr clear
        txa
        clc
        adc #$10+BLOCK_SIZE
        tax
        ldy #$10
	jmp clear
;clearexit:
;	rts
}
        
clear:	;in x first offset
	;in y size
        dey
        tya
        eor #$3f
        asl
        ;bmi clearexit
        sta clearjump+1
        lda #UNUSED_LINE
clearjump:
	bpl +
+       !for i, 64 {
        sta $03+(64-i),x
        }
        rts


	+align256

dd00d018tab:
	!for i,7 {
	!bin "dd00d018_tab",64,1
	}
dd00d018tab_complete:
	!bin "dd00d018_tab",,1


switch_final_picture:
	lda $d011
	bpl switch_final_picture
	sei
	lda #<zwischenirq
	sta $fffe
	lda #>zwischenirq
	sta $ffff
	lda #$ff
	sta $d012
	cli
	ldx #$0f
	lda #$ff
-	sta $08,x
	dex
	bpl -
	lda #$00
	sta $10+7
	lda #$03
	sta $dd00
	lda #$11
	sta $d018
	lda #$00
	sta $d021
	lda #$d0
	sta $d016
	lda #$1b
	sta $d011
-	lda $d012
	cmp #$40
	bcc -
	lda #$01
	sta $d021
	rts

generate_code:
	lda #<GENERATED_CODE
        sta $02
        lda #>GENERATED_CODE
        sta $03
        lda #<code_data
        sta $04
        lda #>code_data
        sta $05
	jsr generate_code_sub

	lda #<GENERATED_CODE_2
        sta $02
        lda #>GENERATED_CODE_2
        sta $03
        lda #<code_data_2
        sta $04
        lda #>code_data_2
        sta $05
	jsr generate_code_sub

	lda #<GENERATED_CODE_3
        sta $02
        lda #>GENERATED_CODE_3
        sta $03
        lda #<code_data_3
        sta $04
        lda #>code_data_3
        sta $05
	jsr generate_code_sub

	lda #<GENERATED_CODE_4
        sta $02
        lda #>GENERATED_CODE_4
        sta $03
        lda #<code_data_4
        sta $04
        lda #>code_data_4
        sta $05

	jsr generate_code_sub

	lda #<GENERATED_CODE_5
        sta $02
        lda #>GENERATED_CODE_5
        sta $03
        lda #<code_data_5
        sta $04
        lda #>code_data_5
        sta $05
	jsr generate_code_sub

	lda #<GENERATED_CODE_6
        sta $02
        lda #>GENERATED_CODE_6
        sta $03
        lda #<code_data_6
        sta $04
        lda #>code_data_6
        sta $05
	jsr generate_code_sub

	lda #<GENERATED_CODE_7
        sta $02
        lda #>GENERATED_CODE_7
        sta $03
        lda #<code_data_7
        sta $04
        lda #>code_data_7
        sta $05
	jsr generate_code_sub

	lda #<GENERATED_CODE_8
        sta $02
        lda #>GENERATED_CODE_8
        sta $03
        lda #<code_data_8
        sta $04
        lda #>code_data_8
        sta $05
generate_code_sub:
        ldy #$00
        sty $06
        sty $07
-       ldx #$00
	lax ($04,x)
	bmi generate_code_end
        lda #CODE_LDA_ABY
        sta ($02),y
        iny
        lda $06
        sta ($02),y
        iny
        lda #>dd00d018tab
        sta ($02),y
	iny
        lda #CODE_STA_ZPX
        sta ($02),y
        iny
        lda $07
        sta ($02),y
        iny
        inc $07
        tya
        clc
        adc $02
        sta $02
        bcc +
        inc $03
+	ldy #$00
	inc $04
	bne +
        inc $05
+	txa
        clc
        adc $06
        sta $06
	jmp -

generate_code_end:
	lda #CODE_RTS
        sta ($02),y
        inc $02
        bne +
        inc $03
+	rts

generate_zoom_code:
	lda #<GENERATED_ZOOM_CODE
	sta $02
	lda #>GENERATED_ZOOM_CODE
	sta $03

	;generater upper codeclear
	ldy #$00
	lda #CODE_LDA_IMM
	sta ($02),y
	iny
	lda #UNUSED_LINE
	sta ($02),y
	iny
	ldx #-CODE_ZOOM_CLEAR
-	lda #CODE_STA_ZPX
	sta ($02),y
	iny
	txa
	sta ($02),y
	iny
	inx
	bne -
	tya
	clc
	adc $02
	sta $02
	bcc +
	inc $03
+	
	ldy #$00
	lda #CODE_LDA_IMM
	sta ($02),y
	iny
	lda #$80
	sta ($02),y
	iny
	lda #CODE_SEC
	sta ($02),y
	iny
	!byte CODE_BIT_ABS
-	ldy #$00

	lda #CODE_LDY_IMM
	sta ($02),y
	iny
	lda dd00d018tab,x
	sta ($02),y
	iny
	lda #CODE_STY_ZPX
	sta ($02),y
	iny
	txa
	eor #$ff
	clc
	adc #$01
	sta ($02),y
	iny
	lda #CODE_INX
	sta ($02),y
	iny
	lda #CODE_ADC_ABS
	sta ($02),y
	iny
	lda #$00
	sta ($02),y
	iny
	lda #$01
	sta ($02),y
	iny
	lda #CODE_BCC
	sta ($02),y
	iny
	lda #-8
	sta ($02),y
	iny

	tya
	clc
	adc $02
	sta $02
	bcc +
	inc $03
+	inx
	cpx #$40
	bne -

	ldy #$00
	lda #CODE_LDA_IMM
	sta ($02),y
	iny
	lda #UNUSED_LINE
	sta ($02),y
	iny
	ldx #$c0
-	lda #CODE_STA_ZPX
	sta ($02),y
	iny
	txa
	sta ($02),y
	iny
	inx
	cpx #$c0+CODE_ZOOM_CLEAR
	bne -
	tya
	clc
	adc $02
	sta $02
	bcc +
	inc $03
+	
	ldy #$00
	lda #CODE_RTS
	sta ($02),y
	rts

SCREENLINE = $0400+40*11

paint_final_picture:
	ldx #$00
	lda #$01
-	sta $0400,x
	sta $0500,x
	sta $0600,x
	sta $0700,x
	inx
	bne -
	ldx #40-1
	lda #$02
-	sta SCREENLINE,x
	dex
	bpl -
	rts



generate_pictures:
	lda #<GENERATED_PICTURES
	sta $05
	lda #>GENERATED_PICTURES
	sta $06

	lda #200
	sta $03

riss_big_loop:
	lda #200
	sec
	sbc $03
	sta $04

	lda $03
	lsr
	sta $02
	ldx #$06
	ldy #$00
riss_loop:
	lda dd00d018_converttab_low,x
	sta dd00d018_ptr+1
	lda dd00d018_converttab_high,x
	sta dd00d018_ptr+2
dd00d018_ptr:
	lda $ffff
	sta ($05),y
	lda $02
	clc
	adc #70
	bcc no_riss_middle
-	sbc $03
	inx
	bcs -

	cpx #32+8
	bcc no_riss_middle
	sta $02
	lda $04
	beq riss_middle_not_exists
-	lda #UNUSED_LINE
	sta ($05),y
	iny
	dec $04
	bne -
	beq riss_middle_end

no_riss_middle:
	sta $02
riss_middle_not_exists:
	iny
riss_middle_end:
	cpy #200
	bne riss_loop
	inc $06
	lax $03
	sbx #16
	stx $03
	bcs riss_big_loop
	lda $06
	sta generated_picture_numbers+1
	rts
	
dd00d018_converttab_low:
	!for i, 32 {
	!byte <(dd00d018tab_complete+(i-1))
	}
	!for i, 16 {
	!byte <(dd00d018tab_complete+(i-1)+64)
	}
	!for i, 32 {
	!byte <(dd00d018tab_complete+(i-1)+32)
	}



dd00d018_converttab_high:
	!for i, 32 {
	!byte >(dd00d018tab_complete+(i-1))
	}
	!for i, 16 {
	!byte >(dd00d018tab_complete+(i-1)+64)
	}
	!for i, 32 {
	!byte >(dd00d018tab_complete+(i-1)+32)
	}

code_data:
!byte 7,4,2,2,2,1,1,1,1,0,1,1,0,1,1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,1,0,1,1,0,1,1,1,1,1,2,2,2,4,7,$ff
        
BLOCK_SIZE = *-1 - code_data

code_data_2:
!byte 6,4,2,2,2,1,1,1,1,1,1,1,0,1,1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,2,2,2,4,6,$ff

code_data_3:
!byte 5,3,2,2,2,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,1,0,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,2,2,2,3,5,$ff

code_data_4:
!byte 4,3,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,1,0,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,3,4,$ff

code_data_5:
!byte 3,3,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,3,3,$ff

code_data_6:
!byte 2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,$ff

code_data_7:
!byte 2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,$ff

code_data_8:
!byte 2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,$ff

;code_data_7:
;!byte 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,$ff

;code_data_2:
;!byte 4,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,4,$ff

;code_data_3:
;!byte 2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,0,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,$ff

;code_data_4:
;!byte 2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,$ff
                           
	ptr1 = 2
	tmp = 4
	tmp2 = 5
        
calc_chars:
	!src "data"

