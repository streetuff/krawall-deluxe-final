	;free space $e000-$faff
        !cpu 6510

	!ifdef RELEASE {
        !src "config.asm"
        !src "../framework/framework.inc"
	!to "vector.plain",plain
	* = PARTORG
	jmp start
        } else {
        !to "vector.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
	jmp start
* = $1000
	!bin "Wip3Out_2k6.sid",,126
        }

SIZE_X = 16
SIZE_Y = 16

BYTES_PER_ROW = SIZE_Y*8

        !macro spare .v {
        !set v_ptr = v_ptr+.v
        }
;HIGH_BIT_ENABLED = 1
;DEBUG = 1

; if FULL_LINE is not set, 8 pixels around the endpoint may not be painted 
FULL_LINE = 1
; if CLEAR_LINE_ACTIVE is not set, the screen will be cleared with one big unrolled code
;CLEAR_LINE_ACTIVE = 1
; if CLEARCODE_ALIGNED, code is a little bit bigger but faster
CLEARCODE_ALIGNED = 1

charset = $e000
charset2 = $e800
screen = $c000

SCROLL_SCREEN = $b900

code_table_byte0 =$f000
code_table_byte1 =code_table_byte0+$100
code_table_byte2 =code_table_byte0+$200
code_table_byte3 =code_table_byte0+$300
code_table_byte4 =code_table_byte0+$400
code_table_byte5 =code_table_byte0+$500
code_table_byte6 =code_table_byte0+$600
code_table_byte7 =code_table_byte0+$700
code_table_branch = code_table_byte0 + $0800
code_table_branch_clear = code_table_byte0 + $0900
invers_tab = code_table_byte0 + $0a00
code_table_number_of_bytes = $80


index_select_tab = $be00
!ifdef CLEAR_LINE_ACTIVE {
clear_octant = index_select_tab + $0100
clear_x1 = clear_octant + $10
clear_y1 = clear_octant + $20
clear_x2 = clear_octant + $30
clear_y2 = clear_octant + $40
clear_div = clear_octant + $50

clear_octant_2 = index_select_tab + $0180
clear_x1_2 = clear_octant_2 + $10
clear_y1_2 = clear_octant_2 + $20
clear_x2_2 = clear_octant_2 + $30
clear_y2_2 = clear_octant_2 + $40
clear_div_2 = clear_octant_2 + $50

clear_section = $0200 
clear_startoffset = $0300
clear_endoffset = $0400

clear_section_2 = $0500 
clear_startoffset_2 = $0600
clear_endoffset_2 = $0700

}

index_tab = $d000

multabhi = $c800
multablo = $ca00
multabhi2 = $cc00
multablo2 = $ce00

multablo_ptr = $02
multabhi_ptr = $04
multablo2_ptr = $06
multabhi2_ptr = $08

	!set v_ptr = $0a

tmp1 = v_ptr
	+spare 1
tmp2 = v_ptr
	+spare 1        
tmp3 = v_ptr
	+spare 1
tmp4 = v_ptr
	+spare 1
;tmp5 = v_ptr
;	+spare 1
tmp6 = v_ptr
	+spare 1
x1 = v_ptr
	+spare 1
y1 = v_ptr
	+spare 1
x2 = v_ptr
	+spare 1
y2 = v_ptr
	+spare 1
dx = v_ptr
	+spare 1
dy = v_ptr
	+spare 1
used_array = v_ptr
	+spare 8
free_array = v_ptr
	+spare 8 
allocated = v_ptr
	+spare 1
alloc_bitmask = v_ptr
	+spare 4
alloc_bitmask_invers = v_ptr
	+spare 4
bitmask = v_ptr
	+spare 8
virtual_x_high = v_ptr
	+spare 8
;virtual_x = v_ptr
;	+spare 8
;virtual_y = v_ptr
;	+spare 8
;virtual_block  = v_ptr
;	+spare 8
;allocation_tab = v_ptr
;	+spare 8

	!ifdef FULL_LINE {
value80 = v_ptr
	+spare 1
	}
ptr1 = v_ptr
	+spare 2
ptr2 = v_ptr
	+spare 2
ptr3 = v_ptr
	+spare 2
ptr4 = v_ptr
	+spare 2
!ifdef CLEAR_LINE_ACTIVE {
clear_index = v_ptr
	+spare 1
clear_index2 = v_ptr
	+spare 1
clear_area_index = v_ptr
	+spare 1
clear_area_index2 = v_ptr
	+spare 1
}
angle_ptr_sin = v_ptr
	+spare 2
angle_ptr_cos = v_ptr
	+spare 2
angle = v_ptr
	+spare 1
anim_phase = v_ptr
	+spare 1
zoom_factor = v_ptr
	+spare 1
value_ptr = v_ptr
	+spare 2
;angleadd = v_ptr
;	+spare 1
;add_val = v_ptr
;	+spare 1
	
 
rot_tab = (end + $ff) & $ff00
bigmultab = rot_tab + $100
x_coord_high_tab = $0200 ; bigmultab + $2000
x_coord_high_tab2 = x_coord_high_tab + $100
x_coord_low_tab = x_coord_high_tab2 + $100
x_coord_bit_tab = x_coord_low_tab + $100
div8tab = x_coord_bit_tab + $100
div8tab2 = div8tab + $100

!ifdef CLEARCODE_ALIGNED {
div8tab_clearcode0 = div8tab2+$100
div8tab_clearcode0_2 = div8tab_clearcode0+$100
}

	!ifdef CLEAR_LINE_ACTIVE {
	} else {
;clearscreen = div8tab2 + $100
;clearscreen2 = clearscreen + 256*8*3+3
	}

	!macro align256 {
		* = (*+$ff) & $ff00
	}

	!macro align128 {
		* = (*+$7f) & $ff80
	}

	!macro align8 {
		* = (*+$7) & $fff8
	}
        !macro sprite_line .x {
        !byte ^.x, >.x, <.x
        }


!macro mul {
multabhi = $c800
multablo = $ca00
multabhi2 = $cc00
multablo2 = $ce00
         ;A/X = A * Y (X LOW)
         STA .MULZP1+1
         STA .MULZP2+1
         EOR #$FF
         STA .MULZP3+1
         STA .MULZP4+1
         SEC
.MULZP1
         LDA multabhi,Y
.MULZP3
         SBC multabhi2,Y
         TAX
.MULZP2
         LDA multablo,Y
.MULZP4
         SBC multablo2,Y
}

!macro imul .adr {
	; x = .adr*y/256
	tya
	eor .adr
	sta .sign+1
	tya
	bpl +
	eor #$ff
	tay
	iny
+	lda .adr
	bpl +
	eor #$ff
	clc
	adc #$01
+	+mul
.sign:
	ldy #$00
	bpl +
	txa
	eor #$ff
	tax
	inx
+	}


!macro imul2 .adr {
	; y is unsigned
	; x = .adr*y/256
+	lda .adr
	bpl +
	eor #$ff
	clc
	adc #$01
+	+mul
	bit .adr
	bpl +
	txa
	eor #$ff
	tax
	inx
+	}


!macro set_angle {
	;in x angle
	lda rot_tab,x
	sec
	ror
	sta angle_ptr_sin+1
	lda #$00
	ror
	sta angle_ptr_sin
	lda rot_tab+$30,x
	sec
	ror
	sta angle_ptr_cos+1
	lda #$00
	ror
	sta angle_ptr_cos
	}
	
!macro calc_xy_angle .newx, .newy {
	;in tmp1 x
	;in tmp2 y
	;in tmp3 new x
	;in tmp4 new y
	; set_angle must be run before
	ldy tmp1
	lda (angle_ptr_cos),y
	ldy tmp2
	sec
	sbc (angle_ptr_sin),y
	;clc
	;adc #$3f
	sta .newx
	ldy tmp1
	lda (angle_ptr_sin),y
	ldy tmp2
	clc
	adc (angle_ptr_cos),y
	;clc
	;adc #$40
	sta .newy
	}

!macro zoom .adr {
	ldy zoom_factor
        beq +        
	+imul2 .adr
	txa
        !byte $2c
+       lax .adr        
	sbx #$c0
	stx .adr
}

        
start:
	!ifdef RELEASE {
	} else {
	sei
	lda #$35
	sta $01
	jsr copy_code_and_init
        lda #$00
        jsr $1000
	}

	!ifdef DEBUG {
-	lda $dc01
	and #$10
	bne -
	}
	;lda #$00
	;sta $d020
	;sta $d021
        ;jsr calc_shifttabs
	!ifdef FULL_LINE {
	lda #$80
	sta value80
	}
	!ifdef CLEAR_LINE_ACTIVE {
	lda #$00
	sta clear_index
	sta clear_index2
	sta clear_area_index
	sta clear_area_index2
	} else {
        ;jsr generate_clearscreens
	}
	jsr init_shiftab
	jsr init_tables
	jsr calcline


	ldy #$00
	sty ptr1
	lda #>charset
	sta ptr1+1
	ldx #$10
	tya
-	sta (ptr1),y
	iny
	bne -
	inc ptr1+1
	dex
	bne -

	lda #25
	sta tmp1
	lda #$c0
	tax
-	ldy #40
scr_ptr:
	sta screen
	inc scr_ptr+1
	bne +
	inc scr_ptr+2
+	sbx #$100-SIZE_Y
	txa
	dey
	bne scr_ptr
	txa
	sbx #<((40*SIZE_Y)-1)
	txa
	dec tmp1
	bne -

	inc screen+$4
        inc screen+$14
        inc screen+$24
        inc screen+$283
        inc screen+$2a3

	ldx #$00
-	lda #$0a
	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00,x
	inx
	bne -

	ldx #$07
        lda #$ff
-	sta charset,x
	sta charset2,x
	dex
        bpl -        


	lda #<(screen + (((25-SIZE_Y)/2)*40+ (40-SIZE_X)/2))
	sta ptr1
	lda #>(screen + (((25-SIZE_Y)/2)*40+ (40-SIZE_X)/2))
	sta ptr1+1
	ldx #$00
--	ldy #$00
-	txa
	sta (ptr1),y
	lda ptr1+1
	pha
	eor #$d8 - >(screen)
	sta ptr1+1
	lda #$01
	sta (ptr1),y
	pla
	sta ptr1+1
	inx
	iny
	lda ptr1
	clc
	adc #40-1
	sta ptr1
	bcc +
	inc ptr1+1
+	cpy #SIZE_Y
	bne -
	lda ptr1
	sec
	sbc #<((40-1)*SIZE_Y-1)
	sta ptr1
	lda ptr1+1
	sbc #>((40-1)*SIZE_Y-1)
	sta ptr1+1
	cpx #<(SIZE_X*SIZE_Y)
	bne --

        inc screen+$ac

        !ifdef RELEASE {
	ADDR_OF_CODE = $b000
        }        
	inc ADDR_OF_CODE
-	lda ADDR_OF_CODE+1
	beq -
        !ifdef RELEASE {
-	lda $d011
	bpl -                        
        jsr FRAMEWORK_IRQ
        } else {
        sei
        }
	jsr init_bigmultab

	lda #$00
	sta $dd00
	!if (SIZE_X & 1) != 0 {
	lda #$c4
	} else {
	lda #$c0
	}
	sta $d016
	lda #$08
	sta $d018
	;lda #$ff
	;sta $d015
	sei
	lda #<irq0
	sta $fffe
	lda #>irq0
	sta $ffff
	lda #$81
	sta $d01a
	sta $d019
	lda #$f8
	sta $d012
	lda #$1b
	sta $d011
	;lda #$37
	;sta $01

	;lda #$d0
	;sta angle
	;lda #$01
	;sta angleadd
	cli
-	lda $d011
	bmi -
-	lda $d012
	bne -                
	lda #$02
	;sta $d020
	sta $d021
	!if index_tab = $d000 {
	lda #$30
	sta $01
	}

	!macro do_line .screen {
	;inc $01
	!if .screen = 0 {
	jsr line
	} else {
	jsr line2
	}
	;dec $01
	}

!macro timemark .val {
	!if 0 {
	php
	sei
	lda $01
	pha
	lda #$35
	sta $01
	lda #.val
	sta $d020
	pla
	sta $01
	plp
	}
}

!macro scroll_out {
	ldx $d016
        dex
        cpx #$c0
        bcs +
        ldx #$c7
+	stx $d016
	bcs +
        inc tmp3        
+	}

!macro do_loader_irq {

loader_irq:
	pha
        txa
        pha
        tya
        pha
        lda $01
        pha
        lda #$35
        sta $01
	!ifdef RELEASE {
	jsr PLAY_MUSIC 
	} else {
	jsr $1003
	}
        lda screen+40
        beq ++
        anc #$00
        sta tmp3
        lda tmp1
        clc
        adc tmp2
        sta tmp2
        lsr
        lsr
        lsr
        lsr
        tay
        beq +        
-	+scroll_out
        dey
        bne -
        lda tmp3
        beq +
        jsr SCROLL_SCREEN
+	lda tmp2
	and #$0f
        sta tmp2
value:	lda #$00
	eor #$01
        sta value+1
        beq +
        ;maximaize scrollout
        lda #$1f 
        cmp tmp1
        lda tmp1
        adc #$00       
        sta tmp1
+
++
	inc $d019
        pla
        sta $01
        pla
        tay
        pla
        tax
        pla
        rti
}        

!macro get_value .clear_y {
	!if .clear_y != 0 {
        ldy #$00
        }
        lda (value_ptr),y
        inc value_ptr
        bne +
        inc value_ptr+1
+
}

	jsr play_anim
-	
	lda #$30
        sta $01
	jsr generate_fadeout_part
	lda #$35
	sta $01
	!ifdef RELEASE {
        ldx #$00
-       lda zwischenpart,x
        sta FRAMEWORK_ADDR-$100,x
        inx
        bpl -
	jsr init_loader_irq
	jmp FRAMEWORK_ADDR-$100

init_loader_irq:
	sei
        lda #<loader_irq
        sta $fffe
        lda #>loader_irq
        sta $ffff
        lda #$00
        sta tmp1
        sta tmp2
        cli
        rts

zwischenpart:
	!pseudopc FRAMEWORK_ADDR-$100 {
	jsr LOAD_NEXT_PART
        jsr DECRUNCH_NEXT_PART
-	lda screen+40
	bne -
	lda #$7b
	sta $d011
        jsr FRAMEWORK_IRQ
        jmp START_NEXT_PART

	+do_loader_irq

	}
	} else {
	jsr init_loader_irq
-	jmp -

init_loader_irq:
	sei
        lda #<loader_irq
        sta $fffe
        lda #>loader_irq
        sta $ffff
        lda #$00
        sta tmp1
        sta tmp2
        cli
        rts

	+do_loader_irq
	}

PHASE = 3

play_anim:
	jsr reset_coord
---	
	ldy #$0a
	ldx #<line
	lda #>line
	jsr paint
	ldy #$08
	ldx #<line2
	lda #>line2
	jsr paint
	jmp ---

!macro coord_pointer_back {
	lda ptr4
	sta value_ptr
	lda ptr4+1
	sta value_ptr+1
}

!macro coord_pointer_next {
	lda value_ptr
	sta ptr4
	lda value_ptr+1
	lda ptr4+1
}

paint:
	stx linefunc+1
	sta linefunc+2
	!if index_tab = $d000 {
	lda #$35
	sta $01
	}
	;dec $d020
	lda #$f7
-	cmp $d012
	bne -
	;inc $d020

	sty $d018
	!if index_tab = $d000 {
	lda #$30
	sta $01
	}
	+timemark 0
	;inc $01
;	lda $01
;	pha
;	lda #$35
;	sta $01
;-	lda $dc01
;	and #$10
;	bne -
;-	lda $dc01
;	and #$10
;	beq -
;	pla
;	sta $01
	cpy #$0a
	bne +
	jsr clearscreen
	jmp ++
+	jsr clearscreen2
++
	;dec $01
	+timemark 1
	lda value_ptr
	sta ptr4
	lda value_ptr+1
	sta ptr4+1
	+get_value 1
        sta tmp6



lineloop:
	+get_value 1
	sta tmp1
	+get_value 0
	sta tmp2
	+calc_xy_angle x1, y1
	+zoom x1
	+zoom y1

	+get_value 1
	sta tmp1
	+get_value 0
	sta tmp2
	+calc_xy_angle x2, y2
	+zoom x2
	+zoom y2
linefunc:
	jsr $2020

        dec tmp6
	beq +
	jmp lineloop
+
	jsr prepare_next_frame
	!if index_tab = $d000 {
	lda #$30
	sta $01
	}
	+timemark 2
	rts
        
prepare_next_frame:
	ldx anim_phase
	lda anim_ptr_low,x
	sta codecall+1
	lda anim_ptr_high,x
	sta codecall+2
codecall:
	jmp 0

anim_ptr_low:
	!byte <zoom_in
	!byte <rotate
	!byte <rotate_zoom_out
	!byte <zoom_in
	!byte <rotate
	!byte <rotate_zoom_out
	!byte <zoom_in
	!byte <rotate
	!byte <rotate_zoom_out
	!byte <zoom_in
	!byte <rotate
	!byte <rotate_zoom_out
        !byte <rotate_cube_init
        !byte <rotate_cube
        !byte <rotate_cube2
        !byte <rotate_cube3
        !byte <rotate_ico_init
        !byte <rotate_ico
        !byte <rotate_ico
        !ifdef RELEASE {
        } else {
        ;!byte <rotate_ico
        ;!byte <rotate_ico
        ;!byte <rotate_ico
        ;!byte <rotate_ico
        ;!byte <rotate_ico
        ;!byte <rotate_ico
        }
        !byte <rotate_ico_slow
	!byte <end_anim

anim_ptr_high:
	!byte >zoom_in
	!byte >rotate
	!byte >rotate_zoom_out
	!byte >zoom_in
	!byte >rotate
	!byte >rotate_zoom_out
	!byte >zoom_in
	!byte >rotate
	!byte >rotate_zoom_out
	!byte >zoom_in
	!byte >rotate
	!byte >rotate_zoom_out
        !byte >rotate_cube_init
        !byte >rotate_cube
        !byte >rotate_cube2
        !byte >rotate_cube3
        !byte >rotate_ico_init
        !byte >rotate_ico
        !byte >rotate_ico
        !ifdef RELEASE {
        } else {
        ;!byte >rotate_ico
        ;!byte >rotate_ico
        ;!byte >rotate_ico
        ;!byte >rotate_ico
        ;!byte >rotate_ico
        ;!byte >rotate_ico
        }
        !byte >rotate_ico_slow
	!byte >end_anim

zoom_in:
	lda #$f8
	ldx zoom_factor
	beq next_anim
	sbx #$f8
	stx zoom_factor
coord_pointer_back:
	+coord_pointer_back
	rts

next_anim:
	inc anim_phase
	jmp prepare_next_frame

rotate:	dec angle
	lda angle
	and #$3f
	cmp #$01
	beq next_anim
	tax
angle_jmp:
	bpl +
	lda sintab,x
	clc
	adc #$80
	lsr
	lsr
	tax

+	+set_angle
	jmp coord_pointer_back

rotate_zoom_out:
	lax zoom_factor
	sbx #$08
	beq rotate_zoom_out_end
	stx zoom_factor
angle_dir:
	dec angle
	lda angle
	and #$3f
	tax
angle_jmp2:
	bpl +
	lda sintab,x
	clc
	adc #$80
	lsr
	lsr
	tax

+	+set_angle
	jmp coord_pointer_back

rotate_zoom_out_end:
	ldx #$20
	stx angle
	+set_angle
	inc anim_phase
        lda angle_dir
        eor #$20
        sta angle_dir
        sta rotate
        cmp #$c6
        bne +
        lda #$c9
        sta angle_jmp
        sta angle_jmp2
+	rts

rotate_cube_init:
        lda #360/4
        sta rotate_counter+1
	inc anim_phase

rotate_cube:
	lax zoom_factor
	beq +
	sbx #$fc
	stx zoom_factor
+	dec rotate_counter+1
rotate_counter:
	lda #$00
	bne +
	lda #<cube_ptr
        sta ptr4
        sta value_ptr
	lda #>cube_ptr
        sta ptr4+1
        sta value_ptr+1
        lda #360/4
        sta rotate_counter2+1
	inc anim_phase
        jmp rotate_cube2_cont
+	rts


rotate_cube2:
	inc angle
	lda angle
	and #$3f
	tax
	+set_angle
rotate_cube2_cont:
	dec rotate_counter2+1
rotate_counter2:
	lda #$00
	bne +
	lda #<cube_ptr
        sta ptr4
        sta value_ptr
	lda #>cube_ptr
        sta value_ptr+1
        lda #$20-1
        sta rotate_counter3+1         
	inc anim_phase
        jmp rotate_cube3_cont
+	
	rts

rotate_cube3:
	dec angle
	lda angle
	and #$3f
	tax
	+set_angle
rotate_cube3_cont:
	dec rotate_counter3+1
rotate_counter3:
	lda #$00
	bne +
	inc anim_phase
+	lax zoom_factor
        sbx #$08
        stx zoom_factor
+	rts


rotate_ico_init:
	ldx #$20
	stx angle
	+set_angle
	lda #<ico_ptr	
        sta value_ptr
        sta ptr4
	lda #>ico_ptr	
        sta value_ptr+1
        sta ptr4+1
        lda #180/2
        sta rotate_ico_counter+1
	inc anim_phase
	rts
        
rotate_ico:
	bit angle
	lda angle
	and #$3f
	tax
	+set_angle
	lax zoom_factor
	beq +
	sbx #$fc
	stx zoom_factor
+	dec rotate_ico_counter+1
rotate_ico_counter:
	lda #$00
	bne +
	lda #<ico_ptr
        sta ptr4
        sta value_ptr
	lda #>ico_ptr
        sta ptr4+1
        sta value_ptr+1
        lda #180/2
        sta rotate_ico_counter+1
        lda #$e6
        sta rotate_ico
	inc anim_phase
+	rts


rotate_ico_slow:
	dec rotate_ico_counter2+1
rotate_ico_counter2:
	lda #$12
	bne +
	lda #<ico_ptr
        sta ptr4
        sta value_ptr
	lda #>ico_ptr
        sta ptr4+1
        sta value_ptr+1
	inc anim_phase
+	rts


end_anim:
	pla
	pla
	pla
	pla
	rts

!ifdef COMMENTED_OUT {
	lda angle
	and #$3f
	tax
	lda sintab,x
	clc
	adc #$80
	lsr
	lsr
	tax
	+set_angle


	inc angle

	dec anim_phase
	beq +
	+count_pointer_up
	jmp ++

+	inc ptr3
	bne +
	inc ptr3+1
	bne +
	jsr reset_coord	
+
;	lda $dc01
;	and #$10
;	bne +
;-	lda $dc01
;	and #$10
;	beq -
++	rts
	}

reset_coord:
	ldx #$20
	stx angle
	+set_angle
	ldx #$00
	stx anim_phase
        inx
	stx zoom_factor
	lda #<(coordlist+2)
        sta value_ptr
        lda #>(coordlist+2)
        sta value_ptr+1
        lda coordlist+1
	eor #$ff
	clc
	adc #$01
        sta ptr3
	lda coordlist
	eor #$ff
	adc #$00
	sta ptr3+1
        rts
        

coordlist:
	!bin "data.sav" ;,,$5e ;,,$21;,,$5e;,,$9f
cube_ptr:
        !bin "3ddat.bin"
ico_ptr:
        !bin "icoall.sav",,2
coordlist_end:
	


irq0:
	sta irq0_old_a+1
	stx irq0_old_x+1
	sty irq0_old_y+1
	!if index_tab = $d000 {
	lda $01
	sta irq0_old_01+1
	lda #$35
	sta $01
	}
	!ifdef DEBUG {
	lda #$0f
	sta $d020
	}


	inc $d019
	!ifdef RELEASE {
	jsr PLAY_MUSIC 
	} else {
	jsr $1003
	}
	!ifdef DEBUG {
	lda #$00
	sta $d020
	}
	!if index_tab = $d000 {
irq0_old_01:
	lda #$00
	sta $01
	}
irq0_old_a:
	lda #$00
irq0_old_x:
	ldx #$00
irq0_old_y:
	ldy #$00
reti:
	rti



	+align256
;sintab:
;	!bin "sintab"
;	!bin "sintab"
divtab_hi:
	!bin "divtab"
divtab_lo = divtab_hi + $100


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
;;                              Line Stuff
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!macro div_fract {
	;a = a/x
	;high byte ignored (assume x > a)
	sta multablo_ptr
	cpx multablo_ptr
	bne +
	lda #$ff
	jmp ++
+	ldy divtab_lo,x	
	sta multabhi_ptr
	eor #$ff
	sta multablo2_ptr
	sta multabhi2_ptr
	lda (multablo_ptr),y
	cmp (multablo2_ptr),y
	lda (multabhi_ptr),y
	sbc (multabhi2_ptr),y
        sta .l0+1
	ldy divtab_hi,x
	;sec
	lda (multablo_ptr),y
	sbc (multablo2_ptr),y
	;clc
.l0:	adc #$00
++	}

!macro div {
	;y/a = a/x
	ldy divtab_lo,x
	;stx tmp2
	sta multablo_ptr
	sta multabhi_ptr
	eor #$ff
	sta multablo2_ptr
	sta multabhi2_ptr
	sec
	lda (multablo_ptr),y
	sbc (multablo2_ptr),y
	lda (multabhi_ptr),y
	sbc (multabhi2_ptr),y
        sta .l0+1
	ldy divtab_hi,x
	sec
	lda (multablo_ptr),y
	sbc (multablo2_ptr),y
	tax
	lda (multabhi_ptr),y
	sbc (multabhi2_ptr),y
	tay
	txa
	clc
l0:	adc #$00
	bcc +
	iny
+
	}



;************ paint horizontal *************************

tabx5:
	!byte 6*branch_length+branch_length_special
	!byte 0*branch_length
	!byte 1*branch_length
	!byte 2*branch_length
	!byte 3*branch_length
	!byte 4*branch_length
	!byte 5*branch_length
	!byte 6*branch_length

tabx5_up:
	!byte 6*branch_length
	!byte 5*branch_length
	!byte 4*branch_length
	!byte 3*branch_length
	!byte 2*branch_length
	!byte 1*branch_length
	!byte 0*branch_length
	!byte 6*branch_length+branch_length_special


	!macro line_horizontal_fragment .charset, .col, .up {
	stx tmp4
	lda index_select_tab,x
	sta ptr1+1
	ldx #$00
        lax (ptr1,x)
	lda code_table_branch,x
	sta .bpl+1
.bpl:	bpl *+2
	!for i, 8 {
	lda .charset + (8-i) + (.col * BYTES_PER_ROW), y
	!if .up = 0 {
	ora code_table_byte0 + ((8-i) * $100),x
	} else {
	ora code_table_byte0 + $80 + ((i-1) * $100),x
	}
	sta .charset + (8-i) + (.col * BYTES_PER_ROW), y
	}
	tya
     	clc
	adc code_table_number_of_bytes,x
        tay
	lda tmp4
	adc tmp3
	tax
	}

	!macro line_horizontal .charset, .up {
	ldx dx
	lda dy
	cpx #$08
	bcs ++
	;trivial line routine
	txa
	sta tmp2
	lsr
	sta tmp1

	ldy y1
	ldx x1
-	
	lda x_coord_low_tab,x
	sta ptr1
	!if .charset = charset {
	lda x_coord_high_tab,x
	} else {
	lda x_coord_high_tab2,x
	}
	sta ptr1+1
	lda (ptr1),y
	ora x_coord_bit_tab,x
	sta (ptr1),y
	lda tmp1
	sec
	sbc dy
	bcs +
	adc dx
	!if .up = 0 {
	iny
	} else {
	dey
	}
+	sta tmp1
	inx
	dec tmp2
	bpl -
	rts

++	+div_fract
	sta ptr1

	!ifdef CLEAR_LINE_ACTIVE {
	!if .charset = charset {
	ldx clear_index
	sta clear_div,x
	asl
	asl
	asl
	sta tmp3

	lda x1
	sta clear_x1,x
	lda x2
	sta clear_x2,x
	lda y1
	sta clear_y1,x
	lda y2
	sta clear_y2,x
	!if .up = 0 {
	lda #$0
	} else {
	lda #$1
	}
	sta clear_octant,x
	inc clear_index
	} else {
	ldx clear_index2
	sta clear_div_2,x
	asl
	asl
	asl
	sta tmp3
	lda x1
	sta clear_x1_2,x
	lda x2
	sta clear_x2_2,x
	lda y1
	sta clear_y1_2,x
	lda y2
	sta clear_y2_2,x
	!if .up = 0 {
	lda #$0
	} else {
	lda #$1
	}
	sta clear_octant_2,x
	inc clear_index2
	}
	} else {
	asl
	asl
	asl
	sta tmp3
	}
	!if .up = 0 {
	lax x1
	} else {
	lax x2
	}
	anc #$07
	tay
	!if .up = 0 {
	lda div8tab,x
	} else {
	lda div8tab2,x
	}
	sta tmp2
	!if .up = 0 {
	lda tabx5,y
	} else {
	lda tabx5_up,y
	}
	sta .branch+1

	!ifdef FULL_LINE {
        !if .charset = charset {
	lda x_coord_high_tab,x
        } else {
	lda x_coord_high_tab2,x
        }
	sta ptr2+1
        
	lda x_coord_low_tab,x
	sta ptr2
        
	}
	!if .up = 0 {
	ldy y1
	} else {
	ldy y2
	}
	!ifdef FULL_LINE {
	lax value80
	} else {
	lda #$80
	}
.branch:bcc *+2
	!for i, 7 {
	!set branch_length_start = *
	!ifdef FULL_LINE {
	lda (ptr2),y
	!if .up = 0 {
	ora #1 << (7-i)
	} else {
	ora #1 << (i)
	}
	sta (ptr2),y
	txa
	adc ptr1
	!if i != 7 {
	tax
	}
	} else {
	adc ptr1
	}
	bcc +
	iny
	!if i != 7 {
	clc
	!set branch_length = * - branch_length_start
+
	} else {
+
	inc tmp2
	!set branch_length_special = * - branch_length_start
	}
	}
	sta .jmp-1
	!if .up = 0 {
	cpy y2
	} else {
	cpy y1
	}
	beq +++
	bcc +++
-	rts 
+++:

	!if .up = 0 {
	ldx x2
	} else {
	ldx x1
	}
	!if .up = 0 {
	lda div8tab,x
	} else {
	lda div8tab2,x
	}
	tax
	cpx tmp2
	bcc -
	lda .line_horizontal_start_adresses_low,x
	sta ptr2
	lda .line_horizontal_start_adresses_high,x
	sta ptr2+1
	ldx #$00
.quit:
	lda #$60
	sta (ptr2,x)
	ldx tmp2
	lda .line_horizontal_start_adresses_low,x
	sta .jmp+1
	lda .line_horizontal_start_adresses_high,x
	sta .jmp+2
	ldx #$00
	!ifdef FULL_LINE {
.jmp:	jsr 0
	stx tmp1
	ldx #$00
	lda #$86
	sta (ptr2,x)

	!if .up = 0 {
	ldx x2
	} else {
	ldx x1
	}
        !if .charset = charset {
	lda x_coord_high_tab,x
        } else {
	lda x_coord_high_tab2,x
        }
	sta ptr2+1
	lda x_coord_low_tab,x
	sta ptr2
	!if .up = 0 {
	lda #$80
	} else {
	lda #$01
	}
	sta tmp2
	txa
	anc #$07
	!if .up != 0 {
	eor #$07
	}
	beq ++
	tax
-	lda (ptr2),y
	ora tmp2
	sta (ptr2),y
	!if .up = 0 {
	lsr tmp2
	} else {
	asl tmp2
	}
	lda tmp1
	adc ptr1
	sta tmp1
	bcc +
	iny
	clc
+
	dex
	bne -
++	rts
	} else {
.jmp:	jsr 0
	ldy #$00
	lda #$86
	sta (ptr2),y
++	rts
	}

.line_horizontal_start_adresses_low:
	!for i,SIZE_X {
	!byte < (.line_horizontal_start_loop + (i-1) * line_horizontal_length)
	}

.line_horizontal_start_adresses_high:
	!for i,SIZE_X {
	!byte > (.line_horizontal_start_loop + (i-1) * line_horizontal_length)
	}

.line_horizontal_start_loop:
	!for i, SIZE_X {
	!set line_horizontal_start = *
	!if .up = 0 {
	+line_horizontal_fragment .charset, i-1,  0
	} else {
	+line_horizontal_fragment .charset, SIZE_X-i, 1
	}
	!set line_horizontal_length = * - line_horizontal_start
	}
	rts

}
        
;************ clear horizontal *************************

        !ifdef CLEAR_LINE_ACTIVE {
        
tabx5_clear:
	!byte 6*branch_length_clear+branch_length_special_clear
	!byte 0*branch_length_clear
	!byte 1*branch_length_clear
	!byte 2*branch_length_clear
	!byte 3*branch_length_clear
	!byte 4*branch_length_clear
	!byte 5*branch_length_clear
	!byte 6*branch_length_clear

tabx5_clear_up:
	!byte 6*branch_length_clear
	!byte 5*branch_length_clear
	!byte 4*branch_length_clear
	!byte 3*branch_length_clear
	!byte 2*branch_length_clear
	!byte 1*branch_length_clear
	!byte 0*branch_length_clear
	!byte 6*branch_length_clear+branch_length_special_clear

        }
          
        !macro line_horizontal_fragment_clear .charset, .col {
	stx tmp4
	lda index_select_tab,x
	sta ptr1+1
	ldx #$00
	lax (ptr1,x)
	lda code_table_branch_clear,x
	sta .bpl+1
        anc #$00
.bpl:	beq *+2
	!for i, 8 {
        sta .charset + (8-i) + (.col * BYTES_PER_ROW), y
        }
	tya
	adc code_table_number_of_bytes,x
	tay
	lda tmp4
	adc tmp3
	tax
        }



	!macro line_horizontal_clear .charset, .up  {
	!if .charset = charset {
	ldx clear_index
	lda clear_x1,x
	sta x1
	lda clear_y1,x
	sta y1
	lda clear_x2,x
	sta x2
	lda clear_y2,x
	sta y2
	lda clear_div,x
	} else {
	ldx clear_index2
	lda clear_x1_2,x
	sta x1
	lda clear_y1_2,x
	sta y1
	lda clear_x2_2,x
	sta x2
	lda clear_y2_2,x
	sta y2
	lda clear_div_2,x
	}
	;ldx dx
	;lda dy
	;+div_fract
	sta ptr1
	asl
	asl
	asl
	sta tmp3
	!if .up = 0 {
	lax x1
	} else {
	lax x2
	}
	anc #$07
	tay
	txa
	!if .up = 0 {
	lda div8tab,x
	} else {
	lda div8tab2,x
	}
	sta tmp2
	!if .up = 0 {
	lda tabx5_clear,y
	} else {
	lda tabx5_clear_up,y
	}
	sta .branch+1

	!ifdef FULL_LINE {
        !if .charset = charset {
	lda x_coord_high_tab,x
        } else {
	lda x_coord_high_tab2,x
        }
	sta ptr2+1
	lda x_coord_low_tab,x
	sta ptr2
	}
	!if .up = 0 {
	ldy y1
	} else {
	ldy y2
	}
	ldx #$80
	txa
.branch:bcc *+2
	!for i, 7 {
	!set branch_length_start_clear = *
	!ifdef FULL_LINE {
	lda #$00
	sta (ptr2),y
	txa
	adc ptr1
	!if i != 7 {
	tax
	}
	} else {
	adc ptr1
	}
	bcc +
	iny
	!if i != 7 {
	clc
	!set branch_length_clear = * - branch_length_start_clear
+
	} else {
+
	inc tmp2
	!set branch_length_special_clear = * - branch_length_start_clear
	}
	}
	!if .up = 0 {
	cpy y2
	} else {
	cpy y1
	}
	beq +++
	bcc +++
	rts 
+++:
	sta .jmp-1
	!if .up = 0 {
	ldx x2
	lda div8tab,x
	} else {
	ldx x1
	lda div8tab2,x
	}
	tax
	lda .line_horizontal_start_adresses_low,x
	sta ptr2
	lda .line_horizontal_start_adresses_high,x
	sta ptr2+1
	ldx #$00
	lda #$60
	sta (ptr2,x)
	ldx tmp2
	lda .line_horizontal_start_adresses_low,x
	sta .jmp+1
	lda .line_horizontal_start_adresses_high,x
	sta .jmp+2
	ldx #$00
	!ifdef FULL_LINE {
.jmp:	jsr 0
	ldx #$00
	lda #$86
	sta (ptr2,x)
	!if .up = 0 {
	ldx x2
	} else {
	ldx x1
	}
	beq ++
        !if .charset = charset {
	lda x_coord_high_tab,x
        } else {
	lda x_coord_high_tab2,x
        }
	sta ptr2+1
	lda x_coord_low_tab,x
	sta ptr2
	lda #$00
	!for i, 7 {
	sta (ptr2),y
	iny
	}
	sta (ptr2),y
++	rts
	} else {
.jmp:	jsr 0
	ldy #$00
	lda #$86
	sta (ptr2),y
	rts
	}

.line_horizontal_start_adresses_low:
	!for i,SIZE_X {
	!byte < (.line_horizontal_start_loop + (i-1) * line_horizontal_length_clear)
	}

.line_horizontal_start_adresses_high:
	!for i,SIZE_X {
	!byte > (.line_horizontal_start_loop + (i-1) * line_horizontal_length_clear)
	}

.line_horizontal_start_loop:
	!for i, SIZE_X {
	!set line_horizontal_start_clear = *
	!if .up = 0 {
	+line_horizontal_fragment_clear .charset, i-1
	} else {
	+line_horizontal_fragment_clear .charset, SIZE_X-i
	}
	!set line_horizontal_length_clear = * - line_horizontal_start_clear
	}
	rts


}


line_horizontal:
	+line_horizontal charset, 0

line_horizontal_up:
	+line_horizontal charset, 1

line_horizontal2:
	+line_horizontal charset2, 0

line_horizontal_up2:
	+line_horizontal charset2, 1

        !ifdef CLEAR_LINE_ACTIVE {
line_horizontal_clear:
	+line_horizontal_clear charset, 0

line_horizontal_up_clear:
	+line_horizontal_clear charset, 1

line_horizontal_clear2:
	+line_horizontal_clear charset2, 0

line_horizontal_up_clear2:
	+line_horizontal_clear charset2, 1
}





;;;;;;;;;;;;;;;;;; line vertical ;;;;;;;;;;;;;

!ifdef CLEAR_LINE_ACTIVE {

!macro set_new_area_code .section, .clear_area_index, .clear_section, .clear_startoffset {
	ldx .clear_area_index
!ifdef CLEARCODE_ALIGNED {
	!if .section != -1 {
	!if .clear_area_index = clear_area_index {
	lda #.section + >clearcode_0
	} else {
	lda #.section + >clearcode_0_2
	}
	}
} else {
	!if .section != -1 {
	lda #.section
	}
}
	sta .clear_section,x
	tya
	sta .clear_startoffset,x
}


!macro close_old_area_code .clear_area_index, .clear_endoffset {
	ldx .clear_area_index

	; checkcode
	;!if .clear_area_index = clear_area_index {
	;lda clear_section,x
	;} else {
	;lda clear_section_2,x
	;}
;-	cmp #$40
;	bcc -
	tya
	sta .clear_endoffset,x
	inc .clear_area_index
}

!macro set_new_area .section, .charset {
	!if .charset = charset {
	+set_new_area_code .section, clear_area_index, clear_section, clear_startoffset
	} else {
	+set_new_area_code .section, clear_area_index2, clear_section_2, clear_startoffset_2
	}
}

!macro close_old_area .charset {
	!if .charset = charset {
	+close_old_area_code clear_area_index, clear_endoffset
	} else {
	+close_old_area_code clear_area_index2, clear_endoffset_2
	}
}


        +align128
clearcode_entry_offset:
        !for i, 32 {
                !byte (32-i)*3
        }
        !for i, 32 {
                !byte beq20_offset
        }
        !for i, 32 {
                !byte beq40_offset
        }
        !for i, 32 {
                !byte beq60_offset
        }

        !macro clearcode .addr {

	!set do_entry_offset_start = *

.do_beq_60:
        tya
        anc #$1f
        sta tmp1
        txa
        adc #$40
        adc tmp1
        tax
        lda #$00
        jsr .do_uncond
	txa
	sbx #$20
	lda #$00
	jsr .do_uncond
	txa
	sbx #$20
	lda #$00
	jsr .do_uncond
        txa
	sbc tmp1
	tax
        ldy tmp1
	bne .do_entry
	rts

.do_beq_40:
        tya
        anc #$1f
        sta tmp1
        txa
        adc #$20
        adc tmp1
        tax
        lda #$00
        jsr .do_uncond
	txa
	sbx #$20
	lda #$00
	jsr .do_uncond
        txa
	sbc tmp1
	tax
        ldy tmp1
	bne .do_entry
	rts

.do_beq_20:
        tya
        anc #$1f
        sta tmp1
	stx tmp2
        txa
        adc tmp1
        tax
        lda #$00
        jsr .do_uncond
        ldx tmp2
        ldy tmp1

        !set beq60_offset = .do_beq_60 - (.do_uncond)
        !set beq40_offset = .do_beq_40 - (.do_uncond)
        !set beq20_offset = .do_beq_20 - (.do_uncond)

	!set do_entry_offset = * - do_entry_offset_start
	 
.do_entry:        
        lda clearcode_entry_offset,y
        sta .do_beq+1
        lda #$00
.do_beq:beq *+2
.do_uncond
        !for i, 32 {
	sta .addr+(32-i),x
        }
        rts
        
} 



!macro clear_section_head .clear_area_index, .clear_section, .clear_startoffset, .clear_endoffset, .clearcode_section_low, .clearcode_section_high {
	dec .clear_area_index
	bmi +
-	ldy .clear_area_index
	!ifdef CLEARCODE_ALIGNED {
	lda .clear_section,y
	sta .jmp+2
	} else {
	ldx .clear_section,y
	lda .clearcode_section_low,x
	sta .jmp+1
	lda .clearcode_section_high,x
	sta .jmp+2
	}
	ldx .clear_startoffset,y
	lda .clear_endoffset,y
	sec
	sbc .clear_startoffset,y
	tay
.jmp:	jsr do_entry_offset ; dummy for CLEARCODE_ALIGNED not set
	dec .clear_area_index
	bpl -

+	inc .clear_area_index
	rts

}

!macro clear_sections .charset {

!if .charset = charset {
	+clear_section_head clear_area_index, clear_section, clear_startoffset, clear_endoffset, .clearcode_section_low, .clearcode_section_high
} else {
	+clear_section_head clear_area_index2, clear_section_2, clear_startoffset_2, clear_endoffset_2, .clearcode_section_low, .clearcode_section_high
}


!ifdef CLEARCODE_ALIGNED { 
	+align256

!if .charset = charset {
clearcode_0:
} else {
clearcode_0_2:
}
	}

.clearcode_0:
	!for i, SIZE_X {
	!set clearcode_0_start = *
	+clearcode .charset+(i-1)*BYTES_PER_ROW
!ifdef CLEARCODE_ALIGNED { 
	!if i != SIZE_X {
	+align256
	}
}
	!set clearcode_0_len = *-clearcode_0_start
	}

!ifdef CLEARCODE_ALIGNED {
.clearcode_section_low:
.clearcode_section_high:
} else {		  
.clearcode_section_low:
	!for i,SIZE_X {
	!byte <(.clearcode_0 + (i-1) * clearcode_0_len + do_entry_offset)
	}

.clearcode_section_high:
	!for i,SIZE_X {
	!byte >(.clearcode_0 + (i-1) * clearcode_0_len + do_entry_offset)
	}
}
}

}

;;;;;;;;;; vertical line ;;;;;;;;;;;;;;;;;;;;;;;


!macro line_vertical .up, .charset {
	ldx dy
	lda dx
	+div_fract
	sta tmp1

	ldx x2
	lda .line_vertical_entry_tab_low,x
	sta ptr1
	lda .line_vertical_entry_tab_high,x
	sta ptr1+1

	ldy #$00
	!ifdef CLEAR_LINE_ACTIVE {
	lda (ptr1),y
	sta .exit_clear_up+1
	}
	lda #$60
	sta (ptr1),y
	ldy y1

	ldx x1

	!ifdef FULL_LINE {
	} else {
	lda #$80
	clc
	!for i, 8 {
		adc tmp1
		bcc +
		!if .up = 0 {
		inx
		} else {
		dex
		}
		clc
+
	}
	sta .new_x+1
	tya
	adc #$08
	tay
	cpy y2
	bcs .exit_clear_up
	}

	lda .line_vertical_entry_tab_low,x
	sta .line_vertical_jmp+1
	lda .line_vertical_entry_tab_high,x
	sta .line_vertical_jmp+2

	!ifdef CLEAR_LINE_ACTIVE {
	txa
	anc #$07
	!if .up != 0 {
	eor #$07
	}
	beq +
!ifdef CLEARCODE_ALIGNED {
	!if .charset = charset {
	lda div8tab_clearcode0,x
	} else {
	lda div8tab_clearcode0_2,x
	}
} else {
	lda div8tab,x
}
	+set_new_area -1, .charset
+	} else {
	clc
	}
.new_x:
	ldx #$80
.line_vertical_jmp:
	jsr 0

        lax x2
	!ifdef CLEAR_LINE_ACTIVE {
	anc #$07
	!if .up != 0 {
	eor #$07
	}
	bne .no_open_close
!ifdef CLEARCODE_ALIGNED {
	!if .charset = charset {
	lda div8tab_clearcode0,x
	} else {
	lda div8tab_clearcode0_2,x
	}
} else {
	lda div8tab,x
}
	+set_new_area -1, .charset
	ldx x2
.no_open_close:
	}
        lda x_coord_low_tab,x
        sta .newptr2+1
        sta .newptr2_2+1

        !if .charset = charset {
	lda x_coord_high_tab,x
        } else {
	lda x_coord_high_tab2,x
        }
        sta .newptr2+2
        sta .newptr2_2+2

        lda x_coord_bit_tab,x
        sta .newtmp1+1

        tya
        ;sec
        sbc y2
        tax
        beq +
	!ifdef FULL_LINE {
	} else {
	sbx #$f8
	bcs +
	}
        
-
.newtmp1:       
        lda #$00
.newptr2:
	ora $ffff,y
.newptr2_2:
        sta $ffff,y
        iny
        inx
        bne -
        
+	
	!ifdef CLEAR_LINE_ACTIVE {
	+close_old_area .charset
	}
.exit_clear_up:
	lda #$8a
	ldy #$00
	sta (ptr1),y
	rts


.line_vertical_code:
	!for i, SIZE_X*8 {
	!ifdef CLEAR_LINE_ACTIVE {
	!if (i & 7) = 1 {
		!set line_vertical_init_code = *
		stx tmp2
		!if .up = 0 {
		+set_new_area ((i-1) >> 3), .charset
		} else {
		+set_new_area ((SIZE_X*8-i) >> 3), .charset
		}
		ldx tmp2
		!set line_vertical_init_code_len = * - line_vertical_init_code
	}
	} else {
		!set line_vertical_init_code_len = 0
	}
	!set line_vertical_code_start = *
-	txa
	adc tmp1
	tax
	!if .up = 0 {
	lda .charset + (((i-1) >> 3) * BYTES_PER_ROW),y
	ora #(1 << (7- ((i-1) & 7)))
	sta .charset + (((i-1) >> 3) * BYTES_PER_ROW),y
	} else {
	lda .charset + (((SIZE_X*8-i) >> 3) * BYTES_PER_ROW),y
	ora #(1 << ((i-1) & 7))
	sta .charset + (((SIZE_X*8-i) >> 3) * BYTES_PER_ROW),y
	}
	iny
	bcc -
	;clc
	!set line_vertical_code_len = * - line_vertical_code_start
	!ifdef CLEAR_LINE_ACTIVE {
	!if (i & 7) = 0 {
		!set line_vertical_init_code2 = *
		stx tmp2
		+close_old_area .charset
		ldx tmp2
		!set line_vertical_init_code_len2 = * - line_vertical_init_code2
	}
	} else {
		!set line_vertical_init_code_len2 = 0
	}
	}
.line_vertical_code_end:
	rts


	;+align128
	!if .up = 0 {

	!set .ptr = .line_vertical_code
.line_vertical_entry_tab_low:
	!for i, SIZE_X*8 {
		!byte <.ptr
		!if ((i & 7) = 1) {
		!set .ptr = .ptr + line_vertical_init_code_len
		}
		!if ((i & 7) = 0) {
		!set .ptr = .ptr + line_vertical_init_code_len2
		}
		!set .ptr = .ptr + line_vertical_code_len
	}


	!set .ptr = .line_vertical_code
.line_vertical_entry_tab_high:
	!for i, SIZE_X*8 {
		!byte >.ptr
		!if ((i & 7) = 1) {
		!set .ptr = .ptr + line_vertical_init_code_len
		}
		!if ((i & 7) = 0) {
		!set .ptr = .ptr + line_vertical_init_code_len2
		}
		!set .ptr = .ptr + line_vertical_code_len
	}

	} else {

	!set .ptr = .line_vertical_code_end
.line_vertical_entry_tab_low:
	!for i, SIZE_X * 8 {
		!if ((i & 7) = 0) {
		!set .ptr = .ptr - line_vertical_init_code_len
		}
		!if ((i & 7) = 1) {
		!set .ptr = .ptr - line_vertical_init_code_len2
		}
		!set .ptr = .ptr - line_vertical_code_len
		!byte <.ptr
	}

	!set .ptr = .line_vertical_code_end
.line_vertical_entry_tab_high:
	!for i, SIZE_X * 8 {
		!if ((i & 7) = 0) {
		!set .ptr = .ptr - line_vertical_init_code_len
		}
		!if ((i & 7) = 1) {
		!set .ptr = .ptr - line_vertical_init_code_len2
		}
		!set .ptr = .ptr - line_vertical_code_len
		!byte >.ptr
	}
	}
}

line_vertical:
	+line_vertical 0, charset
line_vertical_up:
	+line_vertical 1, charset


line_vertical2:
	+line_vertical 0, charset2
line_vertical_up2:
	+line_vertical 1, charset2


!macro swap_coords {
	ldy y2
	ldx y1
	stx y2
	sty y1
	ldy x2
	ldx x1
	stx x2
	sty x1
}

;      \       |       /
;       \      |      /
;        \ O7  | O5  /
;         \    |    /
;          \   |   /
;      O8   \  |  /   O6
;	     \ | /
;             \|/
;-----------------------------
;             /|\
;            / | \
;      O4   /  |  \   O2
;          /   |   \
;         /    |    \
;        /  O3 | O1  \
;       /      |      \
;      /       |       \

	!macro line .charset {

	lda y2
	sec
	sbc y1
	bcc .octant_5_to_8
	sta dy
	lda x2
	sec
	sbc x1
	bcc .octant_3_to_4
	sta dx
	cmp dy
	bcs .octant_2
	!if .charset = charset {
	jmp line_vertical
.octant_2:
	jmp line_horizontal
	} else {
	jmp line_vertical2
.octant_2:
	jmp line_horizontal2
	}

.octant_3_to_4:
	eor #$ff
	adc #$01
	sta dx
	cmp dy
	bcs .octant_4
	!if .charset = charset {
	jmp line_vertical_up
.octant_4:
	+swap_coords
	jmp line_horizontal_up
	} else {
	jmp line_vertical_up2
.octant_4:
	+swap_coords
	jmp line_horizontal_up2
	}

.octant_5_to_8:
	eor #$ff
	adc #$01
	sta dy
	lda x2
	sec
	sbc x1
	bcc .octant_7_to_8
	sta dx
	cmp dy
	bcs .octant_6
	+swap_coords
	!if .charset = charset {
	jmp line_vertical_up
.octant_6:
	jmp line_horizontal_up
	} else {
	jmp line_vertical_up2
.octant_6:
	jmp line_horizontal_up2
	}
	

.octant_7_to_8:
	eor #$ff
	adc #$01
	sta dx
	+swap_coords
	cmp dy
	bcs .octant_8
	!if .charset = charset {
	jmp line_vertical
.octant_8:
	jmp line_horizontal
	} else {
	jmp line_vertical2
.octant_8:
	jmp line_horizontal2
	}
	}

line:
	+line charset

line2:
	+line charset2

!ifdef CLEAR_LINE_ACTIVE {

clearscreen:
	dec clear_index
	bmi +
-	ldx clear_index
	lda clear_octant,x
	bne jmp1
	jsr line_horizontal_clear
	dec clear_index
	bpl -
+	
	inc clear_index
	jmp ++


jmp1:	jsr line_horizontal_up_clear
	dec clear_index
	bpl -
	inc clear_index
++	+clear_sections charset



clearscreen2:
	dec clear_index2
	bmi +
-	ldx clear_index2
	lda clear_octant_2,x
	bne jmp2
	jsr line_horizontal_clear2
	dec clear_index2
	bpl -
+	inc clear_index2
	jmp ++

jmp2:	jsr line_horizontal_up_clear2
	dec clear_index2
	bpl -	
	inc clear_index2
++	+clear_sections charset2

} else {

!ifdef COMMENTED_OUT {
generate_clearscreens:
        lda #<charset
        sta ptr1
        lda #>charset
        sta ptr1+1
        lda #<clearscreen
        sta ptr2
        lda #>clearscreen
        sta ptr2+1
        jsr generate_clearscreen
        lda #<charset2
        sta ptr1
        lda #>charset2
        sta ptr1+1
        lda #<clearscreen2
        sta ptr2
        lda #>clearscreen2
        sta ptr2+1

generate_clearscreen        
        ldy #$00
        lda #$a9
        sta (ptr2),y
        tya
        iny
        sta (ptr2),y
        iny
        ldx #$00
        
-       dex
        bpl +
screen_mask_ptr:
        lda clearmask
        sta tmp1
        inc screen_mask_ptr+1
        ldx #$07
+       asl tmp1
        bcc +
        lda #$8d
        sta (ptr2),y
        iny
        lda ptr1
        sta (ptr2),y
        iny
        lda ptr1+1
        sta (ptr2),y
        iny
+       tya
        clc
        adc ptr2
        sta ptr2
        ldy #$00
        bcc +
        inc ptr2+1
+
        inc ptr1
        bne -
        inc ptr1+1
        lda ptr1+1
        and #$07
        bne -
        lda #$60
        sta (ptr2),y
        rts
     +align256   
} else {

!macro clearscreen .screen {
	!byte $ab,$00
-	
	!for i, 16 {
        !if i = 1 {
	sta .screen + (i-1) * $80+8,x
        } else {
	sta .screen + (i-1) * $80,x
        }
	}
	inx
	bpl -
	rts
}

clearscreen:
	+clearscreen charset

clearscreen2:
	+clearscreen charset2

}
}

init_bigmultab:
	ldy #$00
	sty tmp1

--
	lda #-$80
	sta tmp2
	ldx tmp1
	lda bigmultab_ptr+1
	asl
	lda bigmultab_ptr+2
	rol
	bcc error
	sta rot_tab,x
	sta rot_tab+$40,x

-	ldy tmp1
	lda sintab,y
	tay
	+imul tmp2
bigmultab_ptr:
	stx bigmultab
	inc bigmultab_ptr+1
	bne +
	inc bigmultab_ptr+2
+
	inc tmp2
	inc tmp2
	lda tmp2
	cmp #$80
	bne -
	inc tmp1
	lda tmp1
	cmp #sintab_size
	bne --
	rts

error:
	inc $d020
	jmp error

sintab:
        !byte 0,12,24,36,48,59,70,80
        !byte 89,98,105,112,117,121,124,126
        !byte 127,126,124,121,117,112,105,98
        !byte 89,80,70,59,48,36,24,12
        !byte 0,-12,-24,-36,-48,-59,-70,-80
        !byte -89,-98,-105,-112,-117,-121,-124,-126
        !byte -127,-126,-124,-121,-117,-112,-105,-98
        !byte -89,-80,-70,-59,-48,-36,-24,-12


sintab_size = * - sintab

generate_fadeout_part:
	lda #<screen
        sta ptr1
        lda #>screen
        sta ptr1+1
        lda #<SCROLL_SCREEN
        sta ptr3
        lda #>SCROLL_SCREEN
        sta ptr3+1
        lda #>$d800
	sta tmp1

generate_fadeout_part_sub:        
        lda #25
        sta tmp2
--	ldx #39
-	
	cpx #10
        bcs +++


	ldy #$00
	lda #$ad
        sta (ptr3),y
        ldy #$03
        lda #$8d
        sta (ptr3),y
        ldy #$04
        lda ptr1
        sta (ptr3),y
        ldy #$05
        lda ptr1+1
        sta (ptr3),y
        inc ptr1
        bne +
        inc ptr1+1
        inc tmp1
+       lda ptr1
	ldy #$01
        sta (ptr3),y
        lda ptr1+1
        ldy #$02
        sta (ptr3),y
        lda ptr3
        clc
        adc #6
        sta ptr3
        bcc ++
        inc ptr3+1 
++	jmp cont_generate
        
        

+++	ldy #$00
	lda #$ad
        sta (ptr3),y
        ldy #$06
        sta (ptr3),y
        ldy #$03
        lda #$8d
        sta (ptr3),y
        ldy #$09
        sta (ptr3),y
        ldy #$04
        lda ptr1
        sta (ptr3),y
        ldy #$0a
        sta (ptr3),y
        ldy #$05
        lda ptr1+1
        sta (ptr3),y
        ldy #$0b
        lda tmp1
        sta (ptr3),y
        inc ptr1
        bne +
        inc ptr1+1
        inc tmp1
+       lda ptr1
	ldy #$01
        sta (ptr3),y
        ldy #$07
        sta (ptr3),y
        lda ptr1+1
        ldy #$02
        sta (ptr3),y
        ldy #$08
        lda tmp1
        sta (ptr3),y
        lda ptr3
        clc
        adc #12
        sta ptr3
        bcc +
        inc ptr3+1 
+
cont_generate:	
	dex
        beq +
	jmp -
+       ldy #$00
        lda #$a9
        sta (ptr3),y
        tya
        iny
        sta (ptr3),y
        lda #$8d
        iny
        sta (ptr3),y
        lda ptr1
        iny
        sta (ptr3),y
        iny
        lda ptr1+1
        sta (ptr3),y
        iny
        ;$d800 clear
        lda #$8d
        sta (ptr3),y
        iny
        lda ptr1
        sta (ptr3),y
        iny
        lda ptr1+1
        and #$03
        ora #$d8
        sta (ptr3),y
        iny
        cmp #$da
        bne no_special
        lda ptr1
        cmp #$28
        bcs no_special
        lda #$ad
        sta (ptr3),y
        iny
        lda ptr3
        clc
        adc #06
        sta (ptr3),y
        iny
        lda ptr3+1
        adc #$00
        sta (ptr3),y
        iny
        lda #$d0
        sta (ptr3),y
        iny
        lda #$03
        sta (ptr3),y
        iny
        lda #$ce
        sta (ptr3),y
        iny
        lda ptr3
        clc
        adc #07
        sta (ptr3),y
        iny
        lda ptr3+1
        adc #$00
        sta (ptr3),y
        iny
        lda #$ce
        sta (ptr3),y
        iny
        lda ptr3
        clc
        adc #06
        sta (ptr3),y
        iny
        lda ptr3+1
        adc #$00
        sta (ptr3),y
        iny
        tya
        clc
        adc ptr3
        sta ptr3
	jmp cont_special        
        
no_special:
        lda #$ce
        sta (ptr3),y
        iny
        lda ptr3
        clc
        adc #06
        sta (ptr3),y
        iny
        lda ptr3+1
        adc #$00
        sta (ptr3),y
        iny               
        lax ptr3
        sbx #$f5
        stx ptr3
cont_special:
        bcc +
        inc ptr3+1
+       inc ptr1
	bne +
        inc ptr1+1
+	dec tmp2
	beq ++
        lda ptr3+1
        cmp #$bf
        beq make_c4
        cmp #$cf
        beq make_e8
        cmp #$fa
        bne +
	ldx #$02
        !byte $2c
make_c4:
	ldx #$c4
        !byte $2c
make_e8:
	ldx #$e8                
	ldy #$00
	lda #$4c
        sta (ptr3),y
        tya
        iny
        sta (ptr3),y
        iny
        txa
        sta (ptr3),y
        stx ptr3+1
        lda #$00
        sta ptr3
+       jmp --
++	ldy #$00
	lda #$60
        sta (ptr3),y	
	rts

	;* = $8000
end:



	!ifdef RELEASE {
        } else {
init_stable_timer:

	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	; no interrupts please in this function !
	; reset timer
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #%00000000
        sta $dc0f
        lda #%10000000
        sta $dc0e
        lda #$82
        sta $dc0d
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dc04
        lda #>(63-1)
        sta $dc05
	; but not start it yet !

	;+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	nop
	nop
	nop
        ldy #%10000001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
        sty $dc0e
	;+check_same_page_end
	rts
	}

init_shiftab:
	ldx #$00
	txa
	!byte $c9
lb1:  	tya
	adc #$00
ml1:  	sta multabhi,x
	tay
	cmp #$40
	txa
	ror
ml9:  	adc #$00
	sta ml9+1
	inx
ml0:  	sta multablo,x
	bne lb1
	inc ml0+2
	inc ml1+2
	clc
	iny
	bne lb1

	ldx #$00
	ldy #$ff
-
	lda multabhi+1,x
	sta multabhi2+$100,x
	lda multabhi,x
	sta multabhi2,y
	lda multablo+1,x
	sta multablo2+$100,x
	lda multablo,x
	sta multablo2,y
	dey
	inx
	bne -

	lda #>multablo
	sta multablo_ptr+1
	lda #>multablo2
	sta multablo2_ptr+1
	lda #>multabhi
	sta multabhi_ptr+1
	lda #>multabhi2
	sta multabhi2_ptr+1
	rts


calcline:
	;lda $01
	;pha
	;lda #$30
	;sta $01
	;calc invers tab
	ldx #$00
-	stx tmp1
	!for i,8 {
	asl tmp1
	ror
	}
	sta invers_tab,x
	inx
	bne -

	!if index_tab = $d000 {
	lda #$30
	sta $01
	}
	lda #$00
	sta tmp6
	lda #$00
	sta tmp3
	lda #$00
	sta ptr2
	sta ptr3
	lda #>index_tab
	sta ptr3+1

---
	lda #$00
	sta tmp2
--	lda tmp3
	sta tmp1
	lda #$00
	!for i, 9 {
	sta used_array+(i-1)
	}
	ldx #$08
	ldy #$00
-
	lda bitmask-1,x
	ora used_array,y
	sta used_array,y
	lda tmp1
	clc
	adc tmp2
	sta tmp1
	bcc +
	iny
+
	dex
	bne -
	sty tmp4

	ldx #$00
-	cpx tmp6
	beq do_new_code
	lda tmp6
	sta do_new_code_cmp+1
-	lda used_array
	cmp code_table_byte0,x
	bne next
	lda used_array+1
	cmp code_table_byte1,x
	bne next
	lda used_array+2
	cmp code_table_byte2,x
	bne next
	lda used_array+3
	cmp code_table_byte3,x
	bne next
	lda used_array+4
	cmp code_table_byte4,x
	bne next
	lda used_array+5
	cmp code_table_byte5,x
	bne next
	lda used_array+6
	cmp code_table_byte6,x
	bne next
	lda used_array+7
	cmp code_table_byte7,x
	bne next
        lda tmp4
        cmp code_table_number_of_bytes,x
        bne next
	txa
	ldy tmp2
	sta (ptr3),y
	jmp no_new_code
next:	inx
do_new_code_cmp:
	cpx #00
	bne -

do_new_code:
	;new code
	txa
	ldy tmp2
	sta (ptr3),y

	ldx tmp6
	lda used_array
	sta code_table_byte0,x
	lda used_array+1
	sta code_table_byte1,x
	lda used_array+2
	sta code_table_byte2,x
	lda used_array+3
	sta code_table_byte3,x
	lda used_array+4
	sta code_table_byte4,x
	lda used_array+5
	sta code_table_byte5,x
	lda used_array+6
	sta code_table_byte6,x
	lda used_array+7
	sta code_table_byte7,x
	lda tmp4
	sta code_table_number_of_bytes,x
        cmp #7
        bcc +
        lda #$07
        sta tmp4
+
	asl
	asl
	asl
        adc tmp4
	eor #$ff
	adc #(7*9)+1
	sta code_table_branch,x
        lda tmp4
        asl
        adc tmp4
        eor #$ff
        adc #(7*3)+1
        sta code_table_branch_clear,x
        
	inc tmp6

no_new_code:
	inc tmp2
	beq +
	jmp --
+	inc ptr3+1
	lda tmp3
	clc
	adc #$10
	sta tmp3
	bcs +
	jmp ---
+	


	!if index_tab = $d000 {
	lda #$35
	sta $01
	}

	ldx #$00
	lda #>index_tab
--	ldy #$10
-	
	sta index_select_tab,x
	inx
	dey
	bne -
	clc
	adc #$01
	cpx #$00
	bne --

	; turn table
	ldx #$00
-
	!for i, 8 {
	ldy code_table_byte0 + (i-1) * $100,x
	lda invers_tab,y
	sta code_table_byte0 + (8-i) * $100 + $80,x
	}
	inx
	cpx tmp6
	bne -
	;pla
	;sta $01
	rts

init_tables:
	ldx #$07
	lda #$80
-	sta bitmask,x
	lsr
	dex
	bpl -

	;screentabs

	ldx #$00
	stx ptr1
	ldy #>charset
-	lda ptr1
	sta x_coord_low_tab,x
	sta x_coord_low_tab+1,x
	sta x_coord_low_tab+2,x
	sta x_coord_low_tab+3,x
	sta x_coord_low_tab+4,x
	sta x_coord_low_tab+5,x
	sta x_coord_low_tab+6,x
	sta x_coord_low_tab+7,x
	tya
	sta x_coord_high_tab,x
	sta x_coord_high_tab+1,x
	sta x_coord_high_tab+2,x
	sta x_coord_high_tab+3,x
	sta x_coord_high_tab+4,x
	sta x_coord_high_tab+5,x
	sta x_coord_high_tab+6,x
	sta x_coord_high_tab+7,x

	lda ptr1
	clc
	adc #BYTES_PER_ROW
	sta ptr1
	bcc +
	iny
+
	txa
	sbx #$f8
	bne -

        clc
-       lda x_coord_high_tab,x
        adc #>(charset2 - charset)
        sta x_coord_high_tab2,x
        inx
        bne -

        lda #$80
-       sta x_coord_bit_tab,x
        lsr
        bcc +
        ror
+       inx
        bne -

	ldx #$00
-	txa
	lsr
	lsr
	lsr
	sta div8tab,x
	tay
	eor #$ff
	clc
	adc #SIZE_X
	sta div8tab2,x
!ifdef CLEAR_LINE_ACTIVE {
!ifdef CLEARCODE_ALIGNED {
	tya
	clc
	adc #>clearcode_0
	sta div8tab_clearcode0,x
	tya
	clc
	adc #>clearcode_0_2
	sta div8tab_clearcode0_2,x
}
}
	inx
	bne -
	rts

         
	!ifdef RELEASE {
        } else {
	!src "../monkey/vector_subpart.asm"
        }
     

        
        !ifdef CLEAR_LINE_ACTIVE {
        } else {
clearmask:
	!for i,SIZE_X*SIZE_Y {
	;!byte $ff
	}
        ;!bin "clearmask"
}

