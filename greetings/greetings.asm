
;--------------------------------------------------
;16 charsets + screen + sprites at...
;
; screen  = $7800
; char00  = $4000	$d018 %11100000	bank %00000010
; char01  = $4800	$d018 %11100010	bank %00000010
; char02  = $5000	$d018 %11100100	bank %00000010
; char03  = $5800	$d018 %11100110	bank %00000010
; char04  = $6000	$d018 %11101000	bank %00000010
; char05  = $6800	$d018 %11101010	bank %00000010
; char06  = $7000	$d018 %11101100	bank %00000010
;
; screen  = $b800
; char07  = $a000	$d018 %11101000	bank %00000001
; char08  = $a800	$d018 %11101010	bank %00000001
; char09  = $b000	$d018 %11101100	bank %00000001
;
; screen  = $f000
; char0a  = $c000	$d018 %11100000	bank %00000000
; char0b  = $c800	$d018 %11100010	bank %00000000
; char0c  = $d000	$d018 %11100100	bank %00000000
; char0d  = $d800	$d018 %11100110	bank %00000000
; char0e  = $e000	$d018 %11101000	bank %00000000
; char0f  = $e800	$d018 %11101010	bank %00000000
;
;--------------------------------------------------

        !cpu 6510

	!ifdef RELEASE {
        !src "config.asm"
        !src "../framework/framework.inc"
	!to "greetings.plain",plain
	* = PARTORG
	jmp start
        } else {
        !to "greetings.prg",cbm
         *= $0801
        !by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00
	jmp start
* = $1000
	!bin "Onslaught_HokutoForce_Coop.sid",,126
        }

; uv-map = $8000-$9fff (8k frei zum laden, wenn die chars generiert sind)
; code = $7c00-7fff und $3400-$3800
; koala pic = $2000
; charset = $3800

dest =$2000+(13*320)
buffer =$f400


*=$2000+3
!bin "greetz.kla",5120-1280,2+3
;--------------------------------------------------
*=$3400
valz			!byte $00
valz2		!byte $00
tab04		!byte $10,$d6,$34,$5e,$e5,$43,$6d,$01
			!byte $10,$10,$10,$10,$10,$10,$10,$10
			
;--------------------------------------------------
statemachine
			lda valz
			cmp #8
			beq state_copy_buffer
			cmp #9
			beq state_clear_buffer
			cmp #10
			beq state_print_text
			cmp #11
			beq state_copy_ora_screen1
			cmp #12
			beq state_copy_ora_screen2
			cmp #15
			beq state_reset_val

			rts
			
state_copy_buffer
			jmp copy_buffer

state_clear_buffer
			jmp clear_buffer

state_print_text
			jmp print_text

state_copy_ora_screen1
			jmp copy_ora_screen1

state_copy_ora_screen2
			jmp copy_ora_screen2

state_reset_val
			lda #$00
			sta valz
			rts
;--------------------------------------------------
setcolors
			ldx #$78-1
flo2			lda tab04,y
			sta $0400+(13*40),x
;			lda tabd8,y
;			sta $d800+(13*40),x
			dex
			bpl flo2

			rts
;--------------------------------------------------
copy_ora_screen1
			ldx #$00
cosloop1
			lda buffer,x
			asl
			ora dest,x
			sta dest,x
			lda buffer+$f0,x
			asl
			ora dest+$f0,x
			sta dest+$f0,x

			inx
			cpx #$f0
			bne cosloop1
			
			rts
;--------------------------------------------------
copy_ora_screen2
			ldx #$00
cosloop2
			lda buffer+$1e0,x
			asl
			ora dest+$1e0,x
			sta dest+$1e0,x
			lda buffer+$2d0,x
			asl
			ora dest+$2d0,x
			sta dest+$2d0,x

			inx
			cpx #$f0
			bne cosloop2
			
			rts

;--------------------------------------------------
clear_buffer
			ldx #$00
			txa
cbloop		sta buffer,x
			sta buffer+$f0,x
			sta buffer+$1e0,x
			sta buffer+$2d0,x
			inx
			cpx #$f0
			bne cbloop
			
			rts
;--------------------------------------------------
copy_buffer	ldx #$00
cploop
			lda buffer,x
			sta dest,x
			lda buffer+$f0,x
			sta dest+$f0,x
			lda buffer+$1e0,x
			sta dest+$1e0,x
			lda buffer+$2d0,x
			sta dest+$2d0,x
			inx
			cpx #$f0
			bne cploop
			
			rts
;--------------------------------------------------
print_text		
			jsr get_char
			cmp #$ff
			bne no_reset
			lda #<greetz
			sta get_char+1
			lda #>greetz
			sta get_char+2
			jsr get_char
			inc val4
no_reset
			tay
print_more			
			jsr get_char
			cmp #$ff
			beq print_done
			cmp #$7c
			beq space
			clc
			sbc #$60
			jsr draw_char
			iny
			iny
			iny
			jmp print_more
print_done
			rts	

space		
;			lda #$1b
;			jsr draw_char
			iny
			jmp print_more
;--------------------------------------------------
get_char		lda greetz
			inc get_char+1
			bne no_char
			inc get_char+2
no_char
			rts
;--------------------------------------------------
draw_char
			tax
			lda fontlo,x
			sta drop1+1
			lda fonthi,x
			sta drop1+2
			
			lda drop1+1
			clc
			adc #$88
			sta drop2+1
			lda drop1+2
			adc #$02
			sta drop2+2

			lda drop2+1
			clc
			adc #$88
			sta drop3+1
			lda drop2+2
			adc #$02
			sta drop3+2
			
			lda poslo,y
			sta pos1+1
			lda poshi,y
			sta pos1+2
			
			lda pos1+1
			clc
			adc #$40
			sta pos2+1
			lda pos1+2
			adc #$01
			sta pos2+2

			lda pos2+1
			clc
			adc #$40
			sta pos3+1
			lda pos2+2
			adc #$01
			sta pos3+2


			ldx #$00
drop1		lda $4000,x
pos1			sta $2000,x
drop2		lda $4288,x
pos2			sta $2140,x
drop3		lda $4510,x
pos3			sta $2280,x
			inx
			cpx #24
			bne drop1

			rts
;--------------------------------------------------
fontlo		
			!byte <font+(0*24),<font+(1*24),<font+(2*24),<font+(3*24),<font+(4*24),<font+(5*24),<font+(6*24),<font+(7*24)
			!byte <font+(8*24),<font+(9*24),<font+(10*24),<font+(11*24),<font+(12*24),<font+(13*24),<font+(14*24),<font+(15*24)
			!byte <font+(16*24),<font+(17*24),<font+(18*24),<font+(19*24),<font+(20*24),<font+(21*24),<font+(22*24),<font+(23*24)
			!byte <font+(24*24),<font+(25*24),<font+(25*24),<font+(26*24)
						
fonthi
			!byte >font+(0*24),>font+(1*24),>font+(2*24),>font+(3*24),>font+(4*24),>font+(5*24),>font+(6*24),>font+(7*24)
			!byte >font+(8*24),>font+(9*24),>font+(10*24),>font+(11*24),>font+(12*24),>font+(13*24),>font+(14*24),>font+(15*24)
			!byte >font+(16*24),>font+(17*24),>font+(18*24),>font+(19*24),>font+(20*24),>font+(21*24),>font+(22*24),>font+(23*24)
			!byte >font+(24*24),>font+(25*24),>font+(25*24),>font+(26*24)
;--------------------------------------------------
poslo
			!byte <buffer+(0*8),<buffer+(1*8),<buffer+(2*8),<buffer+(3*8),<buffer+(4*8),<buffer+(5*8),<buffer+(6*8),<buffer+(7*8)
			!byte <buffer+(8*8),<buffer+(9*8),<buffer+(10*8),<buffer+(11*8),<buffer+(12*8),<buffer+(13*8),<buffer+(14*8),<buffer+(15*8)
			!byte <buffer+(16*8),<buffer+(17*8),<buffer+(18*8),<buffer+(19*8),<buffer+(20*8),<buffer+(21*8),<buffer+(22*8),<buffer+(23*8)
			!byte <buffer+(24*8),<buffer+(25*8),<buffer+(26*8),<buffer+(27*8),<buffer+(28*8),<buffer+(29*8),<buffer+(30*8),<buffer+(31*8)
			!byte <buffer+(32*8),<buffer+(33*8),<buffer+(34*8),<buffer+(35*8),<buffer+(36*8),<buffer+(37*8),<buffer+(38*8),<buffer+(39*8)

poshi
			!byte >buffer+(0*8),>buffer+(1*8),>buffer+(2*8),>buffer+(3*8),>buffer+(4*8),>buffer+(5*8),>buffer+(6*8),>buffer+(7*8)
			!byte >buffer+(8*8),>buffer+(9*8),>buffer+(10*8),>buffer+(11*8),>buffer+(12*8),>buffer+(13*8),>buffer+(14*8),>buffer+(15*8)
			!byte >buffer+(16*8),>buffer+(17*8),>buffer+(18*8),>buffer+(19*8),>buffer+(20*8),>buffer+(21*8),>buffer+(22*8),>buffer+(23*8)
			!byte >buffer+(24*8),>buffer+(25*8),>buffer+(26*8),>buffer+(27*8),>buffer+(28*8),>buffer+(29*8),>buffer+(30*8),>buffer+(31*8)
			!byte >buffer+(32*8),>buffer+(33*8),>buffer+(34*8),>buffer+(35*8),>buffer+(36*8),>buffer+(37*8),>buffer+(38*8),>buffer+(39*8)
;--------------------------------------------------
raster0
			sta raster_old0_a+1
			stx raster_old0_x+1
			sty raster_old0_y+1
			lda $01
			sta set01x+1

			lda #$35
			sta $01

			inc $d019

			lda #$00
			sta $d020
	!ifdef RELEASE {
			jsr PLAY_MUSIC 
	} else {
			jsr $1003
	}

			lda val4
			cmp #$01
			bne not_finished
			lda #$bb
			sta $d012
			lda #<raster1
			sta $fffe
			lda #>raster1
			sta $ffff
			
not_finished
set01x		lda #$00
			sta $01

raster_old0_a:	lda #$00
raster_old0_x:	ldx #$00
raster_old0_y:	ldy #$00
			rti

raster1
			sta raster_old1_a+1
			stx raster_old1_x+1
			sty raster_old1_y+1

			inc $d019
			ldx #1
			dex
			bne *-1
			
			lda #$1c
ddset		ldx #%00000010
d8set		ldy #%11100000

			sta $d011
			sty $d018
			stx $dd00

			lda #$d8
			sta $d016

			lda #$06
			sta $d021
			sta $d020
			
	!ifdef RELEASE {
			jsr PLAY_MUSIC 
	} else {
			jsr $1003
	}

			lda #<raster2
			sta $fffe
			lda #>raster2
			sta $ffff
			lda #$fa
			sta $d012
			

raster_old1_a:	lda #$00
raster_old1_x:	ldx #$00
raster_old1_y:	ldy #$00
			rti


raster2
			sta raster_old2_a+1
			stx raster_old2_x+1
			sty raster_old2_y+1

			inc $d019
			ldx #7
			dex
			bne *-1
			nop

			lda #$00
			sta $d020

			lda #$00
			sta $d021
			lda #27
			sta $d018

			lda #$d8
			sta $d016

			lda #$3c
			sta $d011
			
			lda #%00000011
			sta $dd00

			inc val2
			inc val2
			dec val3
			ldx val3
			lda sine,x
;			lsr
			ldx val2
			adc sine,x
			clc
			adc val2
			and #$0f
			tax
			lda tab_d018,x
			sta d8set+1
			lda tab_dd00,x
			sta ddset+1

			lda valz
			and #31
			tay
			
			inc valz2
			lda valz2
			and #1
			bne mainz
			
			jsr setcolors
			jsr statemachine
			inc valz
						
mainz

			lda #<raster1
			sta $fffe
			lda #>raster1
			sta $ffff
			lda #$bb
			sta $d012
			

raster_old2_a:	lda #$00
raster_old2_x:	ldx #$00
raster_old2_y:	ldy #$00
			rti

;--------------------------------------------------
*=$3800
font
!bin "shifted-font.bin"
;--------------------------------------------------
*=$4000
col04
!bin "greetz.kla",640-160,8002
cold8
!bin "greetz.kla",640-160,9002
;--------------------------------------------------
			*=$7c00
start:
                        !byte $a9
                        !bin "greetz.kla",1,2
                        sta $2000
                        !byte $a9
                        !bin "greetz.kla",1,3
                        sta $2001
                        !byte $a9
                        !bin "greetz.kla",1,4
                        sta $2002
                        
			sei
			lda #$7b
			sta $d011

                        !ifdef RELEASE {			
-       		lda zwischenpart,x
        		sta FRAMEWORK_ADDR-$100,x
        		inx
        		bne -
                        lda #$60
                        sta patch_01
                        lda #$06+3
                        sta patch_02+1
                        sta patch_10+1
                        lda #$09+3
                        sta patch_03+1
                        lda #$ee
                        sta patch_04
                        sta patch_05
                        sta patch_06
                        sta patch_07
                        lda #$3f
                        sta patch_08+1
                        lda #$90
                        sta patch_09
                        lda #$00
                        sta zwischencounter+1
        		;jsr FRAMEWORK_IRQ
			jsr FRAMEWORK_ADDR-$100
                        } else {
			lda #$35
			sta $01
                        }
			
			
			ldx #$00
d8fill		lda #$06
			sta $da00+140,x
			sta $db00,x

			lda col04,x
			sta $0400,x
			lda col04+$f0,x
			sta $04f0,x
						
			lda cold8,x
			sta $d800,x
			lda cold8+$f0,x
			sta $d8f0,x
			
			inx
			cpx #$f0
			bne d8fill
			
			ldx #$00
			lda #$00
fill2		sta $0400+(16*40),x
			sta $d800+(16*40),x
			inx
			cpx #40
			bne fill2
			ldx #$00
fill3		lda #$01
			sta $d800+(13*40),x
			inx
			cpx #$78
			bne fill3
			
			lda #$00
			sta $d020
			sta $d021
			lda #$0e
			sta $d022
			lda #$03
			sta $d023
			
			jsr gentex
main
			ldx val3
			lda wtab,x
			sta writebyte+2
			lda #$00
			sta writebyte+1


			lda #$30
			sta $01
			jsr code
			jsr movetex
			lda #$35
			sta $01
			
;			inc $d020
			inc val3
			lda val3
			cmp #16
			bne main

			jsr genscreen
			jsr clear_buffer
						
			lda #$00
			sta val3
			sta val2

			inc val4	; <- rendering finished.

	!ifdef RELEASE {
-                       lda $ffff
                        cmp #$fc
                        bcc -
        }
			lda #$00
			sta $d012
	!ifdef RELEASE {
			lda $d011
			and #$7f
			sta $d011
        }
			lda #$81
			sta $d01a
			sta $d019
                        !ifdef RELEASE {
                        sei
                        }
			lda #<raster0
			sta $fffe
			lda #>raster0
			sta $ffff
			lda #$7f
			sta $dc0d
			lda $dc0d
                        !ifdef RELEASE {
                        } else {
			lda #$00
			jsr $1000
                        }
			cli
                                
	!ifdef RELEASE {
			jsr LOAD_NEXT_PART
        }

					; LOADER HERE!!!

moep			lda val4	; <- all greetz done yet?
			cmp #$02
			bne *-5
        		ldx #$00
                        !ifdef RELEASE {
                        } else {
                        FRAMEWORK_ADDR = $fc00
                        }
-       		lda zwischenpart,x
        		sta FRAMEWORK_ADDR-$100,x
        		inx
        		bne -
        		;jsr FRAMEWORK_IRQ
			jmp FRAMEWORK_ADDR-$100

zwischenpart:
	!pseudopc FRAMEWORK_ADDR-$100 {
-                       lda $d011
                        bpl -
-                       lda $d011
                        bmi -                                
			lda #$0b
			sta $d011
                        sei
patch_10:               lda #$ba
                        sta $d012
                        lda #<zwischenirq
                        sta $fffe
                        lda #>zwischenirq
                        sta $ffff
                        cli
patch_01:
                        !ifdef RELEASE {
        		jsr DECRUNCH_NEXT_PART
        		jmp START_NEXT_PART
                        } else {
        
					; <- next part!
			jmp *-3
                        }                        
zwischenirq:            sta zwischenirq_old_a+1
                        stx zwischenirq_old_x+1
                        sty zwischenirq_old_y+1
                        lda $01
                        sta zwischenirq_old_01+1
                        lda #$35        
                        sta $01
                        TIME_CONST = 50
                        
zwischencounter:        lda #TIME_CONST
                        cmp #TIME_CONST
                        bcs +
                        inc zwischencounter+1
	!ifdef RELEASE {
			jsr PLAY_MUSIC 
	} else {
			jsr $1003
	}
                        jmp end_without_job
+                       lda $d020
                        and #$0f
                        beq makeblue
patch_02:
zwischenline1:          ldx #$ba
                        lda #$00
                        beq makecolorcont
zwischenline2:
patch_03:
makeblue:               ldx #$f9
                        lda #$06
makecolorcont:          ldy $d012
-                       cpy $d012
                        beq -                        
                        sta $d020
                        stx $d012                        
                        cmp #$00
                        bne zwischenirq_cont
	!ifdef RELEASE {
			jsr PLAY_MUSIC 
	} else {
			jsr $1003
	}
patch_04:
                        dec zwischenline2+1
patch_05:
                        dec zwischenline1+1
patch_06:
                        dec zwischenline2+1
                        ;lda zwischenline2+1
                        ;cmp #$e0
                        ;bcs zwischenpart_always 
zwischen_slowdown:
                        lda #$00
                        clc
                        adc #$01
                        sta zwischen_slowdown+1
                        lsr
                        bcc zwischenirq_cont
zwischenpart_always:
patch_07:                        
                        dec zwischenline1+1
zwischenirq_cont:
                        lda zwischenline2+1
                        sec
                        sbc zwischenline1+1
patch_08:
                        cmp #$04
patch_09:
                        bcs end_without_job
                        !ifdef RELEASE {
                        jsr FRAMEWORK_IRQ
                        } else {
-                       jmp -
                        }
                        
end_without_job:                                               
                        inc $d019
zwischenirq_old_01:     lda #$00
                        sta $01                        
zwischenirq_old_y:      ldy #$00
zwischenirq_old_x:      ldx #$00
zwischenirq_old_a:      lda #$00
                        rti
                                                
        }

;--------------------------------------------------
val1			!byte $00
val2			!byte $01
val3			!byte $00
val4			!byte $00
;--------------------------------------------------
genscreen
			ldx #$00
xfill1		txa
			sta texture1,x
			inx
			bne xfill1

			ldx #$00
			txa
xfill0		sta $da80,x
			inx
			cpx #40
			bne xfill0
			
			ldx #$00
xfill2		
			lda #$09
			sta $daa8+(0*40)+4,x
			lda texture1+(0*32),x
			sta $7aa8+(0*40)+4,x
			sta $baa8+(0*40)+4,x
			sta $f2a8+(0*40)+4,x
			lda #$09
			sta $daa8+(0*40)+4,x
			lda texture1+(1*32),x
			sta $7aa8+(1*40)+4,x
			sta $baa8+(1*40)+4,x
			sta $f2a8+(1*40)+4,x
			lda #$09
			sta $daa8+(1*40)+4,x
			lda texture1+(2*32),x
			sta $7aa8+(2*40)+4,x
			sta $baa8+(2*40)+4,x
			sta $f2a8+(2*40)+4,x
			lda #$09
			sta $daa8+(2*40)+4,x
			lda texture1+(3*32),x
			sta $7aa8+(3*40)+4,x
			sta $baa8+(3*40)+4,x
			sta $f2a8+(3*40)+4,x
			lda #$09
			sta $daa8+(3*40)+4,x
			lda texture1+(4*32),x
			sta $7aa8+(4*40)+4,x
			sta $baa8+(4*40)+4,x
			sta $f2a8+(4*40)+4,x
			lda #$09
			sta $daa8+(4*40)+4,x
			lda texture1+(5*32),x
			sta $7aa8+(5*40)+4,x
			sta $baa8+(5*40)+4,x
			sta $f2a8+(5*40)+4,x
			lda #$09
			sta $daa8+(5*40)+4,x
			lda texture1+(6*32),x
			sta $7aa8+(6*40)+4,x
			sta $baa8+(6*40)+4,x
			sta $f2a8+(6*40)+4,x
			lda #$09
			sta $daa8+(6*40)+4,x
			lda texture1+(7*32),x
			sta $7aa8+(7*40)+4,x
			sta $baa8+(7*40)+4,x
			sta $f2a8+(7*40)+4,x
			lda #$09
			sta $daa8+(7*40)+4,x
			inx
			cpx #32
			beq xfill2off
			jmp xfill2
xfill2off

			rts
;--------------------------------------------------
gentex
			ldx #$00
genloop		lda texture1,x
			asl
			asl
			ora texture1,x
			asl
			asl
			ora texture1,x
			asl
			asl
			ora texture1,x
			sta texture1,x
			inx
			bne genloop
			
			rts
;--------------------------------------------------
code
			lda #$00
			sta cval
			
			lda #<map
			sta mod1+1
			lda #>map
			sta mod1+2
			
			lda mod1+1
			clc
			;adc #$00
			sta mod2+1
			lda mod1+2
			adc #$08
			sta mod2+2

			lda mod2+1
			clc
			adc #$00
			sta mod3+1
			lda mod2+2
			adc #$08
			sta mod3+2

			lda mod3+1
			clc
			adc #$00
			sta mod4+1
			lda mod3+2
			adc #$08
			sta mod4+2

;			lda #$60
;			sta mod5+1
;			lda #$35
;			sta mod5+2
codeloop0
			ldy #0
codeloop1
;!for i,8 {
mod1			lax map,y ;+(256*(i-1)),y
			lda texture1,x
			and #%11000000
			sta $f2

mod2			lax map+$800,y ;+(256*(i-1)),y
			lda texture1,x
			and #%00110000
			sta $f3
			
mod3			lax map+$1000,y ;+(256*(i-1)),y
			lda texture1,x
			and #%00001100
			sta $f4
			
mod4			lax map+$1800,y ;+(256*(i-1)),y
			lda texture1,x
			and #%00000011
			ora $f2
			ora $f3
			ora $f4
;mod5			sta $3540+32,y ;($140*(i-1))+32,y
			jsr writebyte
;}

			iny
			beq off1
			jmp codeloop1
off1
			inc cval
			lda cval
			cmp #8
			beq off2
			
			inc mod1+2
			inc mod2+2
			inc mod3+2
			inc mod4+2

;			lda mod5+1
;			clc
;			adc #$40
;			sta mod5+1			
;			lda mod5+2
;			adc #$01
;			sta mod5+2

			jmp codeloop0
off2
			rts
;--------------------------------------------------
writebyte		sta $beef
			inc writebyte+1
			bne nohiwrite
			inc writebyte+2
nohiwrite
			rts
;--------------------------------------------------
cval			!byte $00
;--------------------------------------------------
movetex
			ldx #$00
			ldy #$00
mloop3		
			lda texture1,y
			sta tex1tmp,x
			
			tya
			clc
			adc #16
			tay
			inx
			cpx #16
			bne mloop3
			
			ldx #$00
mloop4
			lda texture1+1+(0*16),x
			sta texture1+(0*16),x
			lda texture1+1+(1*16),x
			sta texture1+(1*16),x
			lda texture1+1+(2*16),x
			sta texture1+(2*16),x
			lda texture1+1+(3*16),x
			sta texture1+(3*16),x
			lda texture1+1+(4*16),x
			sta texture1+(4*16),x
			lda texture1+1+(5*16),x
			sta texture1+(5*16),x
			lda texture1+1+(6*16),x
			sta texture1+(6*16),x
			lda texture1+1+(7*16),x
			sta texture1+(7*16),x
			lda texture1+1+(8*16),x
			sta texture1+(8*16),x
			lda texture1+1+(9*16),x
			sta texture1+(9*16),x
			lda texture1+1+(10*16),x
			sta texture1+(10*16),x
			lda texture1+1+(11*16),x
			sta texture1+(11*16),x
			lda texture1+1+(12*16),x
			sta texture1+(12*16),x
			lda texture1+1+(13*16),x
			sta texture1+(13*16),x
			lda texture1+1+(14*16),x
			sta texture1+(14*16),x
			lda texture1+1+(15*16),x
			sta texture1+(15*16),x
			
			inx
			cpx #15
			bne mloop4

			ldx #$00
			ldy #$00
mloop8
			lda tex1tmp,x
			sta texture1+15,y
			
			tya
			clc
			adc #16
			tay
			inx
			cpx #16
			bne mloop8

			rts
;--------------------------------------------------
tex1tmp		!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
;--------------------------------------------------
wtab			!byte $40,$48,$50,$58,$60,$68,$70,$a0,$a8,$b0,$c0,$c8,$d0,$d8,$e0,$e8
;--------------------------------------------------
tab_d018
!byte %11100000,%11100010,%11100100,%11100110,%11101000,%11101010,%11101100
!byte %11101000,%11101010,%11101100
!byte %11000000,%11000010,%11000100,%11000110,%11001000,%11001010
;--------------------------------------------------
tab_dd00
!byte %00000010,%00000010,%00000010,%00000010,%00000010,%00000010,%00000010
!byte %00000001,%00000001,%00000001
!byte %00000000,%00000000,%00000000,%00000000,%00000000,%00000000
;--------------------------------------------------
*=$7800
sine

!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01,$01,$01,$01,$02
!byte $02,$02,$03,$03,$03,$04,$04,$04,$05,$05,$06,$06,$07,$07,$08,$08
!byte $09,$09,$0a,$0b,$0b,$0c,$0c,$0d,$0e,$0e,$0f,$10,$10,$11,$12,$13
!byte $13,$14,$15,$16,$16,$17,$18,$19,$19,$1a,$1b,$1c,$1c,$1d,$1e,$1f
!byte $20,$20,$21,$22,$23,$24,$24,$25,$26,$27,$27,$28,$29,$2a,$2a,$2b
!byte $2c,$2d,$2d,$2e,$2f,$2f,$30,$31,$31,$32,$33,$33,$34,$35,$35,$36
!byte $36,$37,$37,$38,$38,$39,$39,$3a,$3a,$3b,$3b,$3b,$3c,$3c,$3d,$3d
!byte $3d,$3d,$3e,$3e,$3e,$3e,$3f,$3f,$3f,$3f,$3f,$3f,$3f,$3f,$3f,$3f
!byte $3f,$3f,$3f,$3f,$3f,$3f,$3f,$3f,$3f,$3f,$3f,$3e,$3e,$3e,$3e,$3d
!byte $3d,$3d,$3c,$3c,$3c,$3b,$3b,$3b,$3a,$3a,$39,$39,$38,$38,$37,$37
!byte $36,$36,$35,$34,$34,$33,$33,$32,$31,$31,$30,$2f,$2f,$2e,$2d,$2c
!byte $2c,$2b,$2a,$29,$29,$28,$27,$26,$26,$25,$24,$23,$23,$22,$21,$20
!byte $1f,$1f,$1e,$1d,$1c,$1b,$1b,$1a,$19,$18,$18,$17,$16,$15,$15,$14
!byte $13,$12,$12,$11,$10,$10,$0f,$0e,$0e,$0d,$0c,$0c,$0b,$0a,$0a,$09
!byte $09,$08,$08,$07,$07,$06,$06,$05,$05,$04,$04,$04,$03,$03,$02,$02
!byte $02,$02,$01,$01,$01,$01,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

;--------------------------------------------------
:*=$7900
texture1
;!bin "texture1.raw"
!bin "newtex1.raw"
;--------------------------------------------------
*=$8000
map
!bin "new-uv-map.raw"
;--------------------------------------------------
*=$bc00
greetz		
			!text $00,"|",$ff
			!text $06,"accession",$ff		; 9 =6
			!text $08,"alcatraz",$ff		; 8 =8
			!text $09,"arsenic",$ff			; 7 =9
			!text $0b,"atebit",$ff			; 6 =11
			!text $06,"bauknecht",$ff		; 9 =6
			!text $06,"bitfellas",$ff		; 9 =6
			!text $06,"blueflame",$ff		; 9 =6
			!text $01,"booze||designs",$ff		; 13 =0
			!text $05,"brainstorm",$ff		; 10 =5
			!text $0b,"chorus",$ff			; 6 =11
			!text $05,"conspiracy",$ff		; 10 =5
			!text $0c,"crest",$ff			; 5 =12
			!text $09,"damones",$ff			; 7 =9
			!text $04,"danish||gold",$ff		; 11 =3
			!text $06,"dekadence",$ff		; 9 =6
			!text $01,"division||zero",$ff		;
			!text $08,"drifters",$ff		; 8 =8
			!text $0f,"dss",$ff			; 3 =15
			!text $03,"exclusive|on",$ff		; 12 =2
			!text $0b,"extend",$ff			; 6 =11
			!text $06,"fairlight",$ff		; 9 =6
			!text $05,"farbrausch",$ff		; 10 =5
			!text $0c,"focus",$ff			; 5 =12
			!text $04,"hack|n|trade",$ff		;
			!text $0b,"hitmen",$ff			; 6 =11
			!text $09,"mercury",$ff			; 7 =9
			!text $05,"metalvotze",$ff		; 10 =5
			!text $0b,"nuance",$ff			; 6 =11
			!text $06,"onslaught",$ff		; 9 =6
			!text $0f,"orb",$ff			; 3 =15
			!text $0b,"oxyron",$ff			; 6 =11
			!text $03,"panda|design",$ff		; 12 =2
			!text $06,"paramount",$ff		; 9 =6
			!text $06,"popsy|team",$ff		;
			!text $06,"rabenauge",$ff		; 9 =6
			!text $0b,"rebels",$ff			; 6 =11
			!text $08,"resource",$ff		; 8 =8
			!text $0e,"rbbs",$ff			; 4 =14
			!text $09,"sidwave",$ff			; 7 =9
			!text $04,"silicon||ltd",$ff		; 11 =3
			!text $01,"singular||crew",$ff		; 13 =0
			!text $05,"speckdrumm",$ff		; 10 =5
			!text $0c,"still",$ff			; 5 =12
			!text $0f,"tbl",$ff			; 3 =15
			!text $06,"the|dreams",$ff		; 10 =5
			!text $0c,"titan",$ff			; 5 =12
			!text $0c,"triad",$ff			; 5 =12
			!text $04,"uk||allstars",$ff		; 11 =3
			!text $09,"uprough",$ff			; 7 =9
			!text $0c,"viruz",$ff			; 5 =12
			!text $0b,"vision",$ff			; 6 =11
			!text $03,"wrath|design",$ff		; 12 =2
			!text $0c,"xenon",$ff			; 5 =12
;			!text $0e,"xmen",$ff			; 4 =14
			!text $00,"|",$ff
			!byte $ff

