SHELL := /bin/bash

all: $(PROGRAM).prg

release: $(PROGRAM)+header.prg	

$(PROGRAM).prg: $(PROGRAM).asm ../framework/framework.inc $(EXTRA_FILES)
	acme -v4  -l tmp  $< 
	cat tmp | sort >labels.lst
	source ../tools/convertlabs tmp >labels.lab
	$(PACK_EXTRA)
	rm tmp

$(PROGRAM).plain: release.asm $(PROGRAM).asm ../framework/framework.inc $(EXTRA_FILES)
	acme -v4 -f cbm  -l tmp release.asm 
	cat tmp | sort >labels_raw.lst
	source ../tools/convertlabs tmp >labels_raw.lab
	rm tmp


$(PROGRAM).exo:	$(PROGRAM).plain
	exomizer raw -o $(PROGRAM).exo $(PROGRAM).plain

$(PROGRAM)+header.prg:	$(PROGRAM).exo
	acme -o $(PROGRAM)+header.prg -f plain --setpc 0 config.asm

clean:
	rm -f *.prg *.exo *.lst *.lab *.plain
        
go:	$(PROGRAM).prg
	x64 $(PROGRAM).prg
    

