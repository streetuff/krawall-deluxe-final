#include <SDL/SDL.h>
#include <SDL/sge.h>
#include "polygon.h"
#include "defines.h"

FILE *output_file = NULL;

static int x_coords_1[1000];
static int y_coords_1[1000];
static int x_coords_2[1000];
static int y_coords_2[1000];
static int index_coord = 0;

typedef struct
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char alpha;
} RGB;

RGB dtv_rgb[256] =
{
    {  0,   0,   0  ,   0},
    {  17,  17,  18 ,   0},
    {  31,  31,  33 ,   0},
    {  46,  47,  48 ,   0},
    {  61,  60,  62 ,   0},
    {  76,  76,  77 ,   0},
    {  91,  90,  91 ,   0},
    { 105, 105, 105 ,   0},
    { 117, 117, 116 ,   0},
    { 132, 132, 132 ,   0},
    { 147, 146, 148 ,   0},
    { 161, 161, 162 ,   0},
    { 175, 175, 176 ,   0},
    { 190, 190, 190 ,   0},
    { 205, 205, 207 ,   0},
    { 221, 221, 222 ,   0},
    { 36,  12,  0   ,   0},
    { 45,  31,  0   ,   0},
    { 61,  36,  0   ,   0},
    { 75,  51,   0  ,   0},
    { 89,  64,   0  ,   0},
    { 104,  80,   0 ,   0},
    { 119,  95,  0  ,   0},
    { 135, 110,  10 ,   0},
    { 145, 121,  22 ,   0},
    { 160, 136,  38 ,   0},
    { 174, 151,  51 ,   0},
    { 189, 167,  67 ,   0},
    { 204, 180,  81 ,   0},
    { 218, 195,  95 ,   0},
    { 235, 209, 110 ,   0},
    { 249, 227, 142 ,   0},
    { 55,   0,   0  ,   0},
    { 65,   0,   0  ,   0},
    { 79,  21,   0  ,   0},
    { 94,  35,   0  ,   0},
    {107,  49,   0  ,   0},
    {122,  64,  12  ,   0},
    {138,  78,  26  ,   0},
    {153,  95,  41  ,   0},
    {165, 106,  52  ,   0},
    {178, 121,  68  ,   0},
    {193, 135,  81  ,   0},
    {208, 150,  96  ,   0},
    {223, 164, 111  ,   0},
    {237, 179, 126  ,   0},
    {253, 194, 141  ,   0},
    {255, 211, 164  ,   0},
    { 65,   0,   0  ,   0},
    { 75,   0,   0  ,   0},
    { 90,   7,   9  ,   0},
    {105,  22,  22  ,   0},
    {119,  36,  36  ,   0},
    {134,  51,  52  ,   0},
    {149,  65,  67  ,   0},
    {165,  81,  79  ,   0},
    {175,  92,  92  ,   0},
    {190, 108, 108  ,   0},
    {204, 121, 122  ,   0},
    {219, 137, 138  ,   0},
    {234, 150, 152  ,   0},
    {248, 166, 164  ,   0},
    {255, 180, 182  ,   0},
    {255, 197, 197  ,   0},
    { 66,   0,  20  ,   0},
    { 78,   0,  34  ,   0},
    { 92,   1,  50  ,   0},
    {108,  13,  65  ,   0},
    {122,  27,  78  ,   0},
    {136,  42,  93  ,   0},
    {152,  57, 107  ,   0},
    {166,  72, 123  ,   0},
    {178,  83, 135  ,   0},
    {193,  97, 150  ,   0},
    {207, 112, 166  ,   0},
    {222, 127, 179  ,   0},
    {237, 142, 195  ,   0},
    {251, 157, 208  ,   0},
    {255, 171, 223  ,   0},
    {255, 188, 238  ,   0},
    { 59,   0,  60  ,   0},
    { 72,   0,  73  ,   0},
    { 86,   0,  88  ,   0},
    {101,   9, 105  ,   0},
    {116,  22, 118  ,   0},
    {131,  37, 132  ,   0},
    {144,  53, 148  ,   0},
    {159,  67, 161  ,   0},
    {171,  80, 173  ,   0},
    {186,  93, 189  ,   0},
    {200, 107, 204  ,   0},
    {216, 123, 221  ,   0},
    {231, 137, 232  ,   0},
    {245, 152, 247  ,   0},
    {255, 168, 255  ,   0},
    {255, 184, 255  ,   0},
    { 45,   0,  92  ,   0},
    { 56,   0, 104  ,   0},
    { 73,   0, 120  ,   0},
    { 87,  11, 134  ,   0},
    {101,  24, 147  ,   0},
    {115,  39, 162  ,   0},
    {129,  54, 176  ,   0},
    {147,  69, 193  ,   0},
    {157,  80, 206  ,   0},
    {173,  95, 219  ,   0},
    {187, 108, 234  ,   0},
    {203, 125, 249  ,   0},
    {216, 139, 255  ,   0},
    {231, 153, 255  ,   0},
    {246, 168, 255  ,   0},
    {255, 186, 255  ,   0},
    { 23,   0, 115  ,   0},
    { 37,   0, 121  ,   0},
    { 54,   3, 136  ,   0},
    { 70,  18, 152  ,   0},
    { 82,  32, 164  ,   0},
    { 96,  46, 179  ,   0},
    {110,  61, 195  ,   0},
    {126,  77, 210  ,   0},
    {137,  88, 221  ,   0},
    {152, 102, 236  ,   0},
    {167, 116, 250  ,   0},
    {183, 133, 255  ,   0},
    {196, 146, 255  ,   0},
    {211, 161, 255  ,   0},
    {227, 175, 255  ,   0},
    {242, 193, 255  ,   0},
    {  1,   0, 117  ,   0},
    { 15,   7, 125  ,   0},
    { 30,  13, 140  ,   0},
    { 47,  30, 154  ,   0},
    { 58,  43, 170  ,   0},
    { 73,  58, 184  ,   0},
    { 88,  71, 199  ,   0},
    {104,  87, 215  ,   0},
    {114,  98, 224  ,   0},
    {131, 113, 240  ,   0},
    {140, 127, 255  ,   0},
    {159, 143, 255  ,   0},
    {174, 155, 255  ,   0},
    {189, 172, 255  ,   0},
    {205, 186, 255  ,   0},
    {225, 204, 255  ,   0},
    {  0,   4, 105  ,   0},
    {  0,  12, 112  ,   0},
    {  7,  27, 127  ,   0},
    { 21,  43, 142  ,   0},
    { 36,  56, 156  ,   0},
    { 50,  71, 172  ,   0},
    { 66,  85, 184  ,   0},
    { 82, 100, 200  ,   0},
    { 91, 113, 213  ,   0},
    {106, 127, 227  ,   0},
    {121, 141, 243  ,   0},
    {136, 157, 255  ,   0},
    {151, 171, 255  ,   0},
    {166, 184, 255  ,   0},
    {180, 201, 255  ,   0},
    {203, 217, 255  ,   0},
    {  0,  17,  77  ,   0},
    {  0,  27,  85  ,   0},
    {  0,  42,  99  ,   0},
    {  2,  58, 118  ,   0},
    { 15,  70, 129  ,   0},
    { 31,  86, 145  ,   0},
    { 45, 100, 160  ,   0},
    { 60, 116, 177  ,   0},
    { 71, 128, 187  ,   0},
    { 88, 142, 200  ,   0},
    {101, 156, 214  ,   0},
    {115, 172, 233  ,   0},
    {129, 185, 243  ,   0},
    {145, 200, 255  ,   0},
    {160, 216, 255  ,   0},
    {184, 232, 255  ,   0},
    {  0,  30,  42  ,   0},
    {  0,  42,  49  ,   0},
    {  0,  56,  64  ,   0},
    {  0,  71,  77  ,   0},
    {  2,  85,  96  ,   0},
    { 19, 100, 111  ,   0},
    { 32, 115, 121  ,   0},
    { 47, 130, 140  ,   0},
    { 59, 141, 148  ,   0},
    { 74, 156, 162  ,   0},
    { 89, 171, 177  ,   0},
    {104, 185, 196  ,   0},
    {118, 199, 209  ,   0},
    {133, 214, 221  ,   0},
    {147, 229, 239  ,   0},
    {174, 246, 255  ,   0},
    {  0,  38,   4  ,   0},
    {  0,  51,  10  ,   0},
    {  0,  66,  21  ,   0},
    {  0,  80,  34  ,   0},
    {  1,  94,  53  ,   0},
    { 15, 109,  69  ,   0},
    { 29, 124,  80  ,   0},
    { 45, 139,  99  ,   0},
    { 56, 150, 109  ,   0},
    { 72, 165, 128  ,   0},
    { 86, 180, 142  ,   0},
    {101, 195, 152  ,   0},
    {115, 208, 169  ,   0},
    {131, 224, 182  ,   0},
    {146, 239, 196  ,   0},
    {171, 254, 216  ,   0},
    {  0,  43,   0  ,   0},
    {  0,  55,   0  ,   0},
    {  0,  70,   0  ,   0},
    {  0,  84,   0  ,   0},
    {  8,  98,  11  ,   0},
    { 23, 114,  26  ,   0},
    { 37, 128,  40  ,   0},
    { 52, 143,  57  ,   0},
    { 63, 155,  67  ,   0},
    { 78, 171,  85  ,   0},
    { 94, 184,  99  ,   0},
    {108, 200, 113  ,   0},
    {121, 213, 124  ,   0},
    {137, 228, 139  ,   0},
    {151, 244, 155  ,   0},
    {177, 255, 175  ,   0},
    {  0,  40,   0  ,   0},
    {  0,  54,   0  ,   0},
    {  0,  69,   0  ,   0},
    {  8,  84,   0  ,   0},
    { 22,  97,   0  ,   0},
    { 37, 113,   0  ,   0},
    { 54, 127,   7  ,   0},
    { 69, 142,  24  ,   0},
    { 80, 154,  36  ,   0},
    { 93, 169,  51  ,   0},
    {111, 183,  64  ,   0},
    {123, 198,  79  ,   0},
    {136, 212,  91  ,   0},
    {150, 227, 106  ,   0},
    {167, 242, 122  ,   0},
    {190, 255, 149  ,   0},
    {  0,  37,   0  ,   0},
    {  1,  47,   0  ,   0},
    { 14,  62,   0  ,   0},
    { 30,  77,   0  ,   0},
    { 44,  90,   0  ,   0},
    { 58, 106,   0  ,   0},
    { 74, 121,   0  ,   0},
    { 93, 136,   3  ,   0},
    {103, 147,  13  ,   0},
    {116, 161,  28  ,   0},
    {131, 175,  43  ,   0},
    {145, 192,  59  ,   0},
    {158, 204,  72  ,   0},
    {174, 221,  88  ,   0},
    {192, 236, 102  ,   0},
    {208, 254, 134  ,   0}};


extern SDL_Surface *screen;

void close_szene()
{
    if (output_file)
    {
        fputc(0xff, output_file);
    }
}

static int is_new(int x1, int y1, int x2, int y2)
{
	int i;
        for(i=0; i < index_coord; ++i)
        {
        	if ( (x1 == x_coords_1[i]) &&
                	(y1 == y_coords_1[i]) &&
                	(x2 == x_coords_2[i]) &&
                	(y2 == y_coords_2[i]))
                {
                	break;
                }
        }
        if (i == index_coord)
        {
        	x_coords_1[i] = x1;
        	y_coords_1[i] = y1;
        	x_coords_2[i] = x2;
        	y_coords_2[i] = y2;
                ++index_coord;
                return 1;
        }
        else
        {
        	return 0;
        }
}

#define SWAP x1 = x1_n; x1_n = x2_n; x2_n = x1; \
	y1 = y1_n; y1_n = y2_n; y2_n = y1;

void line(short x1, short y1, short x2, short y2, int color)
{
    int x1_n;
    int y1_n;
    int x2_n;
    int y2_n;
    //char a;
    x1_n = (x1 / GRID) * GRID;
    y1_n = (y1 / GRID) * GRID;
    x2_n = (x2 / GRID) * GRID;
    y2_n = (y2 / GRID) * GRID;
    if (clip_line(&x1_n, &y1_n, &x2_n, &y2_n))
    {
    #if 0
        a = 0x02;
        fputc(a, output_file);
        fputc(color, output_file);
        a = (x1_n+1) >> 1;
        fputc(a, output_file);
        a = (y1_n+1) >> 1;
        fputc(a, output_file);
        a = (x2_n+1) >> 1;
        fputc(a, output_file);
        a = (y2_n+1) >> 1;
        fputc(a, output_file);
    #endif
    	if (y1_n == y2_n)
        {
        	if(x1_n > x2_n)
                {
                	SWAP
                }
        } else {
        	if (y1_n > y2_n)
                {
                	SWAP
                }
        }
	if (is_new(x1_n / GRID, y1_n / GRID, x2_n / GRID, y2_n / GRID))
        {        
        	sge_Line(screen, x1_n, y1_n, x2_n, y2_n, dtv_rgb[color].r, dtv_rgb[color].g, dtv_rgb[color].b);
	}
    }
}

void write_short(int x)
{
	unsigned char a;
        a = x & 0xff;
        fputc(a, output_file);
        //a = (x >> 8) & 0xff;
        //fputc(a, output_file);        
}

void print_lines(void)
{
	int i;
        if (output_file == stdout)
        	printf("#### %d ####\n", index_coord);
        else
        	write_short(index_coord);
        for(i=0; i < index_coord; ++i)
        {
        	if (output_file == stdout)
                {
        		printf("%d %d %d %d\n", x_coords_1[i], y_coords_1[i], x_coords_2[i], y_coords_2[i]);
                }
                else
                {
                	write_short(x_coords_1[i]);
                	write_short(y_coords_1[i]);
                	write_short(x_coords_2[i]);
                	write_short(y_coords_2[i]);
                }
        }
        index_coord = 0;
}        

int check_polygon_no_clockwise(int n, short *x, short *y)
{
    int first = -1;
    int second = -1;
    int v1[2];
    int v2[2];
    int i;
    i = 0;
    do
    {
        ++i;
        if ((x[i] != x[0]) || (y[i] != y[0]))
        {
            first = i;
            break;
        }
    } while (i < n);

    if (first >= 0)
    {
        do
        {
            ++i;
            if (((x[i] != x[0]) || (y[i] != y[0]))
                &&
                ((x[i] != x[first]) || (y[i] != y[first])))
            {
                second = i;
                break;
            }
        }   while (i < n);
    }
    if ((first >= 0) && (second >=0))
    {
        v1[0] = x[first] - x[0];
        v1[1] = y[first] - y[0];
        v2[0] = x[second] - x[0];
        v2[1] = y[second] - y[0];
        if ((v2[0]*v1[1]) - (v2[1]*v1[0]) >= 0)
        {
            return 1;
        }
    }
    return 0;
}

unsigned char round_2(unsigned short val)
{
    unsigned short ret;
    ret = (val+1) >> 1;
    if(ret > 254)
    {
        ret = 254;
    }
    return (unsigned char)ret;
}

void polygon(int n, short *x, short *y, int color, int lined)
{
    int n_n;
    short x_n[n*2];
    short y_n[n*2];
    //unsigned char a;

    n_n = clipped_polygon(n, x, y, x_n, y_n);

    //for(int i=0; i < n; ++i)
    //{
    //    x_n[i] = (x[i]);
    //    y_n[i] = (y[i]);
    //}
    //n_n = n;
    if(!n_n)
    {
        return;
    }
    #if 0
    if (output_file)
    {
        a = n_n | 0x80;
        fputc(a, output_file);
        fputc(color, output_file);
        if (!check_polygon_no_clockwise(n_n,x_n,y_n))
        {
            for (int i=n_n-1; i >= 0; --i)
            {
                a = round_2(x_n[i]);
                fputc(a, output_file);
                a = round_2(y_n[i]);
                fputc(a, output_file);
            }
        }
        else
        {
            for (int i=0; i < n_n; ++i)
            {
                a = round_2(x_n[i]);
                fputc(a, output_file);
                a = round_2(y_n[i]);
                fputc(a, output_file);
            }
        }
    }
    #endif
    if(lined)
    {
        for(int i=0; i < n_n-1; ++i)
        {
            line(x[i],y[i], x[i+1], y[i+1], color);
        }
        line(x[n-1],y[n_n-1], x[0],y[0], color);
    }
    else
    {
        sge_FilledPolygon(screen, n_n, x_n, y_n, 
                      dtv_rgb[color].r, dtv_rgb[color].g, dtv_rgb[color].b);
    }
}


void filled_circle(short x, short y, short r, int color, int planet, int shadow)
{
    if (x+r < 0)
    {
        return;
    }
    if (x-r > SCREEN_X)
    {
        return;
    }
    if (y+r < 0)
    {
        return;
    }
    if (y-r > SCREEN_Y)
    {
        return;
    }
    unsigned char a;
    a = (((x+1) >> 1) >>8) & 0x0f;
    if (planet)
    {
        a |= (planet << 4);
    }
    else
    {
        a |= 0x70;
    }
    fputc(a, output_file);
    if (!planet)
    {
        fputc(color, output_file);
    }
    else
    {
        fputc(shadow, output_file);
    }
    a = ((x+1) >> 1) & 0xff;
    fputc(a, output_file);
    a = ((y+1) >> 1) & 0xff;
    fputc(a, output_file);
    a = ((r+1) >> 1) & 0xff;
    fputc(a, output_file);
    sge_FilledCircle(screen, x, y, r, dtv_rgb[color].r, dtv_rgb[color].g, dtv_rgb[color].b);
}

int calc_dtv_color(int color, double luminance)
{
    int chroma,luma;
    chroma = color & 0xf0;
    luma = color & 0x0f;
    luma = (int)(((double)(luma)) * (luminance+2)/3);
    if (luma < 0)
    {
        luma = 0;
    }
    if (luma > 15)
    {
        luma = 15;
    }
    return chroma | luma;
}



