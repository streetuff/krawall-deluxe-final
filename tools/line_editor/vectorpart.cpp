#include <iostream>
#include <math.h>
#include "vec3.hpp"
#include "coordsystem.h"
#include "object.h"

#define M_PI 3.141592654

using namespace math3d;

#define DEG(i) (((double)i) * M_PI /180.0)

double coordx[] = {200,-80,-0};
double coordy[] = {0,120,-120};

void paint_test_monilith(int x, int y, int z)
{
    CoordSystem coord;
    Object obj;
    int color = 0x06;
    vec3<> p1 = vec3<>(-40,-90,-10);
    vec3<> p2 = vec3<>(40,-90,-10);
    vec3<> p3 = vec3<>(40,90,-10);
    vec3<> p4 = vec3<>(-40,90,-10);
    vec3<> p5 = vec3<>(-40,-90,10);
    vec3<> p6 = vec3<>(40,-90,10);
    vec3<> p7 = vec3<>(40,90,10);
    vec3<> p8 = vec3<>(-40,90,10);

    Planet *p;
    p = new Planet(vec3<>(0,0,0), 150, JUPITER);
    p->set_shadow(240);
    if(z < 50)
    {
        coord.translate(vec3<>(0,0,10600-(z*150)));
    }
    else if(z < 360)
    {
        coord.translate(vec3<>(0,0,3600-(z*10)));
    }
    if(z >= 1000 && z < 1064)
    {
        color += (z-1000)*4;
    }
    if(z >= 1064)
    {
        coord.translate(vec3<>(0,0,(z-1064)*50));
    }
    //obj.add_object(p);

    if(z >= 360)
    {
        coord.rotate_x_y_z((((double)x) /** M_PI / 180*/),
                           (((double)y) /** M_PI / 180*/),
                           (((double)0) /** M_PI / 180*/));
    }
    else
    {
        coord.rotate_x_y_z((((double)x) /** M_PI / 180*/),
                           (((double)0) /** M_PI / 180*/),
                           (((double)0) /** M_PI / 180*/));
    }
    Quadrat *q;
    q = new Quadrat(p4, p3, p2, p1);
    q->set_color(color);
    obj.add_object(q);

    q = new Quadrat(p5, p6, p7, p8);
    q->set_color(color);
    obj.add_object(q);

    q = new Quadrat(p1, p2, p6, p5);
    q->set_color(color);
    obj.add_object(q);

    q = new Quadrat(p8, p7, p3, p4);
    q->set_color(color);
    obj.add_object(q);

    q = new Quadrat(p3, p7, p6, p2);
    q->set_color(color);
    obj.add_object(q);

    q = new Quadrat(p5, p8, p4, p1);
    q->set_color(color);
    obj.add_object(q);

    reset_paint();
    obj.paint(coord);
    do_paint(coord);
}

void paint_test_asteroid(int x, int y, int z, int number)
{
    CoordSystem coord;
    Object obj;
    int color = (int)(10.0 * ((((double)z+360)/1080.0)));

    vec3<> p1 = vec3<>(0,-60+number*5,0);
    vec3<> p2 = vec3<>(-60-number*3,0,40);
    vec3<> p3 = vec3<>(50,0,60+number*2);
    vec3<> p4 = vec3<>(-60,0,00);
    vec3<> p5 = vec3<>(-55,65-number*5,00);
    vec3<> p6 = vec3<>(50+number*1,60,00);
    vec3<> p7 = vec3<>(80-number*5,0,00);
    vec3<> p8 = vec3<>(0,0,-80+number*7);

    coord.rotate_x_y_z((((double)x) /** M_PI / 180*/),
                       (((double)y) /** M_PI / 180*/),
                       (((double)0) /** M_PI / 180*/));

    coord.translate(vec3<>(coordx[number],coordy[number],6800-(z*10)));
    Dreieck *d;
    Quadrat *q;

    d = new Dreieck(p1, p3, p2);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p1, p2, p4);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p4, p2, p5);
    d->set_color(color);
    obj.add_object(d);

    q = new Quadrat(p2, p3, p6, p5);
    q->set_color(color);
    obj.add_object(q);


    d = new Dreieck(p3, p7, p6);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p1, p7, p3);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p1, p8, p7);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p1, p4, p8);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p4, p5, p8);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p5, p6, p8);
    d->set_color(color);
    obj.add_object(d);

    d = new Dreieck(p6, p7, p8);
    d->set_color(color);
    obj.add_object(d);



    reset_paint();
    obj.paint(coord);
    do_paint(coord);
}

void mars_ride(int index)
{
    static int static_index = 0;

    CoordSystem coord;
    Object obj;
    Planet *p;
    if (index < 600)
    {
        int index2 = 600-index;

        p = new Planet(vec3<>(0,0,0), 100, MARS);
        coord.translate(vec3<>((index2*index2*index2)/100000+index2, 0 , 2900-(index*5)));
        p->set_shadow(240);
    }
    else if(index > 600+20)
    {
        index = 1200+20-index;
        int index2 = index-600;

        p = new Planet(vec3<>(0,0,0), 100, MARS);
        coord.translate(vec3<>(index2, 0 , -200 - (600-index)));
        p->set_shadow(56);
        p->set_shadow(240-static_index);
        static_index+=2;
    }
    else
    {
        p = new Planet(vec3<>(0,0,0), 100, MARS);
        coord.translate(vec3<>(0, 0 , -100 - (index-600)*5));
        p->set_shadow(240);
    }
    obj.add_object(p);
    reset_paint();
    obj.paint(coord);
    do_paint(coord);
}

void paint_test(int x, int y, int z)
{
    CoordSystem coord;
    Object obj;
    double col_div;
    coord.rotate_x_y_z((((double)x) /** M_PI / 180*/),
                       (((double)y) /** M_PI / 180*/),
                       (((double)x) /** M_PI / 180*/));
    coord.translate(vec3<>(500*cos(DEG(-x/2)), 0 , (z-180)*(z-180)*(z-180)*(z-180)/100000));

    if (z < 90 )
    {
        col_div = ((double)(120-z)) / 30.0;
    } else if (z > 270)
    {
        col_div = ((double)(z-240)) / 30.0;
    } else
    {
        col_div = 1.0;
    }
    FCircle *fc = new FCircle(vec3<>(0,0,0), 30);
    fc->set_color(int(((double)0x08) / col_div));
    obj.add_object(fc);
    Line *l;
    l = new Line(vec3<>(21,21,-10), vec3<>(100,100,100));
    l->set_color(int(((double)0x0f) / col_div));
    obj.add_object(l);
    l = new Line(vec3<>(-21,21,-10), vec3<>(-100,100,100));
    l->set_color(int(((double)0x0f) / col_div));
    obj.add_object(l);
    l = new Line(vec3<>(21,-21,-10), vec3<>(100,-100,100));
    l->set_color(int(((double)0x0f) / col_div));
    obj.add_object(l);
    l = new Line(vec3<>(-21,-21,-10), vec3<>(-100,-100,100));
    l->set_color(int(((double)0x0f) / col_div));
    obj.add_object(l);


    reset_paint();
    obj.paint(coord);
    do_paint(coord);
}

void paint_test_satelit(int x, int y, int z)
{
    CoordSystem coord;
    Object obj;
    int new_x;
    int new_y;

    if (x < 210)
    {
        new_x = 20;
    } else if (x < 300)
    {
        new_x = x-210+20;
    } else
    {
        new_x = 90+20;
    }
    new_y = new_x+90;

    coord.rotate_x_y_z((((double)new_x) /** M_PI / 180*/),
                       (((double)new_y) /** M_PI / 180*/),
                       (((double)x) /** M_PI / 180*/));
    if (x < 0)
    {
        coord.translate(vec3<>(1100-x*4,0.0,(double)(5*(x-70))));
    } else
    {
        coord.translate(vec3<>(1100-x*4,0.0,(double)(1800-(x+70)*5)));
    }
    vec3<> points[6];

    for (int i = 0; i < 6; ++i)
    {
        points[i] = vec3<>(50.0*cos(DEG((i)*60+30)), 50.0 *sin(DEG((i)*60+30)) ,-70);
    }
    Polygon_Shaded *p_s = new Polygon_Shaded();
    for (int i=5; i >=0; --i)
    {
        p_s->add_point(points[i]);
    }
    p_s->set_color(0x88);
    obj.add_object(p_s);


    for (int i = 0; i < 6; ++i)
    {
        points[i] = vec3<>(50.0*cos(DEG((i)*60+30)), 50.0 *sin(DEG((i)*60+30)) ,70);
    }
    p_s = new Polygon_Shaded();
    for (int i=0; i < 6; ++i)
    {
        p_s->add_point(points[i]);
    }
    p_s->set_color(0x88);
    obj.add_object(p_s);

    for (int i=0; i < 6; ++i)
    {
        Quadrat *quadrat = new Quadrat(vec3<>(50.0*cos(DEG((i)*60+30)), 50.0 *sin(DEG((i)*60+30)) ,-70),
                                       vec3<>(50.0*cos(DEG((i)*60+90)), 50.0 *sin(DEG((i)*60+90)) ,-70),
                                       vec3<>(50.0*cos(DEG((i)*60+90)), 50.0 *sin(DEG((i)*60+90)) ,70),
                                       vec3<>(50.0*cos(DEG((i)*60+30)), 50.0 *sin(DEG((i)*60+30)) ,70));

        quadrat->set_color(0x88);
        obj.add_object(quadrat);
    }
    Line *l = new Line(vec3<>(-45,0,0), vec3<>(-100,0,0));
    l->set_color(0x0f);
    obj.add_object(l);
    l = new Line(vec3<>(45,0,0), vec3<>(100,0,0));
    l->set_color(0x0f);
    obj.add_object(l);

    Polygon_Shaded *p;
    p = new Polygon_Shaded();
    p->add_point(vec3<>(-100, 0,0));
    p->add_point(vec3<>(-130, -30,10));
    p->add_point(vec3<>(-200, -30,33.33333));
    p->add_point(vec3<>(-200, 30,33.33333));
    p->add_point(vec3<>(-130, 30,10));
    p->set_color(0x16);
    obj.add_object(p);

    p = new Polygon_Shaded();
    p->add_point(vec3<>(-130, 30,10));
    p->add_point(vec3<>(-200, 30,33.33333));
    p->add_point(vec3<>(-200, -30,33.33333));
    p->add_point(vec3<>(-130, -30,10));
    p->add_point(vec3<>(-100, 0,0));
    p->set_color(0x16);
    obj.add_object(p);

    p = new Polygon_Shaded();
    p->add_point(vec3<>(100, 0,0));
    p->add_point(vec3<>(130, -30,10));
    p->add_point(vec3<>(200, -30,33.33333));
    p->add_point(vec3<>(200, 30,33.33333));
    p->add_point(vec3<>(130, 30,10));
    p->set_color(0x16);
    obj.add_object(p);

    p = new Polygon_Shaded();
    p->add_point(vec3<>(130, 30,10));
    p->add_point(vec3<>(200, 30,33.33333));
    p->add_point(vec3<>(200, -30,33.33333));
    p->add_point(vec3<>(130, -30,10));
    p->add_point(vec3<>(100, 0,0));
    p->set_color(0x16);
    obj.add_object(p);

    reset_paint();
    obj.paint(coord);
    do_paint(coord);
}

void paint_test_bla(int x, int y, int z)
{
    CoordSystem coord;
    Object obj;

    coord.rotate_x_y_z((((double)x) /** M_PI / 180*/),
                       (((double)y) /** M_PI / 180*/),
                       (((double)z) /** M_PI / 180*/));
    if (x < 180)
    {
        coord.translate(vec3<>(0.0,0.0,(double)(10*(x-15))));
    } else
    {
        coord.translate(vec3<>(0.0,0.0,(double)(3600-(x+15)*10)));
    }

    Quadrat *quadrat = new Quadrat(
                                  vec3<>(100,-100,-100),
                                  vec3<>(-100,-100,-100),
                                  vec3<>(-100,100,-100),
                                  vec3<>(100,100,-100));
    quadrat->set_color(0x1c);
    obj.add_object(quadrat);

    quadrat = new Quadrat(
                         vec3<>(100,100,100),
                         vec3<>(-100,100,100),
                         vec3<>(-100,-100,100),
                         vec3<>(100,-100,100));
    quadrat->set_color(0x5c);
    obj.add_object(quadrat);

    quadrat = new Quadrat(
                         vec3<>(100,-100,-100),
                         vec3<>(100,100,-100),
                         vec3<>(100,100,100),
                         vec3<>(100,-100,100));
    quadrat->set_color(0x83);
    obj.add_object(quadrat);

    quadrat = new Quadrat(
                         vec3<>(-100,100,-100),
                         vec3<>(-100,-100,-100),
                         vec3<>(-100,-100,100),
                         vec3<>(-100,100,100));
    quadrat->set_color(0xcc);
    obj.add_object(quadrat);

    quadrat = new Quadrat(
                         vec3<>(-100,-100,-100),
                         vec3<>(100,-100, -100),
                         vec3<>(100,-100, 100),
                         vec3<>(-100,-100,100));
    quadrat->set_color(0x48);
    obj.add_object(quadrat);

    quadrat = new Quadrat(
                         vec3<>(100,100,-100),
                         vec3<>(-100,100, -100),
                         vec3<>(-100,100, 100),
                         vec3<>(100,100,100));
    quadrat->set_color(0xbc);
    obj.add_object(quadrat);

    FCircle *circle = new FCircle(vec3<>(-430,0,0), 60);
    circle->set_color(0xf);
    obj.add_object(circle);

    Line *l = new Line(vec3<>(-100,0,0), vec3<>(-400,0,0));
    l->set_color(0x1f);
    obj.add_object(l);

    reset_paint();
    obj.paint(coord);
    do_paint(coord);
    //std::cout << result << std::endl;
}

