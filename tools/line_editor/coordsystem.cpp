#include "coordsystem.h"
#include "vec3.hpp"
#include "mat33.hpp"
#include "defines.h"

#define Z_CALC 500

CoordSystem::CoordSystem()
{
    rotation = mat33(1,0,0,0,1,0,0,0,1);
    translation = vec3<>(0,0,0);
}

vec3<> CoordSystem::transform_without_projection(vec3<> input) const
{
    vec3<> rotated = rotation * input;
    rotated += translation;
    return rotated;
}

vec2<> CoordSystem::transform(vec3<> input) const
{
    double res0;
    double res1;

    vec3<> rotated = rotation * input;
    rotated += translation;
    res0 = (rotated[0] * Z_CALC) / (rotated[2] + Z_CALC) +SCREEN_X/2.0;
    res1 = (rotated[1] * Z_CALC) / (rotated[2] + Z_CALC) +SCREEN_Y/2.0;
    return vec2<>(res0, res1);
}

double CoordSystem::scale(double input) const
{
    return (input * Z_CALC) / (translation[2] + Z_CALC);
}

vec3<> CoordSystem::rot(vec3<> input) const
{
    vec3<> rotated = rotation * input;
    return rotated;
}

double CoordSystem::calc_z(vec3<> input) const
{
    vec3<> rotated = rotation * input;
    rotated += translation;
    return rotated[2];
}


void CoordSystem::rotate_x(double angle)
{
    rotation.rotate(angle, 1.0, 0.0, 0.0);
}

void CoordSystem::rotate_y(double angle)
{
    rotation.rotate(angle, 0.0, 1.0, 0.0);
}

void CoordSystem::rotate_z(double angle)
{
    rotation.rotate(angle, 0.0, 0.0, 1.0);
}

void CoordSystem::rotate_x_y_z(double angle_x, double angle_y, double angle_z)
{
    rotate_x(angle_x);
    rotate_y(angle_y);
    rotate_z(angle_z);
}

void CoordSystem::translate(vec3<> addition)
{
    translation += addition;
}

