#ifndef __object_h
#define __object_h

#include "vec3.hpp"

using namespace math3d;

#define JUPITER 2
#define MARS 3

class CoordSystem;

class Base
{
public:
    Base() {};
    virtual ~Base() {};
    virtual void paint(const CoordSystem &system) = 0;
    virtual int get_color(const CoordSystem &system) = 0;
    virtual double calc_z_pos(const CoordSystem &system) = 0;
    void set_color(int col) { };
};

class Line : public Base
{
public:
    Line(vec3<> p1, vec3<> p2) { point1 = p1; point2 = p2; }
    void set_color(int col) { color = col;};
    virtual void paint(const CoordSystem &system);
    virtual int get_color(const CoordSystem &system) { return color; }
    virtual double calc_z_pos(const CoordSystem &system);
private:
    vec3<> point1;
    vec3<> point2;
    int color;
};

class Circle : public Base
{
public:
    Circle(vec3<>p, double r) {middle_point = p; radius = r; };
    virtual void paint(const CoordSystem &system);
    virtual double calc_z_pos(const CoordSystem &system);
    virtual int planet_index() { return 0; }
    virtual void set_shadow(int shad) {}
    virtual int get_shadow() {return 0; }
private:
    vec3<> middle_point;
    double radius;
};

class FCircle : public Circle
{
public:
    FCircle(vec3<>p, double r) : Circle(p, r) { };
    virtual void set_color(int col) { color = col; }
    virtual int get_color(const CoordSystem &system) { return color; }
private:
    int color;
};

class Planet : public Circle
{
    static int planet_color[5];
public:
    Planet(vec3<>p, double r, int index) : Circle(p, r)  { planet_idx = index; shadow = 0; };
    virtual int planet_index() { return planet_idx; }
    virtual int get_color(const CoordSystem &system) { return planet_color[planet_idx-2]; }
    virtual void set_shadow(int shad) {shadow = shad; };
    virtual int get_shadow() {return shadow; };
private:
    int planet_idx;
    int shadow;
};

class Polygon : public Base
{
public:
    Polygon() { n = 0; both_sided = false; };
    virtual void add_point(const vec3<>& point);
    virtual double calc_norm_z(const CoordSystem &system);
    virtual void paint(const CoordSystem &system);
    virtual double calc_z_pos(const CoordSystem &system);
    virtual void set_color(int col) { color = col; };
    virtual int get_color(const CoordSystem &system) {return color; };
    virtual void set_both_sided(void) {both_sided = true;};
protected:
    int n;
    bool both_sided;
    vec3<> points[100];
    int color;
};

class PolygonLine : public Polygon
{
public:
    virtual void paint(const CoordSystem &system);
};

class Polygon_Shaded : public Polygon
{
public:
    virtual int get_color(const CoordSystem &system);
};

class Dreieck : public Polygon_Shaded
{
public:
    Dreieck(const vec3<>& p1, const vec3<>& p2, const vec3<>& p3);
};

class Quadrat : public Polygon_Shaded
{
public:
    Quadrat(const vec3<>& p1, const vec3<>& p2, const vec3<>& p3, const vec3<>& p4);
};

class QuadratLine : public PolygonLine
{
public:
    QuadratLine(const vec3<>& p1, const vec3<>& p2, const vec3<>& p3, const vec3<>& p4);
};

class Object : public Base
{
public:
    Object() { n = 0; };
    ~Object();
    virtual void add_object(Base *object);
    virtual void paint(const CoordSystem &system);
    virtual int get_color(const CoordSystem &system) { return 0; }
    virtual double calc_z_pos(const CoordSystem &system) { return 0; };
private:
    int n;
    Base *obj[100];
};

void reset_paint();
void do_paint(CoordSystem &system);

#endif

