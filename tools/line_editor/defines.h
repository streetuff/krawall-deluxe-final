#ifndef __defines_h_2348904328328423948
#define __defines_h_2348904328328423948

#define GRID 4

#define MENU_SIZE (10*8)

#define SCREEN_X (16*8*GRID)
#define SCREEN_Y (16*8*GRID)

#define TOTAL_X (SCREEN_X + MENU_SIZE)
#define TOTAL_Y (SCREEN_Y)

#endif

