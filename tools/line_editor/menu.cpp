#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <SDL/SDL.h>
#include <SDL/sge.h>
#include <SDL/sge_surface.h>
#include "frame.h"

typedef struct _menu_item{
    void (*function)(void);
    struct _menu_item *next;
    SDLKey key;
    char name[1];
} MenuItem;


MenuItem *menu = NULL;
extern Base *edit_object;
extern SDL_Surface *screen;
extern sge_bmpFont *font;

void clr_screen(void);
void paint_menu(void);

#define MAXINPUT 30
#define TEXT_X ((TOTAL_X-8*MAXINPUT)/2)
#define TEXT_Y ((TOTAL_Y-8)/2)


void print_text(sge_TextSurface *text, const char *menu_name)
{
    clr_screen();
    paint_menu();

    sge_BF_textoutf(screen, font, TEXT_X, TEXT_Y-12, (char *)menu_name);
    sge_Line(screen, TEXT_X-1, TEXT_Y, TEXT_X + (MAXINPUT*8), TEXT_Y, 000,0xff,0xff);
    sge_Line(screen, TEXT_X-1, TEXT_Y+12, TEXT_X + (MAXINPUT*8), TEXT_Y+12, 000,0xff,0xff);
    sge_Line(screen, TEXT_X-1, TEXT_Y, TEXT_X-1, TEXT_Y+12, 000,0xff,0xff);
    sge_Line(screen, TEXT_X + (MAXINPUT*8), TEXT_Y, TEXT_X + (MAXINPUT*8), TEXT_Y+12, 000,0xff,0xff);

    /* The text has changed */ 
    text->draw(); //Draw the new text 
    SDL_Flip(screen);
}

string edit_text(const char *menu_name, const char *default_text)
{
    SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL+50);
    SDL_EnableUNICODE(1); //This is VERY important!!

    sge_TextSurface text(screen, default_text ,TEXT_X, TEXT_Y);
    text.set_bmFont(font); //Use the BM font
    text.change_cursor('*');
    text.max_chars(30);
    text.show_cursor(true); //Show a cursor
    SDL_Event event;
    clr_screen();
    paint_menu();

    print_text(&text, menu_name);
    SDL_Flip(screen);
    do{
        SDL_Delay(10); 

    /* Check events */

        if( SDL_PollEvent(&event) == 1 ){
            if( event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE ) break; 
            if( event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN ) break; 
            if( event.type == SDL_QUIT ) break; 

            /* Let the text class handle the event*/

            if( text.check(&event) ){
                print_text(&text, menu_name);
            }

        } 
    }while(true);
    SDL_EnableUNICODE(0); //This is VERY important!!
    string st;
    if (event.key.keysym.sym == SDLK_RETURN)
    {
        st = text.get_string(false);
    }
    return st;
}

bool yes_no()
{
    string st;
    st = edit_text("Are you sure ?", "yes");
    return st == string("yes");
}

int get_int(const char *text)
{
    string st;
    st = edit_text(text, "1");
    return atoi(st.c_str());
}

void menu_load(void)
{
    edit_object->load();
}

void menu_save(void)
{
    edit_object->save();
}

void menu_new_line(void)
{
    edit_object->set_mode(FM_NEW_LINE);
}

void menu_pick_edge(void)
{
    edit_object->set_mode(FM_PICK_EDGE);
}

void menu_delete_line(void)
{
    edit_object->set_mode(FM_DELETE_LINE);
}

void menu_copy_frame(void)
{
    edit_object->copy_frame();
}

void menu_erase_frame(void)
{
    edit_object->delete_frame();
}

void menu_next_frame(void)
{
    edit_object->next();
}

void menu_prev_frame(void)
{
    edit_object->prev();
}

void menu_invert_history(void)
{
    edit_object->change_direction();
}

void menu_interpolate(void)
{
    int number_of_frames;
    if(edit_object->between_frames())
    {
        number_of_frames = get_int("number of frames");
        edit_object->interpolate(number_of_frames);
    }
}

void menu_quit(void)
{
    if(yes_no())
    {
        delete edit_object;
        SDL_Quit();
        exit(0);
    }
}

void add_menu_item(const char *menu_name, void (*menu_func)(void), SDLKey key)
{
    MenuItem *new_menu;
    MenuItem *current_menu;

    new_menu = (MenuItem *)malloc(sizeof(MenuItem) + strlen(menu_name));
    strcpy(new_menu->name, menu_name);
    new_menu->function = menu_func;
    new_menu->key = key;
    new_menu->next = NULL;

    current_menu = menu;
    if(current_menu)
    {
        while(current_menu->next)
            current_menu = current_menu->next;
        current_menu->next = new_menu;
    }
    else
    {
        menu = new_menu;
    }
}

void init_menu(void)
{
    add_menu_item("load      (l)", menu_load, SDLK_l);
    add_menu_item("save      (s)", menu_save, SDLK_s);
    add_menu_item("new line  (n)", menu_new_line, SDLK_n);
    add_menu_item("pick edge (p)", menu_pick_edge, SDLK_p);
    add_menu_item("del line  (d)", menu_delete_line, SDLK_d);
    add_menu_item("next frame ->", menu_next_frame, SDLK_RIGHT);
    add_menu_item("prev frame <-", menu_prev_frame, SDLK_LEFT);
    add_menu_item("copy frame(c)", menu_copy_frame, SDLK_c);
    add_menu_item("erase frme(e)", menu_erase_frame, SDLK_e);
    add_menu_item("invert his(h)", menu_invert_history, SDLK_h);
    add_menu_item("interpol  (i)", menu_interpolate, SDLK_i);
    add_menu_item("quit      (q)", menu_quit, SDLK_q);
}

void handle_menu(int x, int y)
{
    MenuItem *current_menu = menu;
    int i = 1;
    while(current_menu)
    {
        if (y < 16*i-1)
        {
            current_menu->function();
            return;
        }
        ++i;
        current_menu = current_menu->next;
    }
}

void handle_menu(SDLKey key)
{
    MenuItem *current_menu = menu;
    while(current_menu)
    {
        if(current_menu->key == key)
        {
            current_menu->function();
            return;
        }
        current_menu = current_menu->next;
    }
}


void paint_menu(void)
{
    int i;

    sge_Line(screen, MENU_SIZE, 0, MENU_SIZE, TOTAL_Y-1, 0x00,0xff,0xff);
    sge_Line(screen, MENU_SIZE,0, TOTAL_X-1, 0, 0x00,0xff,0xff);
    sge_Line(screen, MENU_SIZE, TOTAL_Y-1, TOTAL_X-1, TOTAL_Y-1, 0x00,0xff,0xff);
    sge_Line(screen, TOTAL_X-1, 0, TOTAL_X-1, TOTAL_Y-1, 0x00,0xff,0xff);
    MenuItem *current_menu = menu;
    i = 1;
    while(current_menu)
    {
        sge_BF_textoutf(screen, font, 1, 16*(i-1)+1, current_menu->name);
        sge_Line(screen, 0, 16*i-1, MENU_SIZE, 16*i-1, 000,0xff,0xff);
        ++i;
        current_menu = current_menu->next;
    }
    sge_Circle(screen, SCREEN_X/2 + MENU_SIZE, SCREEN_Y/2, min(SCREEN_X, SCREEN_Y)/2, 0x7f,0,0);
}

