/*************************************************************************/
//---------------------------  LineCoordinates  -------------------------//
/*************************************************************************/
#include <stdio.h>
#include "defines.h"

class LineCoordinates
{
public:
    float x_1;
    float y_1;
    float x_2;
    float y_2;

    LineCoordinates(const float x1,const float y1,
                    const float x2,const float y2)
    {
        x_1=x1;
        y_1=y1;
        x_2=x2;
        y_2=y2;
    }
};

/*************************************************************************/
//-------------------------  WindowCoordinates  -------------------------//
/*************************************************************************/

class WindowCoordinates
{
public:
    float x_min;
    float y_min;
    float x_max;
    float y_max;

    WindowCoordinates(const float x1,const float y1,
                      const float x2,const float y2)
    {
        x_min=x1;
        y_min=y1;
        x_max=x2;
        y_max=y2;
    }
};

/*************************************************************************/
//-----------------------------  RegionCode  ----------------------------//
/*************************************************************************/

class RegionCode
{
public:
    int bit_1;
    int bit_2;
    int bit_3;
    int bit_4;

    RegionCode( )
    {
        bit_1=0;
        bit_2=0;
        bit_3=0;
        bit_4=0;
    }

    const int equal_zero( )
    {
        if (bit_1==0 && bit_2==0 && bit_3==0 && bit_4==0)
            return 1;

        return 0;
    }

    const int greater_one( )
    {
        return(bit_1 + bit_2 + bit_3 + bit_4) >  1;
    }

    void get_logical_AND(RegionCode rc1,RegionCode rc2)
    {
        bit_1 =0;
        bit_2 =0;
        bit_3 =0;
        bit_4 =0;

        if (rc1.bit_1==1 && rc2.bit_1==1)
            bit_1=1;

        if (rc1.bit_2==1 && rc2.bit_2==1)
            bit_2=1;

        if (rc1.bit_3==1 && rc2.bit_3==1)
            bit_3=1;

        if (rc1.bit_4==1 && rc2.bit_4==1)
            bit_4=1;
    }
    void get_logical_OR(RegionCode rc1,RegionCode rc2)
    {
        bit_1 =0;
        bit_2 =0;
        bit_3 =0;
        bit_4 =0;
        if (rc1.bit_1==1 || rc2.bit_1==1)
            bit_1=1;

        if (rc1.bit_2==1 || rc2.bit_2==1)
            bit_2=1;

        if (rc1.bit_3==1 || rc2.bit_3==1)
            bit_3=1;

        if (rc1.bit_4==1 || rc2.bit_4==1)
            bit_4=1;
    }

    void get_region_code(const WindowCoordinates wc,
                         const float x,const float y)
    {
        if ((wc.x_min-x)>0)
            bit_1=1;

        if ((x-wc.x_max)>0)
            bit_2=1;

        if ((wc.y_min-y)>0)
            bit_3=1;

        if ((y-wc.y_max)>0)
            bit_4=1;
    }
};

const int clip_line(const WindowCoordinates,LineCoordinates&);

void calculate_intersecting_points(const WindowCoordinates,LineCoordinates&);


/*************************************************************************/
//---------------------------  clip_line( )  ----------------------------//
/*************************************************************************/

const int clip_line(const WindowCoordinates wc,LineCoordinates &lc)
{
    RegionCode rc1,rc2,rc;

    rc1.get_region_code(wc,lc.x_1,lc.y_1);
    rc2.get_region_code(wc,lc.x_2,lc.y_2);
    rc.get_logical_AND(rc1,rc2);

    if (rc1.equal_zero( ) && rc2.equal_zero( ))
    {
        lc.x_1=(int)(lc.x_1+0.5);
        lc.y_1=(int)(lc.y_1+0.5);
        lc.x_2=(int)(lc.x_2+0.5);
        lc.y_2=(int)(lc.y_2+0.5);

        return 1;
    }

    else if (!rc.equal_zero( ))
        return 0;

    else
    {
        calculate_intersecting_points(wc,lc);
        clip_line(wc,lc);
    }

    return 1;
}

/*************************************************************************/
//-----------------  calculate_intersecting_points( )  ------------------//
/*************************************************************************/

void calculate_intersecting_points(const WindowCoordinates wc,
                                   LineCoordinates &lc)
{
    RegionCode rc1,rc2,rc;

    rc1.get_region_code(wc,lc.x_1,lc.y_1);
    rc2.get_region_code(wc,lc.x_2,lc.y_2);

    if (!rc1.equal_zero( ))
    {
        float m = 0.0;
        float x=lc.x_1;
        float y=lc.y_1;

        float dx=(lc.x_2-lc.x_1);

        if (((int)dx)!=0)
            m=((lc.y_2-lc.y_1)/(lc.x_2-lc.x_1));

        if (rc1.bit_1==1)
        {
            x=wc.x_min;
            y=(lc.y_1+(m*(x-lc.x_1)));
        }

        else if (rc1.bit_2==1)
        {
            x=wc.x_max;
            y=(lc.y_1+(m*(x-lc.x_1)));
        }

        else if (rc1.bit_3==1)
        {
            y=wc.y_min;

            if (((int)dx)!=0)
                x=(lc.x_1+((y-lc.y_1)/m));
        }

        else if (rc1.bit_4==1)
        {
            y=wc.y_max;

            if (((int)dx)!=0)
                x=(lc.x_1+((y-lc.y_1)/m));
        }

        lc.x_1=x;
        lc.y_1=y;
    }

    if (!rc2.equal_zero( ))
    {
        float m = 0.0;
        float x=lc.x_2;
        float y=lc.y_2;

        float dx=(lc.x_2-lc.x_1);

        if (((int)dx)!=0)
            m=((lc.y_2-lc.y_1)/(lc.x_2-lc.x_1));

        if (rc2.bit_1==1)
        {
            x=wc.x_min;
            y=(lc.y_2+(m*(x-lc.x_2)));
        }

        else if (rc2.bit_2==1)
        {
            x=wc.x_max;
            y=(lc.y_2+(m*(x-lc.x_2)));
        }

        else if (rc2.bit_3==1)
        {
            y=wc.y_min;

            if (((int)dx)!=0)
                x=(lc.x_2+((y-lc.y_2)/m));
        }

        else if (rc2.bit_4==1)
        {
            y=wc.y_max;

            if (((int)dx)!=0)
                x=(lc.x_2+((wc.y_max-lc.y_2)/m));
        }

        lc.x_2=x;
        lc.y_2=y;
    }
}

WindowCoordinates win(0.0, 0.0, SCREEN_X-1, SCREEN_Y-1);

int in_region(int x, int y)
{
    RegionCode rc;
    rc.get_region_code(win, (float)x, (float)y);
    return rc.equal_zero();
}


int clip_line(int *x1, int *y1, int *x2, int *y2)
{
    LineCoordinates l((float)(*x1), (float)(*y1), (float)(*x2), (float)(*y2));
    if (clip_line(win, l))
    {
        *x1 = (int)(l.x_1);
        *y1 = (int)(l.y_1);
        *x2 = (int)(l.x_2);
        *y2 = (int)(l.y_2);
        return 1;
    } else
    {
        return 0;
    }
}

int calc_edge(int x1, int y1, int x2, int y2, int *x, int *y)
{
    RegionCode rc1;
    RegionCode rc2;
    RegionCode rc_or;

    rc1.get_region_code(win, (float)x1, (float)y1);
    rc2.get_region_code(win, (float)x2, (float)y2);
    rc_or.get_logical_OR(rc1, rc2);
    if (rc_or.greater_one())
    {
        //printf("%d %d %d %d ->", x1, y1, x2, y2);
        if (rc_or.bit_1)
        {
            *x = (int)win.x_min;
        }
        if (rc_or.bit_2)
        {
            *x = (int)win.x_max;
        }
        if (rc_or.bit_3)
        {
            *y = (int)win.y_min;
        }
        if (rc_or.bit_4)
        {
            *y = (int)win.y_max;
        }
        //printf("%d %d\n", *x, *y);
        return 1;
    } else
    {
        return 0;
    }
}

int clipped_polygon(int n_old, short *x_old, short *y_old, short *x_new, short *y_new)
{
    int x1,y1,x2,y2;
    int x3,y3,x4,y4;
    int i;
    int k = 0;
    short x_temp[n_old*2];
    short y_temp[n_old+2];
    int last_clip = -1;

    x2 = x_old[n_old-1];
    y2 = y_old[n_old-1];

    i = 0;
    for (int j=0; j < n_old; ++j)
    {
        x1 = x_old[j];
        y1 = y_old[j];
        if (in_region(x1,y1))
        {
            if (in_region(x2,y2))
            {
                x_temp[i] = x1;
                y_temp[i] = y1;
                ++i;
                last_clip = -1;
            } else
            {
                if (last_clip >= 0)
                {
                    if (calc_edge(x_old[last_clip], y_old[last_clip], x2, y2, &x3, &y3))
                    {
                        x_temp[i] = x3;
                        y_temp[i] = y3;
                        ++i;
                    }
                }
                x3 = x1;
                y3 = y1;
                x4 = x2;
                y4 = y2;
                if (clip_line(&x3,&y3,&x4,&y4))
                {

                    x_temp[i] = x4;
                    y_temp[i] = y4;
                    ++i;                
                    x_temp[i] = x1;
                    y_temp[i] = y1;
                    ++i;
                }
            }
            last_clip = -1;
        } 
        else
        {
            if (last_clip < 0)
            {
                last_clip = j;
            }
            if (in_region(x2,y2))
            {
                x3 = x1;
                y3 = y1;
                x4 = x2;
                y4 = y2;
                if (clip_line(&x3,&y3,&x4,&y4))
                {
                    x_temp[i] = x3;
                    y_temp[i] = y3;
                    ++i;
                    last_clip = j;
                }
            }
        }
        x2 = x1;
        y2 = y1;
    }
    if (i)
    {

        x_new[0] = x_temp[0];
        y_new[0] = y_temp[0];
        k = 1;
        int l;
        for (int j=1; j < i; ++j)
        {
            for (l=0; l < k; ++l)
            {
                if (( x_new[l] == x_temp[j])
                    &&
                    ( y_new[l] == y_temp[j]))
                {
                    break;
                }
            }
            if (l >= k)
            {
                x_new[k] = x_temp[j];
                y_new[k] = y_temp[j];
                k++;
            }
        }
    }
    return k;
}

