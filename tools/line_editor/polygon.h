#ifndef _polygon_h_455445
#define _polygon_h_455445
#include <stdio.h>

extern FILE *output_file;


void line(short x1, short y1, short x2, short y2, int color);
void polygon(int n, short *x, short *y, int color, int lined);
void filled_circle(short x, short y, short r, int color, int planet, int shadow);
void wait_vbl();
void clr_screen();
int clip_line(int *x1, int *y1, int *x2, int *y2);
int clipped_polygon(int n_old, short *x_old, short *y_old, short *x_new, short *y_new);
int calc_dtv_color(int color, double luminance);
void print_lines(void);
#endif

