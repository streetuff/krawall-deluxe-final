#ifndef __frame_h_549834908395435485
#define __frame_h_549834908395435485

#include <stdio.h>
#include <limits.h>
#include <vector>
#include "defines.h"
using namespace std;

typedef enum {
    F_BASE = 0,
    F_LINE,
    F_FRAME,
    F_FILM
} FrameType;

typedef enum {
    L_FIXED = 0,
    L_MOVABLE_1ST_POINT,
    L_MOVABLE_2ND_POINT,
    L_SELECTED,
} LineMovable;

typedef enum {
    FM_NONE = 0,
    FM_NEW_LINE,
    FM_NEW_LINE_SECOND_POINT,
    FM_PICK_EDGE,
    FM_DELETE_LINE
} FrameMode;

class Base {
protected:
    static int m_x;
    static int m_y;
    static int color_red,color_green,color_blue;
    static int m_direction;
    static FILE *file;
public:
    virtual void save() = 0;
    virtual void load(bool insert = false) = 0;
    virtual void paint(int dark = 0) = 0;
    virtual FrameType type() { return F_BASE; };
    virtual Base *deep_copy() = 0;
    virtual void set_movable(int which) {};
    virtual void set_fixed(void) {} ;
    virtual void set_selected(void) {} ;
    virtual void set_mode(FrameMode mode) {};
    virtual void mouse_click(int x, int y) {};
    virtual void cancel() = 0;
    virtual double calc_distance(int x, int y, int point) {return 1e+20; };
    virtual int number_of_points() { return 0; };
    virtual void set_number(int i) {};
    virtual void next() {};
    virtual void prev() {};
    virtual void copy_frame() {};
    virtual void delete_frame() {};
    virtual bool between_frames() { return false; };
    virtual void interpolate(int number_of_frames) {} ;
    static void change_direction() {m_direction = m_direction ^ 1;};
    static void cycle_color();
    static void set_mouse_coord(int x, int y);
};

class Line : public Base {
    Sint16 m_x1,m_y1, m_x2, m_y2;
    Sint16 m_x1_bu,m_y1_bu, m_x2_bu, m_y2_bu;
    LineMovable moveable;
public:
    friend Line *interpolate(Line *l1, Line *l2, double factor);
    virtual void save();
    virtual void load(bool insert = false);
    virtual FrameType type() { return F_LINE; };
    Line(int x1=0, int y1=0);
    Line(const Line &l);
    virtual Base *deep_copy();
    virtual void paint(int dark = 0);
    virtual void set_movable(int which);
    virtual void set_fixed(void);
    virtual void set_selected(void);
    virtual void cancel();
    virtual double calc_distance(int x, int y, int point);
    virtual int number_of_points() { return 2; };
};


class Frame : public Base {
    list<Base *> m_objects;
    int m_current_edge;
    Base *m_current_object;
    FrameMode m_mode;
    void search_nearest_edge();
    int m_number;
    void cleanup();
public:
    friend Frame *interpolate(Frame *f1, Frame *f2, double factor);
    Frame();
    virtual void save();
    virtual void load(bool insert = false);
    virtual Base *deep_copy();
    virtual FrameType type() { return F_FRAME; };
    virtual void mouse_click(int x, int y);
    virtual void cancel();
    virtual void set_mode(FrameMode mode);
    virtual void paint(int dark = 0);
    virtual void set_number(int i) {m_number = i;};
    virtual ~Frame();
};

class Film : public Base {
    list<Base *> m_objects;
    Base *m_current_object;
    list<Base *>::iterator get_current_index();

    void renumber();
public:
    Film();
    void cleanup();
    virtual void save();
    virtual void load(bool insert = false);
    virtual FrameType type() { return F_FILM; }
    virtual void set_mode(FrameMode mode)
    {
        if(m_current_object)
            m_current_object->set_mode(mode);
    }
    virtual void mouse_click(int x, int y)
    {
        if(m_current_object)
            m_current_object->mouse_click(x,y);
    }
    virtual void cancel()
    {
        if(m_current_object)
            m_current_object->cancel();
    }
    virtual void paint(int dark = 0);
    virtual void next();
    virtual void prev();
    virtual void copy_frame();
    virtual void delete_frame();
    virtual bool between_frames();
    virtual void interpolate(int number_of_frames);
    virtual Base *deep_copy() { return NULL; } ;
    virtual ~Film();
};

#endif
