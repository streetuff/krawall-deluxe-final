#include <stdio.h>
#include "types.hpp"
#ifndef WIN32
#include <unistd.h>
#endif
#include <SDL/SDL.h>
#include <SDL/sge.h>
#include <SDL/sge_surface.h>

#include "vec2.hpp"
#include "frame.h"

#define ABS(x) ((x) < 0 ? (-(x)) : (x))

using namespace math3d;
extern SDL_Surface *screen;
extern sge_bmpFont *font;

int Base::m_x = 0;
int Base::m_y = 0;
int Base::color_red = 0;
int Base::color_green = 0xff;
int Base::color_blue = 0xff;
int Base::m_direction = 0;

FILE *Base::file = NULL;

extern string edit_text(const char *menu_name, const char *default_text);
bool yes_no();

void Base::cycle_color()
{
    color_red <<= 1;
    color_green <<= 1;
    color_blue <<= 1;
    if(color_red > 0xff)
    {
        color_red -= 0x100;
        color_green +=1;
    }
    if(color_green > 0xff)
    {
        color_green -= 0x100;
        color_blue +=1;
    }
    if(color_blue > 0xff)
    {
        color_blue -= 0x100;
        color_red +=1;
    }
}

void Base::set_mouse_coord(int x, int y)
{
    if(x < MENU_SIZE)
        x = MENU_SIZE; 
    m_x = (x/GRID)*GRID;
    m_y = (y/GRID)*GRID;
}

Line::Line(const Line &l)
{
    moveable = l.moveable;
    m_x1 = l.m_x1;
    m_x2 = l.m_x2;
    m_y1 = l.m_y1;
    m_y2 = l.m_y2;
}

Line::Line(int x1, int y1)
{
    moveable = L_FIXED;
    m_x1 = x1;
    m_y1 = y1;
    m_x2 = x1;
    m_y2 = y1;
}

void Line::save()
{
    if(file)
    {
        fputc((m_x1-MENU_SIZE)/GRID, file);
        fputc((m_y1)/GRID, file);
        fputc((m_x2-MENU_SIZE)/GRID, file);
        fputc((m_y2)/GRID, file);
    }
}

void Line::load(bool insert)
{
    if(file)
    {
        m_x1 = fgetc(file)*GRID+MENU_SIZE;
        m_y1 = fgetc(file)*GRID;
        m_x2 = fgetc(file)*GRID+MENU_SIZE;
        m_y2 = fgetc(file)*GRID;
    }
}

Base * Line::deep_copy()
{
    Line *n = new Line;
    *n = *this;
    return n;
}

void Line :: paint(int dark)
{
    int x1,y1,x2,y2;
    int color_r, color_g, color_b;
    x1 = m_x1;
    x2 = m_x2;
    y1 = m_y1;
    y2 = m_y2;
    color_r = color_red;
    color_g = color_green;
    color_b = color_blue;
    switch(moveable)
    {
        case L_MOVABLE_1ST_POINT:
            x1 = m_x;
            y1 = m_y;
            sge_Line(screen, m_x1,m_y1, m_x2, m_y2, 0x60,0x60,0x60);
            break;
        case L_MOVABLE_2ND_POINT:
            x2 = m_x;
            y2 = m_y;
            sge_Line(screen, m_x1,m_y1, m_x2, m_y2, 0x60,0x60,0x60);
            break;
        case L_SELECTED:
            break;
        case L_FIXED:
            switch (dark)
            {
                case 0: color_r = color_g = color_b = 0xff;break;
                case 1: color_r = color_g = color_b = 0x6f;break;
                default: color_r = color_g = color_b = 0x2f;break;
            }
            break;
    }
    sge_Line(screen, x1,y1, x2, y2, color_r,color_g,color_b);
}

void Line :: set_movable(int which)
{
    m_x1_bu = m_x1;
    m_y1_bu = m_y1;
    m_x2_bu = m_x2;
    m_y2_bu = m_y2;
    if(which == 0)
        moveable = L_MOVABLE_1ST_POINT;
    else
        moveable = L_MOVABLE_2ND_POINT;
}

void Line :: set_selected()
{
    moveable = L_SELECTED;
}

void Line :: cancel()
{
    switch (moveable)
    {
        case L_MOVABLE_1ST_POINT:
        case L_MOVABLE_2ND_POINT:
            m_x1 = m_x1_bu;
            m_y1 = m_y1_bu;
            m_x2 = m_x2_bu;
            m_y2 = m_y2_bu;
            break;
        default:
            break;
    }
    moveable = L_FIXED;
}

void Line :: set_fixed(void)
{
    switch (moveable)
    {
        case L_MOVABLE_1ST_POINT:
            m_x1 = m_x;
            m_y1 = m_y;
        break;
        case L_MOVABLE_2ND_POINT:
            m_x2 = m_x;
            m_y2 = m_y;
        break;
        case L_SELECTED:
        case L_FIXED:
        break;
    }
    moveable = L_FIXED;
}

double Line :: calc_distance(int x, int y, int point)
{
    switch (point)
    {
        case 0:
            return (double)((x-m_x1)*(x-m_x1)+(y-m_y1)*(y-m_y1));
        case 1:
            return (double)((x-m_x2)*(x-m_x2)+(y-m_y2)*(y-m_y2));
        case -1:
            {
                if(x == m_x1 && x == m_x2 && y == m_y1 && y == m_y2)
                    return -1.0;
                double factor;
                vec2<double> u(m_x1, m_y1);
                vec2<double> v(m_x2, m_y2);
                v = v-u;
                vec2<double> p(x,y);
                factor = (p - u)[0] * v[0] + (p - u)[1] * v[1];
                vec2<double> lot = u + v * (factor / (v.length() * v.length()));
                return ((lot-p).length()) 
                    + (double)(ABS((m_x1+m_x2)/2-x) + ABS((m_y1+m_y2)/2-y)) / 
                                ((double)(TOTAL_X+TOTAL_Y));
            }
        default:
            return 1e+20;
    }
}

Sint16 interpolate(Sint16 a, Sint16 b, double factor)
{
    return (Sint16) (((double)a) * (1.0 - factor) + ((double)b) * factor);
}

Line * interpolate(Line *l1, Line *l2, double factor)
{
    Line *ret = new Line;
    ret->m_x1 = interpolate(l1->m_x1, l2->m_x1, factor);
    ret->m_x2 = interpolate(l1->m_x2, l2->m_x2, factor);
    ret->m_y1 = interpolate(l1->m_y1, l2->m_y1, factor);
    ret->m_y2 = interpolate(l1->m_y2, l2->m_y2, factor);
    return ret;
}

void Frame :: paint(int dark)
{
    list<Base *>::iterator it;

    if((m_current_object == NULL) && 
         ((m_mode == FM_PICK_EDGE)
       || (m_mode == FM_DELETE_LINE))
      )
    {
        for(it=m_objects.begin(); it != m_objects.end(); ++it)
        {
            (*it)->set_fixed();
        }
        search_nearest_edge();
        if(m_current_object)
        {
            m_current_object->set_selected();
            m_current_object = NULL;
        }
    }
    for(it=m_objects.begin(); it != m_objects.end(); ++it)
    {
        (*it)->paint(dark);
    }
    if(!dark)
    {
        sge_BF_textoutf(screen, font, 1, TOTAL_Y-15, (char *)"%4d %4d", (m_x - MENU_SIZE)/GRID, m_y/GRID);
        sge_BF_textoutf(screen, font, 1, TOTAL_Y-15-16, (char *)"Frame %4d %c", m_number, (m_direction) ? '>' : '<');
        const char *mode_string;
        switch(m_mode)
        {
            case FM_NONE: mode_string = "";break;
            case FM_NEW_LINE: mode_string = "new line";break;
            case FM_NEW_LINE_SECOND_POINT: mode_string = "set end point";break;
            case FM_PICK_EDGE: mode_string = "pick edge";break;
            case FM_DELETE_LINE: mode_string = "delete line";break;
            default: mode_string = "unknown"; break;
        }
        sge_BF_textoutf(screen, font, 1, TOTAL_Y-15-16-16, (char *)mode_string);
    }
}

Frame::Frame()
{
    m_mode = FM_NONE;
    m_current_object = NULL;
    m_current_edge = 0;
    m_number = 0;
}

void Frame::save()
{
    if(file)
    {
        fputc(m_objects.size(), file);
        list<Base *>::iterator it;
        for(it=m_objects.begin(); it != m_objects.end(); ++it)
        {
            (*it)->save();
        }
    }
}

void Frame::load(bool insert)
{
    int i;
    int size;
    Line *object;
    if(file)
    {
        size = fgetc(file);
        for(i=0; i < size; ++i)
        {
            object = new Line();
            object->load();
            m_objects.push_back(object);
        }
    }
}


Base * Frame::deep_copy()
{
    list<Base *>::iterator it;
    Base *object;

    Frame *n = new Frame();
    n->m_mode = FM_NONE;
    n->m_current_object = NULL;
    n->m_current_edge = 0;
    n->m_number = 0;

    for(it=m_objects.begin(); it != m_objects.end(); ++it)
    {
        object = (*it)->deep_copy();
        n->m_objects.push_back(object);
    }
    return n;
}

void Frame::cleanup()
{
    list<Base *>::iterator it;
    while((it = m_objects.begin()) != m_objects.end())
    {
    	m_current_object = *it;
        delete m_current_object;
        m_objects.erase(it);
    }
    m_current_object = NULL;
}

Frame::~Frame()
{
    cleanup();
}

Frame *interpolate(Frame *f1, Frame *f2, double factor)
{
    list<Base *>::iterator it_f1;
    list<Base *>::iterator it_f2;
    Line *l_f1;
    Line *l_f2;
    Line *new_line;

    Frame *f = new Frame;
    it_f1 = f1->m_objects.begin();
    it_f2 = f2->m_objects.begin();
    while(it_f1 != f1->m_objects.end() && it_f2 != f2->m_objects.end())
    {
        if(((*it_f1)->type() == F_LINE) && ((*it_f1)->type() == F_LINE))
        {
            l_f1 = (Line *)(*it_f1);
            l_f2 = (Line *)(*it_f2);
            new_line = interpolate(l_f1, l_f2, factor);
            f->m_objects.push_back(new_line);
        }
        ++it_f1;
        ++it_f2;
    }
    return f;
}

void Frame::search_nearest_edge()
{
    Base *current_object;
    list<Base *>::iterator it;
    int j;
    double val;
    Base *nearest_object;
    int nearest_edge;
    double nearest_value;
    double nearest_lot;

    nearest_lot = INT_MAX;
    nearest_object = NULL;
    for(it=m_objects.begin(); it != m_objects.end(); ++it)
    {
        current_object = *it;;
        val = current_object->calc_distance(m_x, m_y, -1);
        if (val < nearest_lot)
        {
            nearest_lot = val;
            nearest_object = current_object;
            nearest_value = INT_MAX;
            
            for(j=0; j < current_object->number_of_points(); ++j)
            {
                val = current_object->calc_distance(m_x, m_y, j);
                if(val < nearest_value)
                {
                    nearest_value = val;
                    nearest_edge = j;
                }
            }
        }
    }
    m_current_object = nearest_object;
    m_current_edge = nearest_edge;
    if (nearest_object == NULL)
        m_mode = FM_NONE;
}

void Frame::set_mode(FrameMode mode)
{
    switch (m_mode)
    {
        case FM_NEW_LINE:
        case FM_NEW_LINE_SECOND_POINT:
        case FM_PICK_EDGE:
            return;
            break;
        default:
            break;
    }
    m_mode = mode;
}

void Frame::mouse_click(int x, int y)
{
    list<Base *>::iterator it;

    switch (m_mode)
    {
        case FM_NONE:
            break;
        case FM_NEW_LINE:
            m_current_object = new Line(m_x, m_y);
            m_objects.push_back(m_current_object);
            m_current_object->set_movable(1);
            m_mode = FM_NEW_LINE_SECOND_POINT;
            break;
        case FM_NEW_LINE_SECOND_POINT:
            m_current_object->set_fixed();
            m_current_object = NULL;
            m_mode = FM_NONE;
            break;
        case FM_PICK_EDGE:
            search_nearest_edge();
            if(m_current_object)
            {
                m_current_object->set_movable(m_current_edge);
                m_mode = FM_NEW_LINE_SECOND_POINT;
            }
            break;
        case FM_DELETE_LINE:
            search_nearest_edge();
            if(m_current_object)
            {
                m_current_object->set_fixed();
                for(it=m_objects.begin(); it != m_objects.end(); ++it)
                {
                    if( (*it)==m_current_object)
                    {
                        delete m_current_object;
                        m_objects.erase(it);
                        m_current_object = NULL;
                        break;
                    }
                }
                m_mode = FM_NONE;
            }
            break;
    }
}

void Frame::cancel()
{
    list<Base *>::iterator it;
    for(it=m_objects.begin(); it != m_objects.end(); ++it)
    {
        (*it)->cancel();
    }
    m_mode = FM_NONE;
    m_current_object = NULL;
}

void Film::cleanup()
{
    list<Base *>::iterator it;
    while((it = m_objects.begin()) != m_objects.end())
    {
    	m_current_object = *it;
        delete m_current_object;
        m_objects.erase(it);
    }
    m_current_object = NULL;
}

Film::~Film()
{
    cleanup();
}

Film::Film()
{
    m_current_object = new Frame();
    m_objects.push_back(m_current_object);
}

void Film::save()
{
    string filename;
    filename = edit_text("save", "icoall.sav");
    file = fopen(filename.c_str(), "wb");
    if(file)
    {
        fputc(m_objects.size()/256, file);
        fputc(m_objects.size()&0xff, file);
        list<Base *>::iterator it;
        for(it=m_objects.begin(); it != m_objects.end(); ++it)
        {
            (*it)->save();
        }
        fclose(file);
    }
    file = NULL;
}

void Film::load(bool insert)
{
    int i;
    int size;
    string filename;
    cleanup();
    filename = edit_text("load", "icoall.sav");
    file = fopen(filename.c_str(), "rb");
    Frame *object;
    if(file)
    {
        size = fgetc(file)*256 + fgetc(file);
        for(i=0; i < size; ++i)
        {
            object = new Frame();
            object->load();
            m_objects.push_back(object);
        }
        m_current_object = *(m_objects.begin());
        fclose(file);
    }
    else
    {
        m_current_object = new Frame();
        m_objects.push_back(m_current_object);
    }
    file = NULL;
    renumber();
}

list<Base *>::iterator Film::get_current_index()
{
    list<Base *>::iterator it;

    for(it=m_objects.begin(); it != m_objects.end(); ++it)
    {
        if ((*it) == m_current_object)
            break;
    }
    if(it == m_objects.end())
        it = m_objects.begin();
    return it;
}

void Film::next()
{
    list<Base *>::iterator it;

    if(m_current_object)
        m_current_object->cancel();
    it = get_current_index();
    ++it;
    if(it == m_objects.end())
        it = m_objects.begin();
    m_current_object = *it;
}

void Film::prev()
{
    list<Base *>::iterator it;

    if(m_current_object)
        m_current_object->cancel();
    it = get_current_index();
    --it;
    if(it == m_objects.end())
        --it;
    m_current_object = *it;
}

void Film::copy_frame()
{
    list<Base *>::iterator it;
    Base *object;
    if(m_current_object)
        m_current_object->cancel();
    it = get_current_index();

    object = (*it)->deep_copy();
    m_objects.insert(it, object);
    renumber();
}

void Film::delete_frame()
{
    list<Base *>::iterator it;
    list<Base *>::iterator it_last;
    if(m_current_object && yes_no())
    {
        it = get_current_index();
        it_last = it;
        --it_last;
        if(it_last == m_objects.end())
        {
            --it_last;
            if(it_last == it)
            {
                /* only one frame left, cancel deletion*/
                return;
            }
        }
        delete m_current_object;
        m_objects.erase(it);
        m_current_object = (*it_last);
    }
}

void Film::renumber()
{
    list<Base *>::iterator it;
    int i=0;
    for(it=m_objects.begin(); it != m_objects.end(); ++it)
    {
        (*it)->set_number(i);
        ++i;
    }
}

bool Film::between_frames()
{
    list<Base *>::iterator it;
    it = get_current_index();
    ++it;
    return it!= m_objects.end(); 
}

void Film::interpolate(int number_of_frames)
{
    Frame *f;
    Frame *f_start;
    Frame *f_end;
    list<Base *>::iterator it;
    double factor_step;
    double factor;

    if(number_of_frames <= 0)
        return;
    it = get_current_index();
    if(it == m_objects.end())
        return;
    if ((*it)->type() != F_FRAME)
        return;
    f_start = (Frame *)(*it);
    ++it;
    if(it == m_objects.end())
        return;
    if ((*it)->type() != F_FRAME)
        return;
    f_end = (Frame *)(*it);
    factor_step = 1.0 / (1.0+(double)number_of_frames);
    factor = 0.0;
    for(int i=0; i < number_of_frames; ++i)
    {
        factor += factor_step;
        f = ::interpolate(f_start, f_end, factor);
        it = m_objects.insert(it, f);
        ++it;
    }
}

void Film::paint(int dark)
{
    list<Base *>::iterator last;
    if(m_direction)
    {
        if(m_current_object)
        {
            last = get_current_index();
            ++last;
            if(last != m_objects.end())
            {
                ++last;
                if(last != m_objects.end())
                    //(*last)->paint(2);
                --last;
                //(*last)->paint(1);
            }
            m_current_object->paint(dark);
        }
    }
    else
    {
        if(m_current_object)
        {
            last = get_current_index();
            --last;
            if(last != m_objects.end())
            {
                --last;
                if(last != m_objects.end())
                    //(*last)->paint(2);
                ++last;
                //(*last)->paint(1);
            }
            m_current_object->paint(dark);
        }
    }
}

