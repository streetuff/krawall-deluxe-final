#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/sge.h>
#undef PI
#include "vec3.hpp"
#include "coordsystem.h"
#include "object.h"
#include "defines.h"
#include "polygon.h"

#define M_PI 3.141592654

SDL_Surface *screen;

void clr_screen()
{
    SDL_Flip(screen);
    sge_Update_ON();
    sge_Update_OFF();
    sge_FilledRect(screen, 0,0, SCREEN_X,SCREEN_Y, 0,0,0);
}

void do_waitvbl()
{
    SDL_Delay(5);
    #if 0
        #if 0
        #ifndef WIN32
        usleep(20000);
        #else
        {
            volatile int loop;
            for(loop = 0; loop < 5000000; ++loop);
        }
        #endif
        #endif
    #endif
    SDL_PumpEvents();
}

#define DEG(i) (((double)i) * M_PI /180.0)

void paint_test_satelit(int x, int y, int z)
{
    CoordSystem coord;
    Object obj;

    coord.rotate_x_y_z((((double)x) /** M_PI / 180*/),
                       (((double)y) /** M_PI / 180*/),
                       (((double)z) /** M_PI / 180*/));
    //if (x < 0)
    //{
    //    coord.translate(vec3<>(1P-x*4,0.0,(double)(5*(x-70))));
    //} else
    //{
    //    coord.translate(vec3<>(1P-x*4,0.0,(double)(1800-(x+70)*5)));
    //}
    #define P 128
    QuadratLine *q;
    q = new QuadratLine(
                         vec3<>(P,-P,-P),
                         vec3<>(P,P,-P),
                         vec3<>(P,P,P),
                         vec3<>(P,-P,P));
    q->set_color(0xf);
    obj.add_object(q);

    q = new QuadratLine(
                        vec3<>(-P,-P,P),
                        vec3<>(-P,P,P),
                        vec3<>(-P,P,-P),
                        vec3<>(-P,-P,-P));
    q->set_color(0xdf);

    obj.add_object(q);

    q = new QuadratLine(
                         vec3<>(-P,P,P),
                         vec3<>(-P,-P,P),
                         vec3<>(P,-P,P),
                         vec3<>(P,P,P));
    q->set_color(0xff);
    obj.add_object(q);

    q = new QuadratLine(
                        vec3<>(P,P,-P),
                        vec3<>(P,-P,-P),
                        vec3<>(-P,-P,-P),
                        vec3<>(-P,P,-P));
    q->set_color(0x1f);
    obj.add_object(q);
    q = new QuadratLine(
                         vec3<>(P,P,-P),
                         vec3<>(-P,P,-P),
                         vec3<>(-P,P,P),
                         vec3<>(P,P,P));
    q->set_color(0x3f);
    obj.add_object(q);
    q = new QuadratLine(
                        vec3<>(P,-P,P),
                        vec3<>(-P,-P,P),
                        vec3<>(-P,-P,-P),
                         vec3<>(P,-P,-P));
    q->set_color(0x5f);
    obj.add_object(q);

    reset_paint();
    obj.paint(coord);
    do_paint(coord);
    print_lines();
}

extern FILE *output_file;

int main(int argc, char** argv)
{
    SDL_Event event;
   
	/* Init SDL */
	SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO);

	/* Set window title */
	SDL_WM_SetCaption("3d", "3d");
    CoordSystem coord;
    Object obj;


    output_file = fopen("3ddat.bin", "wb");

	/* Initialize the display */
	screen = SDL_SetVideoMode(SCREEN_X, SCREEN_Y, 16, SDL_DOUBLEBUF|SDL_HWSURFACE /*|SDL_FULLSCREEN*/);
    int i;
    int j;
    j =0;
    for(i=0;i < 360; i+=4)
    {
        if (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                 case SDL_QUIT:
                        SDL_Quit();
                        exit(1);
                    break;
                default:
                    break;
            }
        }
        clr_screen();
        paint_test_satelit(i,i*2+60,0);
        do_waitvbl();
        ++j;
    }
    fclose(output_file);
    printf("%d\n", j);
    return 0;
}

