#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/sge.h>
#undef PI
#include "vec3.hpp"
#include "coordsystem.h"
#include "object.h"
#include "defines.h"
#include "polygon.h"

#define M_PI 3.141592654

#define GRADE 118.0

SDL_Surface *screen;

vec3<> points[12];

int test_array[12] = {0};
int test_index[5];

int points_in_ebene(int index1, int index2, int index3, int index4, int index5)
{
    vec3<> diff1;
    vec3<> diff2;
    vec3<> richtung;
    vec3<> richtung2;

    diff1 = points[index2]-points[index1];
    diff2 = points[index3]-points[index1];
    //if(diff1.length() <= GRADE*2.0)
    //    return 0;
    richtung = cross_prod(diff1, diff2);
    richtung.normalize();

    diff2 = points[index4]-points[index1];
    richtung2 = cross_prod(diff1, diff2);
    richtung2.normalize();
    if(!(almost_equal(richtung,richtung2)) &&
       !(almost_equal(richtung,-richtung2)))
        return 0;
    diff2 = points[index5]-points[index1];
    richtung2 = cross_prod(diff1, diff2);
    richtung2.normalize();
    if(!(almost_equal(richtung,richtung2)) &&
       !(almost_equal(richtung,-richtung2)))
        return 0;
    return 1;
}

void check_point(int index)
{
    int i;
    int start;
    if(index == 0)
    {
        memset(test_array, 0 , sizeof(test_array));
    }
    if(index == 0)
    {
        start = 0;
    }
    else
    {
        start = test_index[index-1];
    }
    for(i=start; i < 12; ++i)
    {
        if(test_array[i] == 0)
        {
            test_array[i] = 1;
            test_index[index] = i;
            if(index == 4)
            {
                if(points_in_ebene(test_index[0],
                                   test_index[1],
                                   test_index[2],
                                   test_index[3],
                                   test_index[4]))
                {
                    printf("%d %d %d %d %d\n",
                       test_index[0], test_index[1], test_index[2], test_index[3], test_index[4]
                           /*,(points[test_index[0]] - points[test_index[1]]).length() */);
                }
            }
            else
            {
                check_point(index+1);
            }
            test_array[i] = 0;
        }
    }
}

/*

0 2 4 10 11
0 2 5 8 9
0 6 7 8 10
1 3 6 10 11
1 3 7 8 9
1 4 5 8 10
2 6 7 9 11
3 4 5 9 11

0 1 4 6 9
0 1 5 7 11
2 3 4 6 8
2 3 5 7 10





*/

void calc_points()
{
    int i;
    double phi;
    phi = (1+ sqrt(5))/2;

    points[0] = vec3<>(0.0, 1.0, phi);
    points[1] = vec3<>(0.0, -1.0, phi);
    points[2] = vec3<>(0.0, 1.0, -phi);
    points[3] = vec3<>(0.0, -1.0, -phi);

    points[4] = vec3<>(1.0, phi, 0);
    points[5] = vec3<>(-1.0, phi, 0);
    points[6] = vec3<>(1.0, -phi, 0);
    points[7] = vec3<>(-1.0, -phi, 0);

    points[8] = vec3<>(phi, 0.0, 1.0);
    points[9] = vec3<>(phi, 0.0, -1.0);
    points[10] = vec3<>(-phi, 0.0, 1.0);
    points[11] = vec3<>(-phi, 0.0, -1.0);

    for(i=0; i < 12; ++i)
    {
        points[i] *= GRADE;
    }
}

void clr_screen()
{
    SDL_Flip(screen);
    sge_Update_ON();
    sge_Update_OFF();
    sge_FilledRect(screen, 0,0, SCREEN_X,SCREEN_Y, 0,0,0);
}

void do_waitvbl()
{
    SDL_Delay(5);
    #if 0
        #if 0
        #ifndef WIN32
        usleep(20000);
        #else
        {
            volatile int loop;
            for(loop = 0; loop < 5000000; ++loop);
        }
        #endif
        #endif
    #endif
    SDL_PumpEvents();
}

#define DEG(i) (((double)i) * M_PI /180.0)

void paint_test_satelit(int x, int y, int z)
{
    CoordSystem coord;
    Object obj;

    coord.rotate_x_y_z((((double)x) /** M_PI / 180*/),
                       (((double)y) /** M_PI / 180*/),
                       (((double)z) /** M_PI / 180*/));
    //if (x < 0)
    //{
    //    coord.translate(vec3<>(1P-x*4,0.0,(double)(5*(x-70))));
    //} else
    //{
    //    coord.translate(vec3<>(1P-x*4,0.0,(double)(1800-(x+70)*5)));
    //}
    #define P 128
    PolygonLine *p;

    /* penta 1 */
    p = new PolygonLine();
    p->set_color(0x8f);
    p->add_point(points[0]);
    p->add_point(points[2]);
    p->add_point(points[10]);
    p->add_point(points[4]);
    p->add_point(points[11]);
    p->set_both_sided();
    obj.add_object(p);
    /* penta 2 */
    p = new PolygonLine();
    p->set_color(0x4f);
    p->add_point(points[9]);
    p->add_point(points[5]);
    p->add_point(points[8]);
    p->add_point(points[2]);
    p->add_point(points[0]);
    p->set_both_sided();
    obj.add_object(p);
    /* penta 3 */
    p = new PolygonLine();
    p->set_color(0x1f);
    p->add_point(points[6]);
    p->add_point(points[0]);
    p->add_point(points[7]);
    p->add_point(points[8]);
    p->add_point(points[10]);
    p->set_both_sided();
    obj.add_object(p);
    /* penta 4 */
    p = new PolygonLine();
    p->set_color(0x2f);
    p->add_point(points[11]);
    p->add_point(points[6]);
    p->add_point(points[10]);
    p->add_point(points[3]);
    p->add_point(points[1]);
    p->set_both_sided();
    obj.add_object(p);
    /* penta 5 */
    p = new PolygonLine();
    p->set_color(0x3f);
    p->add_point(points[1]);
    p->add_point(points[3]);
    p->add_point(points[8]);
    p->add_point(points[7]);
    p->add_point(points[9]);
    p->set_both_sided();
    obj.add_object(p);
    /* penta 6 */
    p = new PolygonLine();
    p->set_color(0x5f);
    p->add_point(points[10]);
    p->add_point(points[8]);
    p->add_point(points[5]);
    p->add_point(points[1]);
    p->add_point(points[4]);
    p->set_both_sided();
    obj.add_object(p);
    /* penta 7 */
    p = new PolygonLine();
    p->set_color(0x7f);
    p->add_point(points[11]);
    p->add_point(points[9]);
    p->add_point(points[7]);
    p->add_point(points[2]);
    p->add_point(points[6]);
    p->set_both_sided();
    obj.add_object(p);
    /* penta 8 */
    p = new PolygonLine();
    p->set_color(0x9f);
    p->add_point(points[4]);
    p->add_point(points[3]);
    p->add_point(points[5]);
    p->add_point(points[9]);
    p->add_point(points[11]);
    p->set_both_sided();
    obj.add_object(p);

    /* penta 9 */
    p = new PolygonLine();
    p->set_color(0xbf);
    p->add_point(points[0]);
    p->add_point(points[6]);
    p->add_point(points[4]);
    p->add_point(points[1]);
    p->add_point(points[9]);
    p->set_both_sided();
    obj.add_object(p);

    /* penta 10 */
    p = new PolygonLine();
    p->set_color(0xdf);
    p->add_point(points[11]);
    p->add_point(points[1]);
    p->add_point(points[5]);
    p->add_point(points[7]);
    p->add_point(points[0]);
    p->set_both_sided();
    obj.add_object(p);

    /* penta 11 */
    p = new PolygonLine();
    p->set_color(0x0ff);
    p->add_point(points[8]);
    p->add_point(points[3]);
    p->add_point(points[4]);
    p->add_point(points[6]);
    p->add_point(points[2]);
    p->set_both_sided();
    obj.add_object(p);

    /* penta 12 */
    p = new PolygonLine();
    p->set_color(0x0f);
    p->add_point(points[2]);
    p->add_point(points[7]);
    p->add_point(points[5]);
    p->add_point(points[3]);
    p->add_point(points[10]);
    p->set_both_sided();
    obj.add_object(p);

    reset_paint();
    obj.paint(coord);
    do_paint(coord);
    print_lines();
}

extern FILE *output_file;

int main(int argc, char** argv)
{
    SDL_Event event;
   
	/* Init SDL */
	SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO);

	/* Set window title */
	SDL_WM_SetCaption("3d", "3d");
    CoordSystem coord;
    Object obj;

    calc_points();
    check_point(0);


    output_file = fopen("3dico.bin", "wb");

	/* Initialize the display */
	screen = SDL_SetVideoMode(SCREEN_X, SCREEN_Y, 16, SDL_DOUBLEBUF|SDL_HWSURFACE /*|SDL_FULLSCREEN*/);
    int i;
    int j;
    j =0;
    for(i=2;i < 180+2; i+=4)
    {
        if (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                 case SDL_QUIT:
                        SDL_Quit();
                        exit(1);
                    break;
                default:
                    break;
            }
        }
        clr_screen();
        paint_test_satelit(i*2,i+20,0);
        do_waitvbl();
        ++j;
    }
    fclose(output_file);
    printf("%d\n", j);
    return 0;
}

