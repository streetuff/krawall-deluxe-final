/* A very simple SDL/SGE program */
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <SDL/SDL.h>
#include <SDL/sge.h>
#include "polygon.h"
#include <SDL/sge_surface.h>
#include <SDL/sge_textpp.h>
#include "frame.h"

SDL_Surface *screen;
sge_bmpFont *font;

extern void paint_test(int x, int y, int z);
extern void paint_test_satelit(int x, int y, int z);
extern void paint_test_asteroid(int x, int y, int z, int number);
extern void mars_ride(int index);
extern void paint_test_monilith(int x, int y, int z);

void clr_screen()
{
    SDL_Flip(screen);
    sge_Update_ON();
    sge_Update_OFF();
    sge_FilledRect(screen, 0,0, TOTAL_X-1,TOTAL_Y-1, 0,0,0);
}


void do_waitvbl()
{
    SDL_Delay(5);
    #if 0
        #if 0
        #ifndef WIN32
        usleep(20000);
        #else
        {
            volatile int loop;
            for(loop = 0; loop < 5000000; ++loop);
        }
        #endif
        #endif
    #endif
    SDL_PumpEvents();
}

extern void paint_menu(void);
extern void init_menu(void);
extern void handle_menu(int x, int y);
extern void handle_menu(SDLKey key);


Base *edit_object = new Film();

void paint(void)
{
    clr_screen();
    paint_menu();
    edit_object->cycle_color();
    edit_object->paint();
    SDL_Flip(screen);
}

void handle_button(int x, int y)
{
    if(x <= MENU_SIZE)
    {
        handle_menu(x,y);
        return;
    }
    else
    {
        edit_object->mouse_click(x, y);
    }
}

void cancel_button(void)
{
    edit_object->cancel();
}

int main(int argc, char** argv)
{
    SDL_Event event;
    int button;
    int x,y;
   
	/* Init SDL */
	SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO);

	/* Set window title */
	SDL_WM_SetCaption("editor ", "editor");


	/* Initialize the display */
	screen = SDL_SetVideoMode(TOTAL_X, TOTAL_Y, 16, SDL_DOUBLEBUF|SDL_HWSURFACE /*|SDL_FULLSCREEN*/);
    font = sge_BF_OpenFont((char *)"font.bmp", SGE_BFTRANSP|SGE_BFPALETTE);
    if( !font ){
        fprintf(stderr,"font error: %s\n", SDL_GetError());
        exit(1);
    }
    init_menu();
    paint();
    for(;;)
    {

        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_KEYDOWN:
                        handle_menu(event.key.keysym.sym);                        
                    break;
                case SDL_QUIT:
                        handle_menu(SDLK_q);
                    break;
                default:
                    break;
            }
        }
        button = SDL_GetMouseState(&x, &y);
        edit_object->set_mouse_coord(x,y);

        if(button & SDL_BUTTON(1))
        {
            handle_button(x,y);
            do
            {
                paint();
                do_waitvbl();
            } while (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(1));
        }
        if(button & SDL_BUTTON(3))
        {
            cancel_button();
            do
            {
                paint();
                do_waitvbl();
            } while (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(3));
        }
        paint();
        do_waitvbl();
    }
	return 0;
}

