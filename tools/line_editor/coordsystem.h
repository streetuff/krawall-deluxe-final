#ifndef __coordsystem_h
#define __coordsystem_h
#include "vec3.hpp"
#include "vec2.hpp"
#include "mat33.hpp"

using namespace math3d;

class CoordSystem {
public:
    CoordSystem();
    vec3<> transform_without_projection(vec3<> input) const;
    vec2<> transform(vec3<> input) const;
    double scale(double input) const;
    double calc_z(vec3<> input) const;
    vec3<> rot(vec3<> input) const;
    void rotate_x(double angle);
    void rotate_y(double angle);
    void rotate_z(double angle);
    void rotate_x_y_z(double angle_x, double angle_y, double angle_z);
    void translate(vec3<> addition);
private:
    mat33 rotation;
    vec3<> translation;
};


#endif

