#include <stdio.h>
#include <stdlib.h>
#include "object.h"
#include "coordsystem.h"
#include "polygon.h"

int number_of_primitives;
Base *primitives[1000];

void reset_paint()
{
    number_of_primitives  = 0;
}

void add_object_paint(Base *obj)
{
    if (number_of_primitives + 1 >= 1000)
    {
        fprintf(stderr, "too much primitives\n");
        exit(-1);
    }
    primitives[number_of_primitives] = obj;
    ++number_of_primitives;
}

void do_paint(CoordSystem &system)
{
    int i,j;
    Base *swapbase;
    for (i=0; i < number_of_primitives; ++i)
    {
        for (j=0; j < number_of_primitives; ++j)
        {
            if (primitives[i]->calc_z_pos(system) > primitives[j]->calc_z_pos(system))
            {
                swapbase = primitives[i];
                primitives[i] = primitives[j];
                primitives[j] = swapbase;
            }
        }
    }
    for (i=0; i < number_of_primitives; ++i)
    {
        primitives[i]->paint(system);
    }
}

double Line::calc_z_pos(const CoordSystem &system)
{
    return(system.calc_z(point1) + system.calc_z(point2)) / 2 ;
}

void Line:: paint(const CoordSystem &system)
{
    short x1;
    short y1;
    short x2;
    short y2;
    vec2<> turned;
    turned = system.transform(point1);
    x1 = (short)(turned[0]+.5);
    y1 = (short)(turned[1]+.5);
    turned = system.transform(point2);
    x2 = (short)(turned[0]+.5);
    y2 = (short)(turned[1]+.5);
    line(x1, y1, x2, y2, get_color(system));
}

double Circle::calc_z_pos(const CoordSystem &system)
{
    return system.calc_z(middle_point);
}

void Circle:: paint(const CoordSystem &system)
{
    short x;
    short y;
    short new_r;
    vec2<> turned;
    turned = system.transform(middle_point);
    x = (short)turned[0];
    y = (short)turned[1];
    new_r = (short)system.scale(radius);
    filled_circle(x, y, new_r, get_color(system), planet_index(), get_shadow());
}

int Planet::planet_color[5] = {0x27, 0x3a, 0,0,0};

void Polygon::add_point(const vec3<>& point)
{
    if (n+1 >= 100)
    {
        fprintf(stderr, "too much points");
        exit(-1);
    }
    points[n] = point;
    ++n;
}

int Polygon_Shaded::get_color(const CoordSystem &system)
{
    double value;
    vec3<> light = vec3<>(0,0,-1.9);
    vec3<> norm = cross_prod((points[1] - points[0]), (points[2] - points[0]));
    norm = system.rot(norm);
    norm.normalize();
    //std::cout << norm << std::endl;
    value = dot_prod(norm, light);
    int col = calc_dtv_color(color, value); 
    return col;
};

double Polygon:: calc_norm_z(const CoordSystem &system)
{
    vec2<> pt0, pt1, pt2;
    vec3<> norm1;
    if(both_sided)
    {
    	pt0 = system.transform(points[0]);
    	pt1 = system.transform(points[1]);
    	pt2 = system.transform(points[2]);
    }
    else
    {
    	pt0 = system.transform(points[0]);
    	pt1 = system.transform(points[3]);
    	pt2 = system.transform(points[1]);
    }
    pt2 -= pt0;
    pt1 -= pt0;
    norm1 = cross_prod(vec3<>(pt1[0], pt1[1], 0.0), vec3<>(pt2[0],pt2[1], 0.0));
    return (dot_prod(norm1, vec3<>(0.0,0.0, -1.0)));
}

void Polygon:: paint(const CoordSystem &system)
{
    short x[n];
    short y[n];
    int i;
    if ((calc_norm_z(system) >= 0))
    {
        for (i=0; i < n; ++i)
        {
            vec2<> turned;
            turned = system.transform(points[i]);
            x[i] = (short)turned[0];
            y[i] = (short)turned[1];
        }
        polygon(n, x, y, get_color(system), 0);
    }
}

void PolygonLine:: paint(const CoordSystem &system)
{
    short x[n];
    short y[n];
    int i;
    if ((calc_norm_z(system) >= 0))
    {
        for (i=0; i < n; ++i)
        {
            vec2<> turned;
            turned = system.transform(points[i]);
            x[i] = (short)turned[0];
            y[i] = (short)turned[1];
        }
        polygon(n, x, y, get_color(system), 1);
    }
}


double Polygon::calc_z_pos(const CoordSystem &system)
{
    int i;
    double result = 0.0;
    for (i=0; i < n; ++i)
    {
        result += system.calc_z(points[i]);
    }
    return result / (double)n;
}


Dreieck::Dreieck(const vec3<>& p1, const vec3<>& p2, const vec3<>& p3)
{
    add_point(p1);
    add_point(p2);
    add_point(p3);
}

Quadrat::Quadrat(const vec3<>& p1, const vec3<>& p2, const vec3<>& p3, const vec3<>& p4)
{
    add_point(p1);
    add_point(p2);
    add_point(p3);
    add_point(p4);
}

QuadratLine::QuadratLine(const vec3<>& p1, const vec3<>& p2, const vec3<>& p3, const vec3<>& p4)
{
    add_point(p1);
    add_point(p2);
    add_point(p3);
    add_point(p4);
}


Object::~Object()
{
    int i;
    for (i=0; i < n; ++i)
    {
        delete obj[i];
    }
}

void Object::add_object(Base *object)
{
    if (n+1 >= 100)
    {
        fprintf(stderr, "too much objects");
        exit(-1);
    }
    obj[n] = object;
    ++n;
}

void Object::paint(const CoordSystem &system)
{
    int i;
    for (i=0; i < n; ++i)
    {
        add_object_paint(obj[i]);
    }
}

