#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#define MULTICOLOR
#ifdef MULTICOLOR
#define PIX_PER_BYTE 4
#else
#define PIX_PER_BYTE 8
#endif

typedef struct
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char alpha;
} RGB;

RGB dtv_rgb[256] =
{
{ 0, 0, 0, 0} ,
{ 23, 23, 23, 0} ,
{ 42, 42, 42, 0} ,
{ 61, 61, 61, 0} ,
{ 79, 79, 79, 0} ,
{ 97, 97, 97, 0} ,
{ 114, 114, 114, 0} ,
{ 130, 130, 130, 0} ,
{ 147, 147, 147, 0} ,
{ 163, 163, 163, 0} ,
{ 179, 179, 179, 0} ,
{ 195, 195, 195, 0} ,
{ 210, 210, 210, 0} ,
{ 226, 226, 226, 0} ,
{ 241, 241, 241, 0} ,
{ 255, 255, 255, 0} ,
{ 32, 6, 0, 0} ,
{ 51, 28, 0, 0} ,
{ 69, 47, 0, 0} ,
{ 87, 66, 0, 0} ,
{ 105, 84, 0, 0} ,
{ 121, 101, 0, 0} ,
{ 138, 118, 21, 0} ,
{ 154, 134, 41, 0} ,
{ 170, 151, 60, 0} ,
{ 186, 167, 79, 0} ,
{ 202, 183, 96, 0} ,
{ 217, 199, 113, 0} ,
{ 232, 214, 129, 0} ,
{ 248, 229, 146, 0} ,
{ 255, 245, 162, 0} ,
{ 255, 255, 177, 0} ,
{ 51, 0, 0, 0} ,
{ 69, 12, 0, 0} ,
{ 87, 32, 0, 0} ,
{ 104, 52, 0, 0} ,
{ 121, 70, 8, 0} ,
{ 137, 88, 29, 0} ,
{ 154, 105, 48, 0} ,
{ 169, 121, 67, 0} ,
{ 186, 139, 85, 0} ,
{ 202, 155, 102, 0} ,
{ 217, 171, 119, 0} ,
{ 232, 187, 136, 0} ,
{ 247, 202, 152, 0} ,
{ 255, 218, 168, 0} ,
{ 255, 233, 184, 0} ,
{ 255, 248, 199, 0} ,
{ 62, 0, 0, 0} ,
{ 80, 0, 0, 0} ,
{ 97, 18, 5, 0} ,
{ 114, 39, 28, 0} ,
{ 131, 58, 47, 0} ,
{ 147, 75, 66, 0} ,
{ 163, 93, 84, 0} ,
{ 179, 110, 101, 0} ,
{ 195, 127, 118, 0} ,
{ 211, 144, 135, 0} ,
{ 226, 160, 151, 0} ,
{ 241, 176, 167, 0} ,
{ 255, 191, 183, 0} ,
{ 255, 207, 198, 0} ,
{ 255, 222, 214, 0} ,
{ 255, 237, 229, 0} ,
{ 65, 0, 11, 0} ,
{ 83, 0, 32, 0} ,
{ 100, 7, 51, 0} ,
{ 117, 29, 69, 0} ,
{ 134, 48, 87, 0} ,
{ 150, 66, 104, 0} ,
{ 166, 84, 121, 0} ,
{ 181, 101, 137, 0} ,
{ 198, 119, 154, 0} ,
{ 213, 136, 170, 0} ,
{ 228, 152, 186, 0} ,
{ 244, 168, 202, 0} ,
{ 255, 183, 217, 0} ,
{ 255, 199, 232, 0} ,
{ 255, 215, 248, 0} ,
{ 255, 229, 255, 0} ,
{ 59, 0, 54, 0} ,
{ 77, 0, 72, 0} ,
{ 94, 0, 89, 0} ,
{ 111, 24, 107, 0} ,
{ 128, 44, 124, 0} ,
{ 144, 62, 140, 0} ,
{ 160, 80, 156, 0} ,
{ 176, 97, 172, 0} ,
{ 192, 115, 188, 0} ,
{ 208, 131, 204, 0} ,
{ 223, 148, 219, 0} ,
{ 239, 164, 235, 0} ,
{ 253, 179, 249, 0} ,
{ 255, 195, 255, 0} ,
{ 255, 211, 255, 0} ,
{ 255, 226, 255, 0} ,
{ 45, 0, 85, 0} ,
{ 63, 0, 102, 0} ,
{ 80, 2, 119, 0} ,
{ 98, 25, 136, 0} ,
{ 115, 45, 152, 0} ,
{ 131, 63, 168, 0} ,
{ 148, 81, 184, 0} ,
{ 164, 98, 199, 0} ,
{ 180, 116, 215, 0} ,
{ 196, 132, 231, 0} ,
{ 211, 148, 246, 0} ,
{ 227, 165, 255, 0} ,
{ 242, 180, 255, 0} ,
{ 255, 196, 255, 0} ,
{ 255, 212, 255, 0} ,
{ 255, 227, 255, 0} ,
{ 23, 0, 104, 0} ,
{ 43, 0, 121, 0} ,
{ 61, 10, 137, 0} ,
{ 79, 32, 153, 0} ,
{ 97, 51, 169, 0} ,
{ 113, 69, 185, 0} ,
{ 130, 87, 201, 0} ,
{ 146, 104, 216, 0} ,
{ 163, 121, 232, 0} ,
{ 179, 138, 247, 0} ,
{ 194, 154, 255, 0} ,
{ 210, 170, 255, 0} ,
{ 225, 186, 255, 0} ,
{ 241, 201, 255, 0} ,
{ 255, 217, 255, 0} ,
{ 255, 232, 255, 0} ,
{ 0, 0, 108, 0} ,
{ 18, 0, 125, 0} ,
{ 38, 23, 141, 0} ,
{ 57, 43, 157, 0} ,
{ 75, 62, 174, 0} ,
{ 92, 79, 189, 0} ,
{ 109, 97, 205, 0} ,
{ 126, 114, 220, 0} ,
{ 143, 131, 236, 0} ,
{ 159, 147, 251, 0} ,
{ 175, 163, 255, 0} ,
{ 191, 179, 255, 0} ,
{ 206, 195, 255, 0} ,
{ 222, 210, 255, 0} ,
{ 237, 226, 255, 0} ,
{ 252, 241, 255, 0} ,
{ 0, 0, 98, 0} ,
{ 0, 18, 115, 0} ,
{ 13, 38, 131, 0} ,
{ 34, 57, 148, 0} ,
{ 53, 75, 164, 0} ,
{ 71, 92, 180, 0} ,
{ 89, 109, 195, 0} ,
{ 106, 126, 211, 0} ,
{ 123, 143, 227, 0} ,
{ 140, 159, 242, 0} ,
{ 156, 175, 255, 0} ,
{ 172, 191, 255, 0} ,
{ 187, 206, 255, 0} ,
{ 203, 222, 255, 0} ,
{ 219, 237, 255, 0} ,
{ 233, 252, 255, 0} ,
{ 0, 13, 74, 0} ,
{ 0, 33, 91, 0} ,
{ 0, 52, 108, 0} ,
{ 13, 71, 125, 0} ,
{ 34, 88, 142, 0} ,
{ 53, 105, 158, 0} ,
{ 72, 122, 174, 0} ,
{ 89, 139, 189, 0} ,
{ 107, 155, 205, 0} ,
{ 124, 171, 221, 0} ,
{ 140, 187, 236, 0} ,
{ 156, 203, 251, 0} ,
{ 172, 218, 255, 0} ,
{ 188, 233, 255, 0} ,
{ 204, 249, 255, 0} ,
{ 218, 255, 255, 0} ,
{ 0, 27, 38, 0} ,
{ 0, 47, 57, 0} ,
{ 0, 65, 75, 0} ,
{ 0, 83, 92, 0} ,
{ 22, 100, 110, 0} ,
{ 42, 117, 126, 0} ,
{ 61, 134, 143, 0} ,
{ 78, 150, 159, 0} ,
{ 96, 166, 175, 0} ,
{ 113, 182, 191, 0} ,
{ 130, 198, 206, 0} ,
{ 146, 213, 222, 0} ,
{ 162, 229, 237, 0} ,
{ 178, 244, 252, 0} ,
{ 194, 255, 255, 0} ,
{ 209, 255, 255, 0} ,
{ 0, 37, 0, 0} ,
{ 0, 56, 13, 0} ,
{ 0, 74, 34, 0} ,
{ 0, 92, 53, 0} ,
{ 19, 109, 71, 0} ,
{ 39, 125, 89, 0} ,
{ 58, 142, 106, 0} ,
{ 76, 158, 123, 0} ,
{ 94, 174, 140, 0} ,
{ 111, 190, 156, 0} ,
{ 127, 206, 172, 0} ,
{ 144, 221, 188, 0} ,
{ 160, 236, 203, 0} ,
{ 176, 252, 219, 0} ,
{ 192, 255, 234, 0} ,
{ 207, 255, 249, 0} ,
{ 0, 42, 0, 0} ,
{ 0, 61, 0, 0} ,
{ 0, 78, 0, 0} ,
{ 3, 96, 10, 0} ,
{ 26, 113, 31, 0} ,
{ 45, 129, 50, 0} ,
{ 64, 146, 69, 0} ,
{ 82, 162, 86, 0} ,
{ 100, 178, 104, 0} ,
{ 117, 194, 121, 0} ,
{ 133, 209, 137, 0} ,
{ 149, 225, 154, 0} ,
{ 165, 240, 170, 0} ,
{ 181, 255, 185, 0} ,
{ 197, 255, 201, 0} ,
{ 212, 255, 216, 0} ,
{ 0, 41, 0, 0} ,
{ 0, 60, 0, 0} ,
{ 0, 77, 0, 0} ,
{ 21, 95, 0, 0} ,
{ 41, 112, 0, 0} ,
{ 60, 129, 16, 0} ,
{ 78, 145, 36, 0} ,
{ 95, 161, 55, 0} ,
{ 113, 177, 74, 0} ,
{ 129, 193, 92, 0} ,
{ 146, 209, 108, 0} ,
{ 162, 224, 125, 0} ,
{ 178, 239, 141, 0} ,
{ 193, 254, 158, 0} ,
{ 209, 255, 174, 0} ,
{ 224, 255, 189, 0} ,
{ 0, 34, 0, 0} ,
{ 0, 53, 0, 0} ,
{ 23, 71, 0, 0} ,
{ 43, 89, 0, 0} ,
{ 62, 106, 0, 0} ,
{ 79, 123, 0, 0} ,
{ 97, 139, 14, 0} ,
{ 114, 155, 34, 0} ,
{ 131, 172, 54, 0} ,
{ 147, 188, 72, 0} ,
{ 163, 203, 90, 0} ,
{ 179, 219, 107, 0} ,
{ 195, 234, 124, 0} ,
{ 210, 249, 140, 0} ,
{ 226, 255, 157, 0} ,
{ 240, 255, 172, 0} };

#define ABS(x) ((x) < 0 ? -(x) : (x))

static unsigned long long calc_distance(RGB e1, RGB e2)
{
  long r,g,b;
  long rmean;

  rmean = ( (int)e1.r + (int)e2.r ) / 2;
  r = (int)e1.r - (int)e2.r;
  g = (int)e1.g - (int)e2.g;
  b = (int)e1.b - (int)e2.b;
  return (((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8);
}


int get_dtv_index(RGB rgb)
{
    int i;
    int ret;
    unsigned long long min = 0xffffffffffffffffULL;
    unsigned long long cur;
    ret = 0;
    for(i=0; i < 256; ++i)
    {
        cur = calc_distance(rgb, dtv_rgb[i]);
        if(cur < min)
        {
            min = cur;
            ret = i;
        }
    }
    return ret;
}

static SDL_Surface *display;

void open_display(int x, int y)
{

  display = SDL_SetVideoMode( x, y, 16, SDL_SWSURFACE );
  if ( display == NULL )
  {
    fprintf(stderr, "Konnte kein Fenster oeffnen: %s\n",
      SDL_GetError());
    exit(1);
  }
}


int get_dtv_pixel(const SDL_Surface *image, int x, int y)
{
    int pixel_size;
    int bytes_per_line;
    int index;
    const unsigned char *pixels;
    RGB rgbval;
    
    rgbval.alpha = 0;
    if((x < 0) || (x >= image->w))
    {
        fprintf(stderr, "wrong x ccoord %d\n", x);
        exit(-1);
    } 
    if((y < 0) || (y >= image->h))
    {
        fprintf(stderr, "wrong x ccoord %d\n", x);
        exit(-1);
    } 
    pixel_size = image->format->BytesPerPixel;
    bytes_per_line = image->pitch;
    pixels = (const unsigned char *)image->pixels;
   
    switch(pixel_size)
    {
        case 1:
            index = pixels[(bytes_per_line *y)+x];
            rgbval.r = image->format->palette->colors[index].r; 
            rgbval.g = image->format->palette->colors[index].g; 
            rgbval.b = image->format->palette->colors[index].b;
            break; 
        case 3:
            rgbval.r = pixels[((bytes_per_line *y)+x*3)];
            rgbval.g = pixels[((bytes_per_line *y)+x*3)+1];
            rgbval.b = pixels[((bytes_per_line *y)+x*3)+2];
        break;
        default:
            fprintf(stderr, "not supported size of pixel : %d\n", pixel_size);
            exit(-1);
        break;
    }
    return get_dtv_index(rgbval);
    
}

unsigned char *convert_image(const SDL_Surface *image)
{
    int i,j;
    unsigned char *data;
    unsigned char *p;
    data = (unsigned char *)malloc(image->w * image->h);
    p = data;
    for(j=0; j < image->h; ++j)
    {
        for(i=0; i < image->w; ++i)
        {
            *p = get_dtv_pixel(image, i, j);
            ++p;
        }
    }
    return data; 
}

void wait_for_exit(void)
{
    SDL_Event event;
    int quit = 0;
    while(quit == 0)
    {
        SDL_Delay(100);
        while( SDL_PollEvent( &event ) )
        {
            switch( event.type )
            {
                case SDL_KEYDOWN:
                    //printf( "Press: " );
                    //printf( " Name: %s\n", SDL_GetKeyName( event.key.keysym.sym) );
                    if(strcmp(SDL_GetKeyName( event.key.keysym.sym), "q") == 0)
                    {
                        quit = 1;
                    }
                    break;

                case SDL_KEYUP:
                    //printf( "Release: " );
                    //printf( " Name: %s\n", SDL_GetKeyName( event.key.keysym.sym) );
                    break;

                case SDL_QUIT:  // SDL_QUIT  int ein schliessen des windows
                    quit = 1;
                    break;

                default:
                break;
            }
        }
    }
}


void DrawPixel(SDL_Surface *screen, int x, int y,Uint8 R, Uint8 G,Uint8 B)
{
    Uint32 color = SDL_MapRGB(screen->format, R, G, B);

    if ( SDL_MUSTLOCK(screen) )
    {
        if ( SDL_LockSurface(screen) < 0 ) {
            return;
        }
    }

    switch (screen->format->BytesPerPixel) {
        case 1: { /* vermutlich 8 Bit */
            Uint8 *bufp;

            bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
            *bufp = color;
        }
        break;

        case 2: { /* vermutlich 15 Bit oder 16 Bit */
            Uint16 *bufp;

            bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
            *bufp = color;
        }
        break;

        case 3: { /* langsamer 24-Bit-Modus, selten verwendet */
            Uint8 *bufp;

            bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
            if(SDL_BYTEORDER == SDL_LIL_ENDIAN) {
                bufp[0] = color;
                bufp[1] = color >> 8;
                bufp[2] = color >> 16;
            } else {
                bufp[2] = color;
                bufp[1] = color >> 8;
                bufp[0] = color >> 16;
            }
        }
        break;

        case 4: { /* vermutlich 32 Bit */
            Uint32 *bufp;

            bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
            *bufp = color;
        }
        break;
    }

    if ( SDL_MUSTLOCK(screen) )
    {
        SDL_UnlockSurface(screen);
    }
}

void make_converted_image(const unsigned char *data, SDL_Surface *image)
{
    const unsigned char *p;
    p = data;
    int i,j;
    for(j=0; j < image->h; ++j)
    {
        for(i=0; i < image->w; ++i)
        {
            DrawPixel(image, i, j, dtv_rgb[(int)(*p)].r, dtv_rgb[(int)(*p)].g, dtv_rgb[(int)(*p)].b);
            ++p;
        }
    }
}

static unsigned char memory[0xc000];
static unsigned char dd00_tab[256];
static unsigned char d018_tab[256];
static unsigned char dd00d018_tab[256];

#define TEXT_WIDTH 40 // 80 for AGSP, 40 for normal


static const char *asm_text = 
"CALC_HIGHEST_ADDR = $%04x\n"
"CALC_CHARS_WIDTH = %d\n"
"; copy the charset data\n"
"	ldx #<CALC_HIGHEST_ADDR\n"
"	ldy #CALC_CHARS_WIDTH\n"
"load_addr:\n"
"	lda data_end-CALC_CHARS_WIDTH-1,y\n"
"store_addr:\n"
"	sta CALC_HIGHEST_ADDR & $ff00,x\n"
"	txa\n"
"	sbx #$08\n"
"	bcs +\n"
"do_dec_store:\n"
"	dec store_addr+2\n"
"	lda store_addr+2\n"
"	cmp #$3f\n"
"	beq copy_end\n"
"	cmp #$bf\n"
"	bne +\n"
"	lda #$7f\n"
"	sta store_addr+2\n"
"+	cpx #CALC_CHARS_WIDTH-8\n"
"	bne no_store_addr_check\n"
"	lda store_addr+2\n"
"	and #$07\n"
"   	bne +\n"
"	; wrap around in charset\n"
"	ldx #$%02x ; maximum reached char ((f9 * 8) & 0xff)\n"
"	bne do_dec_store ; unconditional\n"
"+	and #$03\n"
"	bne no_store_addr_check\n"
"	ldx #$f8\n"
"	bne do_dec_store ; unconditional\n"
"no_store_addr_check:\n"
"	tya\n"
"	bne +\n"
"	dec load_addr+2\n"
"+	dey\n"
"	jmp load_addr\n"
"copy_end:\n"
"\n"
";calculate screen content\n"
"	lda #<$4000\n"
"	sta ptr1\n"
"	lda #>$4000\n"
"	sta ptr1+1\n"
"	lda #2 ; number of $dd00 pages\n"
"	sta tmp2\n"
"	;lda #12\n"
"	;sta tmp3\n"
"\n"
"---	;lda tmp3\n"
"	;cmp #$06\n"
"	;bcs +\n"
"	;!byte $2c\n"
"+	lda #%d\n"
"	sta tmp\n"
"\n"
"	ldx #CALC_CHARS_WIDTH/8\n"
"--	ldy #$00\n"
"-	txa\n"
"	sta (ptr1),y\n"
"	inx\n"
"	cpx #$80\n"
"	bne +\n"
"	txa\n"
"	sbx #(-CALC_CHARS_WIDTH/8)\n"
"+	iny\n"
"	cpy #CALC_CHARS_WIDTH ; width\n"
"	bne -\n"
"\n"
"	lda ptr1+1\n"
"	sec\n"
"	sbc #-$04\n"
"	sta ptr1+1\n"
"	dec tmp\n"
"	bne --\n"
"   lda #$c0\n"
"   sta ptr1+1\n"
"   ;lax tmp3\n"
"   ;sbx #$08\n"
"   ;stx tmp3\n"
"   dec tmp2\n"
"   bne ---\n"
"   rts\n";

static unsigned char stack[8000];
static int sp;
static void stack_push(unsigned char data) {
    if(sp >= (int)sizeof(stack))
    {
        fprintf(stderr, "stack overflow\n");
        exit(-1);
    }
    stack[sp++] = data;
}

void print_stack(FILE *f)
{
    int sp_loc;
    for(sp_loc = 0; sp_loc < sp; sp_loc += 8)
    {
        fprintf(f, "   !byte $%02x, $%02x, $%02x, $%02x, $%02x, $%02x, $%02x, $%02x\n",
                stack[sp_loc], stack[sp_loc+1], stack[sp_loc+2], stack[sp_loc+3],
                stack[sp_loc+4], stack[sp_loc+5], stack[sp_loc+6], stack[sp_loc+7]);
    }
}

static unsigned int code_width;
static unsigned int code_number_screens;
static unsigned int code_highest;
static unsigned int code_highest_char_addr;

void print_code(FILE *f)
{
    fprintf(f, asm_text,
        code_highest,
        code_width,
        code_highest_char_addr,
        code_number_screens);
    print_stack(f);
    fprintf(f, "data_end:\n");
 }

void convert_to_special_format(unsigned char *data, int w, int h)
{
    int char_step = w/PIX_PER_BYTE;
    int i,j;
    int char_addr;
    int screen_char;
    int screen_addr;
    int char_val;
    int middle_offset;
    int ELF;
    int highest_screen_char = 0;
    int highest_char = 0;

    ELF  = (256 - (TEXT_WIDTH/8)*2) / (w / PIX_PER_BYTE);
    memset(memory, 0, sizeof(memory));
    char_val = 0;
    middle_offset = 0; /*(40-char_step) / 2; */
    for(j=0; j < h; ++j)
    {
        if(j < ELF*8)
        {
            char_addr = (j/ELF * 0x0800);
            screen_char = (TEXT_WIDTH/8) + ((j%ELF) * char_step);
            screen_addr = (j%ELF) * 0x0400;
        }
        else
        {
            char_addr = ((j-(ELF*8))/ELF * 0x0800) + 0x8000;
            screen_char = (TEXT_WIDTH/8) + ((j%ELF) * char_step);
            screen_addr = (j%ELF) * 0x0400 + 0x8000;
        }
        if(screen_char >= 0x80)
        {
            screen_char += TEXT_WIDTH/8;
        }
        if(char_addr < 0x8000)
        {
            dd00_tab[j] = 2;
            d018_tab[j] = (((char_addr) >> 11) * 2) | (((screen_addr) >> 10) * 16);
        }
        else
        {
            dd00_tab[j] = 0;
            d018_tab[j] = (((char_addr-0x8000) >> 11) * 2) | (((screen_addr - 0x8000) >> 10) * 16);
        }
        screen_addr += middle_offset;
        printf("char_addr 0x%04x screen_addr 0x%04x screen_char %d\n", char_addr + screen_char*8,
               screen_addr, screen_char);
        for(i=0; i < w; ++i)
        {
            if((i%PIX_PER_BYTE) ==0)
            {
                char_val = 0;
            }
            #ifdef MULTICOLOR
            char_val *=4;
            //printf("%d ", (data[j*w+i]));
            switch((data[j*w+i]) & 0xf) {
                case 0:
                case 1:
                case 2:
                case 3:
                    char_val |= 3;
                    break;
                case 4:
                case 5:
                case 6:
                    char_val |= 1;
                    break;
                case 7:
                case 8:
                    char_val |= 2;
                    break;
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    char_val |= 0;
                    break;
                default:
                    printf("ins knie %d (%d)\n", i, data[j*w+i]);
            }
            #else
            char_val *=2;
            switch(data[j*w+i]) {
                case 0:
                    char_val |= 0;
                    break;
                default:
                    char_val |= 1;
                    break;
            }
            #endif
            if((i%PIX_PER_BYTE) == PIX_PER_BYTE-1)
            {
                memory[screen_addr] = screen_char;
                memory[char_addr + screen_char*8] = char_val;
                if((char_addr + screen_char*8) > highest_screen_char)
                    highest_screen_char = char_addr + screen_char*8;
                if((screen_char) > highest_char)
                    highest_char = screen_char;
                stack_push(char_val);
                ++screen_addr;
                ++screen_char;
                if(screen_char == 0x80)
                {
                        screen_char += TEXT_WIDTH/8;
                }
            }
        }
        //printf("\n");
    }
    code_width = w/PIX_PER_BYTE;
    code_number_screens = ELF;
    code_highest = highest_screen_char + 0x4000;
    code_highest_char_addr = (highest_char * 8) & 0xf8; 
}


void show_image(const char *name, FILE *f)
{
    SDL_Surface *image;
    SDL_Surface *converted_image;
    SDL_Rect srect, drect;
    int i;
    
    unsigned char *data;

    sp = 0;

    image = IMG_Load(name);
    converted_image = IMG_Load(name);

    if (image == NULL)
    {
        fprintf(stderr, "Das Bild konnte nicht geladen werden:%s\n",
        SDL_GetError());
        exit(-1);
    }

    printf("Size of Image : %d x %d\n", image->w, image->h); 
    open_display(image->w*2, image->h);
    // kopiere das Bild-Surface auf das display-surface
    SDL_BlitSurface(image, NULL, display, NULL);

    // den veraenderten Bereich des display-surface auffrischen
    data = convert_image(image);
    convert_to_special_format(data, image->w, image->h);
    print_code(f);

    //fwrite(memory, sizeof(memory), 1, f);
    fclose(f);
    f = fopen("d018tab", "wb");
    fwrite(d018_tab, image->h, 1, f);
    fclose(f);
    f = fopen("dd00tab", "wb");
    fwrite(dd00_tab, image->h, 1, f);
    fclose(f);
    for(i=0; i < image->h; ++i)
    {
         dd00d018_tab[i] = d018_tab[i] | ((dd00_tab[i]) >> 1);
    }
    f = fopen("dd00d018_tab", "wb");
    fwrite(dd00d018_tab, image->h, 1, f);
    fclose(f);
    make_converted_image(data, converted_image); 
  
    srect.x = 0;
    srect.y = 0;
    srect.w = image->w;  // das gesamte Bild
    srect.h = image->h;   // das gesamte Bild

    // Setzen des Zielbereichs
    drect.x = image->w;
    drect.y = 0;
    drect.w = image->w;
    drect.h = image->h;


    SDL_BlitSurface(converted_image, &srect, display, &drect);
    SDL_Flip(display);
    wait_for_exit();
    free(data);
    
    SDL_FreeSurface(image);
}


int main(int argc, char **argv)
{
    FILE *f;
    atexit(SDL_Quit);
    if(argc < 3)
    {
        printf("usage : %s <in_file> <out_file>\n", argv[0]);
        exit(-1);
    }
    f = fopen(argv[2], "wb");
    if (!f)
    {
        fprintf(stderr, "cannot open \"%s\"\n", argv[1]);
        exit(-1);
    }
    if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
       fprintf(stderr, "SDL konnte nicht initialisiert werden:  %s\n",
         SDL_GetError());
       exit(1);
    }
    show_image(argv[1], f);
    return 0;
}
