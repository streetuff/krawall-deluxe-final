#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define	width 896/2/2
//#define	height 2512/2
#define width (320)
#define height (1064)

unsigned char picture[width*height];
unsigned char chars[width/4*height/8];
unsigned char charset[height/8];
unsigned char characters[256][width*height/4/8][32];
int number_of_char[256];
int number_of_charset;
unsigned char packed[256][width/3];
unsigned char packed_len[256];

#define START_ADRESS 0x4800

int get_charset_addr(int i)
{
    int start_adress;
    start_adress = START_ADRESS;
    while (i > 0)
    {
        start_adress = (start_adress & 0xf800) + 0x800;
        if ((start_adress & 0x3800) == 0)
        {
            start_adress+=0x800;
        }
        if ((start_adress != 0x9000) &&
            (start_adress != 0x9800) &&
            (start_adress != 0xa000) &&
            (start_adress != 0xa800))
        {
            --i;
        }
    }
    return start_adress;
}


int IteratePixel( float x, float y, int maxiter )
{
	int iter = 0;

	float i=0.0F;
	float r=0.0F;

	float i2,r2;

	for(;;)
	{
		i2 = i * i - r * r + x;
		r2 = 2 * i * r + y;
		i = i2;
		r = r2;
		iter++;

		if ( ( i*i + r*r ) > 4.0f )
		{
			break;
		}

		if ( iter >= maxiter )
		{
			break;
		}
	}
	return iter;
}

int find_ascending_chars(const unsigned char *src, int size, unsigned char c)
{
    int len;

    len = 0;
    if(!size)
        return 0;
    if(size > 0x80)
        size = 0x80;
    while(size > 0)
    {
        --size;
        if(*src++ != c)
            break;
        ++len;
        ++c;
    }
    return len;
}

int find_equal_chars(const unsigned char *src, int size)
{
    int len;
    unsigned char cur;
    if(!size)
        return 0;

    len = 0;
    cur = *src++;

    if(size > 0x7f)
        size = 0x7f;
    while(size > 0)
    {
        ++len;
        --size;
        if(*src++ != cur)
            break;
    }
    return len;
}

void pack_charline(int line, const unsigned char *src, int size, unsigned char first)
{
    int len;
    unsigned char *dest;

    dest = packed[line];
    *dest++ = first;
    while (size > 0)
    {
        len = find_ascending_chars(src, size, first);
        if (len >= 1)
        {
            *dest++ = len;
            src += len;
            size -= len;
            first += len;
            continue;
        }
        len = find_equal_chars(src, size);
        //if (len > 2)
        {
            // *dest++ = 0xff;
            *dest++ = len + 0x7f;
            *dest++ = *src;

            src += len;
            size -= len;
            continue;
        }
        // *dest++ = *src++; 
        --size;
    }
    packed_len[line] = dest-packed[line];
    printf("%d = %d\n", line, packed_len[line]);
}

void rip(void)
{
    int i,j,k,l;
    int old_number_of_char;
    number_of_charset = 0;
    number_of_char[number_of_charset] = 0;
    unsigned char cur[32];

    int packed_total;
    int start_char;

    packed_total = 0;
    start_char = 0;
    for(j=0; j < height; j+=8)
    {
        charset[j/8] = number_of_charset;
        old_number_of_char =  number_of_char[number_of_charset];
        if (old_number_of_char == 0)
        {
        	/* first run on this, try if we must take only the halve */
                if(get_charset_addr(number_of_charset) & 0x400)
                {
                	old_number_of_char = 128;
                        number_of_char[number_of_charset] = old_number_of_char;
                        start_char = 128;     
                }
                else
                {
                	start_char = 0;
                }
        }
        for(i=0; i < width; i+=4)
        {
            for(k=0; k < 8; ++k)
            {
                for(l=0; l < 4; ++l)
                {
                    cur[l+4*k] = picture[width*(k+j) + (i+l)];
                }
            }
            for(k=start_char; k < number_of_char[number_of_charset]; ++k)
            {
                for(l=0; l < 32; ++l)
                {
                    if(cur[l] != characters[number_of_charset][k][l])
                        break;
                }
                if(l == 32)
                    break;
            }
            if(k == number_of_char[number_of_charset])
            {
                memcpy(characters[number_of_charset][k], cur, 32);
                ++number_of_char[number_of_charset];
            }
            chars[(j/8)*(width/4)+(i/4)]  = k;
        }
        if(number_of_char[number_of_charset] > (256))
        {
            number_of_char[number_of_charset] = old_number_of_char;
            /* retry the line with new charset */
            ++number_of_charset;
            number_of_char[number_of_charset] = 0;
            j-=8;
        }
        else
        {
            pack_charline(j/8, &chars[(j/8)*(width/4)],
                         width/4, old_number_of_char);
            packed_total += packed_len[j/8];
        }
    }
    printf("total packed : %d\n", packed_total); 
}

void convert_char(unsigned char *dest, unsigned char *src)
{
    int i;
    for(i=0; i < 8; ++i)
    {
        dest[i] = src[i*4] << 6
                | src[i*4+1] << 4
                | src[i*4+2] << 2
                | src[i*4+3];
    }
}

void writecharset(int x)
{
    FILE *f;
    char charset[2048];
    char name[256];
    int i;
    memset(charset, 0, sizeof(charset));
    for(i=0; i < number_of_char[x]; ++i)
    {
        convert_char(charset + i*8, characters[x][i]);
    }
    sprintf(name, "charset%02x", x);
    f = fopen(name, "wb");
    if (get_charset_addr(x) & 0x400)
    {
    	fwrite(charset + 0x400, 1, (number_of_char[x]-128)*8, f);
    }
    else
    {
    	fwrite(charset, 1, number_of_char[x]*8, f);
    }
    fclose(f);

}

void write_include(void)
{
    FILE *f;
    int x;
    int i;
    char name[256];

    f = fopen("include.asm", "wb");
    fprintf(f, "height = %d\n", height);
    fprintf(f, "width = %d\n", width);
    fprintf(f, "number_of_charset = %d\n", number_of_charset+1);
    fprintf(f, "charset_line:\n");
    fprintf(f, "first_videoram = $%04x\n", START_ADRESS-0x400);
    for (x = 0; x < height/8; ++x)
    {
        fprintf(f, "!byte >$%04x\n", get_charset_addr(charset[x])  & 0xf800);
    }
    for (x = 0; x <= number_of_charset; ++x)
    {
        sprintf(name, "charset%02x", x);
        fprintf(f, "* = $%04x\n\n", get_charset_addr(x));
        fprintf(f, "!bin \"%s\"\n", name);
    }
    fclose(f);

    f = fopen("map.asm", "wb");
    for (x = 0; x < height/8; ++x)
    {
        unsigned char *ptr;
        fprintf(f, "mapline_%02x:\n!byte ", x);
        ptr = packed[x];
        for(i=0; i < packed_len[x]; ++i)
        {
            fprintf(f, "$%02x", ptr[i]);
            if (i != (packed_len[x]-1))
            {
                fprintf(f,",");
            }
            else
            {
                fprintf(f,"\n");
            }
        }
    }
    fclose(f);
    f = fopen("maptab.asm", "wb");
    fprintf(f, "maptablow:\n");
    for (x = 0; x < height/8; ++x)
    {
        fprintf(f, "!byte <mapline_%02x\n", x);
    }
    fprintf(f, "maptabhigh:\n");
    for (x = 0; x < height/8; ++x)
    {
        fprintf(f, "!byte >mapline_%02x\n", x);
    }
    fclose(f);
}

int main(void)
{	
    FILE *f;
	memset( picture, 0, sizeof( picture ) );


	float minx=-0.98f;        //-0.78f;
	float maxx= 0.52f; //0.52f;	//1.30
	float miny=-0.91f;
	float maxy= 0.91f;	//1.82

	int x,y;
	float fx,fy,fxstep,fystep;

	fxstep=(maxx-minx)/(float)(width);
	fystep=(maxy-miny)/(float)(height);

	int maxiter=64;

	fy=miny;
        f = fopen("bigpic2.raw", "rb");
        for(x = 0; x < width*height; ++x)
        {
        	char c;
        	//fread(&c, 1, 1, f);
        	fread(&c, 1, 1, f);
                c = c & 3;
                switch(c)
                {
                	case 0: c = 3; break;
                        case 1: c = 2; break;
                        case 2: c = 1; break;
                        case 3: c = 0; break;
                }
        	picture[x] = c & 3;
        }
        fclose(f);
        /* only for test */
        //memcpy(picture+912*width, picture, 152*width);

    rip();
    //f = fopen("data", "wb");
    //fwrite(picture, 1, sizeof( picture ), f);
    //fclose(f);
    for(x=0; x <= number_of_charset; ++x)
    {
        printf("%d %04x\n", number_of_char[x], get_charset_addr(x));
    }
    f = fopen("map", "wb");
    fwrite(chars, 1, sizeof( chars ), f);
    fclose(f);
    int i,j;
    f = fopen("map_unpacked.asm", "w");
    unsigned char *cp = chars;
    for(i=0; i < height/8; ++i)
    {
    	fprintf(f, "mapline_%02x: !byte $%02x", i, *cp++);
        for (j=1; j < width/4; ++j)
        {
        	fprintf(f, ",$%02x", *cp++);
        }
   	fprintf(f,"\n");
    }
    
    fclose(f);
    for(x = 0; x <= number_of_charset; ++x)
    {
        writecharset(x);
    }
    write_include();
    return 0;
}

