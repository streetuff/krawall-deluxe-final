
;.fileopt comment, "Loader install code portion"
;.fileopt compiler, "CA65"
;.fileopt author, "Gunnar Ruthenberg"

__NO_LOADER_SYMBOLS_IMPORT = 1
!src "loader.inc"
!src "version.inc"

!src "cpu.inc"
!src "cia.inc"
!src "kernal.inc"
!if NONBLOCKING_API {
    !src "vic.inc"
}
!if LOAD_ONCE | INSTALL_FROM_DISK {
    VIA2_T1L_H = $1c07; including via.inc would redefine several symbols from cia.inc
}

!src "hal.inc"

;.importzp BLOCKDESTLO

;.importzp MAXTRACK41
;.importzp MAXTRACK71

;.import c1570fix0
;.import c1570fix1
;.import c1570fix2
;.import c1570fix3
;.import c1570fix4


;!macro itoa4 .value {
;            !if (value & $0f > 9) {
;                !byte (value & $0f) + 'a' - 10
;            } else {
;                !byte (value & $0f) + '0'
;            }
;}

;!macro itoa1 value {
;            itoa4 value != 0
;}

;.macro itoa8 value
;            itoa4 value >> 4
;            itoa4 value & $0f
;.endmacro

;.macro itoa16 value
;            itoa8 value >> 8
;            itoa8 value & $ff
;.endmacro



;.segment "EXTZP"; not used otherwise, the EXTZP segment is not
                ; optional in the o65 built-in ld65 config

!ifdef DYNLINK {
    ;.segment "JUMPTABLE"
    ;.segment "DATA"

    ;.macro DYNLINKEXPORT function, label
    ;    .byte function
    ;    .word -1; don't import now, only regard the function as available
    ;.endmacro

    !warn "Exported dynamic link symbols:"
    ;.word endinstjmptable - *; table size, i.e., offset to the code
    ;.word version - endinstjmptable; offset to version string

    ;!src "install-jumptable.inc"
    ;!src "loader-jumptable.inc"
;endinstjmptable:
 } else {
    ;.segment "DISKIO_INSTALL"

    !ifdef INSTADDR {
            * = INSTADDR - 2
            !word * + 2; load address
    }

    !src "install-jumptable.inc"
}

GENERIC_INSTALL = 0; INSTALL_FROM_DISK; XXX TODO

!if GENERIC_INSTALL = 0 {
            ; unfortunately, scopes must be defined before using them, this is why the install code is moved to after the drive code

drivecode41:
!zone cbm1541 {
            !src "drivecode1541.s"
    !if UNINSTALL_RUNS_DINSTALL {
            ;.export drivecode41 : absolute
    }
}

!if DISABLE_1571=0 {
drivecode71:
!zone cbm1571 {
            !src "drivecode1571.s"
	!if UNINSTALL_RUNS_DINSTALL {
            ;.export drivecode71 : absolute
	}
}
}

!if DISABLE_1581=0 {
drivecode81:
!zone cbm1581 {
            !src "drivecode1581.s"
    !if UNINSTALL_RUNS_DINSTALL {
            ;.export drivecode81 : absolute
    }
}
}
}; !GENERIC_INSTALL



            ; Install the loader

            ; in:  x/y - if LOAD_ONCE, lo/hi to 0-terminated filename string
            ; out: c - set on error
            ;      a - status
            ;      x - drive type (one of drivetype)
            ;      y - if status is OK, zp address of version string address
!if NONBLOCKING_API {
_install:
} else {
    !ifdef install {
install2:
    } else {
install:
    }
}

!if LOAD_ONCE | INSTALL_FROM_DISK {
            stx namestrpos+$00
            sty namestrpos+$01
}
            lda #<(version)
            sta BLOCKDESTLO + 0
            lda #>(version)
            sta BLOCKDESTLO + 1

            +BRANCH_IF_NOT_IDLE +
            jmp isinstalld

+           php; i-flag buffer

!if PLATFORM = COMMODORE_PLUS4 {
            ; PUCrunch trashes $f9, restore it
            lda #$00
            sta USE4DY
}
            ; try the drive as denoted by FA (current drive) first
            lda FA
            cmp #MIN_DEVICE_NO
            bcc +
            cmp #MAX_DEVICE_NO + 1
            bcc ++
+           lda #MIN_DEVICE_NO; FA does not contain a drive address (MIN_DEVICE_NO..MAX_DEVICE_NO), try MIN_DEVICE_NO first
++           pha

!if PROTOCOL = TWO_BITS_ATN {
            ; check if there is more than 1 drive on the bus,
            ; to make sure the 2bit+ATN protocol works alright;
            ; this is done via the low-level serial bus routines,
            ; so non-serial bus devices won't respond
            ldx #MIN_DEVICE_NO
checkbus:   stx FA
            lda #$00
            sta STATUS
            jsr drvlistn
            jsr READST
            bmi ++
            pla
            eor #%10000000
            bmi +

            ; more than 1 drive on the bus or generic serial devices present
            sta FA
            jsr UNLSTN

    !if LOAD_ONCE & LOAD_VIA_KERNAL_FALLBACK {
            jsr openfile3; using load via KERNAL fallback, the loader can still load
    }
            plp; i-flag restore
            lda #TOO_MANY_DEVICES
            ldx #DRIVE_GENERIC
            ldy #BLOCKDESTLO
    !if LOAD_VIA_KERNAL_FALLBACK {
            clc; this is not to be regarded as an error
    } else {
            sec
    }
            rts

+           pha
++          jsr UNLSTN
            ldx FA
            inx
            cpx #MAX_DEVICE_NO+1
            bne checkbus
}

            lda #OK
            sta STATUS
            pla
!if PROTOCOL = TWO_BITS_ATN {
            and #%01111111
}
            ; find first available drive
            sta FA
--          pha; device number
            jsr drvlistn
            jsr READST
            pha
            jsr UNLSTN
            pla
            bpl ++; drive present
            ; drive not present, try next address
            ldx FA
            inx
            cpx #MAX_DEVICE_NO + 1
            bne +
            ldx #MIN_DEVICE_NO
+           stx FA
            pla
            cmp FA
            bne --

            plp; i-flag restore
            lda #DEVICE_NOT_PRESENT
            ldx #DEVICE_NONE
            ldy #BLOCKDESTLO
            sec
            rts

++          pla; device number

!if GENERIC_INSTALL {

            ; XXX TODO

} else {; !GENERIC_INSTALL
            ; upload specific drive code depending on the drive model,
            ; so check which model the drive is

            ; check if running on a 1541/70/71 compatible drive
            lda #<$e5c6
            ldx #>$e5c6
            jsr checktype
            cmp #'4'
            beq is1541
!if DISABLE_1571=0 {
            cmp #'7'
            beq is157x
} else {
            cmp #'7'
            beq is1541
}

!if DISABLE_1581=0 {
            ; neither 1541, nor 1570, nor 1571
            ; try 1581
            lda #<$a6e9
            ldx #>$a6e9
            jsr checktype
            ldy #DRIVE_1581
            cmp #'8'
            beq is1581
}
            ; no compatible drive found
notcompat:  plp; i-flag restore
            lda #DEVICE_INCOMPATIBLE
            ldx #DRIVE_GENERIC
            ldy #BLOCKDESTLO
    !if LOAD_VIA_KERNAL_FALLBACK {
            clc; this is not to be regarded as an error
    } else {
            sec
    }
            rts

            ; select appropriate drive code
is1541:     ; find out if 1541 or 1541-C, or 1541-II
            lda #<$c002
            ldx #>$c002
            jsr checktype
            ldy #DRIVE_1541
            cmp #'C'
            bne selectdcod
            ; find out if 1541-C or 1541-II
            lda #<$eaa3
            ldx #>$eaa3
            jsr checktype
            ldy #DRIVE_1541_C
            cmp #$ff
            bne selectdcod
            iny; DRIVE_1541_II
            bne selectdcod

!if DISABLE_1571=0 {
            ; find out if 1570 or 1571
is157x: 
	    cpx #'1' | $80
            lda #MAXTRACK41 + 1
            ldx #OPC_BIT_ABS
            ldy #DRIVE_1570
            bcc +
            lda #MAXTRACK71 + 1
            ldx #OPC_STA_ABS
            iny; DRIVE_1571
+           sta c1570fix0 - drvcodebeg71 + drivecode71
            sta c1570fix1 - drvcodebeg71 + drivecode71
            stx c1570fix2 - drvcodebeg71 + drivecode71
            stx c1570fix3 - drvcodebeg71 + drivecode71
            sta c1570fix4 - drvcodebeg71 + drivecode71
            ; fall through
}

!if DISABLE_1581=0 {
is1581:
}

selectdcod: sty drivetype + $01
            ldx dcodeseltb - DRIVE_1541,y
            lda dcodeselt0,x
            sta dcodesel0
            lda dcodeselt1,x
            sta dcodesel1
            lda dcodeselt2,x
            sta dcodesel2
            lda dcodeselt3,x
            sta dcodesel3
            lda dcodeselt4,x
            sta dcodesel4
            lda dcodeselt5,x
            sta dcodesel5
            lda dcodeselt6,x
            sta dcodesel6
            lda dcodeselt7,x
            sta dcodesel7
            lda dcodeselt8,x
            sta dcodesel8
}; !GENERIC_INSTALL

!if LOAD_ONCE | INSTALL_FROM_DISK {
    !if GENERIC_INSTALL = 0 {
!if DISABLE_1581=0 {
            cpy #DRIVE_1581
            bcs ++
}
            ; quicker head stepping on 1541-71
            jsr drvlistn
            ldx #$06
-           lda drvfaststp,x
            jsr IECOUT
            dex
            bpl -
            jsr UNLSTN
++
    }
            jsr openfile3; exception on error
}
            jsr drvlistn

            ldx #$00
install1:   ldy #$05
-           lda drvrutmw,y
            jsr IECOUT
            dey
            bpl -

            ldy #$23
loadcopy:
dcodesel0 = *+$01
dcodesel1 = *+$02
            lda $0000,x
            jsr IECOUT
            inx
dcodesel2 = *+$01
            cpx #$00
            beq +
            dey
            bne loadcopy
            jsr drvlistn - $03
            clc
            lda #$23
            adc drvrutmw + $02
            sta drvrutmw + $02
            bcc install1
            inc drvrutmw + $01
            bne install1

+           jsr drvlistn - $03
            ldx #$05
-           lda droutrun - $01,x
            jsr IECOUT
            dex
            bne -
            jsr UNLSTN

            +CLEAR_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT
-           +BRANCH_IF_CLK_IN_SET -
            +SET_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT
-           +BRANCH_IF_CLK_IN_CLEAR -

dcodesel3 = * + $01
            ldy #$00
dcodesel4 = * + $02
fastinst:   lda $0000,y
            +SENDBYTE
            iny
            bne +
            inc fastinst + $02
+           cpy dcodesel0
            bne fastinst
            ldx fastinst + $02
            cpx dcodesel1
            bne fastinst

-           +BRANCH_IF_CLK_IN_SET -
            +INSTALL_IDLE

            plp; i-flag restore
isinstalld: lda #OK
drivetype:  ldx #$00
            ldy #BLOCKDESTLO
            clc
            rts

!if NONBLOCKING_API {
            ; Install the loader

            ; in:  a - nmi line mod 8
            ;      x/y - if LOAD_ONCE, lo/hi to 0-terminated filename string
            ; out: c - set on error
            ;      a - status
            ;      x - drive type (one of drivetype)
            ;      y - if status is OK, zp address of version string address

    !ifdef install {
install2:
    } else {
install:
    }

            sec
            sbc #$01
            and #%00000111
            sta loadstatus
            jsr _install

    !if LOAD_VIA_KERNAL_FALLBACK {
            sta initstatus
            bcc +
            cmp #DEVICE_INCOMPATIBLE
            beq +
        !if PROTOCOL = TWO_BITS_ATN {
            cmp #TOO_MANY_DEVICES
            sec
            bne piniterr
        }
+
    } else {
            bcs piniterr
    }
            txa
            pha

            +DISABLE_TIMER_NMI
            +ACK_TIMER_NMI
            php
            ldx #$00
--          lda VIC2_RASTERLINE
-           cmp VIC2_RASTERLINE
            bne -
            cmp loadstatus
            beq +
            dex
            bne --; this is done so the irq is blocked as late and as short as possible

+           sei
            lda loadstatus
-           cmp VIC2_RASTERLINE
            beq -
-           cmp VIC2_RASTERLINE; the timer is started at the approximate beginning of a raster line
            bne -
            +SETUP_TIMER
            plp
            pla
            tax
    !if LOAD_VIA_KERNAL_FALLBACK {
            lda #OK
            sta loadstatus
initstatus = * + $01
            lda #OK
            cmp #OK + 1
            rts
    } else {
            clc
            lda #OK
    }
piniterr:   sta loadstatus
            rts

}; NONBLOCKING_API

            jsr UNLSTN
drvlistn:   lda FA
            jsr LISTEN
            lda #SA_OPENCHANNEL | COMMAND_ERROR_CHANNEL
            jmp LSTNSA

checktype:  sta drvchkmr+$03
            stx drvchkmr+$04
            lda #drvrutmw-drvchkmr
            ldx #<drvchkmr
            ldy #>drvchkmr
            jsr SETNAM
            lda #COMMAND_ERROR_CHANNEL
            ldx FA
            tay
            jsr SETLFS
            jsr OPEN_KERNAL
            bcc +
kernalerr2: pla
            pla
            plp; i-flag restore
            lda #GENERIC_KERNAL_ERROR
            ldx #DEVICE_UNKNOWN
            ldy #BLOCKDESTLO
            sec
            rts
+           ldx #COMMAND_ERROR_CHANNEL
            jsr CHKIN
            jsr CHRIN
            pha
            jsr CHRIN
            pha
            lda #COMMAND_ERROR_CHANNEL
            jsr CLOSE
            jsr CLRCHN
            pla
            tax
            pla
            clc
            rts

!if LOAD_ONCE | INSTALL_FROM_DISK {

openfile3:
!if DISABLE_1571=0 {
	    cpy #DRIVE_1571
            bne no1571

            jsr drvlistn
            ldx #$05
-           lda twosided - $01,x
            jsr IECOUT
            dex
            bne -
            jsr UNLSTN
}
no1571:     lda #KERNALFILENO
            ldx FA
            ldy #$00
            jsr SETLFS
            ldx #$ff
-           inx
namestrpos = *+$01
            lda $0000,x
            bne -
            txa
            ldx namestrpos+$00
            ldy namestrpos+$01

            jsr SETNAM
            jsr OPEN_KERNAL
            bcs jpkernale

            ldx #KERNALFILENO
            jsr CHKIN

            lda #COMMAND_ERROR_CHANNEL
            ldx FA
            tay
            jsr SETLFS
            lda #$00
            jsr SETNAM
            jsr OPEN_KERNAL
            bcc +
            lda #KERNALFILENO
            jsr CLOSE
jpkernale:  jmp kernalerr2
+
            ; read error status through the error channel
            ; so that the led won't keep flashing
            ldx #COMMAND_ERROR_CHANNEL
            jsr CHKIN
            jsr CHRIN
            cmp #'0'
            bne fileerror
            jsr CHRIN
            cmp #'0'
            beq +
fileerror:  jsr CHRIN
            cmp #$0d; newline
            bne fileerror

            pla
            pla
            lda #COMMAND_ERROR_CHANNEL
            jsr CLOSE
            lda #KERNALFILENO
            jsr CLOSE
            jsr CLRCHN
            lda #INVALID_PARAMETERS; FILE_NOT_FOUND
            ldx drivetype + $01
            ldy #BLOCKDESTLO
            plp; i-flag restore
            sec
            rts
+           lda #COMMAND_ERROR_CHANNEL
            jsr CLOSE
            jmp CLRCHN

drvfaststp: !byte MINSTPSP,$01,>VIA2_T1L_H,<VIA2_T1L_H
		!text "W-M"

}; LOAD_ONCE | INSTALL_FROM_DISK

drvchkmr:   !text "M-R"
		!byte $00,$00,$02
dcodesel5 = *+$01
dcodesel6 = *+$02
drvrutmw:   !byte $23,$00,$00
		!text "W-M"
dcodesel7 = *+$00
dcodesel8 = *+$01
droutrun:   !byte $00,$00
	!text"E-M"
twosided:   !text "1M>0U"

!if GENERIC_INSTALL = 0 {

dcodeseltb: !byte DRIVES_1541, DRIVES_1541, DRIVES_1541; drivecode1541 for 1541, 1541-C, 1541-II
!if DISABLE_1571=0 {
            !byte DRIVES_157X, DRIVES_157X                                ; drivecode1571 for 1570, 1571
}
!if DISABLE_1581=0 {
            !byte DRIVES_1581_CMD                                                            ; drivecode1581 for 1581
}

dcodeselt0: !byte <(dinstall41  - drvcodebeg41 + drivecode41)
!if DISABLE_1571=0 {
            !byte <(drivebusy71 - drvcodebeg71 + drivecode71)
}
!if DISABLE_1581=0 {
            !byte <(drivebusy81 - drvcodebeg81 + drivecode81)
}
dcodeselt1: !byte >(dinstall41  - drvcodebeg41 + drivecode41)
!if DISABLE_1571=0 {
            !byte >(drivebusy71 - drvcodebeg71 + drivecode71)
}
!if DISABLE_1581=0 {
            !byte >(drivebusy81 - drvcodebeg81 + drivecode81)
}
dcodeselt2: !byte <(drvprgend41 - drvcodeend41)
!if DISABLE_1571=0 {
            !byte <(drvprgend71 - drivebusy71)
}
!if DISABLE_1581=0 {
            !byte <(drvprgend81 - drivebusy81)
}
dcodeselt3: !byte <drivecode41
!if DISABLE_1571=0 {
            !byte <drivecode71
}
!if DISABLE_1581=0 {
            !byte <drivecode81
}
dcodeselt4: !byte >drivecode41
!if DISABLE_1571=0 {
            !byte >drivecode71
}
!if DISABLE_1581=0 {
            !byte >drivecode81
}
dcodeselt5: !byte >dinstall41
!if DISABLE_1571=0 {
            !byte >drivebusy71
}
!if DISABLE_1581=0 {
            !byte >drivebusy81
}
dcodeselt6: !byte <dinstall41
!if DISABLE_1571=0 {
            !byte <drivebusy71
}
!if DISABLE_1581=0 {
            !byte <drivebusy81
}
dcodeselt7: !byte >dinstall41
!if DISABLE_1571=0 {
            !byte >dinstall71
}
!if DISABLE_1581=0 {
            !byte >dinstall81
}
dcodeselt8: !byte <dinstall41
!if DISABLE_1571=0 {
            !byte <dinstall71
}
!if DISABLE_1581=0 {
            !byte <dinstall81
}
}; !GENERIC_INSTALL


version:    ;.byte "Krill's Loader, version 92, configuration "
            ;itoa4 MIN_DEVICE_NO
            ;itoa4 MAX_DEVICE_NO
            ;.byte '.'
            ;itoa8 PLATFORM
            ;.byte '.'
            ;itoa4 FILESYSTEM
            ;.byte '.'
            ;itoa1 LOAD_ONCE
            ;.byte '.'
            ;itoa8 DIRTRACK
            ;itoa8 DIRSECTOR
            ;itoa8 FILENAME_MAXLENGTH
            ;.byte '.'
            ;itoa8 MINSTPSP
            ;itoa8 MAXSTPSP
            ;itoa8 STEPRACC
            ;.byte '.'
            ;itoa1 UNINSTALL_RUNS_DINSTALL
            ;itoa1 DISABLE_WATCHDOG
            ;itoa1 JUMP_TABLE
            ;itoa1 LOAD_UNDER_D000_DFFF
            ;itoa1 LOAD_TO_API
            ;itoa1 END_ADDRESS_API
            ;itoa1 LOAD_RAW_API
            ;itoa1 LOAD_COMPD_API
            ;itoa1 OPEN_FILE_POLL_BLOCK_API
            ;itoa1 GETC_API
            ;itoa1 GETCHUNK_API
            ;itoa1 MEM_DECOMP_API
            ;itoa1 MEM_DECOMP_TO_API
            ;itoa1 CHAINED_COMPD_FILES
            ;itoa1 LOAD_VIA_KERNAL_FALLBACK
            ;itoa1 LOAD_PROGRESS_API
            ;itoa1 IDLE_BUS_LOCK
            ;itoa1 NONBLOCKING_API
            ;itoa1 NONBLOCKING_WITH_KERNAL_ON
            ;itoa1 UNINSTALL_API
            ;.byte '.'
            ;itoa4 LC_SPEED
            ;.byte '.'
            ;itoa8 POLLINGINTERVAL_STARTLOAD
            ;itoa8 POLLINGINTERVAL_TRACKCHANGE
            ;itoa8 POLLINGINTERVAL_GETBLOCK
            ;itoa8 POLLINGINTERVAL_GETBLOCK
            ;itoa8 POLLINGINTERVAL_BLOCKSOON
            ;itoa8 POLLINGINTERVAL_REPOLL
            ;.byte '.'
            ;itoa4 DECOMPRESSOR
            ;.byte '.'
            ;itoa1 INSTALL_FROM_DISK

	   ;commented out before
           ;.byte '.'
           ;itoa1 FAST_FILE_FORMAT
           ;itoa1 FAST_ENCODING_FORMAT
           ;.byte '.'
           ;itoa1 DECOMPLOAD_TO_API
           ;itoa1 CUSTOM_DRIVE_CODE_API
           ;.byte '.'
           ;itoa1 NO_DECOMPLOAD_OPTIMIZATION
           ;.byte '.'
           ;itoa4 PROTOCOL
           ;.byte '.'
           ;itoa16 STREAM_BUFFERSIZE
           ; .byte 0

!ifdef DYNLINK {
} else {
	!if  * < $d000 {
	} else {
		!error "***** Error: the install code must not exceed $d000, please make sure the DISKIO_INSTALL segment ends below $d000. *****"
	}
}

