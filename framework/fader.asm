	!ifdef INCLUDED {
        } else {

; w0  BIT $D011
;     BPL w0
; w1  BIT $D011
;     BMI w1




!cpu 6502
!to "fader.prg",cbm

;*= $0801
;!byte $0c,$08,$0a,$00,$9e,$38,$37,$30,$34,$00,$00,$00,$00
*=$0800
!bin "music.sid",,126
;--------------------------------------------------
}
	!ifdef INCLUDED {
        OFF = $c000
        OFF2 = $f000
        TIME_FOR_SPACE = $0260
        } else {			
*=$2000
        OFF = $0000
        OFF2 = $0000
	}
charset
!byte %11111111
!byte %11111110
!byte %11111100
!byte %11111000
!byte %11110000
!byte %11100000
!byte %11000000
!byte %10000000

!byte %11111111
!byte %01111111
!byte %00111111
!byte %00011111
!byte %00001111
!byte %00000111
!byte %00000011
!byte %00000001

!byte %00000000
!byte %10000000
!byte %11000000
!byte %11100000
!byte %11110000
!byte %11111000
!byte %11111100
!byte %11111110

!byte %00000000
!byte %00000001
!byte %00000011
!byte %00000111
!byte %00001111
!byte %00011111
!byte %00111111
!byte %01111111

!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000

!byte %11111111
!byte %11111111
!byte %11111111
!byte %11111111
!byte %11111111
!byte %11111111
!byte %11111111
!byte %11111111

!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000

!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
;--------------------------------------------------
;*=$2800
sprite1
!byte %01111100,%01111110,%00011000
!byte %01100110,%01100000,%00111100
!byte %01100110,%01100000,%01100110
!byte %01111100,%01111000,%01111110
!byte %01111000,%01100000,%01100110
!byte %01101100,%01100000,%01100110
!byte %01100110,%01111110,%01100110
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte $00

sprite2
!byte %01111000,%01100110,%00111100
!byte %01101100,%01100110,%01100110
!byte %01100110,%01100110,%00000110
!byte %01100110,%00111100,%00001100
!byte %01100110,%00011000,%00011000
!byte %01101100,%00011000,%00000000
!byte %01111000,%00011000,%00011000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte $00

!byte %11111111,%00000000,%00000000
!byte %11111111,%00000000,%00000000
!byte %11111111,%00000000,%00000000
!byte %11111111,%00000000,%00000000
!byte %11111111,%00000000,%00000000
!byte %11111111,%00000000,%00000000
!byte %11111111,%00000000,%00000000
!byte %11111111,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte %00000000,%00000000,%00000000
!byte $00

!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte %11111111,%11111111,%11111111
!byte $00

	!ifdef INCLUDED {
init_fader:
			ldx #$00
-			lda $0400,x
			sta $0400+OFF2,x
			lda $0500,x
			sta $0500+OFF2,x
			lda $0600,x
			sta $0600+OFF2,x
			lda $0700,x
			sta $0700+OFF2,x
                        inx
                        bne -                        
        } else {
        * = $2200
			sei
        }

			jsr copy_romfont
			jsr font_fadeout
			jsr prepare_screen
			jsr fade_sides			

			lda #%00000111
			sta $d015
			lda #%00001111
			sta $d017
			sta $d01d
			sta $d010
			
			lda #$0e
			sta $d027
			lda #$01
			sta $d028
			sta $d029

			sta $d02a
			
			lda #$56	;($87)
			sta $d002
			sta $d006
			clc
			adc #48
			sta $d004

			lda #$86
			sta $d003
			sta $d005
			clc
			adc #16
			sta $d007
			
			
			lda #$e5+2       ;adjusted (???)
			sta $d000

			lda #$86
			sta $d001
			
			ldx #129
			stx $07f9+OFF2
			inx
			stx $07fa+OFF2
			inx
			stx $07fb+OFF2
			inx
			stx $07f8+OFF2

			lda #$35
			sta $01
	!ifdef INCLUDED {
			sei
                        }
			lda #$01
			sta $d01a
			lda #<raster
			sta $fffe
			lda #>raster
			sta $ffff
			lda #$ff
			sta $d012
			lda $d011
			and #$7f
			sta $d011
			lda #$7f
			sta $dc0d
			lda $dc0d

!ifdef INCLUDED	{
} else {		
			lda #$00
			jsr $0800
}
			cli

!ifdef INCLUDED	{
			rts
} else {                        

;			dec $d020
			lda partend
			cmp #$1
			bne *-5

			dec $d020
			jmp *-3
}

raster
			sta raster_old_a+1
			stx raster_old_x+1
			sty raster_old_y+1
                        lda $01
                        sta raster_old_01+1
                        lda #$35
                        sta $01
main
                        !ifdef INCLUDED {
                        jsr PLAY_MUSIC
                        } else {
			dec $d020
			jsr $0803
			inc $d020
                        }
;			jsr waitrast

			lda fadeflag
			bne nospace

!ifdef INCLUDED {
			lda $fff8
                       	cmp #<TIME_FOR_SPACE
			bne nospace
			lda $fff9
                        cmp #>TIME_FOR_SPACE
                        bne nospace
} else {
			lda $dc01
			cmp #239
			bne nospace
}                        
			ldx #$00
			lda #$46
clearz		sta line1,x
			inx
			cpx #36		
			bne clearz
			inc fadeflag
nospace			
;			ldx #20
;			dex
;			bne *-1
						
;			dec $d020
;			jsr scroll
			jsr scroll
			
			lda $d002
			cmp #$87
			beq stop_sprite
			jsr movesprite
stop_sprite
			jsr blink

			lda fadeflag
			cmp #$01
			bne nofadeout

			jsr fadeoutcount

nofadeout

			inc $d019
raster_old_01:		lda #$00
			sta $01                        
raster_old_a:	lda #$00
raster_old_x:	ldx #$00
raster_old_y:	ldy #$00
			rti

;			jmp main
;--------------------------------------------------
!ifdef INCLUDED	{
waitrast:		lda $fff8
-			cmp $fff8
			beq -
		} else {
waitrast		lda #$fb
			cmp $d012
			bne *-3
                }			
			rts
;--------------------------------------------------
fadeoutcount	inc counter

			lda spritemove
			cmp #$01
			bne no_move
			dec $d000
			dec $d000
no_move
			lda counter
			cmp #60
			beq setcounters
			cmp #60+24
			beq setcounters
			cmp #60+48
			beq setcounters
			cmp #160
			beq endthis
			rts

setcounters	
			ldx #$00
setloop		lda blinkdata+2,x
			sta blinkdata,x
			inx
			cpx #6
			bne setloop
			
			lda #$01
			sta spritemove
			
			rts

endthis		lda #$01
			sta partend
			rts
;--------------------------------------------------
movesprite
			dec $d002
			dec $d004
			dec $d006

			lda $d002
			cmp #$ff
			bne no1_d010
			inc d010pointer
no1_d010
			lda $d004
			cmp #$ff
			bne no2_d010
			inc d010pointer
no2_d010
			ldx d010pointer
			lda d010tab,x
			sta $d010
			rts
;--------------------------------------------------
blink
			inc blinkval
			lda blinkval
			cmp #25
			bne noblink
			lda #$00
			sta blinkval
			
			inc blinkflag
noblink
			lda blinkflag
			and #1
			tax
			lda blinkdata,x
			sta $d015
			
			rts
			
blinkval		!byte $00
blinkflag		!byte $00
blinkdata		!byte %00000110,%00001110,%00000111,%00001111,%00000011,%00001011,%00000001,%00000001
d010tab		!byte %00001110,%00000100,%00000000
d010pointer	!byte $00
fadeflag		!byte $00
counter		!byte $00
spritemove		!byte $00
partend		!byte $00
;--------------------------------------------------
fade_sides
			ldx #$00
sides		lda #$46
mod1			sta $0400+OFF2
			lda #$47
mod2			sta $0427+OFF2

			lda mod1+1
			clc
			adc #40
			sta mod1+1
			lda mod1+2
			adc #0
			sta mod1+2

			lda mod2+1
			clc
			adc #40
			sta mod2+1
			lda mod2+2
			adc #0
			sta mod2+2
			
			inx
			cpx #25
			bne sides


			ldy #$00
sidefade
			jsr waitrast
			
			ldx #$00
sidefade2
			sec
			rol $2000+(7*8)+OFF,x
			sec
			ror $2000+(6*8)+OFF,x
			inx
			cpx #8
			bne sidefade2

			iny
			cpy #8
			bne sidefade

			jsr waitrast

			lda #$c7
			sta $d016

			ldx #$00
			lda #$44
loopz2
			sta $0400+OFF2,x
			sta $0500+OFF2,x
			sta $0600+OFF2,x
			sta $0700+OFF2,x
			inx
			bne loopz2

			rts
;--------------------------------------------------
prepare_screen
			jsr waitrast

			ldx #$00
			lda #$44
loopz
			sta $0400+OFF2,x
			sta $0500+OFF2,x
			sta $0600+OFF2,x
			sta $0700+OFF2,x
			inx
			bne loopz
			
			ldx #$00
cploop					
			lda #$0e
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
			bne cploop
			
			jsr waitrast

			!ifdef INCLUDED {
                        lda #$00
                        sta $dd00
                        lda #$d8
                        } else {		
			lda #24
                        }
			sta $d018
			lda #%01011011
			sta $d011
			lda #$0e
			sta $d020
			sta $d021
			lda #$06
			sta $d022
			lda #$0f
			sta $d023

			rts
;--------------------------------------------------
font_fadeout
			ldy #$00
back			
			jsr waitrast
						
			ldx #$00
chrloop2
			clc
			rol $2800+OFF,x
			clc
			rol $2900+OFF,x
			clc
			rol $2a00+OFF,x
			clc
			rol $2b00+OFF,x
			clc
			rol $2c00+OFF,x
			clc
			rol $2d00+OFF,x
			clc
			rol $2e00+OFF,x
			clc
			rol $2f00+OFF,x
			
			inx
			bne chrloop2
			iny
			cpy #8
			bne back

			rts
;--------------------------------------------------
copy_romfont
			lda $01
                        pha
			lda #$31
			sta $01
			
			ldx #$00
chrloop
			lda $d000,x
			sta $2800+OFF,x
			lda $d100,x
			sta $2900+OFF,x
			lda $d200,x
			sta $2a00+OFF,x
			lda $d300,x
			sta $2b00+OFF,x
			lda $d400,x
			sta $2c00+OFF,x
			lda $d500,x
			sta $2d00+OFF,x
			lda $d600,x
			sta $2e00+OFF,x
			lda $d700,x
			sta $2f00+OFF,x
			inx
			bne chrloop

			pla
			sta $01

			!ifdef INCLUDED {
                        lda #$00
                        sta $dd00
                        lda #$da
                        } else {		
			lda #27
                        }
			sta $d018

			rts
;--------------------------------------------------
scroll
;softscroll		
			inc scrollpos
			inc scrollpos
			lda scrollpos
			and #7
			tax
			lda scrollreg,x
			sta $d016

			lda scrollpos
			and #7
			beq hardscroll



			rts

hardscroll
;			tax
;			lda scrollreg,x

;			sta $d016

			ldx #$00
hardloop1
			lda $0401+(0*40)+OFF2,x
			sta $0400+(0*40)+OFF2,x
			lda $0401+(1*40)+OFF2,x
			sta $0400+(1*40)+OFF2,x
			lda $0401+(2*40)+OFF2,x
			sta $0400+(2*40)+OFF2,x
			lda $0401+(3*40)+OFF2,x
			sta $0400+(3*40)+OFF2,x
			lda $0401+(4*40)+OFF2,x
			sta $0400+(4*40)+OFF2,x
			lda $0401+(5*40)+OFF2,x
			sta $0400+(5*40)+OFF2,x
			lda $0401+(6*40)+OFF2,x
			sta $0400+(6*40)+OFF2,x
			lda $0401+(7*40)+OFF2,x
			sta $0400+(7*40)+OFF2,x
			lda $0401+(8*40)+OFF2,x
			sta $0400+(8*40)+OFF2,x
			lda $0401+(9*40)+OFF2,x
			sta $0400+(9*40)+OFF2,x
			lda $0401+(10*40)+OFF2,x
			sta $0400+(10*40)+OFF2,x
			lda $0401+(11*40)+OFF2,x
			sta $0400+(11*40)+OFF2,x
			inx
			cpx #$28
			bne hardloop1

			ldx #$00
hardloop2
			lda $0401+(12*40)+OFF2,x
			sta $0400+(12*40)+OFF2,x
			lda $0401+(13*40)+OFF2,x
			sta $0400+(13*40)+OFF2,x
			lda $0401+(14*40)+OFF2,x
			sta $0400+(14*40)+OFF2,x
			lda $0401+(15*40)+OFF2,x
			sta $0400+(15*40)+OFF2,x
			lda $0401+(16*40)+OFF2,x
			sta $0400+(16*40)+OFF2,x
			lda $0401+(17*40)+OFF2,x
			sta $0400+(17*40)+OFF2,x
			lda $0401+(18*40)+OFF2,x
			sta $0400+(18*40)+OFF2,x
			lda $0401+(19*40)+OFF2,x
			sta $0400+(19*40)+OFF2,x
			lda $0401+(20*40)+OFF2,x
			sta $0400+(20*40)+OFF2,x
			lda $0401+(21*40)+OFF2,x
			sta $0400+(21*40)+OFF2,x
			lda $0401+(22*40)+OFF2,x
			sta $0400+(22*40)+OFF2,x
			lda $0401+(23*40)+OFF2,x
			sta $0400+(23*40)+OFF2,x
			lda $0401+(24*40)+OFF2,x
			sta $0400+(24*40)+OFF2,x
			inx
			cpx #$28
			bne hardloop2
			

			ldx position
			lda line1,x
			sta $0400+39+(0*40)+OFF2
			sta $0400+39+(6*40)+OFF2
			sta $0400+39+(12*40)+OFF2
			sta $0400+39+(18*40)+OFF2
			sta $0400+39+(24*40)+OFF2
			lda line2,x
			sta $0400+39+(1*40)+OFF2
			sta $0400+39+(7*40)+OFF2
			sta $0400+39+(13*40)+OFF2
			sta $0400+39+(19*40)+OFF2
			lda line3,x
			sta $0400+39+(2*40)+OFF2
			sta $0400+39+(8*40)+OFF2
			sta $0400+39+(14*40)+OFF2
			sta $0400+39+(20*40)+OFF2
			lda line4,x
			sta $0400+39+(3*40)+OFF2
			sta $0400+39+(9*40)+OFF2
			sta $0400+39+(15*40)+OFF2
			sta $0400+39+(21*40)+OFF2
			lda line5,x
			sta $0400+39+(4*40)+OFF2
			sta $0400+39+(10*40)+OFF2
			sta $0400+39+(16*40)+OFF2
			sta $0400+39+(22*40)+OFF2
			lda line6,x
			sta $0400+39+(5*40)+OFF2
			sta $0400+39+(11*40)+OFF2
			sta $0400+39+(17*40)+OFF2
			sta $0400+39+(23*40)+OFF2
			inc position
			lda position
			cmp #6
			bne no_reset

			lda #$00
			sta position
no_reset
			
			rts
;--------------------------------------------------
position		!byte $04
scrollpos		!byte $06
scrollreg		!byte $c7,$c6,$c5,$c4,$c3,$c2,$c1,$c0
;--------------------------------------------------
data
line1
!byte $80,$81,$40,$44,$44,$41
line2
!byte $82,$83,$42,$44,$44,$43
line3
!byte $40,$41,$05,$42,$43,$05
line4
!byte $44,$44,$41,$80,$81,$40
line5
!byte $44,$44,$43,$82,$83,$42
line6
!byte $42,$43,$05,$40,$41,$05
;--------------------------------------------------
