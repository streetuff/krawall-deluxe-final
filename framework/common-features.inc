
!ifdef _COMMON_FEATURES_INC_ {
} else {
_COMMON_FEATURES_INC_ = 1

; features that are shared by both the install and resident functions

;------------------------------------------------------------------------------

!macro ALL_PLATFORMS_FEATS {
    ;.byte PLATFORM, platforms::COMMODORE_64
    !byte PLATFORM, COMMODORE_64
    !warn "  runs on Commodore 64"
    ;.byte PLATFORM, platforms::COMMODORE_128
    !byte PLATFORM, COMMODORE_128
    !warn "  runs on Commodore 128"
    ;.byte PLATFORM, platforms::COMMODORE_16
    !byte PLATFORM, COMMODORE_16
    !warn "  runs on Commodore 16"
    ;.byte PLATFORM, platforms::COMMODORE_PLUS4
    !byte PLATFORM, COMMODORE_PLUS4
    !warn "  runs on Commodore +4"
    ;.byte PLATFORM, platforms::COMMODORE_VIC20
    !byte PLATFORM, COMMODORE_VIC20
    !warn "  runs on Commodore VIC20"
}

!macro DEFAULT_PLATFORM_FEATS {
    ;.byte PLATFORM, platforms::COMMODORE_64
    !byte PLATFORM, COMMODORE_64
    !warn "  runs on Commodore 64"
    ;.byte PLATFORM, platforms::COMMODORE_128
    !byte PLATFORM, COMMODORE_128
    !warn "  runs on Commodore 128"
}

!macro PLATFORM_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_PLATFORM_FEATS {
            !if USER_PLATFORM_FEATS != 0 {
                +USER_PLATFORM_IMPL
            } else {
                +DEFAULT_PLATFORM_FEATS
            }
        } else {
            +DEFAULT_PLATFORM_FEATS
        }
    } else {
        !byte COMMODORE_64
        !warn "  runs on Commodore 64"
        !byte COMMODORE_128
        !warn "  runs on Commodore 128"
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_SERIAL_BUS_FEATS {
    ; one drive on the bus is usually sufficient for applications
    !warn "  may be restricted to one drive on serial bus"
}

!macro SERIAL_BUS_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_SERIAL_BUS_FEATS {
            !if USER_SERIAL_BUS_FEATS != 0 {
                +USER_SERIAL_BUS_FEATS_IMPL
            } else {
                +DEFAULT_SERIAL_BUS_FEATS
            }
        } else {
            +DEFAULT_SERIAL_BUS_FEATS
        }
    } else {
        !if PROTOCOL != TWO_BITS_ATN {
            !byte MANY_DRIVES_ON_SERIAL_BUS
            !warn "  many drives on serial bus"
        } else {
            !warn "  one drive on serial bus"
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_SUPPORTED_DRIVES_FEATS {
    ; generic drive is usually sufficient for applications
    !byte DRIVE_GENERIC
    !warn "  compatible with generic drives"
}

!macro SUPPORTED_DRIVES_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_SUPPORTED_DRIVES_FEATS {
            !if USER_SUPPORTED_DRIVES_FEATS != 0 {
                +USER_SUPPORTED_DRIVES_FEATS_IMPL
            } else {
                +DEFAULT_SUPPORTED_DRIVES_FEATS
            }
        } else {
            +DEFAULT_SUPPORTED_DRIVES_FEATS
        }
    } else {
        ;.byte SUPPORTED_DRIVE, drivetypes::DRIVE_1541
        !byte SUPPORTED_DRIVE, DRIVE_1541
        !warn "  compatible with CBM 1541"
        ;.byte SUPPORTED_DRIVE, drivetypes::DRIVE_1541_C
        !byte SUPPORTED_DRIVE, DRIVE_1541_C
        !warn "  compatible with CBM 1541-C"
        ;.byte SUPPORTED_DRIVE, drivetypes::DRIVE_1541_II
        !byte SUPPORTED_DRIVE, DRIVE_1541_II
        !warn "  compatible with CBM 1541-II"
        ;.byte SUPPORTED_DRIVE, drivetypes::DRIVE_1570
        !byte SUPPORTED_DRIVE, DRIVE_1570
        !warn "  compatible with CBM 1570"
        ;.byte SUPPORTED_DRIVE, drivetypes::DRIVE_1571
        !byte SUPPORTED_DRIVE, DRIVE_1571
        !warn "  compatible with CBM 1571"
        ;.byte SUPPORTED_DRIVE, drivetypes::DRIVE_1581
        !byte SUPPORTED_DRIVE, DRIVE_1581
        !warn "  compatible with CBM 1581"
        ;.byte SUPPORTED_DRIVE, drivetypes::DRIVE_GENERIC
        !byte SUPPORTED_DRIVE, DRIVE_GENERIC
        !warn "  compatible with generic drives"
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_SUPPORTED_DEVICE_NUMBERS_FEATS {
    ; device #8 is usually sufficient for applications
    !byte SUPPORTED_DEVICE_NUMBER, $08
    !warn "  must work with device #8"
}

!macro SUPPORTED_DEVICE_NUMBERS_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_SUPPORTED_DEVICE_NUMBERS_FEATS {
            !if USER_SUPPORTED_DEVICE_NUMBERS_FEATS != 0 {
                +USER_SUPPORTED_DEVICE_NUMBERS_FEATS_IMPL
            } else {
                +DEFAULT_SUPPORTED_DEVICE_NUMBERS_FEATS
            }
        } else {
            +DEFAULT_SUPPORTED_DEVICE_NUMBERS_FEATS
        }
    } else {
    	
        ;.repeat MAX_DEVICE_NO - MIN_DEVICE_NO + 1, I
        ;    !byte SUPPORTED_DEVICE_NUMBER, MIN_DEVICE_NO + I
        ;.endrep
        !for .i, MAX_DEVICE_NO-MIN_DEVICE_NO+1 {
        	!byte SUPPORTED_DEVICE_NUMBER, MIN_DEVICE_NO+(.i-1)
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_FORMAT_FEATS {
    ; standard format is usually sufficient for applications
    !warn "  at least standard CBM format required"
    !byte FORMAT, CBM_STANDARD
}

!macro FORMAT_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_FORMAT_FEATS {
            !if USER_FORMAT_FEATS != 0 {
                +USER_FORMAT_FEATS_IMPL
            } else {
                +DEFAULT_FORMAT_FEATS
            }
        } else {
            +DEFAULT_FORMAT_FEATS
        }
    } else {
        !byte FORMAT, CBM_STANDARD
        !warn "  standard CBM format"
        !byte FORMAT, CBM_EXTENDED
        !warn "  extended CBM format"
        !byte FORMAT, CBM1571_TWOSIDED
        !warn "  twosided standard 1571 format"
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_FILE_FEATS {
    !byte FILE_IDENTIFICATION
    !if FILESYSTEM = DIRECTORY_NAME {
        !byte NAME
        !warn "  file identification by name"
        !byte FILENAME_MAX, FILENAME_MAXLENGTH
        !warn "  max. file name length FILENAME_MAXLENGTH chars"
        !byte MAX_OPEN, 1
        !warn "  max. 1 file open at the same time"
        ; applications usually load files with matching filename lengths
        !warn "  extra filename chars ignored not checked"
        ; applications usually don't need wildcards
        !warn "  no wildcards needed"
        ; the number of first files accessible is usually sufficient for applications
        !warn "  first files accessible ignored"
    } else {
    	!if FILESYSTEM = TRACK_SECTOR {
        !byte TRACK_SECTOR
        !warn "  file identification by track and sector"
        !byte MAX_OPEN, 1
        !warn "  max. 1 file open at the same time"
        }
    }
}

!macro FILE_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_FILE_FEATS {
            !if USER_FILE_FEATS != 0 {
                +USER_FILE_FEATS_IMPL
            } else {
                +DEFAULT_FILE_FEATS
            }
        } else {
            DEFAULT_FILE_FEATS
        }
    } else {
        !byte FILE_IDENTIFICATION
        !if FILESYSTEM = DIRECTORY_NAME {
            !byte NAME
            !warn "  file identification by name"
            !byte FILENAME_MAX, FILENAME_MAXLENGTH
            !warn "  max. file name length FILENAME_MAXLENGTH chars"
            !byte MAX_OPEN, 1
            !warn "  max. 1 file open at the same time"
            !warn "  extra filename chars not ignored"
            !byte FILENAME_WILDCARDS, WILDCARDS_ALLOWED
            !warn "  wildcards in the file name allowed"
            !byte FIRST_FILES_ACCESSIBLE, NO_FIRST_FILES_LIMITATION
            !warn "  all files in a directory accessible"
        } else {
            !if FILESYSTEM = TRACK_SECTOR {
            !byte TRACK_SECTOR
            !warn "  file identification by track and sector"
            !byte MAX_OPEN, 1
            !warn "  max. 1 file open at the same time"
            }
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_TIMER_FEATS {
    ; timers are usually not used by applications
    !warn "  may use CIA 1 TA IRQ"
    !warn "  may use CIA 1 TB IRQ"
    !warn "  may use CIA 2 TA NMI"
    !warn "  may use CIA 2 TB NMI"
}

!macro TIMER_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_TIMER_FEATS {
            !if USER_TIMER_FEATS != 0 {
                +USER_TIMER_FEATS_IMPL
            } else {
                +DEFAULT_TIMER_FEATS
            }
        } else {
            DEFAULT_TIMER_FEATS
        }
    } else {
        !byte DOESNT_USE_CIA1_TA_IRQ, CIA1_TA_IRQ_NOT_USED
        !warn "  does not use CIA 1 TA IRQ"
        !byte DOESNT_USE_CIA1_TB_IRQ, CIA1_TB_IRQ_NOT_USED
        !warn "  does not use CIA 1 TB IRQ"
        !byte DOESNT_USE_CIA2_TA_NMI, CIA2_TA_NMI_NOT_USED
        !warn "  does not use CIA 2 TA NMI"
        !byte DOESNT_USE_CIA2_TB_NMI, CIA2_TB_NMI_NOT_USED
        !warn "  does not use CIA 2 TB NMI"
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_PALNTSCACCEL_FEATS {
    ; PAL compatibility is usually enough for applications
    !byte PAL, PAL_COMPATIBLE
    !warn "  must be PAL compatible"
    !warn "  may be NTSC compatible"
    !warn "  may be acceleration compatible"
}

!macro PALNTSCACCEL_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_PALNTSCACCEL_FEATS {
            !if USER_PALNTSCACCEL_FEATS != 0 {
                +USER_PALNTSCACCEL_FEATS_IMPL
            } else {
                +DEFAULT_PALNTSCACCEL_FEATS
            }
        } else {
            +DEFAULT_PALNTSCACCEL_FEATS
        }
    } else {
        !byte PAL, PAL_COMPATIBLE
        !warn "  PAL compatible"
        !byte NTSC, NTSC_COMPATIBLE
        !warn "  NTSC compatible"
        !warn "  not acceleration compatible"
    }
}

;------------------------------------------------------------------------------

!if NONBLOCKING_API {
    !macro DEFAULT_TIMER_FEATS_NB {
        ; timers are usually not used by applications
        !warn "  may use CIA 1 TA IRQ"
        !warn "  may use CIA 1 TB IRQ"
        !warn "  may use CIA 2 TA NMI"
        !warn "  may use CIA 2 TB NMI"
    }

    !macro TIMER_FEATS_NB {
        !ifdef DYNLINKLDR {
            !ifdef USER_TIMER_FEATS_NB {
                !if USER_TIMER_FEATS_NB != 0 {
                    +USER_TIMER_FEATS_NB_IMPL
                } else {
                    +DEFAULT_TIMER_FEATS_NB
                }
            } else {
                +DEFAULT_TIMER_FEATS_NB
            }
        } else {
            !byte DOESNT_USE_CIA1_TA_IRQ, CIA1_TA_IRQ_NOT_USED
            !warn "  does not use CIA 1 TA IRQ"
            !byte DOESNT_USE_CIA1_TB_IRQ, CIA1_TB_IRQ_NOT_USED
            !warn "  does not use CIA 1 TB IRQ"
            !warn "  uses CIA 2 TA NMI"
            !warn "  uses CIA 2 TB NMI"
        }
    }
}

;------------------------------------------------------------------------------

!if NONBLOCKING_API {
    !macro DEFAULT_PALNTSCACCEL_FEATS_NB {
        ; PAL compatibility is usually enough for applications
        !byte PAL, PAL_COMPATIBLE
        !warn "  must be PAL compatible"
        !warn "  may be NTSC compatible"
        !warn "  may be acceleration compatible"
    }
    
    !macro PALNTSCACCEL_FEATS_NB {
        !ifdef DYNLINKLDR {; PAL compatibility is usually sufficient for applications
            !ifdef USER_PALNTSCACCEL_FEATS_NB {
                !if USER_PALNTSCACCEL_FEATS_NB != 0 {
                    +USER_PALNTSCACCEL_FEATS_IMPL_NB
                } else {
                    +DEFAULT_PALNTSCACCEL_FEATS_NB
                }
            } else {
                +DEFAULT_PALNTSCACCEL_FEATS_NB
            }
        } else {
            !byte PAL, PAL_COMPATIBLE
            !warn "  PAL compatible"
            !warn "  not NTSC compatible"
            !warn "  not acceleration compatible"
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_LOAD_TO_FEATS {
    !if LOAD_TO_API {
        !byte DESTINATION_OVERRIDE, DEST_OVERRIDE
        !warn " with destination override"
    } else {
        !warn " no destination override"
    }
}

!macro LOAD_TO_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_LOAD_TO_FEATS {
            !if USER_LOAD_TO_FEATS != 0 {
                +USER_LOAD_TO_FEATS_IMPL
            } else {
                +DEFAULT_LOAD_TO_FEATS
            }
        } else {
            +DEFAULT_LOAD_TO_FEATS
        }
    } else {
        !if LOAD_TO_API {
            !byte DESTINATION_OVERRIDE, DEST_OVERRIDE
            !warn " with destination override"
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_REENTRANCE_FEATS {
    !warn "  not re-entrant, no guards"
}

!macro REENTRANCE_FEATS {
    !ifdef DYNLINKLDR {
        !ifdef USER_REENTRANCE_FEATS {
            !if USER_REENTRANCE_FEATS != 0 {
                +USER_REENTRANCE_FEATS_IMPL
            } else {
                +DEFAULT_REENTRANCE_FEATS
            }
        } else {
            +DEFAULT_REENTRANCE_FEATS
        }
    } else {
        !warn "  not re-entrant, no guards"
    }
}

;------------------------------------------------------------------------------

}; !_COMMON_FEATURES_INC_

