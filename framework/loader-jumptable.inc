
!src "loader.inc"
!src "option-check.inc"
!src "common-features.inc"

;------------------------------------------------------------------------------

!macro DEFAULT_INTERRUPT_RESIFEATS {
    ; screen on, interrupts may be delayed, no nmis allowed, no sprites in non-irq space, is usually sufficient for applications
    !byte INTERRUPT_CONSTRAINTS, INTERRUPTS_POSSIBLE
    !warn "  interrupts may be delayed, no sprites or interrupts used"
}

!macro INTERRUPT_RESIFEATS {
    !ifdef DYNLINK {
        !byte INTERRUPT_CONSTRAINTS, ARBITRARY_INTERRUPTS
        !warn "  screen on, no interrupt delays, nmis allowed, no sprite limitations"
    } else {
        !ifdef USER_INTERRUPT_RESIFEATS {
            !if USER_INTERRUPT_RESIFEATS != 0 {
                +USER_INTERRUPT_RESIFEATS_IMPL
            } else {
                +DEFAULT_INTERRUPT_RESIFEATS
            }
        } else {
            +DEFAULT_INTERRUPT_RESIFEATS
        }
    }
}

;------------------------------------------------------------------------------

!if HAS_DECOMPRESSOR {
    !macro DEFAULT_COMPRESSION_RESIFEATS {
        !byte COMPRESSION
        !if DECOMPRESSOR = LEVELCRUSH {
            !if LC_SPEED = 0 {
                !byte TABOO_LEVELCRUSH_SPEED_0
                !warn "  decompression with Taboo Levelcrush at speed 0"
            } else {
            	!if LC_SPEED = 1 {
                !byte TABOO_LEVELCRUSH_SPEED_1
                !warn "  decompression with Taboo Levelcrush at speed 1"
            } else {
            	!if LC_SPEED = 2 {
                !byte TABOO_LEVELCRUSH_SPEED_2
                !warn "  decompression with Taboo Levelcrush at speed 2"
            } else {
            	!if LC_SPEED = 3 {
                !byte TABOO_LEVELCRUSH_SPEED_3
                !warn "  decompression with Taboo Levelcrush at speed 3"
            } else {
            	!if LC_SPEED = 4 {
                !byte TABOO_LEVELCRUSH_SPEED_4
                !warn "  decompression with Taboo Levelcrush at speed 4"
            } else {
            	!if LC_SPEED = 5 {
                !byte TABOO_LEVELCRUSH_SPEED_5
                !warn "  decompression with Taboo Levelcrush at speed 5"
            } else {
            	!if LC_SPEED = 6 {
                !byte TABOO_LEVELCRUSH_SPEED_6
                !warn "  decompression with Taboo Levelcrush at speed 6"
            }
            }}}}}}
        } else {
        	!if DECOMPRESSOR = BYTEBOOZER {
            !byte BYTEBOOZER
            !warn "  decompression with ByteBoozer"
        } else {
        	!if DECOMPRESSOR = PUCRUNCH {
            !byte PUCRUNCH
            !warn "  decompression with PuCrunch"
        } else {	if DECOMPRESSOR = EXOMIZER {
            !byte EXOMIZER
            !warn "  decompression with Exomizer"
        }
        }
        }
        }
    }

    !macro COMPRESSION_RESIFEATS {
        !ifdef DYNLINK {
           + DEFAULT_COMPRESSION_RESIFEATS
        } else {
            !ifdef USER_COMPRESSION_RESIFEATS {
                !if USER_COMPRESSION_RESIFEATS != 0 {
                    +USER_COMPRESSION_RESIFEATS_IMPL
                } else {
                    +DEFAULT_COMPRESSION_RESIFEATS
                }
            } else {
                +DEFAULT_COMPRESSION_RESIFEATS
            }
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_LOAD_UNDER_IO_RAM_RESIFEATS {
    !if LOAD_UNDER_D000_DFFF {
        !byte UNDER_IO_RAM, D000_DFFF_SETS_RAM_CONFIG
        !warn "  has to be able to load to $d000..$dfff"
    } else {
        !warn "  does not have to be able to load to $d000..$dfff"
    }
}

!macro LOAD_UNDER_IO_RAM_RESIFEATS {
    !ifdef DYNLINK {
        !if LOAD_UNDER_D000_DFFF {
            !byte UNDER_IO_RAM, D000_DFFF_SETS_RAM_CONFIG
            !warn "  loads to $d000..$dfff"
        } else {
            !warn "  no loading to $d000..$dfff"
        }
    } else {
        !ifdef USER_LOAD_UNDER_IO_RAM_RESIFEATS {
            !if USER_LOAD_UNDER_IO_RAM_RESIFEATS != 0 {
                +USER_LOAD_UNDER_IO_RAM_RESIFEATS_IMPL
            } else {
                +DEFAULT_LOAD_UNDER_IO_RAM_RESIFEATS
            }
        } else {
            +DEFAULT_LOAD_UNDER_IO_RAM_RESIFEATS
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_NON_BLOCKING_RESIFEATS {
    !warn "  is blocking"
}

!macro NON_BLOCKING_RESIFEATS {
    !if DYNLINKLDR {
        !warn "  is blocking"
    } else {
        !ifdef USER_NON_BLOCKING_RESIFEATS {
            !if USER_NON_BLOCKING_RESIFEATS != 0 {
                +USER_NON_BLOCKING_RESIFEATS_IMPL
            } else {
                +DEFAULT_NON_BLOCKING_RESIFEATS
            }
        } else {
            +DEFAULT_NON_BLOCKING_RESIFEATS
        }
    }
}

;------------------------------------------------------------------------------

!if NONBLOCKING_API {
    !macro DEFAULT_NON_BLOCKING_RESIFEATS_NB {
        !byte NON_BLOCKING, IS_NONBLOCKING
        !warn "  is non-blocking"
    }

    !macro NON_BLOCKING_RESIFEATS_NB {
        !ifdef DYNLINK {
            !byte NON_BLOCKING, IS_NONBLOCKING
            !warn "  is non-blocking"
        } else {
            !ifdef USER_NON_BLOCKING_RESIFEATS_NB {
                !if USER_NON_BLOCKING_RESIFEATS_NB != 0 {
                    +USER_NON_BLOCKING_RESIFEATS_NB_IMPL
                } else {
                    DEFAULT_NON_BLOCKING_RESIFEATS_NB
                }
            } else {
                +DEFAULT_NON_BLOCKING_RESIFEATS_NB
            }
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_MEMDECOMP_TO_RESIFEATS {
    !if MEM_DECOMP_TO_API {
        !byte DESTINATION_OVERRIDE, DEST_OVERRIDE
        !warn "  with destination override"
    } else {
        !warn "  without destination override"
    }
}

!macro MEMDECOMP_TO_RESIFEATS {
    !ifdef DYNLINK {
        !if MEM_DECOMP_TO_API {
            !byte DESTINATION_OVERRIDE, DEST_OVERRIDE
            !warn "  with destination override"
        } else {
            !warn "  without destination override"
        }
    } else {
        !ifdef USER_MEMDECOMP_TO_RESIFEATS {
            !if USER_MEMDECOMP_TO_RESIFEATS != 0 {
                +USER_MEMDECOMP_TO_RESIFEATS_IMPL
            } else {
                +DEFAULT_MEMDECOMP_TO_RESIFEATS
            }
        } else {
            +DEFAULT_MEMDECOMP_TO_RESIFEATS
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_CHAINED_COMPD_FILES_RESIFEATS {
    !if CHAINED_COMPD_FILES {
        !byte CHAINED_FILES, CHAINED_FILES_POSSIBLE
        !warn "  must allow for chained files"
    } else {
        !warn "  does not need to allow for chained files"
    }
}

!macro CHAINED_COMPD_FILES_RESIFEATS {
    !ifdef DYNLINK {
        !if CHAINED_COMPD_FILES {
            !byte CHAINED_FILES, CHAINED_FILES_POSSIBLE
            !warn "  allows for chained files"
        } else {
            !warn "  does not nallow for chained files"
        }
    } else {
        !ifdef USER_CHAINED_COMPD_FILES_RESIFEATS {
            !if USER_CHAINED_COMPD_FILES_RESIFEATS != 0 {
                +USER_CHAINED_COMPD_FILES_RESIFEATS_IMPL
            } else {
                +DEFAULT_CHAINED_COMPD_FILES_RESIFEATS
            }
        } else {
            +DEFAULT_CHAINED_COMPD_FILES_RESIFEATS
        }
    }
}

;------------------------------------------------------------------------------

!macro DEFAULT_LOAD_RAW_ENDADDR_RESIFEATS {
    !if END_ADDRESS_API {
        !byte RETURNS_ENDADDR, ENDADDR_RETURNED
        !warn "  must return end address"
    } else {
        !warn "  does not need to return end address"
    }
}

!macro LOAD_RAW_ENDADDR_RESIFEATS {
    !ifdef DYNLINK {
        !if END_ADDRESS_API {
            !byte RETURNS_ENDADDR, ENDADDR_RETURNED
            !warn "  returns end address"
        } else {
            !warn "  does not return end address"
        }
    } else {
        !ifdef USER_LOAD_RAW_ENDADDR_RESIFEATS {
            !if USER_LOAD_RAW_ENDADDR_RESIFEATS != 0 {
                +USER_LOAD_RAW_ENDADDR_RESIFEATS_IMPL
            } else {
                +DEFAULT_LOAD_RAW_ENDADDR_RESIFEATS
            }
        } else {
            +DEFAULT_LOAD_RAW_ENDADDR_RESIFEATS
        }
    }
}

;------------------------------------------------------------------------------

!macro LOADFEATS_COMMON {
    +PLATFORM_FEATS
    +SERIAL_BUS_FEATS
    +SUPPORTED_DRIVES_FEATS
    +SUPPORTED_DEVICE_NUMBERS_FEATS
    +FORMAT_FEATS
    +INTERRUPT_RESIFEATS
    +FILE_FEATS
    +LOAD_UNDER_IO_RAM_RESIFEATS
    +REENTRANCE_FEATS
}


; generate jump table or module function table

!if LOAD_RAW_API {

    !macro LOAD_RAW_FEATURES {
        !warn " loadraw"
        +LOADFEATS_COMMON
        +LOAD_TO_FEATS
        +LOAD_RAW_ENDADDR_RESIFEATS
        +TIMER_FEATS
        +PALNTSCACCEL_FEATS
        +NON_BLOCKING_RESIFEATS
        !warn "  loads unpacked"
        !warn "  chained files not possible"
        !byte END_OF_LIST
    }

    !ifdef DYNLINK {
        +DYNLINKEXPORT LOAD_RAW, loadraw
        +LOAD_RAW_FEATURES
    } else {
    	!if DYNLINKLDR {
        !byte LOAD_RAW
        LOAD_RAW_FEATURES

        loadraw = __DISKIO_RUN__
        _A_ = 3
    } else {
        ;.export loadraw

        !if JUMP_TABLE {
            !warn " loadraw"
loadraw:    jmp loadraw2
        }
    }}
} else {
    _A_ = 0
}

!if LOAD_COMPD_API {

    !macro LOAD_COMPRESSED_FEATURES {
        !warn " loadcompd"
        +LOADFEATS_COMMON
        +COMPRESSION_RESIFEATS
        +CHAINED_COMPD_FILES_RESIFEATS
        +TIMER_FEATS
        +PALNTSCACCEL_FEATS
        +NON_BLOCKING_RESIFEATS
        !byte END_OF_LIST
    }

    !ifdef DYNLINK {
        +DYNLINKEXPORT LOAD_COMPRESSED_EXO, loadcompd
        +LOAD_COMPRESSED_FEATURES
    } else {
    	!if DYNLINKLDR {
        !byte LOAD_COMPRESSED_EXO
        +LOAD_COMPRESSED_FEATURES

        loadcompd = __DISKIO_RUN__+_A_
        _B_ = 3
    } else {
        ;.export loadcompd

        !if JUMP_TABLE {
            !warn " loadcompd"
loadcompd:  jmp loadcompd2
        }
    }}
} else {
    _B_ = 0
}

!if NONBLOCKING_API {

    !macro LOAD_RAW_FEATURES_NB {
        !warn " loadrawnb"
        +LOADFEATS_COMMON
        +LOAD_TO_FEATS
        +LOAD_RAW_ENDADDR_RESIFEATS
        +TIMER_FEATS_NB
        +PALNTSCACCEL_FEATS_NB
        +NON_BLOCKING_RESIFEATS_NB
        !warn "  loads unpacked"
        !warn "  chained files not possible"
        !byte END_OF_LIST
    }

    !ifdef DYNLINK {
        +DYNLINKEXPORT LOAD_FILE_RAW_NB, loadrawnb
        +LOAD_RAW_FEATURES_NB
    } else {
    	!if DYNLINKLDR {
        !byte LOAD_FILE_RAW_NB
        +LOAD_RAW_FEATURES_NB

        loadrawnb = __DISKIO_RUN__+_A_+_B_
        _C_ = 3
    } else {
        ;.export loadrawnb
        !if (JUMP_TABLE) {
		!ifdef DYNLINK {
		} else {
            !warn " loadrawnb"
loadrawnb:  jmp loadrawnb2
        }}
    }}
} else {
    _C_ = 0
}; NONBLOCKING_API

!if OPEN_FILE_POLL_BLOCK_API {
    !macro OPEN_FILE_FEATURES {
        !warn " openfile"
        +LOADFEATS_COMMON
        +LOAD_TO_FEATS
        +TIMER_FEATS
        +PALNTSCACCEL_FEATS
        +NON_BLOCKING_RESIFEATS
        !byte END_OF_LIST
    }

    !macro POLL_LOADING_FEATURES {
        !warn " pollblock"
        +PLATFORM_FEATS
        +LOAD_UNDER_IO_RAM_RESIFEATS
        +LOAD_RAW_ENDADDR_RESIFEATS
        +INTERRUPT_RESIFEATS
        +TIMER_FEATS
        +PALNTSCACCEL_FEATS
        +REENTRANCE_FEATS
        +NON_BLOCKING_RESIFEATS
        !byte END_OF_LIST
    }

    !ifdef DYNLINK {
        ;DYNLINKEXPORT OPEN_FILE, openfile
        +OPEN_FILE_FEATURES

        ;DYNLINKEXPORT POLL_LOADING, pollblock
        +POLL_LOADING_FEATURES
    } else {
    	!if DYNLINKLDR {
        !byte OPEN_FILE
        +OPEN_FILE_FEATURES

        openfile = __DISKIO_RUN__+_A_+_B_+_C_
        _D_ = 3

        !byte POLL_LOADING
        POLL_LOADING_FEATURES

        pollblock = __DISKIO_RUN__+_A_+_B_+_C_+_D_
        _E_ = 3
    } else {
        ;.export openfile
        ;.export pollblock

        !if JUMP_TABLE {
	    !if NONBLOCKING_API=0 {
            !warn " openfile"
openfile:       jmp openfile2
            !warn " pollblock"
pollblock:      jmp pollblock2
	}
        }
	}
    }
} else {
    _D_ = 0
    _E_ = 0
}

!if GETC_API {

    !macro GETC_FEATURES {
        !warn " getc"
        +PLATFORM_FEATS
        +LOAD_UNDER_IO_RAM_RESIFEATS
        +LOAD_RAW_ENDADDR_RESIFEATS
        +INTERRUPT_RESIFEATS
        +TIMER_FEATS
        +PALNTSCACCEL_FEATS
        +REENTRANCE_FEATS
        +NON_BLOCKING_RESIFEATS
        !byte END_OF_LIST
    }

    !ifdef DYNLINK {
        +DYNLINKEXPORT READ_BYTE, getc
        +GETC_FEATURES
    } else {
    	!if DYNLINKLDR {
        !byte READ_BYTE
        +GETC_FEATURES

        getc = __DISKIO_RUN__+_A_+_B_+_C_+_D_+_E_
        _F_ = 3
    } else {
        ;.export getc

        !if JUMP_TABLE {
            !warn " getc"
getc:            jmp getc2
        }
    }}
} else {
_F_ = 0
}; GETC_API

!if GETCHUNK_API {

    !macro GETCHUNK_FEATURES {
        !warn " getchunk"
        +PLATFORM_FEATS
        +LOAD_UNDER_IO_RAM_RESIFEATS
        +LOAD_RAW_ENDADDR_RESIFEATS
        +INTERRUPT_RESIFEATS
        +TIMER_FEATS
        +PALNTSCACCEL_FEATS
        +REENTRANCE_FEATS
        +NON_BLOCKING_RESIFEATS
        !byte END_OF_LIST
    }

    !ifdef DYNLINK {
        +DYNLINKEXPORT GET_BYTES, getchunk
        +GETCHUNK_FEATURES
    } else {
    	!if DYNLINKLDR {
        !byte GET_BYTES
        +GETCHUNK_FEATURES

        getchunk = __DISKIO_RUN__+_A_+_B_+_C_+_D_+_E_+_F_
        _G_ = 3
    } else {
        ;.export getchunk

        !if JUMP_TABLE {
            !warn " getchunk"
getchunk:        jmp getchunk2
        }
    }}
} else {
_G_ = 0
}; GETCHUNK_API

!if MEM_DECOMP_API {

    !macro MEMDECOMP_FEATS_COMMON {
        +ALL_PLATFORMS_FEATS
        +COMPRESSION_RESIFEATS
        !ifdef DYNLINK {
            !byte PAL, PAL_COMPATIBLE
            !warn "  PAL compatible"
            !byte NTSC, NTSC_COMPATIBLE
            !warn "  NTSC compatible"
            !byte ACCELERATION, ACCELERATION_COMPATIBLE
            !warn "  acceleration compatible"
        } else {
            ; PAL compatibility is usually sufficient for applications
            !byte PAL, PAL_COMPATIBLE
            !warn "  must be PAL compatible"
            !warn "  may be NTSC compatible"
            !warn "  may be acceleration compatible"
        }
        !byte UNDER_IO_RAM, D000_DFFF_POSSIBLE
        !warn " decompression to $d000..$dfff when $01 is set accordingly"
        +MEMDECOMP_TO_RESIFEATS
        +INTERRUPT_RESIFEATS
        +TIMER_FEATS
        +REENTRANCE_FEATS
        +NON_BLOCKING_RESIFEATS
    }

    !ifdef DYNLINK {
        !warn " memdecomp"
        +DYNLINKEXPORT MEM_DECOMPRESS_EXO, memdecomp
        +MEMDECOMP_FEATS_COMMON
        !byte END_OF_LIST
    } else {
    	!if DYNLINKLDR {
        !warn " memdecomp"
        !byte MEM_DECOMPRESS_EXO
        +MEMDECOMP_FEATS_COMMON
        !byte END_OF_LIST

        memdecomp = __DISKIO_RUN__+_A_+_B_+_C_+_D_+_E_+_F_+_G_
        _H_ = 3
    } else {
        ;.export memdecomp

        !if JUMP_TABLE {
            !warn " memdecomp"
memdecomp:  jmp memdecomp2
        }
    }}

    !if CHAINED_COMPD_FILES {
        !ifdef DYNLINK {
            !warn " cmemdecomp"
            +DYNLINKEXPORT MEM_CDECOMPRESS_EXO, cmemdecomp
            +MEMDECOMP_FEATS_COMMON
            +CHAINED_COMPD_FILES_RESIFEATS
            !byte END_OF_LIST
        } else {
        	!if DYNLINKLDR {
            !warn " cmemdecomp"
            !byte MEM_CDECOMPRESS_EXO
            +MEMDECOMP_FEATS_COMMON
            +CHAINED_COMPD_FILES_RESIFEATS
            !byte END_OF_LIST

            cmemdecomp = __DISKIO_RUN__+_A_+_B_+_C_+_D_+_E_+_F_+_G_+_H_
            _I_ = 3
        } else {
            ;.export cmemdecomp

            !if JUMP_TABLE {
                !warn " cmemdecomp"
cmemdecomp: jmp cmemdecomp2
            }
        }}
    } else {
        _I_ = 0
    }
} else {
    _H_ = 0
    _I_ = 0
}

!if UNINSTALL_API {
    !macro UNINSTALL_FEATURES {
        !warn " uninstall"
        +PLATFORM_FEATS
        +INTERRUPT_RESIFEATS
        !ifdef DYNLINK {
            !byte PAL, PAL_COMPATIBLE
            !warn "  PAL compatible"
            !byte NTSC, NTSC_COMPATIBLE
            !warn "  NTSC compatible"
            !byte ACCELERATION, ACCELERATION_COMPATIBLE
            !warn "  acceleration compatible"
        } else {
            ; PAL compatibility is usually sufficient for applications
            !byte PAL, PAL_COMPATIBLE
            !warn "  must be PAL compatible"
            !warn "  may be NTSC compatible"
            !warn "  may be acceleration compatible"
        }
        +REENTRANCE_FEATS
        !if NONBLOCKING_API {
        +TIMER_FEATS_NB
        !byte NON_BLOCKING, IS_NONBLOCKING
        } else {
        !warn "  for non-blocking routines"
        +TIMER_FEATS
        +NON_BLOCKING_RESIFEATS
        }
        !byte END_OF_LIST
    }

    !ifdef DYNLINK {
        +DYNLINKEXPORT UNINSTALL, uninstall
        +UNINSTALL_FEATURES
    } else {
    	!if DYNLINKLDR {
        !byte UNINSTALL
        +UNINSTALL_FEATURES

        uninstall = __DISKIO_RUN__+_A_+_B_+_C_+_D_+_E_+_F_+_G_+_H_+_I_
        _J_ = 3
    } else {
        ;.export uninstall

        !if (JUMP_TABLE) {
        	!ifdef DYNLINK {
                } else {
            !warn " uninstall"
uninstall:  jmp uninstall2
    		}
    	}
        }
    }
} else {
    _J_ = 0
}; UNINSTALL_API


!ifdef DYNLINK {
    !byte END_OF_LIST
base:

!if DYNLINKLDR {
    !byte END_OF_LIST
}
    !if DYNLINKLDR {
        RESIDENTJUMPTABLESIZE    = _A_+_B_+_C_+_D_+_E_+_F_+_G_+_H_+_I_+_J_
        NUMIMPORTEDRESIDENTFUNCS = RESIDENTJUMPTABLESIZE / 3
        !if JUMP_TABLE {
        DYNLINKOVERHEAD          = (NUMIMPORTEDINSTALLFUNCS + NUMIMPORTEDRESIDENTFUNCS) * 2
        } else {
        DYNLINKOVERHEAD          = RESIDENTJUMPTABLESIZE + (NUMIMPORTEDINSTALLFUNCS + NUMIMPORTEDRESIDENTFUNCS) * 2
        }

        loadstatus               = .lobyte(__DISKIO_ZP_RUN__ + LOADSTATUSOFFS)

        loadaddrlo               = .lobyte(__DISKIO_ZP_RUN__ + LOADADDRLOOFFS)
        loadaddrhi               = .lobyte(__DISKIO_ZP_RUN__ + LOADADDRHIOFFS)

        decdestlo                = .lobyte(__DISKIO_ZP_RUN__ + DECDESTLOOFFS)
        decdesthi                = .lobyte(__DISKIO_ZP_RUN__ + DECDESTHIOFFS)

        endaddrlo                = .lobyte(__DISKIO_ZP_RUN__ + ENDADDRLOOFFS)
        endaddrhi                = .lobyte(__DISKIO_ZP_RUN__ + ENDADDRHIOFFS)

        bytesloadedlo            = .lobyte(__DISKIO_ZP_RUN__ + BYTESLOADEDLOOFFS)
        bytesloadedhi            = .lobyte(__DISKIO_ZP_RUN__ + BYTESLOADEDHIOFFS)

        param4                   = .lobyte(__DISKIO_ZP_RUN__ + PARAM4OFFS)
        param5                   = .lobyte(__DISKIO_ZP_RUN__ + PARAM5OFFS)
    }
}

