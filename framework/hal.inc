
!ifdef _HAL_INC_ {
} else {
_HAL_INC_ = 1


!if PLATFORM = COMMODORE_PLUS4 {

!src "cpu.inc"
!src "ted.inc"


IO_PORT_DIR_COMMON = IO_PORT_SERIAL_DATA_IN_INPUT | IO_PORT_SERIAL_CLK_IN_INPUT | IO_PORT_SERIAL_DATA_OUT_OUTPUT | IO_PORT_SERIAL_CLK_OUT_OUTPUT | IO_PORT_SERIAL_ATN_OUT_OUTPUT; $08
                                        ; effectively, this is  ; effectively, this is
                                        ; the kernal flag:      ; the idle flag:
                                        ; 0 = input  = loader,  ; 0 = input  = idle,
                                        ; 1 = output = kernal   ; 1 = output = not idle
IO_PORT_DIR_KERNAL = IO_PORT_DIR_COMMON |      IO_PORT_CST_MTR  | (0 & IO_PORT_CST_RD); $0f

IO_PORT_DIR_IDLE   = IO_PORT_DIR_COMMON | (0 & IO_PORT_CST_MTR) | (0 & IO_PORT_CST_RD); $07

IO_PORT_DIR_WAKEUP = IO_PORT_DIR_COMMON | (0 & IO_PORT_CST_MTR) |      IO_PORT_CST_RD ; $17 - XXX TODO switch port to 0 for CST_RD

CLOCK              = (0 & IO_PORT_SERIAL_DATA_OUT) | (0 & IO_PORT_SERIAL_CLK_OUT)
CLOCK_ATN_HI       = CLOCK |      IO_PORT_SERIAL_ATN_OUT ; 1st and 3rd bit pairs
CLOCK_ATN_LO       = CLOCK | (0 & IO_PORT_SERIAL_ATN_OUT); 2nd and 4th bit pairs


!macro CLEAR_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT {
            lda #IO_PORT_CST_MTR | (0 & IO_PORT_SERIAL_DATA_OUT) | (0 & IO_PORT_SERIAL_CLK_OUT) | (0 & IO_PORT_SERIAL_ATN_OUT); $08
            sta IO_PORT
}

!macro SET_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT {
            lda #IO_PORT_CST_MTR | IO_PORT_SERIAL_DATA_OUT | (0 & IO_PORT_SERIAL_CLK_OUT) | (0 & IO_PORT_SERIAL_ATN_OUT); $09
            sta IO_PORT
}

!macro BRANCH_IF_IDLE to {
            ldx #IO_PORT_DIR_IDLE
            cpx IO_PORT_DIRECTION
            beq to
}

!macro BRANCH_IF_NOT_IDLE to {
            lda #IO_PORT_DIR_IDLE
            cmp IO_PORT_DIRECTION
            bne to
}

!if LOAD_VIA_KERNAL_FALLBACK {
    KERNALFILENO   = $02

    !macro BRANCH_IF_INSTALLED .to {
            lda #IO_PORT_DIR_KERNAL
            cmp IO_PORT_DIRECTION
            bne .to
    }

    !macro BRANCH_IF_NOT_INSTALLED .to {
            lda #IO_PORT_DIR_KERNAL
            cmp IO_PORT_DIRECTION
            beq .to
    }
}

!macro SENDBYTE {
            pha
            lda #FORCE_SINGLE_CLOCK
            ora TED_CHARGEN
            sta TED_CHARGEN
            pla
            ldx #$08
--           lsr
            pha
            lda IO_PORT
            and #~IO_PORT_SERIAL_DATA_OUT
            bcc +
            ora #IO_PORT_SERIAL_DATA_OUT
+           eor #IO_PORT_SERIAL_CLK_OUT
            sta IO_PORT
            pla
            dex
            bne --
            pha
            pla
            lda #~FORCE_SINGLE_CLOCK
            and TED_CHARGEN
            sta TED_CHARGEN
}

!macro BRANCH_IF_DATA_IN_CLEAR .to {
            bit IO_PORT
            bpl .to
}

!macro BRANCH_IF_CLK_IN_CLEAR .to {
            bit IO_PORT
            bvc .to
}

!macro BRANCH_IF_CLK_IN_SET .to {
            bit IO_PORT
            bvs .to
}

!macro BRANCH_IF_CLK_IN_OR_DATA_IN_CLEAR .to {
            bit IO_PORT
            bpl .to
            bvc .to
}

!macro INSTALL_IDLE {
            lda #IO_PORT_CST_MTR | IO_PORT_SERIAL_DATA_OUT | IO_PORT_SERIAL_CLK_OUT | IO_PORT_SERIAL_ATN_OUT
            sta IO_PORT
            lda #IO_PORT_DIR_IDLE
            sta IO_PORT_DIRECTION
}

!macro WAKEUP {
            +SET_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT
            lda #IO_PORT_DIR_WAKEUP
            sta IO_PORT_DIRECTION
}

!macro PREPARE_ATN_STROBE {
            ldx #CLOCK_ATN_HI
}

!macro BEGIN_ATN_STROBE {
            stx IO_PORT
}

!macro END_ATN_STROBE {
            ; dynamically adjust the getbyte result;
            ; this is needed because VICE and YAPE
            ; emulate the processor port differently
            ; with regard to the data direction
            lda IO_PORT
            and #$ff-(IO_PORT_SERIAL_DATA_IN | IO_PORT_CST_WRT_IN | IO_PORT_SERIAL_ATN_OUT | IO_PORT_SERIAL_CLK_OUT | IO_PORT_SERIAL_DATA_OUT)
            sta + + $01
            ora #CLOCK_ATN_LO
            lsr
            lsr
            eor #CLOCK_ATN_HI
            eor :+ + $01
            lsr
            lsr
            eor #CLOCK_ATN_LO
            eor :+ + $01
            lsr
            lsr
            eor #CLOCK_ATN_HI
+           eor #$00
            sta adjustgetb + $01

            lda #CLOCK_ATN_LO
            sta IO_PORT
            lda #FORCE_SINGLE_CLOCK
            ora TED_CHARGEN
            sta TED_CHARGEN
}

!macro GETBYTE {
            ldx #CLOCK_ATN_HI                   ; 2
            lda IO_PORT                         ; 3
            stx IO_PORT; ATN high               ; 3
            lsr                                 ; 2
            lsr                                 ; 2
            ldx #CLOCK_ATN_LO                   ; 2
            nop                                 ; 2
            nop                                 ; 2
                                                ; = 18 (21)

            eor IO_PORT                         ; 3
            stx IO_PORT; ATN low                ; 3
            lsr                                 ; 2
            lsr                                 ; 2
            ldx #CLOCK_ATN_HI                   ; 2
            nop                                 ; 2
            nop                                 ; 2
            nop                                 ; 2
                                                ; = 18 (20)

            eor IO_PORT                         ; 3
            stx IO_PORT; ATN high               ; 3
            lsr                                 ; 2
            lsr                                 ; 2
            ldx #CLOCK_ATN_LO                   ; 2
            nop                                 ; 2
adjustgetb: eor #$00                            ; 2
                                                ; = 16 (19)

            eor IO_PORT
            stx IO_PORT; ATN low
            nop
            nop
}

!macro ENDGETBYTE {
            pha; special block number
            lda #~FORCE_SINGLE_CLOCK
            and TED_CHARGEN
            sta TED_CHARGEN
            pla; special block number
}

!macro IDLE {
    !if LOAD_ONCE {
            ; uninstall
            ldx #IO_PORT_CST_MTR | (0 & IO_PORT_SERIAL_DATA_OUT) | (0 & IO_PORT_SERIAL_CLK_OUT) | (0 & IO_PORT_SERIAL_ATN_OUT); $08
            stx IO_PORT
            ldx #IO_PORT_DIR_KERNAL
    } else {
            ldx #IO_PORT_CST_MTR | IO_PORT_SERIAL_DATA_OUT | IO_PORT_SERIAL_CLK_OUT | IO_PORT_SERIAL_ATN_OUT
            stx IO_PORT
            ldx #IO_PORT_DIR_IDLE
    }
            stx IO_PORT_DIRECTION
}

!macro SET_IO_KERNAL {
            CLEAR_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT
            lda #IO_PORT_DIR_KERNAL
            sta IO_PORT_DIRECTION
}

}


!src "cia.inc"


!ifdef _CIA_INC_ {; is not defined if via.inc had been included before

; CIA2 DDRA ($DD02) definitions
CIA2_DDRA_COMMON   = CIA_SERIAL_DATA_IN_INPUT | CIA_SERIAL_CLK_IN_INPUT | CIA_VIC2_BANK_OUTPUT; $03
                                                                                                                           ; effectively, this is the kernal flag;
                                                                                                                           ; 0 = input  = loader,
; DATA OUT, CLK OUT, ATN out are low; RS232 is low                                                                         ; 1 = output = kernal
CIA2_DDRA_KERNAL   = CIA2_DDRA_COMMON | CIA_SERIAL_DATA_OUT_OUTPUT | CIA_SERIAL_CLK_OUT_OUTPUT | CIA_SERIAL_ATN_OUT_OUTPUT | CIA_RS232_OUTPUT; $3f

; DATA OUT, CLK OUT, ATN OUT are high; RS232 is high
CIA2_DDRA_IDLE     = CIA2_DDRA_COMMON | CIA_SERIAL_DATA_OUT_INPUT  | CIA_SERIAL_CLK_OUT_INPUT  | CIA_SERIAL_ATN_OUT_INPUT  | CIA_RS232_INPUT ; $03

; DATA OUT is high, CLK OUT, ATN out are low; RS232 is high
CIA2_DDRA_WAKEUP   = CIA2_DDRA_COMMON | CIA_SERIAL_DATA_OUT_INPUT  | CIA_SERIAL_CLK_OUT_OUTPUT | CIA_SERIAL_ATN_OUT_OUTPUT | CIA_RS232_INPUT ; $1b

!if PLATFORM != COMMODORE_PLUS4 {

CLOCK              = CIA2_DDRA_COMMON | CIA_SERIAL_DATA_OUT_OUTPUT | CIA_SERIAL_CLK_OUT_OUTPUT
CLOCK_ATN_HI       = CLOCK | CIA_SERIAL_ATN_OUT_INPUT  | CIA_RS232_OUTPUT; 1st and 3rd bit pairs
CLOCK_ATN_LO       = CLOCK | CIA_SERIAL_ATN_OUT_OUTPUT | CIA_RS232_INPUT ; 2nd and 4th bit pairs

!macro CLEAR_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT {
            lda #($ff-CIA_SERIAL_DATA_OUT_OUTPUT) & ($ff-CIA_SERIAL_CLK_OUT_OUTPUT) & ($ff-CIA_SERIAL_ATN_OUT_OUTPUT)
            and CIA2_PRA
            sta CIA2_PRA
}

!macro SET_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT {
            lda #CIA2_DDRA_WAKEUP
            sta CIA2_DDRA
}

!macro BRANCH_IF_IDLE .to {
            ldx #CIA2_DDRA_IDLE
            cpx CIA2_DDRA
            beq .to
}

!macro BRANCH_IF_NOT_IDLE .to {
            lda #CIA2_DDRA_IDLE
            cmp CIA2_DDRA
            bne .to
}

!if LOAD_VIA_KERNAL_FALLBACK {
    KERNALFILENO   = $02

    !macro BRANCH_IF_INSTALLED .to {
            lda #CIA2_DDRA_KERNAL
            cmp CIA2_DDRA
            bne .to
    }

    !macro BRANCH_IF_NOT_INSTALLED .to {
            lda #CIA2_DDRA_KERNAL
            cmp CIA2_DDRA
            beq .to
    }
}

!macro SENDBYTE {
            ldx #$08
--           lsr
            pha
            lda CIA2_DDRA
            and #$ff-CIA_SERIAL_DATA_OUT_OUTPUT
            bcs *+4
            ora #CIA_SERIAL_DATA_OUT_OUTPUT
+           eor #CIA_SERIAL_CLK_OUT_OUTPUT
            sta CIA2_DDRA
            pla
            dex
            bne --
}

!macro BRANCH_IF_DATA_IN_CLEAR .to {
            bit CIA2_PRA
            bpl .to
}

!macro BRANCH_IF_CLK_IN_CLEAR .to {
            bit CIA2_PRA
            bvc .to
}

!macro BRANCH_IF_CLK_IN_SET .to {
            bit CIA2_PRA
            bvs .to
}

!macro BRANCH_IF_CLK_IN_OR_DATA_IN_CLEAR .to {
            bit CIA2_PRA
            bpl .to
            bvc .to
}

!macro INSTALL_IDLE {
            lda #CIA2_DDRA_IDLE
            sta CIA2_DDRA
}

!macro WAKEUP {
    !if IDLE_BUS_LOCK {
            ; when the loader is idle, the user is
            ; allowed to write anything to $dd00 -
            ; set it to a known and valid state here
            +CLEAR_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT
    }
            ldy #CIA2_DDRA_WAKEUP
            sty CIA2_DDRA
}

!macro PREPARE_ATN_STROBE {
            ldx #CLOCK_ATN_HI
}

!macro BEGIN_ATN_STROBE {
            stx CIA2_DDRA
}

!macro END_ATN_STROBE {
            lda #CLOCK_ATN_LO
            sta CIA2_DDRA - CLOCK_ATN_HI,x
}

!macro GETBYTE {
            ldx #CLOCK_ATN_HI                   ; 2
            lda CIA2_PRA - CLOCK_ATN_HI,x       ; 5
            stx CIA2_DDRA; ATN high             ; 4
            lsr                                 ; 2
            lsr                                 ; 2
            php                                 ; 3
            nop                                 ; 2
            ldx #CLOCK_ATN_LO                   ; 2
                                                ; = 22

            ora CIA2_PRA                        ; 4
            stx CIA2_DDRA; ATN low              ; 4
            lsr                                 ; 2
            lsr                                 ; 2
            plp                                 ; 4
            eor #RS232_OUT                      ; 2
            ldx #CLOCK_ATN_HI                   ; 2
                                                ; = 20

            eor CIA2_PRA                        ; 4
            stx CIA2_DDRA; ATN high             ; 4
            lsr                                 ; 2
            lsr                                 ; 2
            sta + + $01                        ; 4
            lda #SERIAL_DATA_IN | SERIAL_CLK_IN ; 2
            ldx #CLOCK_ATN_LO                   ; 2 + 1
                                                ; = 21

            and CIA2_PRA - CLOCK_ATN_LO,x
            stx CIA2_DDRA; ATN low
+           ora #$00; this is done to make sure the
                    ; currently set VIC bank is irrelevant
}

!macro ENDGETBYTE {
            ; nothing to do
}


!macro IDLE {
    !if LOAD_ONCE {
            ; uninstall
            ldx #CIA2_DDRA_KERNAL
    } else {
            ldx #CIA2_DDRA_IDLE
    }
            stx CIA2_DDRA
}

!macro SET_IO_KERNAL {
            lda #CIA2_DDRA_KERNAL
            sta CIA2_DDRA
}

}


!macro SETUP_TIMER {
            lda #<(CYCLES_PER_LINE_PAL * 8 - $01); timing interval is 8 scanlines XXX TODO NTSC: user-supplied flag or check?
            sta CIA2_TA_LO
            lda #>(CYCLES_PER_LINE_PAL * 8 - $01)
            sta CIA2_TA_HI
            lda #$00
            sta CIA2_TB_LO
            sta CIA2_TB_HI
            lda #FORCE_LOAD | TIMER_START
            sta CIA2_CRA
}

!macro SET_TIMER {
            sta CIA2_TB_LO
            lda #COUNT_TA_UNDF | FORCE_LOAD | TIMER_START
            sta CIA2_CRB
}

!macro ENABLE_TIMER_NMI {
            lda #CIA_SET_INTF | TIMERB_IRQ
            sta CIA2_ICR
}

!macro DISABLE_TIMER_NMI {
            lda #CIA_CLR_INTF | TIMERB_IRQ
            sta CIA2_ICR
}

!macro ACK_TIMER_NMI {
            bit CIA2_ICR
}

}; _CIA_INC_


!macro DO_UNINSTALL {
    !if NONBLOCKING_API {
            +DISABLE_TIMER_NMI
            +ACK_TIMER_NMI
    }
            +SET_IO_KERNAL
    !if UNINSTALL_RUNS_DINSTALL=0 {
-           +BRANCH_IF_CLK_IN_OR_DATA_IN_CLEAR -
    } else {
-           +BRANCH_IF_CLK_IN_CLEAR -
    }
}


!macro BRANCH_IF_CHANGING_TRACK .to {
            +BRANCH_IF_DATA_IN_CLEAR .to
}

!macro BRANCH_IF_READY .to {
            +BRANCH_IF_CLK_IN_SET .to
}

!macro BRANCH_IF_NOT_READY .to {
            +BRANCH_IF_CLK_IN_CLEAR .to
}

!macro WAITREADY {
-           +BRANCH_IF_NOT_READY -
}

KERNALFILENO   = $02


}; !_HAL_INC_
