
;.fileopt comment, "Loader resident code portion"
;.fileopt compiler, "CA65"
;.fileopt author, "Gunnar Ruthenberg"

__NO_LOADER_SYMBOLS_IMPORT = 1
!src "loader.inc"
!src "kernal.inc"

!src "cpu.inc"
!src "cia.inc"

!src "hal.inc"

;.importzp BLOCKDESTLO
;.importzp BLOCKINDEX
;.importzp LOADYBUF
;.importzp YPNTRBUF
;.importzp LASTBLKIDX
;.importzp LASTBLKSIZ
;.importzp LOADDESTPTR
;.importzp PACCUBUF
;.importzp DECOMPVARS

;.importzp GETCHUNK_VARS

!if BYTESTREAM {
;.export loadedtb
}


;.macpack longbranch


;.segment "DISKIO"


!if GETCHUNK_API {
CHUNKSWTCH         = GETCHUNK_VARS + $00
LASTPC             = GETCHUNK_VARS + $01
LASTSP             = GETCHUNK_VARS + $03
LASTXREG           = GETCHUNK_VARS + $04; LASTXREG and LASTYREG
LASTYREG           = GETCHUNK_VARS + $04; are never used at the same time
CHUNKBEGLO         = GETCHUNK_VARS + $05
CHUNKBEGHI         = GETCHUNK_VARS + $06
CHUNKENDLO         = GETCHUNK_VARS + $07
CHUNKENDHI         = GETCHUNK_VARS + $08
}

DEVICE_NOT_PRESENT_LOD = $00
; special block numbers
SPECIAL_BLOCK_NOS  = $fe
LOAD_FINISHED      = $fe; loading finished successfully
LOAD_ERROR         = $ff; file not found or illegal track or sector



!ifdef DYNLINK {
    !macro DYNLINKEXPORT .function, .label {
        !byte function, <(label - base), >(label - base)
    }

    ;.segment "JUMPTABLE"
    ;.segment "DATA"

    !warn "Exported dynamic link symbols:"

    !word endresijmptable - *

} else {
    !ifdef RESIADDR {
            * = RESIADDR - 2
            !word * + 2; load address
    }

    !if JUMP_TABLE {
    !warn "Jump table (loader):"
    }
}

!src "loader-jumptable.inc"; this also checks sensibility of options
endresijmptable:



;.if (!(.defined(DYNLINK))) & (!LOAD_VIA_KERNAL_FALLBACK)
;    .assert (* <= $d000) | (* >= $e000), error, "***** Error: The resident code must not reside at $d000..$dfff, please make sure the DISKIO segment does not overlap with that memory range. *****"
;}


!if LOAD_RAW_API {

            ; --- load file without decompression ---
            ; in:  x - <(filename) or track (depends on FILESYSTEM setting in config.inc)
            ;      y - >(filename) or sector (depends on FILESYSTEM setting in config.inc)
            ;      c - if LOAD_TO_API != 0, c = 0: load to address as stored in the file
            ;                               c = 1: load to caller-specified address (loadaddrlo/hi)
            ; out: c - set on error
            ;      a - status

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the IO space at $d000 is enabled
!ifdef loadraw {
loadraw2:
} else {
loadraw:
}

            jsr openfile
    !if LOAD_VIA_KERNAL_FALLBACK {
            bcs openerror; only with kernal fallback because only then the call might fail
    }
-           jsr _pollblock
            bcc -
            cmp #OK + 1
    !if LOAD_VIA_KERNAL_FALLBACK {
openerror:
    }
            rts

}; LOAD_RAW_API

!if LOAD_COMPD_API {

            ; --- load a compressed file ---
            ; in:  x - <(filename) or track (depends on FILESYSTEM setting in config.inc)
            ;      y - >(filename) or sector (depends on FILESYSTEM setting in config.inc)
            ;      c - if DECOMPLOAD_TO_API != 0, c = 0: load to address as stored in the file
            ;                                     c = 1: load to caller-specified address (loadaddrlo/hi, ignored)
            ; out: c - set on error
            ;      a - status

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the IO space at $d000 is enabled
!ifdef loadcompd {
loadcompd2:
} else {
loadcompd:
}
    !if LOAD_TO_API {
            ; there is no DECOMPLOAD_TO_API yet
            clc
    }
            jsr openfile
    !if LOAD_VIA_KERNAL_FALLBACK {
            ; only with kernal fallback because only then the call might fail
            bcc +
            rts
+
        !if GETC_API | GETCHUNK_API {
            !if PLATFORM != COMMODORE_PLUS4 {
            ;+BRANCH_IF_INSTALLED dontenablerom
            lda #MEMCONFIG_IO_KERNAL
            sta IO_PORT
            }
dontenablerom:
        }
    }
            ; throw exception on stream error
            tsx
    !if GETCHUNK_API {
            stx LASTSP
    } else {
            stx stackpntr + $01
    }
    !if EXCEPTIONS & (LOAD_RAW_API | GETC_API | GETCHUNK_API | OPEN_FILE_POLL_BLOCK_API) {
            inc maybethrow + $01; throw exception on stream error
    }

    !if GETCHUNK_API {
            lda #$ff
            sta CHUNKSWTCH; switch the GETCHUNK_API routines off
            sta CHUNKENDLO; probably fails if the decompressed
            sta CHUNKENDHI; data goes up to and including $ffff
    }

    !if LOAD_VIA_KERNAL_FALLBACK {
            ;+BRANCH_IF_INSTALLED nodeploadf

        !if LOAD_UNDER_D000_DFFF {
            !if PLATFORM != COMMODORE_PLUS4 {
            lda #MEMCONFIG_ALL_RAM
            sta IO_PORT
            }
        }
        !if CHAINED_COMPD_FILES {
            jmp ++
--          jsr getckernal; skip load
            jsr getckernal; address
++          jsr decompress
            jsr READST; EOF reached?
            beq --
            ; close the file
        } else {
            jsr decompress
            ; ignore subsequent chained compressed files
        }
-           jsr getckernal
            bcc -
            bcs compdeof
nodeploadf:
    }; LOAD_VIA_KERNAL_FALLBACK

    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            lda #MEMCONFIG_ALL_RAM
            sta IO_PORT
        }
    }

    !if CHAINED_COMPD_FILES {

            jmp jsrdecomp

decomploop: jsr getc; skip load address
            jsr getc
jsrdecomp:  jsr decompress
        !if GETC_API {
            lda getcmemfin + $01
            cmp endaddrlo
            lda getcmemfin + $02
            sbc endaddrhi
        } else {
            lda getcmem + $01
            cmp endaddrlo
            lda getcmem + $02
            sbc endaddrhi
        }
            bcc decomploop; decompress all compressed sub-files that may be inside the compressed file
    } else {
            jsr decompress
    }; CHAINED_COMPD_FILES
            ; decompression is done

            lda getcmem + $02
            bne compdeof
            jsr getnewblk; handle special case that decompressing is as quick as loading,
                         ; this call will fetch the loading finished flag and ack loading

            ; loading and decompression is done
compdeof:   lda #OK
            clc; all okay

            ; fall through
maybethrow:
    !if LOAD_RAW_API | GETC_API | GETCHUNK_API | OPEN_FILE_POLL_BLOCK_API {
            ldy #$00
            beq erreofrts
    }
            ; throw exception
        !if GETCHUNK_API {
            ldx LASTSP
        } else {
stackpntr:  ldx #$00
        }
            txs
    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            ldx memconfig + $01
            stx IO_PORT
        }
    }
    !if DECOMPRESSOR = PUCRUNCH {
            bcs +
            ; return the execution address in x/y
            ldx lo + $01
            ldy hi + $01
+
    }
erreofrts:  rts

} else {; !LOAD_COMPD_API

    !if EXCEPTIONS {
maybethrow: ldy LASTPC + $01
            beq ++
            stx + + $01
            ; throw exception
            ldx LASTSP
            txs
        !if LOAD_UNDER_D000_DFFF {
            !if PLATFORM != COMMODORE_PLUS4 {
            ldx memconfig + $01
            stx IO_PORT
            }
        }
+          ldx #$00
++         rts
    }; EXCEPTIONS
}; LOAD_COMPD_API

!if NONBLOCKING_API{

NB_KERNAL_ON = LOAD_VIA_KERNAL_FALLBACK | NONBLOCKING_WITH_KERNAL_ON

            ; in:  x/y  - if FILESYSTEM = DIRECTORY_NAME, lo/hi to 0-terminated filename string
            ;             if FILESYSTEM = TRACK_SECTOR, a: track, x: sector
            ;      p4   - block fetch time slice when downloading a block in n*8 rasterlines ($00: get a whole block at a time) XXX TODO
            ;      p5   - main loop time slice when downloading a block in n*8 rasterlines (param4 = $00 or param = $00: none) XXX TODO
            ;      c    - if LOAD_TO_API != 0, c = 0: load to address as stored in the file
            ;                                 c = 1: load to caller-specified address (loadaddrlo/hi)
            ; out: c   - set on error
            ;      a   - status
!ifdef loadrawnb {
loadrawnb2:
} else {
loadrawnb:
}

            lda loadstatus
            eor #BUSY
            beq pnmi0 - $01

            lda #<(pnmi0)
    !if NB_KERNAL_ON {
            sta NMINV + $00
    }
            sta NMI_VECTORLO
            lda #>(pnmi0)
    !if NB_KERNAL_ON {
            sta NMINV + $01
    }
            sta NMI_VECTORHI

            jsr openfile
            bcc +
            sta loadstatus
            rts
+           txa
            pha
            php
            pla
            ldx #OPC_NOP
            and #FLAG_I
            bne +
            ; irqs are enabled
            lda #OPC_BNE
            ldx #OPC_CLI
            +SKIPWORD
+           lda #OPC_BIT_ZP; irqs are disabled
            sta pseiclisw1
            stx pseiclisw2
            lda #BUSY
            sta loadstatus
            lda #POLLINGINTERVAL_STARTLOAD - $01
            +SET_TIMER
            +ACK_TIMER_NMI
            +ENABLE_TIMER_NMI
            pla
            tax
            clc
            rts

pnmi0:      +BRANCH_IF_READY ppoll

    !if LOAD_VIA_KERNAL_FALLBACK {
            pha
            +BRANCH_IF_NOT_INSTALLED +
            pla
    }

            +ACK_TIMER_NMI
            rti


    !if LOAD_VIA_KERNAL_FALLBACK {
+           pla
    }
ppoll:      bit loadstatus
            bvc pnopoll - $00; only if bit 6 is set, some non-blocking operation is going on

            sta PACCUBUF
            lda #POLLINGINTERVAL_REPOLL - $01
            +SET_TIMER
            pla
            pha
            and #FLAG_I
pseiclisw1: bne pnopoll - $02; don't download the block yet if the i-flag was set, i.e., if an irq handler was being executed
pseiclisw2: cli; downloading the block may still be interrupted by irqs
            txa
            pha
            tya
            pha
            +DISABLE_TIMER_NMI
            jsr pollblock
            bcs pendload
            cmp #CHANGING_TRACK
            beq ptrkchan
            lda #<(pnmi1)
    !if NB_KERNAL_ON {
            sta NMINV + $00
    }
            sta NMI_VECTORLO
            lda #>(pnmi1)
    !if NB_KERNAL_ON {
            sta NMINV + $01
    }
            sta NMI_VECTORHI

            lda #POLLINGINTERVAL_GETBLOCK - $01
            +SKIPWORD
ptrkchan:   lda #POLLINGINTERVAL_TRACKCHANGE - $01
            +SET_TIMER
            +ACK_TIMER_NMI
            +ENABLE_TIMER_NMI

pcontlod:   pla
            tay
            pla
            tax
            lda PACCUBUF

pnopoll:    +ACK_TIMER_NMI
            rti
pendload:   sta loadstatus
            +DISABLE_TIMER_NMI
            bne pcontlod; JMP

pnmi1:      pha
            lda #<(pnmi0)
    !if NB_KERNAL_ON {
            sta NMINV + $00
    }
            sta NMI_VECTORLO
            lda #>(pnmi0)
    !if NB_KERNAL_ON {
            sta NMINV + $01
    }
            sta NMI_VECTORHI
            lda #POLLINGINTERVAL_BLOCKSOON - $01
            +SET_TIMER
            pla
            +ACK_TIMER_NMI
            rti
}; NONBLOCKING_API


            ; --- open a file ---
            ; in:  x - <(filename) or track (depends on FILESYSTEM setting in config.inc)
            ;      y - >(filename) or sector (depends on FILESYSTEM setting in config.inc)
            ;      c - if LOAD_TO_API != 0, c = 0: load to address as stored in the file
            ;                               c = 1: load to caller-specified address (loadaddrlo/hi)
            ; out: c - set on error
            ;      a - status

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the IO space at $d000 is enabled
!ifdef openfile {
openfile2:
} else {
openfile:
}

!if LOAD_ONCE=0 {
            sty BLOCKINDEX; parameter buffer
}

!if LOAD_TO_API {
            lda #OPC_STA_ZP
            bcc +
            lda #OPC_LDA_ZP
+           sta storeladrl
            sta storeladrh
}

!if LOAD_PROGRESS_API | BYTESTREAM {
            ldy #$00
    !if MAINTAIN_BYTES_LOADED {
            sty bytesloadedlo
            sty bytesloadedhi
    }
    !if BYTESTREAM {
        !if EXCEPTIONS & LOAD_COMPD_API & (LOAD_RAW_API | GETC_API | GETCHUNK_API | OPEN_FILE_POLL_BLOCK_API) {
            sty maybethrow + $01; return errors to the caller
        }
        !if GETC_API {
            sty getcmemfin + $01
            sty getcmemfin + $02
        } else {
            sty getcmem + $01
        }
            sty getcmem + $02
            sty getdbyte + $02
            sty blockindex + $01
            sty LASTBLKIDX
        !if GETCHUNK_API {
            sty LASTPC + $01; entry switch; return errors to the caller, no exceptions
            sty CHUNKSWTCH; switch the GETCHUNK_API routines on
            sty CHUNKENDLO
            sty CHUNKENDHI
        }
            dey
            sty YPNTRBUF
            lda #<getcload
            ldy #>getcload
            jsr puttoloadb
    }
}; LOAD_PROGRESS_API | BYTESTREAM

!if MEM_DECOMP_TO_API {
    !if DECOMPRESSOR = PUCRUNCH {
            lda #OPC_BIT_ZP
            sta storedadrl
            sta storedadrh
    } else {
            lda #OPC_STA_ZP
            sta storedadrl
            sta storedadrh
    }
}

!if LOAD_UNDER_D000_DFFF {
    !if PLATFORM != COMMODORE_PLUS4 {
            lda IO_PORT
            sta memconfig + $01
    }
}

!if LOAD_VIA_KERNAL_FALLBACK {
            +BRANCH_IF_INSTALLED nofallback

    !if BYTESTREAM {
            lda #<getckernal
            ldy #>getckernal
            jsr puttoloadb
    }
            ; loader is not installed,
            ; load via KERNAL calls
ldrnotinst:
    !if PLATFORM != COMMODORE_PLUS4 {
            lda IO_PORT
            sta kernaloff + $01
            lda #MEMCONFIG_IO_KERNAL
            sta IO_PORT
    }
    !if LOAD_ONCE {
            ldx #KERNALFILENO
            jsr CHKIN
            tay
            php
            bcc +
            cmp #KERNAL_FILENOTOPEN
            bne +
            jsr READST
            plp
            tax
            lda #FILE_NOT_OPEN
            rts
+           jsr READST
            plp
            tax
    } else {
            stx namestrpos + $00
        !if LOAD_PROGRESS_API | BYTESTREAM {
            ldy BLOCKINDEX; get buffered parameter
        }
            sty namestrpos + $01
            ldx #$ff
-           inx
namestrpos = *+$01
            lda $0000,x
            bne -
            txa
            pha; name length
            lda #KERNALFILENO
            ldx FA
            ldy #$00
            jsr SETLFS
            pla; name length
            ldx namestrpos + $00
            ldy namestrpos + $01
            jsr SETNAM
            jsr OPEN_KERNAL
            bcc +
            tax
            cmp #KERNAL_FILEOPEN
            bne +
            ; error
            lda #FILE_OPEN
            rts
+
    }; !LOAD_ONCE
            lda #GENERIC_KERNAL_ERROR
            bcc +
            ; error
            rts
+
            ldx #KERNALFILENO
            jsr CHKIN
            ; file not found is not detected at this point
            ; but the kernalgbyt function will return an error
            ; when trying to get the first file data byte
            ; (i.e., after "getting" the load address);
            ; the busy led will keep flashing
    !if LOAD_TO_API {
            lda #OPC_STA_ZP
            cmp storeladrl
            beq +
            lda loadaddrlo
            sta LOADDESTPTR + $00
            lda loadaddrhi
            sta LOADDESTPTR + $01
            jsr CHRIN; skip load
            jsr CHRIN; address
            jmp kernopenok
+
    }
            jsr CHRIN
kernalstrl: sta LOADDESTPTR + $00
            sta loadaddrlo
            jsr CHRIN
kernalstrh: sta LOADDESTPTR + $01
            sta loadaddrhi
kernopenok:
    !if GETC_API | GETCHUNK_API {
        !if PLATFORM != COMMODORE_PLUS4 {
            lda kernaloff + $01
            sta IO_PORT
        }
    }
            jmp fopenokay
nofallback:
}; LOAD_VIA_KERNAL_FALLBACK

            +WAKEUP

!if BYTESTREAM | END_ADDRESS_API {
            lda #$00
    !if BYTESTREAM {
            ldy #loadedtbend - loadedtb - 1
-           sta loadedtb,y; clear the bitfield denoting the blocks already loaded
            dey
            bpl -
    }
    !if END_ADDRESS {
           ;lda #$00
            sta endaddrlo
            sta endaddrhi
    }
}

!if !LOAD_ONCE {
            +WAITREADY

    !if FILESYSTEM = DIRECTORY_NAME {
            ; x still contains the lobyte/track function parameter
            stx BLOCKDESTLO; pointer hibyte is already stored at BLOCKINDEX = BLOCKDESTLO + 1
    }

    !if INSTALL_FROM_DISK {
        !if FILESYSTEM = TRACK_SECTOR {
            txa
            pha
        }
            lda #LOADFILE
            jsr sendbyte
            ldx #$07; XXX fix for plus4
-           dex
            bne -
            +WAITREADY
    }

    !if FILESYSTEM = DIRECTORY_NAME {

            ldy #$00
sendname:   lda (BLOCKDESTLO),y

            pha
        !if INSTALL_FROM_DISK {
            jsr sendbyte
        } else {
            +SENDBYTE
        }
            pla
            beq +
            iny
            cpy #FILENAME_MAXLENGTH
            bne sendname
+
    } else {
    	!if FILESYSTEM = TRACK_SECTOR {

        !if INSTALL_FROM_DISK {
            pla
        } else {
            txa
        }
            jsr sendbyte; send track
            lda BLOCKINDEX
            jsr sendbyte; send sector

    }}

    !if LOAD_VIA_KERNAL_FALLBACK {
            ; check whether the loader is still installed
            ldx #$07; XXX fix for plus4
-           dex
            bne -
            +BRANCH_IF_DATA_IN_CLEAR fopenokay

            ; if not, try again with kernal routines
            +SET_IO_KERNAL
            ldx BLOCKDESTLO + 0
            ldy BLOCKDESTLO + 1
            jmp ldrnotinst
    }

}; !LOAD_ONCE

fopenokay:  clc
returnok:   lda #OK; $00
            tax; file descriptor, always 0 since this loader
               ; only supports one open file at a time
dorts:
            rts


            ; --- poll for a block to download ---
            ; in:  nothing
            ; out: c - set on error or eof, cleared otherwise
            ;      a - status

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the IO space at $d000 is enabled
!ifdef pollblock {
pollblock2:
} else {
pollblock:
}

_pollblock:
!if LOAD_VIA_KERNAL_FALLBACK{
            +BRANCH_IF_INSTALLED getblnofbk

    !if GETC_API | GETCHUNK_API {
        !if PLATFORM != COMMODORE_PLUS4 {
            lda #MEMCONFIG_IO_KERNAL
            sta IO_PORT
        }
    }
            ldx #$fe
kernalgblk: jsr kernalgbyt
            bcc +
            cmp #EOF
            beq +++; carry is set on branch
            sec
            rts
+
    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            ldy #MEMCONFIG_ALL_RAM
            sty IO_PORT
        }
    }
            ldy #$00
            sta (LOADDESTPTR),y
    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            ldy #MEMCONFIG_IO_KERNAL
            sty IO_PORT
        }
    }
            inc LOADDESTPTR + $00
            bne +
            inc LOADDESTPTR + $01
+           dex
            bne kernalgblk
            clc
+++         lda #OK
            rts
getblnofbk:

}; LOAD_VIA_KERNAL_FALLBACK

!if OPEN_FILE_POLL_BLOCK_API {
            lda #FILE_NOT_OPEN
            +BRANCH_IF_IDLE dorts; will set the carry flag if branching
}; OPEN_FILE_POLL_BLOCK_API

!if OPEN_FILE_POLL_BLOCK_API | NONBLOCKING_API {
            jsr getblock
            bcs evalerr
            lda #CHANGING_TRACK
           ;clc
            +BRANCH_IF_CHANGING_TRACK dorts
           ;clc
            bcc returnok; jmp
} else {; !(OPEN_FILE_POLL_BLOCK_API | NONBLOCKING_API)
            jsr getblock
            bcc returnok
}; !(OPEN_FILE_POLL_BLOCK_API | NONBLOCKING_API)

            ; accu is DEVICE_NOT_PRESENT_LOD ($00), LOAD_FINISHED ($fe, file loaded successfully), or LOAD_ERROR ($ff, file not found or illegal t or s) here
evalerr:
            +IDLE

            cmp #LOAD_FINISHED
            beq returnok; returns with carry set
            clc
            ; accu = $ff (LOAD_ERROR) -> INVALID_PARAMETERS
            ; accu = $00 (DEVICE_NOT_PRESENT_LOD) -> DEVICE_NOT_PRESENT
            adc #DEVICE_NOT_PRESENT
pollfail:   sec
            rts

            !if DEVICE_NOT_PRESENT - INVALID_PARAMETERS = 1 {
            } else {
            	!error, "Error: Invalid code optimization"
	    }
            
getblock:   lda #DEVICE_NOT_PRESENT_LOD
            +PREPARE_ATN_STROBE
            +WAITREADY
            bmi pollfail; branch if device not present

            +BEGIN_ATN_STROBE
!if PLATFORM = COMMODORE_PLUS4 {
            jsr getblkrts; rts - waste some time to make sure the drive is ready
} else {
            jsr getblkrts - $01; clc : rts - waste some time to make sure the drive is ready
}
            +END_ATN_STROBE

            jsr getbyte; get block index or error/eof code
            sta BLOCKINDEX

!if BYTESTREAM {
            jsr loadedsub
            ora loadedtb,y
            sta loadedtb,y; mark this block as loaded
            lda BLOCKINDEX
}
            bne not1stblk

            ; first block: get load address
            jsr getbyte; block size
            pha
            jsr getbyte; load address lo
storeladrl: sta loadaddrlo; is changed to lda on load_to
            sta BLOCKDESTLO
            jsr getbyte; load address hi
storeladrh: sta loadaddrhi; is changed to lda on load_to
            sta storebyte + $02
            pla
            sec
            sbc #$02
            bcs fin1stblk; jmp

not1stblk:  cmp #SPECIAL_BLOCK_NOS; check for special block numbers: LOAD_FINISHED ($fe, loading finished successfully), LOAD_ERROR ($ff)
!if LOAD_PROGRESS_API | END_ADDRESS {
	    bcc +
            jmp polldone
+
} else {
            bcs polldone
}

            ; calculate the position in memory according to the block number,
            ; this is performing: pos = loadaddr + blockindex * 254 - 2
            lda loadaddrlo
           ;clc
            sbc BLOCKINDEX
            php
            clc
            sbc BLOCKINDEX
            sta BLOCKDESTLO
            lda loadaddrhi
            adc BLOCKINDEX
            plp
            sbc #$01
            sta storebyte + $02

            jsr getbyte; get block size

fin1stblk:

!if MAINTAIN_BYTES_LOADED {
            pha; block size - 1
            sec
            adc bytesloadedlo
            sta bytesloadedlo
            bcc +
            inc bytesloadedhi
+           pla; block size - 1
}
            ; a contains block size - 1
    !if BYTESTREAM {
            sta blocksize
    }

!if LOAD_UNDER_D000_DFFF {
    !if PLATFORM != COMMODORE_PLUS4 {
            ldy #OPC_LDX_IMM; enable getbyte loop
    } else {
            ldy #OPC_STA_ABSY; enable getbyte loop
    }
} else {
            ldy #OPC_STA_ABSY; enable getbyte loop
}
            +SKIPWORD
getbyte:    ldy #OPC_RTS; disable getbyte loop
            sty blockrts

            tax
            eor #$ff
!if LOAD_UNDER_D000_DFFF {
    !if PLATFORM != COMMODORE_PLUS4 {
            cpy #OPC_LDX_IMM
    } else {
            cpy #OPC_STA_ABSY
    }
} else {
            cpy #OPC_STA_ABSY
}
            bne getblkloop
            tay
           ;sec
            txa
            adc BLOCKDESTLO
            sta storebyte + $01
            bcs getblkloop
            dec storebyte + $02

getblkloop: +GETBYTE
blockrts:
!if LOAD_UNDER_D000_DFFF {
    !if PLATFORM != COMMODORE_PLUS4 {
            ldx #MEMCONFIG_ALL_RAM
            stx IO_PORT
    }
}
storebyte:  sta $0000,y
!if LOAD_UNDER_D000_DFFF {
    !if PLATFORM != COMMODORE_PLUS4 {
memconfig:  ldx #MEMCONFIG_IO
            stx IO_PORT
    }
}
            iny
            bne getblkloop

;.if .defined(DYNLINKLDR) & (!(.defined(DYNLINK)))
;            .assert >(* + 1) = >(getblkloop), warning, "***** Performance warning: Page boundary crossing (getblkloop). Please relocate the DISKIO segment a few bytes up or down. *****"
;}

!if END_ADDRESS {

            ldx storebyte + $01
            cpx endaddrlo
            ldy storebyte + $02
            iny
            tya
            sbc endaddrhi
            bcc +
            stx endaddrlo
            sty endaddrhi
+
}
            clc; ok

polldone:   +ENDGETBYTE
getblkrts:  rts

!if INSTALL_FROM_DISK | (FILESYSTEM != DIRECTORY_NAME) {
sendbyte:   +SENDBYTE
            rts
}

!if (!GETC_API) & (GETCHUNK_API | LOAD_COMPD_API) & CHAINED_COMPD_FILES {
getc:
    !if DECOMPRESSOR = PUCRUNCH {
            jmp (toloadbt + $01)
    } else {
            jmp (toloadb0 + $01)
    }
}

!if GETC_API {

            ; --- get a byte from the file stream ---
            ; in:  nothing
            ; out: a - value if c = 0, status otherwise
            ;      c - error or EOF

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the
            ; IO space at $d000 is disabled if data is accessed at $d000..$dfff
    !ifdef getc {
getc2:
    } else {
getc:
    }

getcjmp:    jmp getcload
}; GETC_API


!if GETCHUNK_API {

            ; --- get a chunk from the file stream ---
            ; in:  x/y   - lo/hi of chunk size
            ; out: a     - status
            ;      x/y   - lo/hi of chunk address
            ;      p4/p5 - lo/hi of retrieved chunk size
            ;      c     - set on error

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the
            ; IO space at $d000 is disabled if data is accessed at $d000..$dfff
    !ifdef getchunk {
getchunk2:
    } else {
getchunk:
    }
            clc
            txa
            adc CHUNKENDLO
            sta CHUNKENDLO
            tya
            adc CHUNKENDHI
            sta CHUNKENDHI
            jmp decompress
}; GETCHUNK_API

!if LOAD_VIA_KERNAL_FALLBACK {

            ; get a byte from the file's byte-stream using the KERNAL API
            ; sets ram configuration and buffers the y register
    !if BYTESTREAM {
getckernal:
        !if LOAD_UNDER_D000_DFFF | (GETC_API | GETCHUNK_API) {
            !if PLATFORM != COMMODORE_PLUS4 {
            lda #MEMCONFIG_IO_KERNAL
            sta IO_PORT
            }
        }
            sty LOADYBUF
            jsr kernalgbyt
        !if LOAD_UNDER_D000_DFFF {
            !if PLATFORM != COMMODORE_PLUS4 {
            ldy memconfig + $01
            sty IO_PORT
            }
        } else {
            !if GETC_API | GETCHUNK_API {
            !if PLATFORM != COMMODORE_PLUS4 {
            ldy #MEMCONFIG_IO
            sty IO_PORT
            }
        }}
            ldy LOADYBUF
            rts
    }; BYTESTREAM

            ; get a byte from the file using the KERNAL API
            ; in  : nothing
            ; out : a - status on error
            ;     : c - set on error
kernalgbyt: jsr READST; get KERNAL status byte
            bne +
    !if MAINTAIN_BYTES_LOADED {
            inc bytesloadedlo
            bne ++++
            inc bytesloadedhi
++++
    }
            jsr CHRIN
            clc
            rts
+           pha; KERNAL status byte
            lda #KERNALFILENO
            jsr CLOSE
            jsr CLRCHN
    !if END_ADDRESS_API {
        !if MAINTAIN_BYTES_LOADED {
            clc
            lda bytesloadedlo
            adc loadaddrlo
            sta endaddrlo
            lda bytesloadedhi
            adc loadaddrhi
            sta endaddrhi
        } else {
            lda LOADDESTPTR + $00
            sta endaddrlo
            lda LOADDESTPTR + $01
            sta endaddrhi
        }
    }
    !if PLATFORM != COMMODORE_PLUS4 {
kernaloff:  lda #$00
            sta IO_PORT
    }
            pla; KERNAL status byte
            cmp #KERNAL_STATUS_EOF
            bne kernalerr
            ; EOF
            lda #EOF
           ;sec
            rts
kernalerr: sec
            tax
            bmi ++
            and #KERNAL_STATUS_FILE_NOT_FOUND
            beq +
    !if EXCEPTIONS {
            lda #INVALID_PARAMETERS
            +SKIPWORD
+           lda #GENERIC_KERNAL_ERROR
            +SKIPWORD
++          lda #DEVICE_NOT_PRESENT
            jmp maybethrow
    } else {
            lda #INVALID_PARAMETERS
            rts
+           lda #GENERIC_KERNAL_ERROR
            rts
++          lda #DEVICE_NOT_PRESENT
            rts
    }; BYTESTREAM
}; LOAD_VIA_KERNAL_FALLBACK

!if BYTESTREAM | HAS_DECOMPRESSOR {
            ; get a byte from the file's byte-stream, read from memory

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the
            ; IO space at $d000 is disabled if data is accessed at $d000..$dfff
getcmem:    lda $0000
    !if GETC_API {
            clc
    }
            inc getcmem + $01
            beq getcmeminc
            rts; one extra byte for one less cycle

getcmeminc: inc getcmem + $02
    !if GETC_API {
            sty LOADYBUF
getcmemchk: pha
            php
            lda getcmem + $02
            cmp endaddrhi
            bne ++
            sta getcmemfin + $02
           ;sec
            lda endaddrlo
            sbc getcmem + $01
            beq setcmemeof
            sta YPNTRBUF
            lda getcmem + $01
            sta getcmemfin + $01
            lda #<getcmemfin
            ldy #>getcmemfin
--          jsr puttoloadb
++          ldy LOADYBUF
            plp
            pla
            rts

getcmemfin: lda $0000
            inc getcmemfin + $01
            clc
            dec YPNTRBUF
            bne getcmemfin - $01
            pha
            php
            sty LOADYBUF
setcmemeof: lda #<getcmemeof
            ldy #>getcmemeof
            bne --; jmp

getcmemeof:
        !if LOAD_UNDER_D000_DFFF {
            !if PLATFORM != COMMODORE_PLUS4 {
            lda memconfig + $01
            sta IO_PORT
            }
        }
            lda #EOF
            sec
    }; GETC_API
            rts
}; BYTESTREAM | HAS_DECOMPRESSOR

!if BYTESTREAM {
            ; get a byte from the file's byte-stream, download a file block before if possible

            ; when LOAD_UNDER_D000_DFFF is non-0, this call assumes that the
            ; IO space at $d000 is disabled if data is accessed at $d000..$dfff
getcload:   sty LOADYBUF

getcload2:  ldy YPNTRBUF
getdbyte:   lda $0000,y
    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            ldy #MEMCONFIG_IO
            sty IO_PORT
        }
    }
            inc YPNTRBUF
            beq blockindex; branch to process next stream block
            +BRANCH_IF_READY getnewblk; download block as soon as possible

    !if GETC_API {
            clc
    }
loadbytret:
    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            ldy #MEMCONFIG_ALL_RAM
            !if GETC_API {
            bcc +
            ldy memconfig + $01
+
            }
            sty IO_PORT
        }
    }
            ldy LOADYBUF
            rts

firstblock: ; set stream buffer pointers
            lda #$ff
            eor blocksize
            sta YPNTRBUF
            lda storebyte + $01
            sta getdbyte + $01
            lda storebyte + $02
            sta getdbyte + $02
            inc blockindex + $01; first block has been downloaded,
                                ; set flag to skip waiting for the next block download

    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            lda #MEMCONFIG_ALL_RAM
            sta IO_PORT
        }
    }
            bne getcload2; jmp: return first file byte and maybe download more blocks before that

blockindex: ldy #$00; block index and flag to skip waiting for the next block to download,
                    ; the value is increased for every loaded block and set to $ff after loading is finished
            stx xbuf + $02
            bne chkloaded

            ; first block
            +SKIPBYTE
waitforblk: pla; current stream byte
            jsr getnewblk2
            bcs xbuf + $01; branch on error
    !if LOAD_UNDER_D000_DFFF {
        !if PLATFORM != COMMODORE_PLUS4 {
            ldy #MEMCONFIG_IO
            sty IO_PORT
        }
    }
            ldy blockindex + $01
            beq firstblock

chkloaded:  pha; current stream byte
            tya
            iny; block index
            beq xbuf - $01; $ff = last block had been loaded already, clear carry - okay
            jsr loadedsub
            and loadedtb,y
            beq waitforblk; branch if the next block in the stream is not yet loaded

            ; advance stream pointer
            ; this is not done after the first file block had been downloaded
            lda #$fd
            ldy blockindex + $01
            cpy LASTBLKIDX
            bne +
           ;sec
            lda LASTBLKSIZ
+           tax
            sec
            adc getdbyte + $01
            sta getdbyte + $01
            bcc +
            inc getdbyte + $02
+           lda getdbyte + $02

            txa
            eor #$ff
            sta YPNTRBUF

            inc blockindex + $01
            bne xbuf - $01; jmp; clear carry - okay

getnewblk:  stx xbuf + $02
getnewblk2: pha; current stream byte
            jsr getblock
            bcs gotstatus; branch if block number is DEVICE_NOT_PRESENT_LOD ($00), LOAD_FINISHED ($fe), LOAD_ERROR ($ff, file not found or illegal t or s)

            lda BLOCKINDEX; update last
            cmp LASTBLKIDX; block index
            bcc xbuf + $00; and size
            sta LASTBLKIDX
blocksize = * + 1
            lda #$00
            sta LASTBLKSIZ

            clc; okay
xbuf:       pla; current stream byte
            ldx #$00
    !if LOAD_UNDER_D000_DFFF | GETC_API {
            jmp loadbytret
    } else {
            ldy LOADYBUF
            rts
    }

            ; the status byte has been received, end loading
gotstatus:  pha; DEVICE_NOT_PRESENT_LOD ($00), LOAD_FINISHED ($fe), or LOAD_ERROR ($ff, file not found or illegal t or s)

            ; switch to memory-read only getc routine
            clc
            lda YPNTRBUF
            adc getdbyte + $01
            sta getcmem + $01; current stream buffer position lo
    !if GETC_API {
            sta getcmemfin + $01
    }
            lda YPNTRBUF
            bne +
            sec
+           lda #$00
            adc getdbyte + $02; current stream buffer position hi
    !if GETC_API {
            sta getcmemfin + $02
    }
            jsr setgetcmem

            lda #$ff
            sta blockindex + $01; mark load finished

            pla; DEVICE_NOT_PRESENT_LOD ($00), LOAD_FINISHED ($fe), or LOAD_ERROR ($ff, file not found or illegal t or s)
            jsr evalerr ; if accu is $00 (device not present), or $ff (file not found or illegal t or s),
                        ; return with an error, otherwise continue
            bcc xbuf + $00
            ; error or EOF
            cmp #OK
    !if GETC_API {
           ;sec
            bne +
            ; EOF
            pla; current stream byte
            clc; okay
            ldx xbuf + $02
            jmp getcmemchk
+
    } else {
            beq xbuf - $01; clear carry: okay
    }
            ; an error occured, stop loading and/or decompressing, return error to the caller,
            ; a = status
           ;sec
    !if (GETC_API | GETCHUNK_API) {
            ; the current stream byte is still on the stack
            tsx
            inx
            txs
    }
    !if EXCEPTIONS {
            jmp maybethrow
    } else {
            rts
    }

loadedsub:  tay
            and #%00000111
            tax
            tya
            lsr
            lsr
            lsr
            tay
            lda loadedor,x
            rts

loadedor:   !byte $80,$40,$20,$10; or-values for the bitfield
            !byte $08,$04,$02,$01

loadedtb:   !fill 32,0; bitfield for already-loaded blocks, 256 bits for 64 kB minus 514 (256*2 + 2) bytes of memory
loadedtbend:
}; BYTESTREAM

!if MEM_DECOMP_API {

            ; --- decompress a compressed file from memory ---
            ; in:  a   - compression method (ignored)
            ;      x/y - lo/hi of compressed file in memory
            ;      c   - if MEMDECOMP_TO_API != 0, c = 0: decompress to address as stored in the file
            ;                                      c = 1: decompress to caller-specified address (loadaddrlo/hi)

            ; out: undefined
!ifdef memdecomp {
memdecomp2:
} else {
memdecomp:
}
            stx getcmem + $01
    !if BYTESTREAM {
            tya
            jsr setgetcmem
        !if GETC_API {
            lda #$00
            sta endaddrhi
        }
    } else {
            sty getcmem + $02
    }

    !if CHAINED_COMPD_FILES {
            jmp +


            ; --- decompress a chained compressed file from memory ---
            ; --- use this for subsequent decompression in a chained compressed file ---
            ; in:  a - compression method (ignored)
            ;      c - if MEMDECOMP_TO_API != 0, c = 0: decompress to address as stored in the file
            ;                                    c = 1: decompress to caller-specified address (loadaddrlo/hi)
            ; out: undefined
!ifdef cmemdecomp {
cmemdecomp2:
} else {
cmemdecomp:
}

        !if GETC_API {
            lda #$00
            sta endaddrhi
        }
            jsr getcmem; skip load address,
            jsr getcmem; the state of the c-flag is preserved
+
    }; CHAINED_COMPD_FILES

    !if MEM_DECOMP_TO_API {
        !if DECOMPRESSOR = PUCRUNCH {
            lda #OPC_BIT_ZP
            bcc +
            lda #OPC_LDA_ZP
+           sta storedadrl
            sta storedadrh
        } else {
            lda #OPC_STA_ZP
            bcc +
            lda #OPC_LDA_ZP
+           sta storedadrl
            sta storedadrh
        }
    }

    !if GETCHUNK_API {
            lda #$ff
            sta CHUNKSWTCH; switch the GETCHUNK_API routines off
            sta CHUNKENDLO; probably fails if the decompressed
            sta CHUNKENDHI; data goes up to and including $ffff
                          ; (but this probably happens anyways because of the in-place depacking data offset of ~3 bytes,
                          ; making the compressed data load to $00, $01 and the zeropage after destination pointer wrap)
    }

    !if DECOMPRESSOR = PUCRUNCH {
            jsr decompress
            ; return the execution address in x/y
            ldx lo + $01
            ldy hi + $01
            rts
    } else {
            jmp decompress
    }

}; MEM_DECOMP_API

!if UNINSTALL_API {

            ; --- uninstall the loader ---
            ; in:  nothing
            ; out: undefined
    !ifdef uninstall {
uninstall2:
    } else {
uninstall:
    }
            +DO_UNINSTALL
            rts
}; UNINSTALL_API

!if HAS_DECOMPRESSOR {

    !if DECOMPRESSOR = PUCRUNCH {
        chunkdestlo = OUTPOS + $00
        chunkdesthi = OUTPOS + $01
    } else {
        chunkdestlo = decdestlo
        chunkdesthi = decdesthi
    }

    !macro CHUNKENTRY {
        !if GETCHUNK_API {
            !if LOAD_VIA_KERNAL_FALLBACK {
                !if PLATFORM != COMMODORE_PLUS4 {
            lda IO_PORT
            sta kernaloff + $01
                }
            }
            lda CHUNKSWTCH
            bne begindecomp
            tsx
            stx LASTSP
            lda LASTPC + $01
            beq begindecomp
            lda CHUNKENDLO
            cmp chunkdestlo
            lda CHUNKENDHI
            sbc chunkdesthi
	    bcs +
            jmp chunkret; branch if the desired chunk is already available
+           +CHUNKRESTORE
            jmp (LASTPC)
begindecomp:
            ; throw exception on stream error
            sec
            !if LOAD_COMPD_API {
            rol maybethrow + $01
            } else {
            rol LASTPC + $01
            }
        }
    }

    !macro CHUNKSETUP {
        !if GETCHUNK_API {
            lda CHUNKSWTCH
            bne nochunksetup
            clc
            lda chunkdestlo
            sta CHUNKBEGLO
            adc CHUNKENDLO
            sta CHUNKENDLO
            lda chunkdesthi
            sta CHUNKBEGHI
            adc CHUNKENDHI
            sta CHUNKENDHI
nochunksetup:
        }
    }

    !macro CHUNKCHECK {
        !if GETCHUNK_API {
            ;.local notcomplet

            lda CHUNKENDLO
            cmp chunkdestlo
            lda CHUNKENDHI
            sbc chunkdesthi
            !if DECOMPRESSOR = PUCRUNCH {
            lda #$00; the z-flag needs to be set
            }
            bcs .notcomplet; branch if chunk not complete yet
            jsr chunkout; return chunk
.notcomplet:
        }
    }

    !macro CHUNKEOF {
        !if GETCHUNK_API {
            jmp chunkeof
        }
    }

    !macro CHUNKSUB {
    	!zone CHUNKSUB_zone {
        !if GETCHUNK_API {
chunkout:   +CHUNKBACKUP
            clc
            pla
            adc #$01
            sta LASTPC + $00
            pla
            adc #$00
            sta LASTPC + $01
            ldx LASTSP
            txs
chunkret:   sec
            lda CHUNKENDLO
            sbc CHUNKBEGLO
            sta param4
            lda CHUNKENDHI
            sbc CHUNKBEGHI
            sta param5
            ldx CHUNKBEGLO
            ldy CHUNKBEGHI
            lda CHUNKENDLO
            sta CHUNKBEGLO
            lda CHUNKENDHI
            sta CHUNKBEGHI
            !if LOAD_VIA_KERNAL_FALLBACK {
                !if PLATFORM != COMMODORE_PLUS4 {
            lda kernaloff + $01
            sta IO_PORT
                }
            }
            lda #OK
            clc
            rts

chunkeof:   lda CHUNKSWTCH
            bne .chunkok

            !if CHAINED_COMPD_FILES {

                !if LOAD_VIA_KERNAL_FALLBACK {
            ;.local closefile
            ;.local nofallback

                    !if LOAD_UNDER_D000_DFFF {
                        !if PLATFORM != COMMODORE_PLUS4 {
            lda #MEMCONFIG_IO_KERNAL
            sta IO_PORT
                        }
                    }

            +BRANCH_IF_INSTALLED .nofallback

            jsr getc
            bcc .skiploadad
            cmp #EOF
            beq .chunkend
                !if PLATFORM != COMMODORE_PLUS4 {
                    !if LOAD_UNDER_D000_DFFF {
            ldy memconfig + $01
            sty IO_PORT
                    } else {
            ldy kernaloff + $01
            sty IO_PORT
                    }
                }
            sec
            rts
.nofallback:
                }; LOAD_VIA_KERNAL_FALLBACK

            ;.local skiploadad

                !if GETC_API {
            lda getcmemfin + $01
            cmp endaddrlo
            lda getcmemfin + $02
            sbc endaddrhi
                } else {
            lda getcmem + $01
            cmp endaddrlo
            lda getcmem + $02
            sbc endaddrhi
                }; GET_API
            bcs .chunkend

            jsr getc; skip load
.skiploadad: jsr getc; address

            lda #$00
            sta CHUNKENDLO
            sta CHUNKENDHI
            sta LASTPC + $01
            beq .chunkchain

            }; CHAINED_COMPD_FILES

.chunkend:   lda #<.dochunkeof
            sta LASTPC + $00
            lda #>.dochunkeof
            sta LASTPC + $01
.chunkchain: sec
            lda chunkdestlo
            sbc CHUNKBEGLO
            sta param4
            lda chunkdesthi
            sbc CHUNKBEGHI
            sta param5
.chunknext:  ldx CHUNKBEGLO
            ldy CHUNKBEGHI
.chunkok:    lda #OK
            clc
            rts
.dochunkeof:
            !if LOAD_VIA_KERNAL_FALLBACK {
            ;.local closefile
            ;.local installed

                !if LOAD_UNDER_D000_DFFF {
                    !if PLATFORM != COMMODORE_PLUS4 {
            lda #MEMCONFIG_IO_KERNAL
            sta IO_PORT
                    }
                }

            +BRANCH_IF_INSTALLED .installed
.closefile:  jsr getckernal
            bcc .closefile
            +SKIPWORD
.installed:  lda #EOF
                !if LOAD_UNDER_D000_DFFF {
                    !if PLATFORM != COMMODORE_PLUS4 {
            ldy memconfig + $01
            sty IO_PORT
                    }
                }
            sec
            rts

            } else {
            lda #EOF
            sec
            rts
            }; LOAD_VIA_KERNAL_FALLBACK

        }; GETCHUNK_API
	}
    }; CHUNKSUB

    !if DECOMPRESSOR = PUCRUNCH {

        !macro CHUNKBACKUP {
        }

        !macro CHUNKRESTORE {
            lda #$00; the z-flag needs to be set
        }

        !src "pudecomp.s"

    } else {
	!if DECOMPRESSOR = BYTEBOOZER {

        !macro CHUNKBACKUP {
            sty LASTYREG
        }

        !macro CHUNKRESTORE {
            ldy LASTYREG
        }

        !src "bbdecomp.s"

    } else {
	!if DECOMPRESSOR = LEVELCRUSH {

        !macro CHUNKBACKUP {
            stx LASTXREG
        }

        !macro CHUNKRESTORE {
            ldx LASTXREG
        }

        !src "lcdecomp.s"

    } else {
	!if DECOMPRESSOR = EXOMIZER {

        !macro CHUNKBACKUP {
        }

        !macro CHUNKRESTORE {
            ldx #$00
            ldy #$00
        }

        FORWARD_DECRUNCHING = 1

        get_crunched_byte = getcmem

        !src "exodecomp.s"

        decompress = decrunch

    } else {
        !error "***** Error: The selected decompressor option is not yet implemented. *****"
    }}}}

        +CHUNKSUB
}

!if BYTESTREAM | HAS_DECOMPRESSOR {
setgetcmem: sta getcmem + $02
            lda #<getcmem
            ldy #>getcmem

            ; patch the various calls to the getchar routines,
            ; one out of five functions is used:
            ; getcmem    - get a char from memory after the whole file is loaded
            ; getcmemfin - get a char from the last file block in memory
            ; getcmemeof - get EOF
            ; getcload   - get a char and before that, download a file block if possible/necessary
            ; getckernal - get a char when using the KERNAL API as fallback
puttoloadb:
    !if GETC_API {
            sta getcjmp + $01
            sty getcjmp + $02
    }
    !if DECOMPRESSOR != NONE {
            +SETDECOMPGETBYTE
    }
            rts
}; YTESTREAM | HAS_DECOMPRESSOR

!ifdef DYNLINK {
} else {
    !if LOAD_VIA_KERNAL_FALLBACK {
        !if (* <= $d000) {
	} else {
	!error  "***** Error: The resident code must not exceed $d000, please make sure the DISKIO segment ends below $d000. *****"
	}
    } else {
        !if(* <= $d000) | (* >= $e000) {
	} else {
	 !error, "***** Error: The resident code must not reside at $d000..$dfff, please make sure the DISKIO segment does not overlap with that memory range. *****"  
	}
    }
}

