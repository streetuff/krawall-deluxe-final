
        !cpu 6510

!src "loader.inc"
!src "cpu.inc"
!src "via.inc"

!ifdef STANDALONE {
    __NO_LOADER_SYMBOLS_IMPORT = 1

    ;.export LOWMEMEND  : absolute
    ;.export RUNMODULE  : absolute
    ;.export checkchg   : absolute
    ;.export ddliteon   : absolute
    ;.export decodebyte : absolute
    ;.export dgetbyte   : absolute
    ;.export drivebusy  : absolute
    ;.export driveidle  : absolute
    ;.export dsctcmps   : absolute
    ;.export gcrencode  : absolute
    ;.export getblkchid : absolute
    ;.export getblkstid : absolute
    ;.export getblcurts : absolute
    ;.export getblcurtr : absolute
    ;.export getblkscan : absolute
    ;.export trackseek  : absolute
    ;.export getnumscts : absolute
    ;.export duninstall : absolute
} else {; STANDALONE
    ;.export MAXTRACK41
}; !STANDALONE

!src "loader-kernel1541.inc"

!if UNINSTALL_RUNS_DINSTALL {
    !ifdef STANDALONE {
    } else {
        ;.export drvcodebeg41 : absolute
    }
    ;.export drvprgend41 : absolute

    DINSTALLBUFFER = .KERNEL - drvprgend41 + dinstall41
}; UNINSTALL_RUNS_DINSTALL


!macro GETBYTE_IMPL_COMMON {
            ; must not trash x
            sta VIA1_PRB
            ldy #DATA_OUT | DATA_IN
--          cpy VIA1_PRB; wait for CLK high
            bcs --
            ldy VIA1_PRB
            cpy #CLK_IN | DATA_IN
            ror
-           cpy VIA1_PRB; wait for CLK low
            beq -
            ldy VIA1_PRB
            cpy #DATA_IN
            ror
            bcc --
}

!macro GETBYTE_IMPL_NOWDOG {
            lda #%10000000; CLK OUT lo: drive is ready
            +GETBYTE_IMPL_COMMON
}

!macro GETBYTE_IMPL {
            lda #%10000000; CLK OUT lo: drive is ready
            sta VIA2_T1C_H; reset watchdog time-out
            +GETBYTE_IMPL_COMMON
}

            !pseudopc $001a {

!ifdef STANDALONE {
            !word * + 2
}; STANDALONE

            ;assert * > ROMOS_HEADER_SECTOR, error, "***** 1541 drivecode starts too low in memory. *****"
	    !if * > ROMOS_HEADER_SECTOR {
	    } else {
		!error, "***** 1541 drivecode starts too low in memory. *****"
	    }
drvcodebeg41:

!ifdef STANDALONE {
} else {
            !byte >(drvcodeend41 - * + $0100 - $01); init transfer count hi-byte
            !byte >(drvprgend41 - * + $0100 - $01); init transfer count hi-byte for re-install
}; !STANDALONE

.dcodinit:

!if UNINSTALL_RUNS_DINSTALL = 0 {
            ; before loading the first file, the current track number is
            ; retrieved by reading any block header on the disk -
            ; however, if the loader is uninstalled before loading anything,
            ; it needs to know the more or less correct current track number
            ; in order to seek to track 18 before reset
            lda ROMOS_HEADER_TRACK
            sta CURTRACK

            lda #T1_IRQ_ON_LOAD | PA_LATCHING_ENABLE; watchdog irq: count phi2 pulses, one-shot;
                                                    ; enable port a latching to grab one gcr byte at a time
                                                    ; rather than letting the gcr bitstream scroll through
                                                    ; port a (applies to 1541 and Oceanic OC-118, but not
                                                    ; 1541-II)
            sta VIA2_ACR
            lda #READ_MODE | BYTE_SYNC_ENABLE
            sta VIA2_PCR

            ; watchdog initialization

            lda #$00
    !if INSTALL_FROM_DISK {
            ldx #JOBCODE0400 - JOBCODE0400 + $80
    } else {
            ldx #JOBCODE0600 - JOBCODE0400 + $80
    }
-           sta <(JOBCODE0400 - $80),x; clear job queue
            dex
            bmi -

           ;ldx #IRQ_CLEAR_FLAGS | $7f; $7f
            stx VIA1_IER; no irqs from via 1
            stx VIA2_IER; no irqs from via 2

            lda #IRQ_SET_FLAGS | IRQ_TIMER_1
            sta VIA2_IER; timer 1 irqs from via 2

            lda #JOBCODE_EXECUTE
            sta JOBCODE0300; execute watchdog handler at $0300 on watchdog time-out

            lda #NUMMAXSECTORS
}; !UNINSTALL_RUNS_DINSTALL

            sta NUMSECTORS
            stx LOADEDMODULE
            stx DISKCHANGED

            lda #CLK_OUT | CLK_IN | DATA_IN | ATN_IN
-           cmp VIA1_PRB
            bne -; no watchdog

            ; before spinning up the motor and finding the current track,
            ; wait until a file is requested to be loaded at all

            ; fade off the busy led if lit
            lda VIA2_PRB
            and #BUSY_LED
            beq +
            lda #$ff
+           tax
-           jsr .lightsub
            lda VIA1_PRB
            cmp #CLK_OUT | CLK_IN | DATA_IN | ATN_IN
            beq -; wait until there is something to do

            ; check for reset or uninstallation
            cmp #CLK_OUT | CLK_IN | DATA_IN
            beq +
            jmp .duninstall
+
            lda #VIA_WRITE_PROTECT
            and VIA2_PRB
            sta DISKCHANGEBUFFER; store light sensor state for disk removal detection

            ldy #$0f

            ;assert * >= SENDGCRTABLE + $1f, error, "***** 'mkgcrdec' overwrites itself. *****"
	    !if (* >= SENDGCRTABLE + $1f) {
	    } else {
		!error "***** 'mkgcrdec' overwrites itself. *****"
	    }
.mkgcrdec:   lda .sendgcrraw,y
            ldx GCRENCODE,y
            sta SENDGCRTABLE,x
            dey
            bpl .mkgcrdec

            lda LINKTRACK
            bne .findtrackn; branch if the drive had already seeked before the loader has been started
            ; the drive was reset immediately before running the loader -
            ; step down a track: this works normally if the stepping bits are congruent with the stepper motor;
            ; however, it may happen that the bits are unaligned (opposite to the actual stepper position, bit 1
            ; reversed), this alone does not move the head but stepping makes it go into the direction opposite to
            ; the one desired when moving; the stepping down two halftracks will actually step up and step down one
            ; halftrack each and thus will end up on the same track as before, but align the stepper bits to the motor.
            ldx #$02
            stx CURTRACK
            dex
            jsr .trackseekx

            ; find current track number
            ; this assumes the head is on a valid half track
.findtrackn: lda #-$01; invalid track number -> no track step
            ldy #ANYSECTOR
            jsr .getblkstid; no sector link sanity check, set CURTRACK
            bcs +

            lda GCRBUFFER + $04
            lsr
            asl GCRBUFFER + $03
            rol
            asl GCRBUFFER + $03
            rol
            asl GCRBUFFER + $03
            rol
            and #GCR_NIBBLE_MASK
            tay
            lda GCRBUFFER + $03
            jsr .decodesub; track
            cmp CURTRACK; getblkstid sets CURTRACK at this stage,
                        ; the value is inferred by eoring the header checksum
                        ; and all header field values except the current track
            beq .initdone
+           clc
            lda #1 << BITRATE_SHIFT
            adc VIA2_PRB
            sta VIA2_PRB; cycle through the 4 bit-rates
            bne .findtrackn; jmp

.initdone:   lda #OPC_EOR_ZP
            sta .headerchk - $02
            lda #OPC_BNE
            sta .headerchk
            lda #OPC_STA_ABS
            sta .putbitrate
            lda #OPC_STX_ZP
            sta .putnumscts
            jmp .setbitrate; returns to runmodule

.LOWMEMEND = *
.KERNEL:

!if UNINSTALL_RUNS_DINSTALL {
.GETDRIVECODE = .getdrvcode - .drivebusy + $0800

            brk; padding
.runcodeget: ldx #<(.stackend - $07)
            txs
-           lda <(.drivebusy - $80),x
            sta $0780,x
            inx
            bpl -
            ldx #<(drvcodebeg41 - $01)
            jmp .GETDRIVECODE

.getdrvcode: inx
            bne +
.getroutputhi = * + $08
            inc .getroutputhi - .getdrvcode + .GETDRIVECODE
            ; there is no watchdog while receiving the code
+           jsr .dgetbyte - .drivebusy + $0800
	    !if drvcodebeg41 <= $100 {
	    sta $0000,x
	    } else {
            sta (drvcodebeg41 - $01) & $ff00,x
	    }
            cpx #<(drvprgend41 - $01)
            bne .getdrvcode
            dec drvcodebeg41 + $01
            bne .getdrvcode
            jmp .restart
}; UNINSTALL_RUNS_DINSTALL

            ; common code for all configurations

.dgetbyte:   +GETBYTE_IMPL
            rts

            ; must not change the state of the carry flag
; 5
.drivebusy:  sty VIA1_PRB
            sei; disable watchdog
            rts

; 10
.checkchg:   lda VIA2_PRB
            and #VIA_WRITE_PROTECT
            cmp DISKCHANGEBUFFER
            sta DISKCHANGEBUFFER
            rts

; 21
.gcrencode:  pha
            and #BINARY_NIBBLE_MASK
            tax
            lda GCRENCODE,x
            sta LONIBBLES,y
            pla
            lsr
            lsr
            lsr
            lsr
            tax
            lda GCRENCODE,x
            sta HINIBBLES,y
            rts

; 23
.waitsync:   ldx #$00
-           lda VIA2_T1C_H
            beq .wsynctmout; will return $00 in the accu
            bit VIA2_PRB
            bmi -
            bit VIA2_PRA
            clv
            bvc *
            lsr VIA2_PRA; is never $00 but usually $52 (header) or $55 (data)
            clv
.wsynctmout: rts

; 10
.gcrtimeout: jsr .checkchg; check light sensor for disk removal
            beq +
            sec
            ror DISKCHANGED; set the new disk flag when disks have been changed
            sec
+           rts

          ; * >= $0100
.stack:
            ;assert stack >= $0100, error, "***** 1541 stack too low in memory. *****"
	    !if .stack >= $100 {
	    } else {
		!error "***** 1541 stack too low in memory. *****"
	    }

!if LOAD_ONCE {
            !fill 3; padding, best used for bigger stack
}
            !word $00, $00, .dcodinit - $01, .runmodule - $01; return addresses for install
.stackend:
            ;assert stackend < $0200, error, "***** 1541 stack too high in memory. *****"
	    !if (.stackend < $0200) {
	    } else {
		!error "***** 1541 stack too high in memory. *****"
	    }

            ; getblock calls
            ; in: a: track
            ;     y: sector, ANYSECTORSANELINK, ANYSECTOR or UNPROCESSEDSECTOR

.getblcurts: ; loadfile code
            ldy SECTORTOFETCH; negative: any unprocessed sector, positive: this specific sector; sector link sanity check

            ; get the next block passing, check against stored id
.getblcurtr: lda CURTRACK

!if LOAD_ONCE {
            ; get the block at track a, sector y, check against stored id
.getblkchid:
.getblkstid: ldx #OPC_CMP_ZP
} else {; !LOAD_ONCE
            ; get the block at track a, sector y, check against stored id
.getblkchid: ldx #OPC_CMP_ZP
            +SKIPWORD
            ; get the block at track a, sector y, store read id
.getblkstid: ldx #OPC_STA_ZP
}; !LOAD_ONCE
            sty REQUESTEDSECTOR
            jsr .trackseeks; sets storputid0 and storputid1
                          ; stores the number of blocks on
                          ; the current track in NUMSECTORS

            lda #OPC_BNE; full gcr fetch and checksumming
.getblkscan: sta .scanswt0
            sta .scanswt1

.readblock:  lda #$ff
            sta VIA2_T1C_H; reset the sync time-out
-           jsr .waitsync
            beq .gcrtimeout; returns with carry set on time-out
            ; check if the sync is followed by a sector header
            bcs -; if not, wait for next sync mark

            ; read the sector header
            ldx #$06
.getheader:  bvc *
            lda VIA2_PRA
            clv
            sta GCRBUFFER + $00,x
            dex
            bpl .getheader

            ; check if the sector header's field values match the expectations -
            ; the header is only checksummed after the data fetch since there
            ; is not enough time to do it now

            ; decode sector number
            asl GCRBUFFER + $04
            lda GCRBUFFER + $05
            rol
            and #GCR_NIBBLE_MASK
            tay
            lda GCRBUFFER + $04
            jsr .decodesub + $00
            cmp NUMSECTORS; check if sector number is within range of the allowed
                          ; sector numbers for the current track
            bcs .readblock
            sta LOADEDSECTOR; store away the sector number, it is returned in the
                            ; x-register on success
            tax           ; current sector number
            lda TRACKLINKTAB,x

            cpx REQUESTEDSECTOR
            beq .waitdatahd; branch if requested sector

            ; bit:bpl:bvc won't work because the v-flag
            ; is unstable while the disk is spinning
            ldy REQUESTEDSECTOR
            bmi .waitdatahd; branch if ANYSECTOR or ANYSECTORSANELINK
            iny
            bpl .readblock ; branch if not UNPROCESSEDSECTOR

            ; no specific sector requested -
            ; out-of-order sector fetch
           ;lda TRACKLINKTAB,x
            tax          ; check whether the current block has already been
                         ; loaded into the computer's memory
            bmi .readblock; if yes, wait for next sector

.waitdatahd: sta BLOCKINDEX_CBM; store sector index

            ; wait for data block sync
            lda #$ff
            sta VIA2_T1C_H; reset the sync time-out
            jsr .waitsync
            ; check if the sync is followed by a data block
            bcc .readblock; if not, wait for next sector
                         ; error or cycle candidate

            ; read and partially inflate the gcr nibbles to 8 bits
            bvc *
            lda VIA2_PRA                   ;    ;   11222223
            asr #(GCR_NIBBLE_MASK << 1) | 1;    ;   ...22222:3 - and + lsr
            clv
            bvc *
           ;ldx #$00
.loaddata:   ldy VIA2_PRA                   ; 14 ;   33334444      1 - cycle 14 in [0..25]
            sta HINIBBLES + $00,x          ; 19
            tya                            ; 21
            ror                            ; 23 ;   33333444
            lsr                            ; 25 ;   .3333344
            lsr                            ; 27 ;   ..333334
            lsr                            ; 29 ;   ...33333   - final: 3
            sta LONIBBLES + $00,x          ; 34
            txa                            ; 36
            sbx #-$03                      ; 38 ; x = x + 3
            tya                            ; 40 ;   33334444
            ldy VIA2_PRA                   ; 44 ;   45555566     2 - cycle 44 in [32..51]
            clv                            ; 46                    - cycle 46 in [32..51]
            cpy #$80                       ; 48 ; 4:45555566
            rol                            ; 50 ;   33344444
            and #GCR_NIBBLE_MASK           ; 52 ;   ...44444   - final: 4
               ; 52 cycles in [0..51]

            bvc *                          ;  3 ;   3 cycles variance
            sta HINIBBLES + $01 - 3,x      ;  8
            tya                            ; 10 ;   45555566
            asr #%01111111                 ; 12 ;   ..555556:6 - and + lsr
            sta LONIBBLES + $01 - 3,x      ; 17
            lda VIA2_PRA                   ; 21 ;   66677777     3 - cycle 16 in [0..25]
            tay                            ; 23
            ror                            ; 25 ;   66667777
            lsr LONIBBLES + $01 - 3,x      ; 32 ;   ...55555:6 - final: 5
            ror                            ; 34 ;   66666777
            lsr                            ; 36 ;   .6666677
            lsr                            ; 38 ;   ..666667
            lsr                            ; 40 ;   ...66666   - final: 6
            sta HINIBBLES + $02 - 3,x      ; 45
            lda VIA2_PRA                   ; 49 ;   00000111     4 - cycle 49 in [32..51]
            lsr                            ; 51 ;   .0000011:1
            sta HINIBBLES + $03 - 3,x      ; 56
            tya                            ; 58 ;   66677777
            and #GCR_NIBBLE_MASK           ; 60 ;   ...77777   - final: 7
            sta LONIBBLES + $02 - 3,x      ; 65
            lda VIA2_PRA                   ; 69 ;   11222223     0 - cycle 69 in [64..77]
            clv                            ; 71                    - cycle 71 in [64..77]
            ror                            ; 73 ;   11122222:3
            sta LONIBBLES + $03 - 3,x      ; 78
               ; 78 cycles in [0..77]

            bvc *                          ;  3 ; 3 cycles variance
            and #GCR_NIBBLE_MASK           ;  5 ;   ...22222   - final: 2
            inx                            ;  7
.scanswt0:   bne .loaddata                   ; 10

               ; a 5-GCR-bytes cycle minimally takes approximately 130 cycles in speed zone 11, 52 + 78 = 130

            ;assert >(*) = >(loaddata), error, "***** Page boundary crossing in GCR fetch loop, fatal cycle loss. *****"

	    !if >(*) = >(.loaddata) {
	    } else {
		!error "***** Page boundary crossing in GCR fetch loop, fatal cycle loss. *****"
	    }

            tay                            ; 11 ;   ...22222
            lda VIA2_PRA                   ; 15 ;   33334444     1 - cycle 15 in [0..25]
            jsr .decodesub - $01            ; decode data checksum
            tay

            ; finish gcr inflation and checksum the data
            ldx #$00
.gcrfinish:  lda LONIBBLES + $03,x          ;   11122222     4
            lsr HINIBBLES + $03,x          ;   ..000001:1   7
            ror                            ;   11112222     2
            lsr HINIBBLES + $03,x          ;   ...00000:1   7 - final: 0
            ror                            ;   11111222     2
            lsr                            ;   .1111122     2
            lsr                            ;   ..111112     2
            lsr                            ;   ...11111     2 - final: 1
            sta LONIBBLES + $03,x          ;                5 = 30
            tya                            ;                2
            ldy HINIBBLES + $00,x          ;                4
            eor GCRDECODEHI,y              ;                4
            ldy LONIBBLES + $00,x          ;                4
            eor GCRDECODELO,y              ;                4
            ldy HINIBBLES + $01,x          ;                4
            eor GCRDECODEHI,y              ;                4
            ldy LONIBBLES + $01,x          ;                4
            eor GCRDECODELO,y              ;                4
            ldy HINIBBLES + $02,x          ;                4
            eor GCRDECODEHI,y              ;                4
            ldy LONIBBLES + $02,x          ;                4
            eor GCRDECODELO,y              ;                4
            ldy HINIBBLES + $03,x          ;                4
            eor GCRDECODEHI,y              ;                4
            ldy LONIBBLES + $03,x          ;                4
            eor GCRDECODELO,y              ;                4
            tay                            ;                2
            txa                            ;                2
            sbx #-$04; x = x + 4           ;                2
.scanswt1:   bne .gcrfinish                  ;                3 = 75
                                           ;                  = 105

            ;assert >(*) = >(gcrfinish), error, "***** Page boundary crossing in GCR finishing loop, unnecessary cycle loss. *****"
	    !if >(*) = >(.gcrfinish) {
	    } else {
    		!error "***** Page boundary crossing in GCR finishing loop, unnecessary cycle loss. *****"
	    }

            tya
            beq +; check whether data checksum is ok
            txa
            bne +; don't checksum if only the first few bytes have been
                  ; decoded for scanning
            jmp .errorret

+           ; checksum sector header
            ; this is done only now because there is no time for that between
            ; the sector header and data block
            lda GCRBUFFER + $06
            asr #(GCR_NIBBLE_MASK << 1) | 1; and + lsr
            tay
            lda GCRBUFFER + $05
            jsr .decodesub - $01; checksum
            sta GCRBUFFER + $06
            lax GCRBUFFER + $02
            lsr
            lsr
            lsr
            tay
            txa
            asl GCRBUFFER + $01
            rol
            asl GCRBUFFER + $01
            rol
            and #GCR_NIBBLE_MASK
            jsr .decodesub + $03; ID1
            sta GCRBUFFER + $05
            lda GCRBUFFER + $01
            lsr
            lsr
            lsr
            tay
            lda GCRBUFFER + $00
            jsr .decodesub - $01; ID0
            tay
            eor GCRBUFFER + $05; ID1
            eor LOADEDSECTOR
            eor GCRBUFFER + $06; checksum
            sta CURTRACK; is changed to eor CURTRACK after init
.headerchk:  !byte OPC_BIT_ZP, <(.errorret - * - $02); is changed to bne errorret
                                                         ; after init, wait for next sector if
                                                         ; sector header checksum was not ok
                                                         ; error or cycle candidate

            lda GCRBUFFER + $05; ID1
            ldx #$00; set z-flag which won't be altered by the store opcodes
.storputid0: cpy ID0; cpy ID0/sty ID0
            bne +
.storputid1: cmp ID1; cmp ID1/sta ID1

+           clc; the next opcode may be an rts, so denote operation successful here
.dsctcmps:   bne .errorret; branch if the disk ID does not match

            ldy #$00
            jsr .decodebyte; decode the block's first byte (track link)
            sta LINKTRACK
            jsr .decodebyte; decode the block's second byte (sector link)
            tay

            ldx REQUESTEDSECTOR
            inx
            beq ++; branch on ANYSECTOR
            ; gets here on ANYSECTORSANELINK, UNPROCESSEDSECTOR, or requested sector

.checklink:  sty LINKSECTOR
            ; sector link sanity check
            ldy LINKTRACK
            beq +
            cpy #MAXTRACK41 + 1; check whether track link is within the valid range
            bcs .errorret; if not, return error
            jsr .getnumscts
            dex
            cpx LINKSECTOR; check whether sector link is within the valid range
            bcc .errorret; branch if sector number too large

            ; the link track is returned last so that the z-flag
            ; is set if this block is the file's last one
+           ldy LINKSECTOR  ; return the loaded block's sector link sector number
++          ldx LOADEDSECTOR; return the loaded block's sector number
            lda LINKTRACK   ; return the loader block's sector link track number
            clc             ; operation successful
            rts

.lightsub:   txa
            tay
            beq ++++
-           iny
            bne -
            tay
            jsr .ddliteon
-           dey
            bne -
            dex
            bne +
            and #$ff-MOTOR   ; turn off motor
+           and #$ff-BUSY_LED; turn off busy led
.store_via2: sta VIA2_PRB
.errorret:   sec
++++        rts

.ddliteon:   lda #BUSY_LED
            ora VIA2_PRB
            bne .store_via2

; 16
.decodebyte: ldx HINIBBLES,y
            lda GCRDECODEHI,x
            ldx LONIBBLES,y
            ora GCRDECODELO,x
            ldx NUMFILES; loadfile code
            iny
            rts

; 11
            ror
.decodesub:  lsr
            lsr
            lsr
            tax
            lda GCRDECODEHI,y
            ora GCRDECODELO,x
            rts

	    ; was sollen diese asserts ? */
            ;assert * >= $0300, error, "***** 1541 watchdog IRQ/BRK handler located below $0300. *****"
	    !if * >= $0300 {
	    } else {
		;!error "***** 1541 watchdog IRQ/BRK handler located below $0300. *****"
	    }
            ;assert * <= $0300, error, "***** 1541 watchdog IRQ/BRK handler located above $0300. *****"
	    !if * <= $0300 {
	    } else {
		;!error "***** 1541 watchdog IRQ/BRK handler located above $0300. *****"
	    }

            ; configuration-dependent code

!if UNINSTALL_RUNS_DINSTALL {

.watchdgirq: lda #BUSY_LED | MOTOR
            jsr .ddliteon + $02
            lda #$12; ROM dir track
            jsr .trackseek; ignore error (should not occur)
            ldx #$ff
            ; fade off the busy led and reset the drive
-           jsr .lightsub
            txa
            bne -
            jmp (RESET_VECTOR)
.duninstall:
-           jsr .lightsub
            txa
            bne -
            jmp .runcodeget

} else {

.watchdgirq: ldx #$ff

.duninstall: txa
            pha
            beq +
            lda #BUSY_LED | MOTOR
            jsr .ddliteon + $02
+           lda #$12; ROM dir track
            jsr .trackseek; ignore error (should not occur)
            pla
            ; fade off the busy led if lit, and reset the drive
            beq ++
            ldx #$ff
-           jsr .lightsub
            txa
            bne -
++          jmp (RESET_VECTOR)

}; !UNINSTALL_RUNS_DINSTALL

.trackseeks: stx .storputid1
            dex; OPC_STA_ZP/OPC_CMP_ZP -> OPC_STY_ZP/OPC_CPY_ZP
            stx .storputid0
.trackseek:  tax; destination track
.trackseekx: lda #MOTOR; turn on the motor
            jsr .ddliteon + $02
            txa ;destination track
            beq .setbitrate; don't do anything if invalid track
            cmp #MAXTRACK41 + 1
            bcs .setbitrate; don't do anything if invalid track
            sec
            sbc CURTRACK
            beq .setbitrate
            ; do the track jump
            stx CURTRACK
            ldy #$00
            sty CURRSTEPSPEEDLOW
            bcs +
            eor #$ff- $00; invert track difference
            adc #$01
            iny
+           sty TRACKINC
            asl; half-tracks
            tay

            ; TRACKINC         step bits ... store
            ; $00 (move up)    %00 %00 %01 -> %01
            ;     (inwards)    %01 %01 %11 -> %10
            ;                  %10 %10 %01 -> %11
            ;                  %11 %11 %11 -> %00
            ; $01 (move down)  %00 %01 %11 -> %11
            ;     (outwards)   %01 %00 %01 -> %00
            ;                  %10 %11 %11 -> %01
            ;                  %11 %10 %01 -> %10

            lda #$80 | (MINSTPSP + 1)
.trackstep:  sta VIA2_T1C_H
            tax
            lda TRACKINC
            eor VIA2_PRB
            sec
            rol
            and #TRACK_STEP
            eor VIA2_PRB
            sta VIA2_PRB
            dey
            beq .setbitrate
            txa
.headaccl:   cmp #$80 | MAXSTPSP
            beq .noheadacc
            pha
           ;sec
            lda CURRSTEPSPEEDLOW
            sbc #STEPRACC
            sta CURRSTEPSPEEDLOW
            pla
            sbc #$00
.noheadacc:  cpx VIA2_T1C_H
            beq .noheadacc; wait until the counter hi-byte has decreased by 1
            dex
            bmi .headaccl
            bpl .trackstep; jmp

.bneuninstl: bne .duninstall

            ; bit-rates:
            ; 31+   (17): 00 (innermost)
            ; 25-30 (18): 01
            ; 18-24 (19): 10
            ;  1-17 (21): 11 (outermost)
.setbitrate: ldy CURTRACK
            jsr .getnumscts
.putbitrate: bit VIA2_PRB  ; is changed to sta VIA2_PRB after init
.putnumscts: bit NUMSECTORS; is changed to stx NUMSECTORS after init

            ; fall through

.getnumscts: lda VIA2_PRB
            ora #SYNC_MARK | BITRATE_MASK; $e0
            ldx #21
            cpy #18
            bcc +
            dex
            dex; 19
            sbc #1 << BITRATE_SHIFT; $60 -> $40
            cpy #25
            bcc +
            dex; 18
            sbc #1 << BITRATE_SHIFT; $40 -> $20
            cpy #31
            bcc +
            dex; 17
            sbc #1 << BITRATE_SHIFT; $20 -> $00
+           rts

.driveidle:  jsr .lightsub; fade off the busy led
            jsr .gcrtimeout; check light sensor for disk removal
            lda VIA1_PRB
            cmp #CLK_OUT | CLK_IN | DATA_IN | ATN_IN
            beq .driveidle; wait until there is something to do

            cmp #CLK_OUT | CLK_IN | DATA_IN
            bne .bneuninstl; check for reset or uninstallation

            ; execute command

            txa
            beq .runmodule; check whether the busy led has been completely faded off
            jsr .ddliteon ; if not, turn it on

.runmodule:
!if LOAD_ONCE = 0 {
            sec
            ror VIA2_T1C_H; reset watchdog time-out, this also clears the possibly
                          ; pending timer 1 irq flag
            +ENABLE_WATCHDOG; enable watchdog, the computer might be reset while sending over a
                           ; byte, leaving the drive waiting for handshake pulses
}; !LOAD_ONCE

!if INSTALL_FROM_DISK {
            jsr .dgetbyte
            cmp LOADEDMODULE
            bne .RUNMODULE
            ;sta LOADEDMODULE
            !byte $02
            nop

            ; load module loader
            ; the module library track and sector are stored in
            ; LIBTRACK and LIBSECTOR, respectively

            ldy #CLK_OUT
            jsr .drivebusy; disables watchdog

-           lda LIBTRACK
            ldy LIBSECTOR ; sector link sanity check
            jsr .getblkchid; compare id
            bcs -
            ldy #$02
-           jsr .decodebyte
            sta .RUNMODULE - $02,y
            bne -

            ; fall through
}; INSTALL_FROM_DISK

            ; module space
.RUNMODULE:

!ifdef STANDALONE {
} else {
    !if INSTALL_FROM_DISK=0 {
            !src "drivecode1541-loadfile.s"
    } else {; !INSTALL_FROM_DISK

            ldy #CLK_OUT
            jsr .drivebusy; disables watchdog

            ; load a module
            ; the module file track and sector are stored in
            ; LIBTRACK and LIBSECTOR, respectively

.LOADMODULE:
-           lda LIBTRACK
            ldy LIBSECTOR ; sector link sanity check
            jsr .getblkchid; check id
            bcs -

            ldy #$02
-           jsr .decodebyte
            sta POINTER + $00 - 3,y; POINTER + 0: lo, POINTER + 1: hi, POINTER + 2: page count
            cpy #$05
            bne -

            ldx #.loadmodend - .loadmodule - 1
-           lda .loadmodule,x
            sta LOWMEM + $03,x
            dex
            bpl -
            jmp LOWMEM + $03

.loadmodule: jsr .decodebyte; always decodes the whole block
            dey
            sta (POINTER),y
            iny
            bne .loadmodule
            clc
            lda #$fe
            adc POINTER + $00
            sta POINTER + $00
            bcc +
            inc POINTER + $01
            dec POINTER + $02
            beq .modloaded
-
+           lda LINKTRACK
            beq .modloaded
            ldy LINKSECTOR
            jsr .getblkchid; compare id, sector link sanity check
            bcs -
            ldy #$02
            bne .loadmodule; jmp
.modloaded:  jmp .RUNMODULE
.loadmodend:

            ;!assert loadmodend - loadmodule + LOWMEM + $03 < LOWMEMEND, error, "***** loadmodule too large. *****"
	    !if loadmodend - loadmodule + LOWMEM + $03 < LOWMEMEND {
	    } else {
		!error "***** loadmodule too large. *****"
	    }

    }
}

;DRVCODE41END = *
;.export DRVCODE41END

.sendgcrraw:
            .BIT0DEST = 3
            .BIT1DEST = 1
            .BIT2DEST = 2
            .BIT3DEST = 0

            !for i,$10 {
            !set I = 0xff-(i-1) 
                !byte (((I >> 0) & 1) << .BIT0DEST) | (((I >> 1) & 1) << .BIT1DEST) | (((I >> 2) & 1) << .BIT2DEST) | (((I >> 3) & 1) << .BIT3DEST)
            }

drvcodeend41:
            ; following code is transferred using KERNAL routines, then it is
            ; run and gets the rest of the code

            ; entry point
dinstall41:
            sei
!if LOAD_ONCE {
            lda #$ff-MOTOR ; the motor is on because of the
            and VIA2_PRB; file open operation immediately
            sta VIA2_PRB; before running this code
}
!if INSTALL_FROM_DISK {
            lda ROMOS_HEADER_TRACK
            sta LIBTRACK
            lda ROMOS_HEADER_SECTOR
            sta LIBSECTOR
}
            lda ROMOS_TRACK_DIFF
            sta LINKTRACK

            lda #CLK_OUT
            sta VIA1_PRB
            lda #VIA_ATN_IN_INPUT | VIA_PIO7_INPUT | VIA_ATNA_OUT_OUTPUT | VIA_CLK_OUT_OUTPUT | VIA_CLK_IN_INPUT | VIA_DATA_OUT_OUTPUT | VIA_DATA_IN_INPUT | VIA_DEVICE_NUMBER_OUTPUT
            sta VIA1_DDRB

-           lda VIA1_PRB; wait for DATA IN = high
            lsr
instalwait: bcc -

!ifdef STANDALONE {
            ldx #<(stackend - $05)
            txs
            rts; jumps to dcodinit
} else {

            ldx #<(drvcodebeg41 - $01)
.dgetrout:   inx
            bne +
            inc .dgetputhi
+           +GETBYTE_IMPL_NOWDOG; there is no watchdog while installing
.dgetputhi = * + $02
            ;sta a:>(drvcodebeg41 - $01) << 8,x
	    !if drvcodebeg41 <= $100 {
	    sta $0000,x
	    } else {
            sta (drvcodebeg41 - $01) & $ff00,x
	    }
            cpx #<(drvcodeend41 - $01)
            bne .dgetrout
            dec drvcodebeg41
            bne .dgetrout

}; !STANDALONE

.restart:    lda #CLK_OUT; drivebusy
            sta VIA1_PRB

!if UNINSTALL_RUNS_DINSTALL {
            lda #T1_IRQ_ON_LOAD | PA_LATCHING_ENABLE; watchdog irq: count phi2 pulses, one-shot;
                                                    ; enable port a latching to grab one gcr byte at a time
                                                    ; rather than letting the gcr bitstream scroll through
                                                    ; port a (applies to 1541 and Oceanic OC-118, but not
                                                    ; 1541-II)
            sta VIA2_ACR
            lda #READ_MODE | BYTE_SYNC_ENABLE
            sta VIA2_PCR

            ldx #<(.stackend - $05)
            txs

            ; before loading the first file, the current track number is
            ; retrieved by reading any block header on the disk -
            ; however, if the loader is uninstalled before loading anything,
            ; it needs to know the more or less correct current track number
            ; in order to seek to track 18 before reset
            lda ROMOS_HEADER_TRACK; both are at the
            sta CURTRACK          ; same location

            ; watchdog initialization

            lda #$00
    !if INSTALL_FROM_DISK {
            ldx #JOBCODE0400 - JOBCODE0400 + $80
    } else {
            ldx #JOBCODE0600 - JOBCODE0400 + $80
    }
-           sta <(JOBCODE0400 - $80),x; clear job queue
            dex
            bmi -

           ;ldx #IRQ_CLEAR_FLAGS | $7f; $7f
            stx VIA1_IER; no irqs from via 1
            stx VIA2_IER; no irqs from via 2

            lda #IRQ_SET_FLAGS | IRQ_TIMER_1
            sta VIA2_IER; timer 1 irqs from via 2

            lda #JOBCODE_EXECUTE
            sta JOBCODE0300; execute watchdog handler at $0300 on watchdog time-out

            lda #NUMMAXSECTORS
            ldy #CLK_OUT
            jmp .drivebusy; returns to dcodinit
} else {
            ldx #<(.stackend - $05)
            txs
            rts; jumps to dcodinit
}; !UNINSTALL_RUNS_DINSTALL


drvprgend41:
            } ; pseudopc

