
    !src "loader.inc"
!src "cia.inc"
!src "cpu.inc"

.BUFFER                = $00
.SYS_SP                = $01
.FILETRACK             = $0b
.FILESECTOR            = $0c
.FILENAMEHASH0         = .FILETRACK
.FILENAMEHASH1         = .FILESECTOR
!if (FILESYSTEM = DIRECTORY_NAME) & (LOAD_ONCE=0) {
.FILEINDEX             = .FILETRACK
}
.HASHVALUE0LO          = $0d
.HASHVALUE0HI          = $0e
.HASHVALUE1LO          = $0f
.HASHVALUE1HI          = $10
.NUMFILES              = $11
.CURRDIRBLOCKTRACK     = $12
.CURRDIRBLOCKSECTOR    = $13
.CYCLESTARTENDTRACK    = $14
.CYCLESTARTENDSECTOR   = $15
.DIRCYCLEFLAG          = $16
;BLOCKBUFFERJOBTRACK  = $17
;BLOCKBUFFERJOBSECTOR = $18
.NEXTDIRBLOCKTRACK     = $19
.NEXTDIRBLOCKSECTOR    = $1a
.FIRSTDIRSECTOR        = $1b
.BLOCKINDEX            = $1c
.SYSIRQVECTOR_LO       = $1d
.SYSIRQVECTOR_HI       = $1e

.JOBCODESTABLE         = $02
.JOBTRKSCTTABLE        = $0b
.OPEN_FILE_TRACK       = $4c
.LED_FLAG              = $79
.IRQVECTOR_LO          = $0192
.IRQVECTOR_HI          = $0193
.HDRS2                 = $01bc
.DIRTRACK81            = $022b
.OPEN_FILE_SECTOR      = $028b
.DIRSECTOR81           = $00  ; literal

.RESET_TIMERB          = $cb9f
.STROBE_CONTROLLER     = $ff54

.READ_DV               = $80
.SEEK_DV               = $8c

.OK_DV                 = $00

.BLOCKBUFFER           = $0900
.SENDTABLELO           = $0a00
.SENDTABLEHI           = $0b00

.LINKTRACK             = .BLOCKBUFFER+$00
.LINKSECTOR            = .BLOCKBUFFER+$01

.BINARY_NIBBLE_MASK    = %00001111

.MAXTRACK81            = 80
.MAXSECTOR81           = 39

.WATCHDOG_PERIOD       = $20; 32 * 65536 cycles at 2 MHz = 1.049 s

;!if DISABLE_WATCHDOG {
;    !macro ENABLE_WATCHDOG {
;        sei
;    }
;} else {
;    !macro ENABLE_WATCHDOG {
;        cli
;    }
;}

!pseudopc $0300 {

!if UNINSTALL_RUNS_DINSTALL {
    ;.export drvcodebeg81 : absolute
    ;.export drivebusy81  : absolute
}; UNINSTALL_RUNS_DINSTALL

drvcodebeg81: !byte >(drivebusy81 - * + $0100 - $01); init transfer count hi-byte

.SENDNIBBLETAB:
            .BIT0DEST = 3
            .BIT1DEST = 1
            .BIT2DEST = 2
            .BIT3DEST = 0

            !for i,$10 {
            !set I = 0xff-(i-1) 
                !byte (((I >> 0) & 1) << .BIT0DEST) | (((I >> 1) & 1) << .BIT1DEST) | (((I >> 2) & 1) << .BIT2DEST) | (((I >> 3) & 1) << .BIT3DEST)
            }

.filename:

.dcodinit:   tsx
            stx .SYS_SP

!if LOAD_ONCE {
            jsr drivebusy81
-           lda CIA_PRB
            and #ATN_IN | ATNA_OUT | CLK_OUT | CLK_IN | DATA_OUT | DATA_IN
            cmp #ATN_IN |            CLK_OUT | CLK_IN |            DATA_IN
            bne -; no watchdog
            ; the busy led might be enabled
            lda #DRIVE_LED
            bit CIA_PRA
            beq ++
            ; turn it off if so
            ldx #$ff
-           jsr .lightsub
            txa
            bne -
++
} else {; !LOAD_ONCE
            lda CIA_PRA
            jsr .motrledoff
}; LOAD_ONCE

            ldx #$00
-           txa
            and #.BINARY_NIBBLE_MASK
            tay
            lda .SENDNIBBLETAB,y
            sta .SENDTABLELO,x
            txa
            lsr
            lsr
            lsr
            lsr
            tay
            lda .SENDNIBBLETAB,y
            sta .SENDTABLEHI,x
            inx
            bne -

            ; watchdog initialization
            lda .IRQVECTOR_LO
            sta .SYSIRQVECTOR_LO
            lda .IRQVECTOR_HI
            sta .SYSIRQVECTOR_HI
            lda #$ff
            sta CIA_TA_LO
            sta CIA_TA_HI
            lda #COUNT_PHI2 | FORCE_LOAD | CONTINUOUS | TIMER_START
            sta CIA_CRA
!if DISABLE_WATCHDOG = 0 {
            jsr .initwatchd
}
            lda #CIA_CLR_INTF | EVERY_IRQ
            sta CIA_ICR
            lda #CIA_SET_INTF | TIMERB_IRQ
            sta CIA_ICR

!if UNINSTALL_RUNS_DINSTALL {
            lda #>(drvcodebeg81-$01)
            sta .dgetputhi
            lda #OPC_BIT_ZP
            sta .instalwait
}; UNINSTALL_RUNS_DINSTALL

            ldx #$00

!if (FILESYSTEM = DIRECTORY_NAME) & (LOAD_ONCE = 0) {
            stx .NUMFILES
}

!if LOAD_ONCE = 0 {
            jsr drivebusy81
-           lda CIA_PRB
            and #ATN_IN | ATNA_OUT | CLK_OUT | CLK_IN | DATA_OUT | DATA_IN
            cmp #ATN_IN |            CLK_OUT | CLK_IN |            DATA_IN
            bne -; no watchdog
}; !LOAD_ONCE

.drividle:  jsr .lightsub; fade off the busy led
            lda CIA_PRB
            and #ATN_IN | ATNA_OUT | CLK_OUT | CLK_IN | DATA_OUT | DATA_IN
            cmp #ATN_IN |            CLK_OUT | CLK_IN |            DATA_IN
            beq .drividle; wait until there is something to do

            cmp #                    CLK_OUT | CLK_IN |            DATA_IN
            beq +
            jmp .duninstall; check for reset or uninstallation

            ; load a file

+           txa
            beq .beginload; check whether the busy led has been completely faded off
            jsr .ddliteon ; if not, turn it on
.beginload:
!if LOAD_ONCE {
            ldy .OPEN_FILE_SECTOR
            ldx .OPEN_FILE_TRACK
} else {
    !if DISABLE_WATCHDOG = 0 {
            jsr .enablewdog; enable watchdog, the computer might be reset while sending over a
                          ; byte, leaving the drive waiting for handshake pulses
    }
            ; get starting track and sector of the file to load

    !if FILESYSTEM = DIRECTORY_NAME {

            ldx #-$01
            stx .DIRCYCLEFLAG
.getfilenam: jsr .dgetbyte; get filename
            beq +
            inx
            sta .filename,x
            cpx #FILENAME_MAXLENGTH - 1
            bne .getfilenam
+           jsr .drwaitrkch; disables watchdog
            jsr .gethashval
            sta .FILENAMEHASH1
            stx .FILENAMEHASH0

            lda .NUMFILES
            beq .newdisk
            lda CIA_PRA; query DISK_CHANGE
            bmi .samedisk

.newdisk:    ; a new disk has been inserted

            lda .DIRTRACK81
            ldy #.DIRSECTOR81
            jsr .getblock
            bcs .newdisk
           ;lda LINKTRACK
            sta .CYCLESTARTENDTRACK
           ;ldy LINKSECTOR
            sty .CYCLESTARTENDSECTOR
            sty .FIRSTDIRSECTOR
            ldx #-$01
            stx .DIRCYCLEFLAG

            ; directory cycling: fill the dir buffer
.filldirbuf: ldx #$00
            stx .NUMFILES
.nextdirsct: bit CIA_PRA; query DISK_CHANGE
            bpl .newdisk

            ; a contains the current dir track number
            ; y contains the current dir sector number
            sta .CURRDIRBLOCKTRACK
            jsr .getblock
            bcs .newdisk
           ;ldx CURRSECTOR
            stx .CURRDIRBLOCKSECTOR
           ;ldy LINKSECTOR
           ;lda LINKTRACK
            bne +
            lda .DIRTRACK81
            ldy .FIRSTDIRSECTOR
+           sta .NEXTDIRBLOCKTRACK
            sty .NEXTDIRBLOCKSECTOR

            ldy #$03
.dgdirloop:  ldx .NUMFILES
            lda .BLOCKBUFFER + $00,y; get file's start track
            beq + ; skip non-files denoted by track 0
            sta .DIRTRACKS,x
            lda .BLOCKBUFFER + $01,y; get file's start sector
            sta .DIRSECTORS,x
            jsr .fnamehash
            pha
            txa
            ldx .NUMFILES
            sta .FILENAMEHASHVAL0,x
            pla
            sta .FILENAMEHASHVAL1,x

            inc .NUMFILES
            cpx #.DIRBUFFSIZE - 1
            ; branch if dir buffer is full
            ; little flaw for the sake of saving on code size:
            ; when starting to cycle through the directory, the
            ; files in the dir block the last file currently in the dir
            ; buffer is in, will all be added to the buffer when it will
            ; be filled on the subsequent file load - this is why the
            ; minimum dir buffer size is 9 files
            bcs ++
+           tya
            and #%11100000
           ;clc
            adc #$23
            tay
            bcc .dgdirloop; process all entries in a dir block

            lda .NEXTDIRBLOCKTRACK
            ldy .NEXTDIRBLOCKSECTOR
            cmp .CYCLESTARTENDTRACK
            bne .nextdirsct
            cpy .CYCLESTARTENDSECTOR
            bne .nextdirsct
           ;sec
            inc .DIRCYCLEFLAG; cycle complete
            bcs .samedisk; jmp

++          lda .CURRDIRBLOCKTRACK
            sta .NEXTDIRBLOCKTRACK
            lda .CURRDIRBLOCKSECTOR
            sta .NEXTDIRBLOCKSECTOR

            ; the disk was not changed, or the dir has just been read
.samedisk:   ldx .NUMFILES
.nextfile:   dex
            bpl .findfile; check all dir entries in the buffer

            ; the dir buffer does not contain the file,
            ; so cycle through the directory to find it
            lda .NUMFILES    ; don't cycle if the
            cmp #.DIRBUFFSIZE; dir buffer holds
            bcc +          ; all directory files
            lda .NEXTDIRBLOCKTRACK
            ldy .NEXTDIRBLOCKSECTOR
            bit .DIRCYCLEFLAG; check if a
            bmi .filldirbuf  ; cycle is complete
+           sec
            jmp .filenotfnd

.findfile:   lda .FILENAMEHASH0
            eor .FILENAMEHASHVAL0,x
            bne .nextfile
            lda .FILENAMEHASH1
            eor .FILENAMEHASHVAL1,x
            bne .nextfile

            stx .FILEINDEX; store index of file to jump to the track of the file
                         ; following this one in the dir, after loading

            ; store number of the dir block loaded last,
            ; it is used to start the dir check cycle if
            ; the next file is not found in the dir buffer;
            ; it is also checked on the subsequent load to determine if the
            ; dir check cycle is complete and the file be said to be not found
            lda .CURRDIRBLOCKTRACK
            sta .CYCLESTARTENDTRACK
            sta .NEXTDIRBLOCKTRACK
            lda .CURRDIRBLOCKSECTOR
            sta .CYCLESTARTENDSECTOR
            sta .NEXTDIRBLOCKSECTOR

            jsr .ddliteon
            lda .DIRTRACKS,x
            ldy .DIRSECTORS,x
            tax

    } else {
    !if FILESYSTEM = TRACK_SECTOR {

            jsr .dgetbyte; get starting track
            tax
            jsr .dgetbyte; get starting sector
            jsr .drwaitrkch; disables watchdog
            tay
            txa

    }}
}; !LOAD_ONCE

            ; check for illegal track or sector
            beq .toillegal + $00
            cpx #.MAXTRACK81 + 1
            bcs .toillegal + $01
            cpy #.MAXSECTOR81 + 1
            bcc +
.toillegal:  sec
            jmp .illegalts

+           jsr .ddliteon
            txa; FILETRACK

            ldx #$00
            stx .BLOCKINDEX
.loadblock:  sta .JOBTRKSCTTABLE + ((.BLOCKBUFFER - $0300) / 128) + 0
            sty .JOBTRKSCTTABLE + ((.BLOCKBUFFER - $0300) / 128) + 1
-           jsr .getblockag
            bcs -

           ;ldy LINKSECTOR
           ;lda LINKTRACK
            pha; LINKTRACK
            beq +
            ldy #$ff
+           lda .LINKSECTOR
            pha; LINKSECTOR
            sty .dsendcmp + $01
            dey
            dey
            sty .BLOCKBUFFER+$01; block length
            lda .BLOCKINDEX
            jsr .sendblock; send the block over
            inc .BLOCKINDEX
            pla; LINKSECTOR
            tay
            pla; LINKTRACK
            bne .loadblock

            ; loading is finished

!if (FILESYSTEM = DIRECTORY_NAME) & (LOAD_ONCE = 0) {

            ldx .FILEINDEX
            inx
            cpx .NUMFILES
            lda .DIRTRACKS,x
            bcc +
            lda .DIRTRACK81
+           jsr .trackseek

            clc

.filenotfnd: ; branches here with carry set on file not found

} else {
            clc
}

.illegalts:  ; or illegal t or s

            jsr .sendstatus

            ldx #$01; turn motor and busy led off
            lda #DRIVE_LED; check if busy led is lit
            and CIA_PRA
            beq +
            ldx #$ff; fade off the busy led, then turn motor off
!if LOAD_ONCE {
+
            jmp .duninstall
} else {
    !if DISABLE_WATCHDOG = 0 {
+           jsr .enablewdog
    }
-           bit CIA_PRB
            bpl -; wait until the computer has acknowledged the file transfer
            sei; disable watchdog
            jmp .drividle
}

.initcontrl: lda CIA_PRA
            and #DRIVE_LED
            beq +
            lda #$ff
+           sta .LED_FLAG
            lda .SYSIRQVECTOR_LO
            sta .IRQVECTOR_LO
            lda .SYSIRQVECTOR_HI
            sta .IRQVECTOR_HI
            jmp .RESET_TIMERB

.trackseek:  tax
            dex
            stx .HDRS2 + ((.BLOCKBUFFER - $0300) / 128)
            jsr .initcontrl
            lda #.SEEK_DV
            ldx #(.BLOCKBUFFER - $0300) / 256
!if DISABLE_WATCHDOG {
            jmp .STROBE_CONTROLLER; move head to the start track of the next file in the
                                 ; directory

} else {; DISABLE_WATCHDOG = 0
            jsr .STROBE_CONTROLLER; move head to the start track of the next file in the
                                 ; directory

            ; fall through

.initwatchd: ; the i-flag is set here
            lda #<(.watchdgirq)
            sta .IRQVECTOR_LO
            lda #>(.watchdgirq)
            sta .IRQVECTOR_HI
            lda #<(.WATCHDOG_PERIOD)
            sta CIA_TB_LO
            lda #>(.WATCHDOG_PERIOD)
            sta CIA_TB_HI
            rts

.enablewdog: lda #COUNT_TA_UNDF | FORCE_LOAD | ONE_SHOT | TIMER_START
            sta CIA_CRB
            bit CIA_ICR
            +ENABLE_WATCHDOG
            rts
}; DISABLE_WATCHDOG = 0

.lightsub:   txa
            tay
            beq .ddliteon - $01
-           nop
            bit OPC_BIT_ZP
            iny
            bne -
            tay
            jsr .ddliteon
-           nop
            bit OPC_BIT_ZP
            dey
            bne -
            dex
            bne +
.motrledoff: ora #MOTOR     ; turn off motor
+           and #$ff-DRIVE_LED; turn off drive led
.store_cia:  sta CIA_PRA
            rts

.ddliteon:   lda #DRIVE_LED
            ora CIA_PRA
            bne .store_cia

.getblock:   sta .JOBTRKSCTTABLE + ((.BLOCKBUFFER - $0300) / 128) + 0
            sty .JOBTRKSCTTABLE + ((.BLOCKBUFFER - $0300) / 128) + 1
.getblockag: jsr .initcontrl
            lda #.READ_DV
            ldx #(.BLOCKBUFFER - $0300) / 256
            jsr .STROBE_CONTROLLER
!if DISABLE_WATCHDOG = 0 {
            jsr .initwatchd
}
            lda .JOBCODESTABLE + ((.BLOCKBUFFER - $0300) / 256); FD does not return the error status in the accu
            cmp #.OK_DV + 1
            ; the link track is returned last so that the z-flag
            ; is set if this block is the file's last one
            ldy .LINKSECTOR
            ldx .JOBTRKSCTTABLE + ((.BLOCKBUFFER - $0300) / 128) + 1; LOADEDSECTOR
            lda .LINKTRACK
            rts

!if UNINSTALL_RUNS_DINSTALL {
    !if DISABLE_WATCHDOG = 0 {
.watchdgirq: ldx #$ff
            ; fade off the busy led and reset the drive
-           jsr .lightsub
            txa
            bne -
            ; the 1581 moves the head to track 1 after reset,
            ; so it's not moved to the directory track here
            jmp (RESET_VECTOR)
    }
.duninstall:
-           jsr .lightsub
            txa
            bne -
            ldx .SYS_SP
            txs
            jsr .initcontrl; restore the irq vector
            jmp dinstall81

} else {

    !if DISABLE_WATCHDOG = 0 {
.watchdgirq: ldx #$ff
    }
.duninstall: txa
            pha
            lda .DIRTRACK81
            jsr .trackseek
            pla
            ; fade off the busy led (if lit) and reset the drive
            beq ++
            ldx #$ff
-           jsr .lightsub
            txa
            bne -
++           ldx .SYS_SP
            txs
            jmp .initcontrl
}

!if (FILESYSTEM = DIRECTORY_NAME) & (LOAD_ONCE = 0) {

.fnamehash:  ldx #-$01
-           lda .BLOCKBUFFER + $02,y
            iny
            cmp #' ' | $80
            beq .gethashval
            inx
            sta .filename,x
            cpx #FILENAME_MAXLENGTH - 1
            bne -

.gethashval: clc
            stx .HASHVALUE0LO
            stx .HASHVALUE1LO
            stx .HASHVALUE0HI
            stx .HASHVALUE1HI
.hashloop:   lda .filename,x
            adc .HASHVALUE0LO
            sta .HASHVALUE0LO
            bcc +
            inc .HASHVALUE0HI
            clc
+           adc .HASHVALUE1LO
            sta .HASHVALUE1LO
            lda .HASHVALUE0HI
            adc .HASHVALUE1HI
            sta .HASHVALUE1HI
            dex
            bpl .hashloop
            adc .HASHVALUE1LO
            tax
            lda .HASHVALUE0LO
            adc .HASHVALUE0HI
            rts
}

            ; carry: clear = ok, set = load error
.sendstatus: lda #$00
            sta .dsendcmp+$01
            sbc #$01; carry clear: result is $00-$02=$fe - loading finished successfully
                    ; carry set:   result is $00-$01=$ff - load error
            sta .JOBTRKSCTTABLE + ((.BLOCKBUFFER - $0300) / 128) + 0; make sure that a track
                                                                  ; change is not flagged
.sendblock:  sta .BLOCKBUFFER + $00; block index
!if DISABLE_WATCHDOG = 0 {
            jsr .enablewdog
}
            lda #DATA_OUT
            sta CIA_PRB; block ready signal
.waitready:  bit CIA_PRB
            bpl .waitready
            ldy #$00
.sendloop:
!if DISABLE_WATCHDOG = 0 {
            lda #COUNT_TA_UNDF | FORCE_LOAD | ONE_SHOT | TIMER_START
            sta CIA_CRB      ; 2 + 4; service watchdog
}
            ldx .BLOCKBUFFER,y; 4
            lda .SENDTABLELO,x; 4
-           bit CIA_PRB
            bmi -
            sta CIA_PRB

            asl              ; 2
            and #$ff-ATNA_OUT   ; 2

-           bit CIA_PRB
            bpl -
            sta CIA_PRB

            ldx .BLOCKBUFFER,y; 4
            lda .SENDTABLEHI,x; 4
-           bit CIA_PRB
            bmi -
            sta CIA_PRB

            asl              ; 2
            and #$ff-ATNA_OUT   ; 2

.dsendcmp:   cpy #$00         ; 2
            iny              ; 2

-           bit CIA_PRB
            bpl -
            sta CIA_PRB
            bcc .sendloop

-           bit CIA_PRB
            bmi -
            lda .LINKTRACK
            cmp .JOBTRKSCTTABLE + ((.BLOCKBUFFER - $0300) / 128) + 0
            beq drivebusy81; pull DATA_OUT high when changing tracks
.drwaitrkch: ldy #CLK_OUT | DATA_OUT; flag track change
            +SKIPWORD

            ; following code is transferred using KERNAL routines, then it is
            ; run and gets the rest of the code

drivebusy81: ldy #CLK_OUT
            sty CIA_PRB
            sei; disable watchdog
            rts

            ; must not trash x
.dgetbyte:   lda #%10000000; CLK OUT lo: drive is ready
            sta .BUFFER
            sta CIA_PRB
---        lda #CLK_IN
-           bit CIA_PRB
            beq -
            lda CIA_PRB
            lsr
            ror .BUFFER
            lda #CLK_IN
-           bit CIA_PRB
            bne -
            lda CIA_PRB
            lsr
            ror .BUFFER
            bcc ---
            lda .BUFFER
            rts

DRVCODE81END = *
;.export DRVCODE81END

!if (FILESYSTEM = DIRECTORY_NAME) & (LOAD_ONCE=0) & (UNINSTALL_RUNS_DINSTALL=0) {
    .DIRBUFFSIZE      = (.BLOCKBUFFER - *) / 4
    .DIRTRACKS        = *
    .DIRSECTORS       = .DIRTRACKS + .DIRBUFFSIZE
    .FILENAMEHASHVAL0 = .DIRSECTORS + .DIRBUFFSIZE
    .FILENAMEHASHVAL1 = .FILENAMEHASHVAL0 + .DIRBUFFSIZE
    .DIRBUFFEND       = .FILENAMEHASHVAL1 + .DIRBUFFSIZE

            ;assert DIRBUFFSIZE >= 9, error, "***** Dir buffer too small. *****"
	    !if .DIRBUFFSIZE >= 9 {
	    } else {
		!error "***** Dir buffer too small. *****"
	    }

    DIRBUFFSIZE81 = .DIRBUFFSIZE
    ;export DIRBUFFSIZE81
}

!if UNINSTALL_RUNS_DINSTALL = 0 {
            ;assert * <= BLOCKBUFFER, error, "***** 1581 drive code too large. *****"
    	    !if * <= .BLOCKBUFFER {
	    } else {
    		!error "***** 1581 drive code too large. *****"
	    }
}
            ; entry point

dinstall81:  jsr drivebusy81; does sei

-           lda CIA_PRB; wait for DATA IN = high
            lsr
.instalwait: bcc -
            ldx #<(drvcodebeg81 - $01)
.dgetrout:   inx
            bne +
            inc .dgetputhi
+           jsr .dgetbyte
.dgetputhi = *+$02
            ;sta a:>(drvcodebeg81 - $01) << 8,x
            sta (drvcodebeg81-$01) & $ff00,x
            cpx #<(drivebusy81 - $01)
            bne .dgetrout
            dec drvcodebeg81
            bne .dgetrout
            jmp .dcodinit

!if (FILESYSTEM = DIRECTORY_NAME) & (LOAD_ONCE = 0) & (UNINSTALL_RUNS_DINSTALL) {
    .DIRBUFFSIZE      = (.BLOCKBUFFER - *) / 4
    .DIRTRACKS        = *
    .DIRSECTORS       = .DIRTRACKS + .DIRBUFFSIZE
    .FILENAMEHASHVAL0 = .DIRSECTORS + .DIRBUFFSIZE
    .FILENAMEHASHVAL1 = .FILENAMEHASHVAL0 + .DIRBUFFSIZE
    .DIRBUFFEND       = .FILENAMEHASHVAL1 + .DIRBUFFSIZE

            ;assert DIRBUFFSIZE >= 9, error, "***** Dir buffer too small. *****"
	    !if .DIRBUFFSIZE >= 9 {
	    } else {
		!error "***** Dir buffer too small. *****"
	    }

    DIRBUFFSIZE81 = .DIRBUFFSIZE
    ;.export DIRBUFFSIZE81
}

!if UNINSTALL_RUNS_DINSTALL {
            ;.assert * <= BLOCKBUFFER, error, "***** 1581 drive code too large. *****"
	    !if * <= .BLOCKBUFFER {
	    } else {
		!error "***** 1581 drive code too large. *****"
	    }
}

drvprgend81:
            } ;pseudopc

