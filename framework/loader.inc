
!ifdef _LOADER_INC_ {
} else {

_LOADER_INC_ = 1

;.pushseg
;.segment "CODE"; import symbols as absolute by default

!src "diskio.inc"


; available protocols, file systems, and decompressors

;.enum PROTOCOLS     ; the available transfer protocols - in this version, only TWO_BITS_ATN is implemented

   ;ONE_BIT         ; clocking every bit. allows for arbitrary interrupts, yet it's slower than TWO_BITS_ATN
                    ; and TWO_BITS but has no iec bus restrictions - not implemented yet    
   ;TWO_BITS        ; clocking every byte, the 8 bits within a byte are transferred strictly synchronously
                    ; and in groups of 2, no iec bus restrictions
                    ; allows for badlines, but does not allow for arbitrary
                    ; interrupts, so sprites are forbidden in the main loop, also sei and cli are used,
                    ; so interrupts are delayed for maximum 2 rasterlines - not implemented yet
   ;TWO_BITS_NOBL   ; same as TWO_BITS but without bad line checks, so bad lines are not allowed outside irq handlers,
                    ; has the same drive-side routine as TWO_BITS - not implemented yet
   ;TWO_BITS_BURST  ; same as TWO_BITS but with bigger irq delay,
                    ; the fastest available protocol - not implemented yet
    TWO_BITS_ATN = 3    ; the fastest protocol that allows for arbitrary interrupts (sprites, bad lines, etc.)
                    ; but it works with only 1 iec device on the bus.
;.endenum

;.enum FILESYSTEMS   ; the available file systems - in this version, only DIRECTORY_NAME, and TRACK_SECTOR is implemented

    DIRECTORY_NAME = 0 ; operation on single files that are referenced using the directory, where the filenames
                    ; can be 1 to 16 chars long - no wildcard matching, so * and ? are treated as normal characters;
                    ; each file of the maximum 144 per 1541 disk side is accessible
    DIRECTORY_INDEX = 2 ; operation on single files that are identified by their position in the directory,
                    ; non-files (track 0) are counted
    TRACK_SECTOR = 1  ; operation on single files that are referenced using track and sector position,
                    ; the filename does not have to appear in the directory
   ;DIRECTORY_HASH  ; operation on single files that are referenced using the directory, where the "filenames"
                    ; are a 16 bit hash number on the actual file names
   ;IFFL            ; operation on large iffl files (file databases in single files, one for each disk side)
                    ; where the files are referenced by indices into the iffl files. one big file looks cooler
                    ; than lots of small single files and also removes sector allocation overhead - not implemented yet
;.endenum

;.enum DECOMPRESSORS ; the available decompression routines - in this version, EXOSTREAM and GPACKER are not implemented yet

    EXOMIZER = 10       ; Magnus Lind's Exomizer - no fast decompression, but nearly optimal compression ratio (rle+lz+huffman)
    PUCRUNCH = 9       ; Pasi Ojala's Pucrunch - not so fast decompression but very good compression ratio (rle+lz+huffman)
    BYTEBOOZER = 8     ; HCL/Booze Design's ByteBoozer - pretty fast decompression and good compression ratio (lz, a little better than Levelcrush)
    LEVELCRUSH = 3     ; Taboo Levelcrush - pretty fast decompression and good compression ratio (lz)
   ;GPACKER         ; Graham/Oxyron's G-Packer - very fast decompression and okay compression ratio (rle) - not implemented yet
    
    NONE = 4           ; no decompressor used

;.endenum

;.enum MODULES
    LOADFILE = 0 ; ???
;.endenum


!ifdef EXTCONFIGPATH {
    !src "loaderconfig.inc"
} else {
    !src "config.inc"
}; !EXTCONFIGPATH

!src "option-check.inc"


!if FILESYSTEM = DIRECTORY_NAME {
    FILE_NOT_FOUND = INVALID_PARAMETERS
} else {
	!if FILESYSTEM = DIRECTORY_INDEX {
    INVALID_INDEX = INVALID_PARAMETERS
} else {
	!if FILESYSTEM = TRACK_SECTOR {
    ILLEGAL_TRACK_OR_SECTOR = INVALID_PARAMETERS
}
}
}

!ifdef __NO_LOADER_SYMBOLS_IMPORT {
} else {

!macro IMPORT .symbol {
    !ifndef DYNLINKLDR  {
    	!if  DYNLINKLDR {
        ;;import .symbol
        }
    }
}


!if PLATFORM != COMMODORE_PLUS4 {
; Set the VIC bank
; in:  a - VIC bank (0..3)
; out: undefined
!macro SET_VIC_BANK .bank {
        lda #.bank & 3
        sta CIA2_PRA
}
}

; Import the loader code from a dynamically linked library file
; in:  a/x - lo/hi to import request structure
; out: c   - set on error
;      a   - diskio::status, on diskio::status::GENERIC_KERNAL_ERROR, the KERNAL error code is returned in x
;      x   - if status is diskio::status::OK, lo-byte of version string address,
;            if status is diskio::status::WRONG_VERSION, required major version number
;      y   - if status is diskio::status::OK, hi-byte of version string address,
;            if status is diskio::status::WRONG_VERSION, found (but bad) major version number
;;import importldr
!macro IMPORT_LOADER .request {
        lda #<(.request)
        ldx #>(.request)
        jsr importldr
}

; Install the loader
; in:  a   - if NONBLOCKING_API is non-0, nmi line mod 8
;      x/y - if LOAD_ONCE, lo/hi to 0-terminated filename string
; out: c   - set on error
;      a   - status
;      x   - drive type (one of diskio::drivetypes)
;      y   - if status is diskio::status::OK, zeropage address of version string address

;IMPORT install

!if NONBLOCKING_API = 0 {
    !if LOAD_ONCE | INSTALL_FROM_DISK {
    !macro LOADER_INSTALL .filename_lo, .filename_hi {
        ldx .filename_lo
        ldy .filename_hi
        jsr install
    }
    } else {
    !macro LOADER_INSTALL {
        jsr install
    }
    }
} else {
    !if LOAD_ONCE | INSTALL_FROM_DISK {
    !macro LOADER_INSTALL .nmi_line, .filename_lo, .filename_hi {
        lda .nmi_line
        ldx .filename_lo
        ldy .filename_hi
        jsr install
    }
    } else {
    !macro LOADER_INSTALL .nmi_line {
        lda .nmi_line
        jsr install
    }
    }
}

!if LOAD_RAW_API {
; Load a file without decompression
; in:  x/y - if FILESYSTEM = FILESYSTEMS::DIRECTORY_NAME, lo/hi to 0-terminated filename string
;            if FILESYSTEM = FILESYSTEMS::TRACK_SECTOR, a: track, x: sector
;      c   - if LOAD_TO_API != 0, c = 0: load to address as stored in the file
;                                 c = 1: load to caller-specified address (loadaddrlo/hi)
; out: c   - set on error
;      a   - status
;IMPORT loadraw
!if !LOAD_ONCE {
    !macro LOADRAW .namelo_or_track, .namehi_or_sector {
    !if LOAD_TO_API {
        clc
    }
        ldx .namelo_or_track
        ldy .namehi_or_sector
        jsr loadraw
    }

    !if LOAD_TO_API {
    !macro LOADRAW_LOADTO .namelo_or_track, .namehi_or_sector, .dest_lo, dest_hi {
        sec
        lda .dest_lo
        sta loadaddrlo
        lda .dest_hi
        sta loadaddrhi
        ldx .namelo_or_track
        ldy .namehi_or_sector
        jsr loadraw
    }
    };  LOAD_TO_API
} else {; LOAD_ONCE
    !macro LOADRAW {
    !if LOAD_TO_API {
        clc
    }
        jsr loadraw
    }

    !if LOAD_TO_API {
    !macro LOADRAW_LOADTO .dest_lo, .dest_hi {
        sec
        lda .dest_lo
        sta loadaddrlo
        lda .dest_hi
        sta loadaddrhi
        jsr loadraw
    }
    }; LOAD_TO_API
}; LOAD_ONCE
}; LOAD_RAW_API

!if LOAD_COMPD_API {
; Load a compressed file
; in:  x/y - if FILESYSTEM = FILESYSTEMS::DIRECTORY_NAME, lo/hi to 0-terminated filename string
;            if FILESYSTEM = FILESYSTEMS::TRACK_SECTOR, a: track, x: sector
;      c   - if DECOMPLOAD_TO_API != 0, c = 0: load to address as stored in the file
;                                       c = 1: load to caller-specified address (loadaddrlo/hi)
; out: c   - set on error
;      a   - status
;      x/y - if DECOMPRESSOR = DECOMPRESSORS::PUCRUNCH, lo/hi of the loaded file's execution address
;IMPORT loadcompd
!if !LOAD_ONCE {
    !macro LOADCOMPD .namelo_or_track, .namehi_or_sector {
        ldx .namelo_or_track
        ldy .namehi_or_sector
        jsr loadcompd
    }
} else {; LOAD_ONCE
    !macro LOADCOMPD {
        jsr loadcompd
    }
}
}; LOAD_COMPD_API

!if NONBLOCKING_API {
; non-blockingly Load a file without decompression
; in:  x/y - if FILESYSTEM = FILESYSTEMS::DIRECTORY_NAME, lo/hi to 0-terminated filename string
;            if FILESYSTEM = FILESYSTEMS::TRACK_SECTOR, a: track, x: sector
;      c   - if LOAD_TO_API != 0, c = 0: load to address as stored in the file
;                                 c = 1: load to caller-specified address (loadaddrlo/hi)
; out: c   - set on error
;      a   - status
;IMPORT loadrawnb
!if LOAD_ONCE=0 {
    !macro LOADRAW_NB .namelo_or_track, .namehi_or_sector {
    !if LOAD_TO_API {
        clc
    }
        ldx .namelo_or_track
        ldy .namehi_or_sector
        jsr loadrawnb
    }

    !if LOAD_TO_API {
    !macro LOADRAW_LOADTO_NB namelo_or_track, namehi_or_sector, dest_lo, dest_hi {
        sec
        lda .dest_lo
        sta loadaddrlo
        lda .dest_hi
        sta loadaddrhi
        ldx .namelo_or_track
        ldy .namehi_or_sector
        jsr loadrawnb
    }
    };  LOAD_TO_API
} else {; LOAD_ONCE
    !macro LOADRAW_NB {
    !if LOAD_TO_API {
        clc
    }
        jsr loadrawnb
    }

    !if LOAD_TO_API {
    !macro LOADRAW_LOADTO_NB .dest_lo, .dest_hi {
        sec
        lda .dest_lo
        sta loadaddrlo
        lda .dest_hi
        sta loadaddrhi
        jsr loadrawnb
    }
    }; LOAD_TO_API
}; LOAD_ONCE

; Loop until a non-blocking operation is finished
; in:  nothing
; out: c - set on error
;      a - status
!macro LOOP_BUSY .loop {
    ;.local error

        lda loadstatus
        cmp #BUSY
        beq loop
        cmp #OK
        bne +
        clc
+
}
}; NONBLOCKING_API

!if OPEN_FILE_POLL_BLOCK_API {
; Open a file
; in:  x/y - if FILESYSTEM = FILESYSTEMS::DIRECTORY_NAME, lo/hi to 0-terminated filename string
;            if FILESYSTEM = FILESYSTEMS::TRACK_SECTOR, a: track, x: sector
;      c   - if LOAD_TO_API != 0, c = 0: load to address as stored in the file
;                                 c = 1: load to caller-specified address (loadaddrlo/hi)
; out: c - set on error
;      a - status
;IMPORT openfile
!if LOAD_ONCE = 0 {
    !macro OPENFILE .namelo_or_track, .namehi_or_sector {
    !if LOAD_TO_API {
        clc
    }
        ldx .namelo_or_track
        ldy .namehi_or_sector
        jsr openfile
    }

    !if LOAD_TO_API {
    !macro OPENFILE_LOADTO .namelo_or_track, .namehi_or_sector, .dest_lo, .dest_hi {
        sec
        lda .dest_lo
        sta loadaddrlo
        lda .dest_hi
        sta loadaddrhi
        ldx .namelo_or_track
        ldy .namehi_or_sector
        jsr openfile
    }
    }; LOAD_TO_API
} else {; LOAD_ONCE
    !macro OPENFILE {
    !if LOAD_TO_API {
        clc
    }
        jsr openfile
    }

    !if LOAD_TO_API {
    !macro OPENFILE_LOADTO .dest_lo, .dest_hi {
        sec
        lda .dest_lo
        sta loadaddrlo
        lda .dest_hi
        sta loadaddrhi
        jsr openfile
    }
    }; LOAD_TO_API
};  LOAD_ONCE

; Check if a block is ready to download
; in:  nothing
; out: v - set if a block is ready to download, cleared otherwise
!macro BLOCKREADY {
        bit $dd00; CIA2_PRA
}

; Poll for a block after successfully opening a file
; in:  nothing
; out: c - set on error or eof, cleared otherwise
;      a - status
;IMPORT pollblock
!macro POLLBLOCK {
        jsr pollblock
}
}; OPEN_FILE_POLL_BLOCK_API

!if GETC_API {
; Get a byte from the file stream
; in:  nothing
; out: a - value if c = 0, status otherwise
;      c - error or EOF
;IMPORT getc
!macro GETC {
        jsr getc
}
}; GETC_API

!if GETCHUNK_API {
; Get a chunk from the file stream
; in:  x/y - lo/hi of chunk size
; out: a     - status
;      x/y   - lo/hi of chunk address
;      p4/p5 - lo/hi of retrieved chunk size
;      c     - set on error
;IMPORT getchunk
!macro GETCHUNK .sizelo, .sizehi {
        ldx .sizelo
        ldy .sizehi
        jsr getchunk
}
}; GETCHUNK_API


!if MEM_DECOMP_API {
; Decompress a compressed file from memory
; in:  x/y - lo/hi of compressed file in memory
;      c   - if MEMDECOMP_TO_API != 0, c = 0: decompress to address as stored in the file
;                                      c = 1: decompress to caller-specified address (loadaddrlo/hi)
; out: x/y - if DECOMPRESSOR = DECOMPRESSORS::PUCRUNCH, lo/hi of the file's execution address
;IMPORT memdecomp
!macro MEMDECOMP .source_lo, .source_hi {
    !if MEM_DECOMP_TO_API {
        clc
    }
        ldx .source_lo
        ldy .source_hi
        jsr memdecomp
}

!if MEM_DECOMP_TO_API {
    !macro MEMDECOMP_TO .source_lo, .source_hi, .dest_lo, .dest_hi {
        sec
        lda .dest_lo
        sta decdestlo
        lda .dest_hi
        sta decdesthi
        ldx .source_lo
        ldy .source_hi
        jsr memdecomp
    }
}

!if CHAINED_COMPD_FILES {
; Decompress a subsequent compressed file from memory
; in:  c - if MEMDECOMP_TO_API != 0, c = 0: decompress to address as stored in the file
;                                    c = 1: decompress to caller-specified address (loadaddrlo/hi)
; out: x/y - if DECOMPRESSOR = DECOMPRESSORS::PUCRUNCH, lo/hi of the file's execution address
;IMPORT cmemdecomp
!macro CMEMDECOMP {
    !if MEM_DECOMP_TO_API {
        clc
    }
        jsr cmemdecomp
}

!if MEM_DECOMP_TO_API {
    !macro CMEMDECOMP_TO .dest_lo, .dest_hi {
        sec
        lda .dest_lo
        sta decdestlo
        lda .dest_hi
        sta decdesthi
        jsr cmemdecomp
    }
}
}; CHAINED_COMPD_FILES
}; MEM_DECOMP_API

!if UNINSTALL_API {
; Uninstall the loader
; in:  nothing
; out: undefined
;IMPORT uninstall
!macro LOADER_UNINSTALL {
        jsr uninstall
	}
}; UNINSTALL_API


; Loader segments symbols

;;import __DISKIO_ZP_START__
;;import __DISKIO_ZP_END__
;;import __DISKIO_ZP_RUN__
;;import __DISKIO_ZP_SIZE__
;
;;import __DISKIO_INSTALL_START__
;;import __DISKIO_INSTALL_END__
;;import __DISKIO_INSTALL_RUN__
;;import __DISKIO_INSTALL_SIZE__

;;import __DISKIO_START__
;;import __DISKIO_END__
;;import __DISKIO_RUN__
;;import __DISKIO_SIZE__

} ; !__NO_LOADER_SYMBOLS_IMPORT

!ifdef DYNLINKLDR {
} else {
    DYNLINKLDR = 0
}

!if DYNLINKLDR {
    !macro PREPARE_IMPORT .request {
        lda #<(__DISKIO_INSTALL_RUN__)
        sta .request + INSTALL_BASE + 0
        lda #>(__DISKIO_INSTALL_RUN__)
        sta .request + INSTALL_BASE + 1
        lda #<(__DISKIO_INSTALL_SIZE__)
        sta .request + INSTALL_SIZE + 0
        lda #>(__DISKIO_INSTALL_SIZE__)
        sta .request + INSTALL_SIZE + 1

        lda #<(__DISKIO_RUN__)
        sta .request + RESIDENT_BASE + 0
        lda #>(__DISKIO_RUN__)
        sta .request + RESIDENT_BASE + 1
        lda #<(__DISKIO_SIZE__)
        sta .request + RESIDENT_SIZE + 0
        lda #>(__DISKIO_SIZE__)
        sta .request + RESIDENT_SIZE + 1
    }
}

!ifdef __NOIMPORTVARS {
} else {
    !if DYNLINKLDR=0 {
        ;importzp loadstatus

        ;importzp loadaddrlo
        ;importzp loadaddrhi

        ;importzp decdestlo
        ;importzp decdesthi

        ;importzp endaddrlo
        ;importzp endaddrhi

        ;importzp bytesloadedlo
        ;importzp bytesloadedhi

        ;importzp param4
        ;importzp param5
    }
}  ; !__NOIMPORTVARS


!if UNINSTALL_RUNS_DINSTALL {


    !src "hal.inc"

    !ifdef __NO_LOADER_SYMBOLS_IMPORT {
    } else {
        ;import drivecode41
        ;import drvcodebeg41
        ;import drvprgend41

        ;import drivecode71
        ;import drvcodebeg71
        ;import drivebusy71

        ;import drivecode81
        ;import drvcodebeg81
        ;import drivebusy81
    }; !__NO_LOADER_SYMBOLS_IMPORT
    !macro __UPLOAD_DRIVECODE .drivecode_address, .driveprgend, .driveprg {

        ;.local uploadloop
        ;.local bitset
        ;.local noincaddrh

        jsr uninstall

        +SET_DATA_OUT_CLEAR_CLK_OUT_CLEAR_ATN_OUT

        lda #>(.drivecode_address)
        sta .uploadloop + $02
        ldy #<(.drivecode_address)
.uploadloop:
        lda $0000,y
        +SENDBYTE
        iny
        bne .noincaddrh
        inc .uploadloop + $02
.noincaddrh:
        cpy #<(.drivecode_address + .driveprgend - .driveprg)
        bne .uploadloop
        ldx .uploadloop + $02
        cpx #>(.drivecode_address + .driveprgend - .driveprg)
        bne .uploadloop
    }

    !macro UPLOAD_DRIVECODE_1541 .drivecode_address {
        +__UPLOAD_DRIVECODE .drivecode_address, drvprgend41, drvcodebeg41
    }; UPLOAD_DRIVECODE

    !macro UPLOAD_DRIVECODE_1571 .drivecode_address {
        +__UPLOAD_DRIVECODE .drivecode_address, drivebusy71, drvcodebeg71
    }; UPLOAD_DRIVECODE

    !macro UPLOAD_DRIVECODE_1581 .drivecode_address {
        +__UPLOAD_DRIVECODE .drivecode_address, drivebusy81, drvcodebeg81
    }; UPLOAD_DRIVECODE
}; UNINSTALL_RUNS_DINSTALL

;.popseg

}; _LOADER_INC_
