!ifdef __framework {
} else {

__framework = 1

FRAMEWORK_ADDR = $fc00

LOAD_NEXT_PART = FRAMEWORK_ADDR+0
RESET_DRIVE = FRAMEWORK_ADDR+3
DECRUNCH_NEXT_PART = FRAMEWORK_ADDR+6
START_NEXT_PART = FRAMEWORK_ADDR+9
MEMCPY = FRAMEWORK_ADDR+12	; src a, dst y, size in pages x
FRAMEWORK_IRQ = FRAMEWORK_ADDR+15
PLAY_MUSIC = FRAMEWORK_ADDR+18
COPY_PACKED = FRAMEWORK_ADDR+21 ;in y new destination

FIRST_USED_ZP = $f3

} 
