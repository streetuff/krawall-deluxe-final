
!src "loader.inc"
!src "option-check.inc"
!src "common-features.inc"

;------------------------------------------------------------------------------

!macro DEFAULT_INTERRUPT_INSTFEATS {
    ; when installing the drive routines, screen on is usually sufficient for applications
    !byte INTERRUPT_CONSTRAINTS, SCREEN_REMAINS_ON
    !warn "  interrupts may be delayed, no sprites or nmis used"
}

!macro INTERRUPT_INSTFEATS {
    !ifdef DYNLINK {
        !byte INTERRUPT_CONSTRAINTS, INTERRUPTS_POSSIBLE
        !warn "  interrupts may be delayed, no nmis allowed, no sprites in non-irq space"
    } else {
        !ifdef USER_INTERRUPT_INSTFEATS {
            !if USER_INTERRUPT_INSTFEATS != 0 {
                +USER_INTERRUPT_INSTFEATS_IMPL
            } else {
                +DEFAULT_INTERRUPT_INSTFEATS
            }
        } else {
            +DEFAULT_INTERRUPT_INSTFEATS
        }
    }
}

;------------------------------------------------------------------------------

!if NONBLOCKING_API=0 {
    !macro DEFAULT_NON_BLOCKING_INSTFEATS {
        !warn "  does not enable non-blocking routines"
    }

    !macro NON_BLOCKING_INSTFEATS {
        !ifdef DYNLINK {
            !warn "  does not enable non-blocking routines"
        } else {
            !ifdef USER_NON_BLOCKING_INSTFEATS {
                !if USER_NON_BLOCKING_INSTFEATS != 0 {
                    +USER_NON_BLOCKING_INSTFEATS_IMPL
                } else {
                    +DEFAULT_NON_BLOCKING_INSTFEATS
                }
            } else {
                +DEFAULT_NON_BLOCKING_INSTFEATS
            }
        }
    }

} else {

    !macro DEFAULT_NON_BLOCKING_INSTFEATS {
        !byte NON_BLOCKING, IS_NONBLOCKING
        !warn "  enables non-blocking routines"
    }

    !macro NON_BLOCKING_INSTFEATS {
        !ifdef DYNLINK {
            !byte NON_BLOCKING, IS_NONBLOCKING
            !warn "  enables non-blocking routines"
        } else {
            !ifdef USER_NON_BLOCKING_INSTFEATS {
                !if USER_NON_BLOCKING_INSTFEATS != 0 {
                    +USER_NON_BLOCKING_INSTFEATS_IMPL
                } else {
                    +DEFAULT_NON_BLOCKING_INSTFEATS
                }
            } else {
                +DEFAULT_NON_BLOCKING_INSTFEATS
            }
        }
    }

}

;------------------------------------------------------------------------------

!macro INSTALL_FEATURES {
    !warn " install"
    +PLATFORM_FEATS
    +SERIAL_BUS_FEATS
    +SUPPORTED_DRIVES_FEATS
    +SUPPORTED_DEVICE_NUMBERS_FEATS
    +FORMAT_FEATS
    +INTERRUPT_INSTFEATS
    !if LOAD_ONCE {
        +FILE_FEATS
        +LOAD_TO_FEATS
    }
    +REENTRANCE_FEATS
    +NON_BLOCKING_INSTFEATS
    !if NONBLOCKING_API = 0 {
    +PALNTSCACCEL_FEATS
    +TIMER_FEATS
    } else {
    +PALNTSCACCEL_FEATS_NB
    +TIMER_FEATS_NB
    }
    !byte END_OF_LIST
}

!ifdef DYNLINK {
    !byte INSTALL, <(install - base), >(install - base)
    +INSTALL_FEATURES
} else {
	!if DYNLINKLDR {
    !byte INSTALL
    +INSTALL_FEATURES

    install = __DISKIO_INSTALL_RUN__
    _0_ = 3
} else {
    ;.export install
    !if (JUMP_TABLE) {
        !warn "Jump table (install):"
        !warn " install"
install:    jmp install2
    }
  }
}

!if DYNLINKLDR {
    !byte END_OF_LIST

    INSTALLJUMPTABLESIZE    = _0_
    NUMIMPORTEDINSTALLFUNCS = INSTALLJUMPTABLESIZE / 3
}

