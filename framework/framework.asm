	!to "framework.prg",cbm
        !cpu 6510

	!src "framework.inc"

        *= $07fd
        ; * = $0801
        ;!by $0b,$08,$00,$00,$9e,$32,$30,$36,$31,$00,$00,$00

        jmp start

	; mucke
SID:
        ;!bin "music.sid",,126
        !bin "krawall_part1.sid",,2
        ;!bin "tuffitune.sid",,2
start:
	ldx #init_part_len
-       lda init_part,x
        sta $033c,x
        dex
        bpl -
        jmp $033c

init_part:
	!pseudopc $033c {        
	sei
        lda #$30
        sta $01
        ldy #<end_of_framework
        lda #$00
frameworkptr:
	sta end_of_framework & $ff00,y
        iny
        bne frameworkptr
        inc frameworkptr+2
        bne frameworkptr
        lda #$37
        sta $01       
	jsr init_framework

        lda #>fader
        ldy #>$e000
        ldx #>fader_len
        jsr memcpy
        jsr init_fader
	jsr load_next_part
        jsr decrunch_next_part
-	lda partend
	beq -        
        jsr framework_irq
        jmp start_next_part
}
init_part_len = * - init_part

        !src "zp-c64.s"

INSTALLER_ADDR:
	!src "install.s"


init_framework:
	lda #$7f
        sta $dc0d
        lda $dc0d
	jsr install

	lda #$35
        sta $01
	ldx #00
-
ptr_src:lda framework_copy,x
ptr_dst:sta FRAMEWORK_ADDR,x
        inx
        bne -
        stx $fff8
        stx $fff9
	inc ptr_src+2
	inc ptr_dst+2
	bne -
	lda #OPC_RTI
        sta $fffc
        lda #<$fffc
        sta $fffa
        lda #>$fffc
        sta $fffb
        lda #$00
        tax
        jsr SID
        jmp framework_irq

framework_copy:
	!pseudopc FRAMEWORK_ADDR {

LOAD_NEXT_PART:        
	jmp load_next_part
RESET_DRIVE:
	jmp uninstall         
DECRUNCH_NEXT_PART:
        jmp decrunch_next_part
START_NEXT_PART:
        jmp start_next_part
MEMCPY:
        jmp memcpy
FRAMEWORK_IRQ:
        jmp framework_irq
PLAY_MUSIC:
        jmp play_music

COPY_PACKED:
	;jmp copy_code
        


copy_packed:
	;in y adresse
        lda endaddrhi
	sec
        sbc get_crunched_byte+2
        tax
        inx       
        lda get_crunched_byte+2
        sty get_crunched_byte+2

memcpy:
	sta memcpy_src+2
        cpy memcpy_src+2
        bcs memcpy_up
        sty memcpy_dst+2
        lda $01
        pha
        lda #$30
        sta $01
        ldy #$00
memcpy_src:
	lda $ff00,y
memcpy_dst:
	sta $ff00,y
        iny
        bne memcpy_src
        inc memcpy_src+2
        inc memcpy_dst+2
        dex
        bne memcpy_src
        pla
        sta $01
        rts

memcpy_up:
	sta memcpy_src2+2
        sty memcpy_dst2+2
        txa
        clc
        adc memcpy_src2+2
        sta memcpy_src2+2
        txa
        clc
        adc memcpy_dst2+2
        sta memcpy_dst2+2
        lda $01
        pha
        lda #$30
        sta $01
        ldy #$00
memcpy_up_loop:
        dec memcpy_src2+2
        dec memcpy_dst2+2
memcpy_src2:
	lda $ff00,y
memcpy_dst2:
	sta $ff00,y
        iny
        bne memcpy_src2
        dex
        bne memcpy_up_loop
        pla
        sta $01
        rts


get_crunched_byte:
	lda $c0de
        inc get_crunched_byte+1
        bne +
        inc get_crunched_byte+2
+	rts

framework_irq:
	sei
	lda #$fa
        sta $d012
        lda $d011
        and #$7f
        sta $d011
        lda #$81
        sta $d01a
        sta $d019
        lda #<frame_irq
        sta $fffe
        lda #>frame_irq
        sta $ffff
        cli
        rts
        
frame_irq:
	sta frame_irq_old_a+1
        stx frame_irq_old_x+1
	sty frame_irq_old_y+1
        lda $01
        sta frame_irq_old_01+1
        lda #$35
        sta $01
        inc $d019
        jsr PLAY_MUSIC
frame_irq_old_01:
	lda #$00
        sta $01
frame_irq_old_y:
	ldy #$00
frame_irq_old_x:
	ldx #$00
frame_irq_old_a:
	lda #$00
        rti
                                      
	        
        
play_music:
	inc $fff8
        bne +
        inc $fff9
+
	jmp SID+3
        ;rts
                        
	!src "loader.s"
        !src "krilldecr.asm"	

load_next_part:
	lda #$00
	sta loadaddrlo
	sta loadaddrhi
	ldx #<partname
	ldy #>partname
	jsr loadraw
	lda loadaddrlo
	ora loadaddrhi
	beq load_error
        ;bcs load_error
load_continue:
        inc partname		;prepare for next load
	lda partname
	cmp #'9'+1
	bne +
	lda #'A'
	sta partname
+
        ldy #$00
	lda (loadaddrlo),y
        sta <zp_dest_lo
        sta start_next_part_jmp+1
        iny
        lda (loadaddrlo),y
        sta <zp_dest_hi
        sta start_next_part_jmp+2
        iny
        lda (loadaddrlo),y
        sta fff8_low_cmp+1
        iny
        lda (loadaddrlo),y
        sta fff8_high_cmp+1        
        
        lax loadaddrlo
        sbx #$fc
        stx get_crunched_byte+1
        lda loadaddrhi
        adc #$00
        sta get_crunched_byte+2
        rts

load_error:
	lda #$60	;rts
	sta decrunch_next_part
	lda #$4c
	sta start_next_part
	;create endless loop
	lda #<start_next_part
	sta start_next_part+1
	lda #>start_next_part
	sta start_next_part+2
	jmp uninstall

decrunch_next_part:
	lda $01
	pha
	lda #$30
	sta $01
	jsr decrunch
	pla
	sta $01
	rts
	    
start_next_part:
        lda $fff8
        sta first_value+1
        lda $fff9
        sta second_value+1
        lda $fff8
        cmp first_value+1
        bne start_next_part
        lda $fff9
        cmp second_value+1
        bne start_next_part
first_value:
	lda #$00	
fff8_low_cmp:
        cmp #$00
second_value:
	lda #$00
fff8_high_cmp:
        sbc #$00
        bcc start_next_part
start_next_part_jmp:
	jmp $c0de

              
partname:
	!byte "0"
        !byte 0

        }

framework_len = * - framework_copy

	* = (*+$ff) & $ff00
fader:
	!pseudopc $e000 {
        INCLUDED = 1
        !src "fader.asm"
        }
end_of_framework:        
	* = (*+$ff) & $ff00
fader_len = * - fader          
